<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'grapevine');

/** MySQL database username */
define('DB_USER', 'grapevine');

/** MySQL database password */
define('DB_PASSWORD', 'YjJB4qihMEtG');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'qZxki_}m:5{>Y0MIy+0T}lRgWWRZ>WjC_`~e>q9I361#~8M4-C/Hy&[$gaI`JEJv');
define('SECURE_AUTH_KEY',  'Rz(x*p2l6I^{U,qxClt(]{r[F:u2st*0<}K~pz$RFEIkD4MSvBQ|vkF+*scrs?OF');
define('LOGGED_IN_KEY',    '+eW3v[DQoH~lF/i7#m=n%m5J:o$!%:IQJPTY]{& >AtK-(.9aLp=#vf}Susp*}6@');
define('NONCE_KEY',        'g}0|rCUVch DE!85R,X!i,?V%}>d_I|:sS%c =j;-]X{DsWK+yGN|nQ%B?&;KgUR');
define('AUTH_SALT',        '>}z=ijiwd7ih;M]gEU&4?bGu).t+F<TH2cmY)V<nlh&%HwT%zdkM]M-Zj8F^At|?');
define('SECURE_AUTH_SALT', '7oktGI.-S789?gM]8!`?/P2qA{jky-I}0_6t>col|U%jao 481iASe,}7kH2{b%B');
define('LOGGED_IN_SALT',   '!F8-tczJb&a1-v>UVVi<$e<t|Z,F^Ez*w<z.^n%8$A!l&q)P;USBx`Ih)V}sSG @');
define('NONCE_SALT',       '7yoT1rLvjn/t}JjXF-mI+c&C|(ob~5R+=*[|sP<rF>!j$4]WvCHh+LR4f>tGY]Ax');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

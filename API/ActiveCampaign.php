<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Class that handles sending data and updating data to Active Campaign
 *
 * @author baldesalame
 */
class ActiveCampaign {
    
    private $url;
    private $key;
    public function __construct($url, $key) {
        $this->url = $url;
        $this->key = $key;
    }
    
    public function send_campain_data($post_params, $post_data) {

        $url = $this->url;


        $params = $post_params;

// here we define the data we are posting in order to perform an update
        $post = $post_data;

// This section takes the input fields and converts them to the proper format
        $query = "";
        foreach ($params as $key => $value)
            $query .= $key . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

// This section takes the input data and converts it to the proper format
        $data = "";
        foreach ($post as $key => $value)
            $data .= $key . '=' . urlencode($value) . '&';
        $data = rtrim($data, '& ');

// clean up the url
        $url = rtrim($url, '/ ');

// This sample code uses the CURL library for php to establish a connection,
// submit your request, and show (print out) the response.
        if (!function_exists('curl_init'))
           // die('CURL not supported. (introduced in PHP 4.0.2)');

// If JSON is used, check if json_decode is present (PHP 5.2.0+)
        if ($params['api_output'] == 'json' && !function_exists('json_decode')) {
            //die('JSON not supported. (introduced in PHP 5.2.0)');
        }

// define a final API request - GET
        $api = $url . '/admin/api.php?' . $query;

        $request = curl_init($api); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_POSTFIELDS, $data); // use HTTP POST to send form data
//curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment if you get no gateway response and are using HTTPS
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

         $response = (string) curl_exec($request); // execute curl post and store results in $response
// additional options may be required depending upon your server configuration
// you can find documentation on curl options at http://www.php.net/curl_setopt
        curl_close($request); // close curl object

        if (!$response) {
          //  die('Nothing was returned. Do you have a connection to Email Marketing server?');
        }

// This line takes the response and breaks it into an array using:
// JSON decoder
        $result = json_decode($response, true);
        $contact_id = '';
     //   print_r($result);
        if (count($result) > 0) {
            
            if (isset($result['subscriber_id'])) {
                $contact_id = $result['subscriber_id'];
            }

            return $contact_id;
        }
    }
    
    
    public function send_new_user_data($action, $post_data)
    {
      
        $params = array(
            // the API Key can be found on the "Your Settings" page under the "API" tab.
            // replace this with your API Key
            'api_key' => $this->key,
            // this is the action that adds a contact
            'api_action' => $action,
            'api_output' => 'json',
        );



        $contact_id = $this->send_campain_data($params, $post_data);
       
         
        
        
        return $contact_id;
    }
}


<?php

require_once('../wp-load.php');
require_once 'PushClass.php';

/**
 * Description of Webservice
 *
 * @author baldesalame
 */
class Webservice {

    public $data;
    public $google_api_key;
    public $push;

    public function __construct() {
      
        global $wpdb;
        $this->data = $wpdb;
        $this->google_api_key = "AIzaSyBv5rxWqJWNXg6R3VgwWKjmB0d4CsJgEAI";
        $this->push = new PushClass();
    }

    public function register($email, $username, $f_name, $l_name, $user_location, $current_location, $pass, $phone_number, $region, $interests, $fb_id, $gender, $validation_field)    
    {

        $results = $this->data->get_results("SELECT * FROM users WHERE email = '$email'", ARRAY_A);
        $country = '';
        $country_arr = explode(",", $region);
        
        /*if(empty($pass) OR $pass == NULL){
            echo'{"error_codes": -9999, "message":"Password Required"}';
            
            exit();
        }*/
        
        if(count($country_arr) > 0){
            $country = end($country_arr);
        }
        
        if (count($results) > 0) {
            echo'{"error_codes": -1, "message":"Email already exists"}';
        } else {

            $coordinates = array();
            $coord_data = $this->get_coordinates($region);
            if(count($coord_data) > 0){
           
                 $coordinates['lat']= $coord_data['lat'];
                 $coordinates['lng']  = $coord_data['lng'];
         
            }
            else{
               
                 $coordinates['lat'] = 0;
                $coordinates['lng'] = 0;
          
            }
            $coord_obj = json_encode($coordinates);
            
            $token = uniqid();
           $this->data->insert(
                'users', array(
                'user_password' => $pass,
                'username' => $username,
                'last_name' => $l_name,
                'first_name' => $f_name,
                'email' => $email,
                'location' => $user_location,
                'tel_number' => $phone_number,
                'name' => $f_name. ' '. $l_name,
                'region' => $region,
                'current_location' => $current_location,
                'home_town_coordinates' => $coord_obj,
                'facebook_id' => $fb_id,
                'gender' => $gender,
               //  'token' => $token,
                 $validation_field => $token,
                'interests' => $interests,
                'country' => $country
            ));
            $id = $this->data->insert_id;
            
            $decoded_interests = json_decode($interests, true);
            $interest_tags = '';
            if(isset($decoded_interests['interests'])){
               $interest_tags = implode(",", $decoded_interests['interests']);
            }
            if(!empty($id)){
            
            //Insert users data into Active campain
            
            $post_data = array(
                'email' => $email,
                'name' => $f_name. ' '. $l_name,
                'phone' => $phone_number,
                'field[%REGION%, 0]' => $region,
                'field[%INTERESTS%, 0]' => $interests,
                'field[%GENDER%, 0]' => $gender,
                'field[%FPL%, 0]' => 1,
               'field[%COUNTRY%, 0]' => $country,
               /* 'field[%LASTNAME%, 0]' => $l_name,
                'field[%PHONE%, 0]' => $phone_number,*/
                
               'tags' => $interest_tags,
                
                
                // assign to lists:
                'p[1]' => 1, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                'status[1]' => 1 // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 1)
            );
            
            
            
         //   $this->update_data('users', array('email' => $email), array('log_data' => $post_data));
            
             $this->send_new_user_data('contact_add', $post_data);
            //send token also

            $response = array(
                'error_codes' => 0,
                $validation_field => $token,
                'username' => $username,
                'last_name' => $l_name,
                'first_name' => $f_name,
                'email' => $email,
                'location' => $user_location,
                'tel_number' => $phone_number,
                'name' => $f_name. ' '. $l_name,
                'region' => $region,
                'current_location' => $current_location,
                'home_town_coordinates' => $coord_obj,
                'facebook_id' => $fb_id,
                'country' => $country,
                'gender' => $gender,
                'interests' => $interests,
                'id' => $id);
            $da = json_encode($response);
            
            echo $da;
            }else{
                 echo'{"error_codes": -1, "message":"Registration failed"}';
            }
        }
    }

    /*
     * Get the current user data
     */

    public function get_user_data($user_id, $email = false) {
        $param = '';
        if (!empty($email)) {
            $param = "email= '$email'";
        } else {
            $param = "id = '$user_id'";
        }
        $results = $this->data->get_results("SELECT * FROM users WHERE $param", ARRAY_A);

        if (count($results) > 0) {
            //$results['error_codes'] = 0;
            $res = array(
                'error_codes' => 0,
                'data' => $results
            );
            
            echo json_encode($res);
        } else {
            echo'{"error_codes": -1003}';
        }
    }

    public function get_coordinates($region) {
        $key = $this->google_api_key;
        $coordinates = array();
        if(empty($region)){
            
        }else{
        $encoded = urlencode($region);
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=$encoded&key=AIzaSyBv5rxWqJWNXg6R3VgwWKjmB0d4CsJgEAI";
        $geocode = file_get_contents($url);
        $geocode = json_decode($geocode);
        //print_r($geocode);
        $location = $geocode->results[0]->geometry->location;
        $lat = $location->lat;
        $lng = $location->lng;

        
            $coordinates['lat'] = $lat;
          $coordinates['lng'] = $lng;


        
        }
        return $coordinates;
    }

    /*
     * Function to check login details and if ok log users in
     */

    public function user_login($email, $pass, $facebook_id, $validation_field) {
        $enc_pass = trim($pass); //sha1($pass);
        $cleanemail = trim($email);
        $dynamic_fields = '';
        if (empty($facebook_id) OR $facebook_id == null) {
            $dynamic_fields = "user_password = '$enc_pass'";
        } else {
            $dynamic_fields = "facebook_id = '$facebook_id'";
        }
      //  echo $dynamic_fields;
        $token = uniqid();
        if(!empty($cleanemail)){
        $results = $this->data->get_results("SELECT * FROM users WHERE email = '$cleanemail' AND $dynamic_fields", ARRAY_A);
       
    
        if (count($results) > 0) {
            //unset($results[0]['user_password']);
            // unset($results[0]['active_campaign_id']);
               $this->update_data('users', array('email' => $email), array($validation_field => $token));
               $new_token =  $this->getTableFieldData('users', $validation_field, 'email', $email);
               
               $results[0][$validation_field] = $new_token;
                $res = array(
                'error_codes' => 0,
               
                'user_data' => $results
            );
            
           
            echo json_encode($res);
            
           
        } else {
           
            echo'{"error_codes": -1, "message":"Login failed"}';
            // return 'failed';
        }
        
        }
        else{
            echo'{"error_codes": -1, "message":"Email field is empty"}'; 
        }
    }

    /*
     * Function to update user data
     */

    public function update_user_data($user_id, $data) {
        
       $ac_id =  $this->getTableFieldData('users', 'active_campaign_id', 'id', $user_id);
       $email =  $this->getTableFieldData('users', 'email', 'id', $user_id);
        foreach ($data as $key => $value) {
            if ($value != "" || $value != null) {
                $this->data->update(
                        'users',
                        array(
                        $key => $value
                        ),
                        array(
                    "id" => $user_id
                ));
                
                $this->update_active_campaign_data($email, $ac_id, $key, $value);
            }
        }
        
        $res  = array("error_codes" => 0);
        echo json_encode($res);
    }
    
    /*
     * Update users data in Active Campaign
     */
    
    public function update_active_campaign_data($email, $ac_id, $key, $value)
    {
        
        $ac_key = '';
        $tags = '';
       // $new_email = $name = $phone = $interests = $firstname = $lastname = $gender = $region = '';
        if($key == 'email'){
            $ac_key = 'email';
        }else if($key == 'tel_number'){
             $ac_key = 'phone';
        }else if($key == 'interests'){
             $ac_key = 'field[%INTERESTS%, 0]';
             
             $decoded_interests = json_decode($value, true);
            $interest_tags = '';
            if(isset($decoded_interests['interests'])){
                $interest_tags = implode(",", $decoded_interests['interests']);
            }
            
            $tags = $interest_tags;
            
            $full_tags = array("red", "white", "rose", "connoisseur", "gallery", "wine_school", "social", "fizz", "dessert", "travel", "food");
            $remove_data = array(
                    'id' => $ac_id,
                    'email' => $email,
                    'tags[]' => 'red', 
                   'tags[]'=>'white', 
                    'tags[]' => 'rose', 
                    'tags[]' =>'connoisseur', 
                    'tags[]' =>'gallery ',
                    'tags[]' =>'wine_school', 
                    'tags[]' =>'social', 
                    'tags[]' =>'fizz',
                    'tags[]' =>'dessert',
                    'tags[]' =>'food',
                    'tags[]' =>'travel',
                    
                    
                'p[1]' => 1, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                'status[1]' => 1 // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 1)
            );
            
            $this->send_new_user_data('contact_tag_remove', $remove_data);
        }else if($key == 'region'){
             $ac_key = 'field[%REGION%, 0]';
        }else if($key == 'name'){
             $ac_key = 'name';
        }else if($key == 'first_name'){
             $ac_key = 'field[%FIRSTNAME%, 0';
        }else if($key == 'last_name'){
             $ac_key = 'field[%LASTSTNAME%, 0';
        }
        else if($key == 'gender'){
             $ac_key = 'field[%GENDER%, 0';
        }
       /* if(array_key_exists('name', $data)){
            $name = $data['name'];
        } 
        if(array_key_exists('email', $data)){
            $new_email = $data['email'];
        } 
        if(array_key_exists('tel_number', $data)){
            $phone = $data['tel_number'];
        } 
        
        if(array_key_exists('region', $data)){
            $region = $data['region'];
        } 
        
        if(array_key_exists('first_name', $data)){
            $firstname = $data['first_name'];
        } 
        
        if(array_key_exists('gender', $data)){
            $gender = $data['gender'];
        } 
        
        if(array_key_exists('interests', $data)){
            $interests = $data['interests'];
        } 
        
        if(array_key_exists('last_name', $data)){
            $lastname = $data['last_name'];
        } */
        
        
        
        
        $post_data = array(
                    'id' => $ac_id,
                    'email' => $email,
                    $ac_key => $value,
                    /*'field[%INTERESTS%, 0]' => $interests,
                    'field[%REGION%, 0]' => $region,
                    'field[%EMAIL%, 0' => $new_email,
                    'name' => $name,
                    'field[%FIRSTNAME%, 0' => $firstname,
                    'field[%GENDER%, 0' => $gender,
                    'field[%LASTSTNAME%, 0' => $lastname,*/
                    
                'p[1]' => 1, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                'status[1]' => 1 // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 1)
            );
        
        if($key == 'interests'){
            $post_data['tags'] = $interest_tags;
        }
       
         $this->send_new_user_data('contact_edit', $post_data);
        
    }

    /*
     * Function to get events depending on the location chosen by the user
     */

    public function get_events($location, $selected_location, $distance, $order_by, $offset= false) {
        
        $radius = 7000.16;
        $lat = 0;
        $lng = 0;
        $default_coordinates = $this->get_user_home_coordinates($user_id);

        $default_location = json_decode($default_coordinates, true);

        // If we have data from post.
        // if (!empty($location)) {
        //     $lat = $location['lat'];
        //     $lng = $location['lng'];

        //     if ($lat == 0 AND $lat == 0) {
        //         $lat = $default_location['lat'];
        //         $lng = $default_location['lng'];
        //     }
        // }
        // // no post data.
        // else {
        //     if (count($default_location) > 0) {
        //         $lat = $default_location['lat'];
        //         $lng = $default_location['lng'];
        //     }
        // }
        $address = $selected_location;
        $address = str_replace(" ", "+", $address);
    
        $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=$region");
        $json = json_decode($json);

        $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
        $lng = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
       // return $lat.','.$long;

        // echo $lat .','.$lng;

        $maxLat = (float) $lat + rad2deg($distance / $radius);
        $minLat = (float) $lat - rad2deg($distance / $radius);

        $maxLng = (float) $lng + rad2deg($distance / $radius / cos(deg2rad((float) $lat)));
        $minLng = (float) $lng - rad2deg($distance / $radius / cos(deg2rad((float) $lat)));
        $arguments_array = array();



        if ($order_by != '') {

            $arguments_array['meta_key'] = 'event_date';
            $arguments_array['orderby'] = 'meta_value';
             $arguments_array['order']   = $order_by;
           // $arguments_array['orderby'] = array('meta_value_num' => $order_by, 'date' => $order_by);
           // $arguments_array['meta_key'] = 'date';
           
        } else {
            $arguments_array['orderby'] = 'date';
            $arguments_array['order'] = 'DESC';
        }
        //echo $maxLat;
        $arguments_array['post_type'] = 'post';
        $arguments_array['cat'] = '2';
        $arguments_array['post_status'] = 'publish';
        $data = array();
       // $arguments_array['meta_query'] = $args;
        $post_data = array();
        // echo "<pre>";
        // print_r($arguments_array);
        // echo "</pre>";
        $query = new WP_Query($arguments_array);
  // echo "<pre>";
  //       print_r($query);
  //       echo "</pre>";

        foreach ($query->posts as $data_array) {

            // $post_meta = get_post_meta($data_array->ID);
            $formated_date = date("jS F Y", strtotime(get_field('date', $data_array->ID)));
            //$sort_date = date("Y-m-d", strtotime(get_field('date', $data_array->ID)));
            $data['id'] = $data_array->ID;
            $data['title'] = $data_array->post_title;
            $data['guid'] = $data_array->guid;
            $data['featured_image'] = get_field('featured_image', $data_array->ID);
            $data['time'] = get_field('time', $data_array->ID);
            $data['date'] = get_field('date', $data_array->ID);
            $data['sort_date'] = $sort_date;
            $data['formated_date'] = $formated_date;
            $data['post_code'] = get_field('post_code', $data_array->ID);
            $data['address'] = get_field('address', $data_array->ID);

            // echo "<pre>";
            // print_r($data_array);
            // echo "</pre>";

           // if ($location == "" OR $location == null) {
            if ($lat == "" OR $lng == null OR $lat == null) {
                
                  array_push($post_data, $data);
                
            }else if($lat == 0 AND $lng == 0){
               array_push($post_data, $data); 
            } else {

                $coordinate_data = get_field('map_cordinates', $data_array->ID);
                // print_r($coordinate_data);

                if (count($coordinate_data) > 0) {

                    $ev_lat = $coordinate_data['lat'];
                    $ev_lng = $coordinate_data['lng'];
                }

                if ($ev_lng >= $minLng AND $ev_lng <= $maxLng AND $ev_lat >= $minLat AND $ev_lat <= $maxLat) {

                    array_push($post_data, $data);
                }
            }
        }
        
        usort($post_data, array($this, 'cmp'));
        
        $res = array();
        if(count($post_data) > 0)   {
             
             
             $res['error_codes'] = 0;
         }
         else{
             $res['error_codes'] = -1003;
         }
         
         if(!empty($offset)){
             $res['data'] = array_slice($post_data,$offset, 6);
         }else{
        
         $res['data'] = $post_data;
         }
        echo json_encode($res);
    }

    public function reset_password($email, $new_pass) {
        
        $email_check = $this->getTableFieldData('users', 'email', 'email', $email);
        $res = array();
        if (!empty($email_check) AND ! empty($new_pass)) {
            $this->update_data('users', array('email' => $email), array('user_password' => $new_pass));
           $res['error_codes'] = 0;
        } else {
            $res['error_codes'] = -1;
        }
        
        echo json_encode($res);
    }
    
    public function reset_forgot_password($email, $new_pass, $vt) {
        
        $email_check = $this->data->get_results("SELECT * FROM users WHERE email = '$email' AND validation_token = '$vt'", ARRAY_A);
        $res = array();
        if (count($email_check) > 0) {
            $this->update_data('users', array('email' => $email), array('user_password' => $new_pass));
            $token = uniqid();
           // $this->update_data('users', array('email' => $email), array('token' => $token));
           $res['error_codes'] = 0;
        } else {
            $res['error_codes'] = -1;
        }
        
        echo json_encode($res);
    }
    
    
    
    /*
     * Check to make sure the email address and user_id are valid
     */
    public function validate_forgot_password_link($user_id, $email)
    {
        $results = $this->data->get_results("SELECT * FROM users WHERE email = '$email' AND id = '$user_id'", ARRAY_A);
        
        if (count($results) > 0) {
            
            return 1;
        }else{
            return 0;
        }
    }
    
    public function forgot_password_handler($email)
    {
       
        //$encrypter = new Encrypter();
        
       $results = $this->data->get_results("SELECT * FROM users WHERE email = '$email'", ARRAY_A);
       
        if (count($results)> 0) {
         
            $d = array(
                'uid' => $results[0]['id'],
                'email' => $results[0]['email'],
                'expires' => strtotime('+24 hour', time())
            );
            
           
            $s = serialize($d);
            
            
           // $t = $encrypter->encode($s);
            
            $t = base64_encode($s);
          
          $reset_link = get_site_url()."/API/forgot-password.php?t=$t";
            $ac_id = $results[0]['active_campaign_id'] ;
            //Insert users data into Active campain
            $message = "Please folow this link to reset your password"."\r\n";
            $message .= $reset_link;
          //  mail($email, 'forgot password link', $message);
           // echo $message;
            
            $post_data = array(
                'id' => $ac_id,
                'email' => $email,
                'field[%FPL%, 0]' => $reset_link,
                
                
                // assign to lists:
                'p[1]' => 1, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                'status[1]' => 1 // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 1)
            );
            
          //  print_r($post_data);
            
             $this->send_new_user_data('contact_edit', $post_data);
             $res['error_codes'] = 0;
        } else {
            $res['error_codes'] = -1003;
             $res['message'] = 'Could not retrieve data';
        }
        
        echo json_encode($res);
        
    }

    public function update_data($table, $where, $data) {
        $this->data->update(
                $table, $data, $where);
    }

    /*
     * Get feeds matching the user's interests, the greater match is the better match
     */

    public function get_feeds($user_id, $offset = false) {

        $interest_data = $this->getTableFieldData('users', 'interests', 'id', $user_id);
        $interests = json_decode($interest_data, true);
        // print_r($interests['interests']);
        $post_per_page = '';
        //if($offset != "" || $offset == 0){
        if(!empty($offset)){
            $post_per_page = 9;
        }
        $args = array(
            'posts_per_page' => $post_per_page,
            'offset'         => $offset,
            'cat'            => '3',
            'post_status'    => 'publish',
            //'tag'            => $user_id == 'public' ? '' : $interests['interests'],
            'orderby'        => 'date',
            'order'          => 'DESC',
            'post_type'      => 'post',
            'meta_query'     => array(
                array(
                    'key' => '_thumbnail_id',
                )
            )
            
           //'meta_key'=>'_thumbnail_id'
        );



        $user_tags = array();

        if( $user_id != 'public' ){

            $args['tag'] = $interests['interests'];

            $tags = $interests['interests'];
            if (count($tags) > 0) {
                foreach ($tags as $key => $value) {


                    if ($value == "" || $value == null) {
                        unset($tags[$key]);
                    }
                }
            }
            else{
                $tags = array();
            }

            $user_tags = array_values($tags);

        }

        
       
        $query = new WP_Query($args);


        $post_data = array();
        $data = array();

        foreach ($query->posts as $data_array) {
/*
             echo 'DA:<pre>';
            print_r($data_array);
            echo '</pre>';
*/
            $post_meta = get_post_meta($data_array->ID);
/*
            echo 'post_meta:<pre>';
            print_r($post_meta);
            echo '</pre>';
*/
            $thumb_url_array = wp_get_attachment_image_src($post_meta['_thumbnail_id'][0], 'thumbnail-size', true);
$thumb_url = $thumb_url_array[0];
/*
echo 'thumb_url_array:<pre>';
            print_r($thumb_url_array);
            echo '</pre>';
            
            $img = ''; 
  */          
            $tags                   = wp_get_post_tags($data_array->ID, array('fields' => 'names'));
            $match                  = array_intersect($user_tags, $tags);
            $match_count            = count($match);
            $data['match']          = $match_count;
            $data['id']             = $data_array->ID;
            $data['title']          = $this->sanitise_data($data_array->post_title);
            $data['content']        = $this->sanitise_data($data_array->post_content);
            $data['summary']        = $this->sanitise_data(get_field('summary', $data_array->ID));
            $data['featured_image'] = $thumb_url_array[0];//$img;// $this->sanitise_data(get_field('featured_image', $data_array->ID));
            $data['slider_image']   = $this->sanitise_data(get_field('slider_image', $data_array->ID));
            $data['time']           = $this->sanitise_data(get_field('time', $data_array->ID));
            $data['date']           = $this->sanitise_data(get_field('date', $data_array->ID));
            $data['post_date']      = $this->sanitise_data($data_array->post_date);
            $data['tags']           = $tags;
     /*      
            echo 'D:<pre>';
            print_r($data);
            echo '</pre>';
       */     
            if(in_array('featured', $tags)){
                
            }else{
            array_push($post_data, $data);

            }

        }
       // usort($post_data, array($this, 'cmp'));
       // $post_data['featured_articles'] = $this->get_featured_articles();
        
        $res = array();
         if(count($post_data) > 0)   {
             $res['error_codes'] = 0;
         }
         else{
             $res['error_codes'] = -1003;
             
         }
         
         $res['data'] = $post_data;
         $res['featured_articles'] = $this->get_featured_articles();
         
         
         
       
        
        echo json_encode($res);
    }
    
     public function sanitise_data($data)
    {
       if($data == "" OR $data == FALSE OR $data == NULL){
           return "";
       }else{
           return $data;
       }
    }
    public function get_featured_articles() {
        $args = array(
            'posts_per_page' => '5',
            'offset'         => 0,
            'category_name'  => 'featured',
            
            'orderby'        => 'date',
            'order'          => 'DESC',
            'post_type'      => 'post'
        );

        $query = new WP_Query($args);

        $post_data = array();
        foreach ($query->posts as $data_array) {
            
            $post_meta       = get_post_meta($data_array->ID);
            $thumb_url_array = wp_get_attachment_image_src($post_meta['_thumbnail_id'][0], 'thumbnail-size', true);
            $thumb_url       = $thumb_url_array[0];

             $data['title']          = $data_array->post_title;
             $data['id']             = $data_array->ID;
             $data['summary']        = get_field('summary', $data_array->ID);
             $data['featured_image'] = $thumb_url; //get_field('featured_image', $data_array->ID);
             $data['time']           = get_field('time', $data_array->ID);
             $data['post_date']      = $data_array->post_date;
             $data['slider_image']   = get_field('slider_image', $data_array->ID);

             $sql = 'select wt.name, wt.term_id, wtr.term_taxonomy_id, wtt.taxonomy from wp_term_relationships as wtr JOIN wp_terms as wt on wtr.term_taxonomy_id = wt.term_id join wp_term_taxonomy as wtt on wtt.term_id = wt.term_id where wtr.object_id = ' . $data_array->ID . ' ';

             $result = $this->data->get_results( $sql , ARRAY_A);
             
             $data['tags']           = $result;

            array_push($post_data, $data);

        }

        return $post_data;
    }

    public function cmp($a, $b) {
        $result = 0;
        $sc1 = $a['sort_date'];//(int) $a['match'];
        $sc2 = $b['sort_date'];//(int) $b['match'];


        if ($sc1 > $sc2) {
            $result = 1;
        } else if ($sc1 < $sc2) {
            $result = -1;
        }/* else {
            $time1 = $a['post_date'];
            $time2 = $b['post_date'];

            if ($time1 < $time2) {
                $result = 1;
            } else if ($sc1 > $sc2) {
                $result = -1;
            }
        }*/

        return $result;
    }
    
    /*
     * Get post preview
     */
    public function get_post_preview($post_id) {
        
        $data_array = get_post($post_id);
      $cat = get_the_category($post_id);
    
        $data = array();
        $res = array();
        if(count($data_array) > 0){
       $res['error_codes'] = 0;
        $formated_date = date("jS F Y", strtotime($data_array->post_date));
        
         
        $data['author'] = get_user_by('id', $data_array->post_author)->data->display_name;

       // $data['comment'] = $data_array->comment_count;
       // $data['comment_count'] = $comment_count->total_comments ;
        
       

        $data['id'] = $data_array->ID;
        $data['title'] = $data_array->post_title;
        $content = $data_array->post_content;
         $data['content'] = apply_filters('the_content', $content);
        $data['featured_image'] = get_field('featured_image', $data_array->ID);
        $data['summary'] = get_field('summary', $data_array->ID);
        $data['slide_image'] = get_field('side_image', $data_array->ID);
        $data['formatted_date'] = $formated_date;
        $data['date'] = get_field('date', $data_array->ID);
        $data['time'] = get_field('time', $data_array->ID);
        $data['category'] = $cat[0]->name;
       
        $res['data'] = $data;
        
       
        }else{
             $res['error_codes'] = -1003;
        }
        echo json_encode($res);
      
    }

    public function get_article($article_id, $user_id = false) {
         $data_array = get_post($article_id);
        
       $comments = $this->get_comments_data($article_id, $user_id);
        $data = array();
        $res = array();
        if(count($data_array) > 0){
            
        $res['error_codes'] = 0;
       $comment_count = wp_count_comments($article_id);
        $formated_date = date("jS F Y", strtotime($data_array->post_date));
        
         
        $data['author'] = get_user_by('id', $data_array->post_author)->data->display_name;

       // $data['comment'] = $data_array->comment_count;
       // $data['comment_count'] = $comment_count->total_comments ;
        
        $data['comment_count'] = count($comments);

        $data['id'] = $data_array->ID;
        $data['title'] = $data_array->post_title;
        $content = $data_array->post_content;
         $data['content'] = apply_filters('the_content', $content);
        $data['featured_image'] = get_field('featured_image', $data_array->ID);
        $data['summary'] = get_field('summary', $data_array->ID);
        $data['side_image'] = get_field('side_image', $data_array->ID);
        $data['formatted_date'] = $formated_date;
        $data['date'] = get_field('date', $data_array->ID);
        $data['time'] = get_field('time', $data_array->ID);
        $data['comments'] = $comments;
        $res['data'] = $data;
        
       
        }else{
             $res['error_codes'] = -1003;
        }
         echo json_encode($res);
    }


    public function get_event($event_id) {

        $data_array = get_post($event_id);
        $data = array();
        $res = array();
        if(count($data_array) > 0){
         $res['error_codes'] = 0;
        $formated_date = date("jS F Y", strtotime(get_field('date', $data_array->ID)));
        $data['id'] = $data_array->ID;
        $data['title'] = $data_array->post_title;
        //$data['content'] = $data_array->post_content;
        
        $content = $data_array->post_content;
         $data['content'] = apply_filters('the_content', $content);
        $data['guid'] = $data_array->guid;
        $data['top_image'] = get_field('top_image', $data_array->ID);
        $data['video'] = get_field('event_video', $data_array->ID);
        $data['date'] = get_field('date', $data_array->ID);
        $data['formatted_date'] = $formated_date;
        $data['time'] = get_field('time', $data_array->ID);
        $data['address'] = get_field('address', $data_array->ID);
        $data['post_code'] = get_field('post_code', $data_array->ID);
        
        $res['data'] = $data;
        
        }else{
            $res['error_codes'] = -1003;
        }
        
        echo json_encode($res);
        
    }

    public function get_user_home_coordinates($user_id) {

        $location = '';

        $selected_location = $this->get_selected_location($user_id);

        if ($selected_location == 1) {
            $location = 'home_town_coordinates';
        } elseif ($selected_location == 2) {
            $location = 'current_location';
        }
        $coord = $this->getTableFieldData('users', $location, 'id', $user_id);

        return $coord;
    }

    public function get_selected_location($user_id) {

        $location = $this->getTableFieldData('users', 'location', 'id', $user_id);

        return $location;
    }

    public function getTableFieldData($table, $field, $where, $where_value) {
        $result = $this->data->get_results("SELECT $field FROM $table WHERE $where = '$where_value'", ARRAY_A);

        if (count($result) > 0) {
            return $result[0]["$field"];
        }
    }

    public function get_facebook_id($email) {
        $fb_id = $this->getTableFieldData('users', 'facebook_id', 'email', $email);


        $res = '';
        if (!empty($fb_id)) {
            $res = array(
                'error_codes' => 0,
                'fb_id' => $fb_id);
        } else {
            $res = array(
                'error_codes' => -1003,
            );
        }

        echo json_encode($res);
    }
    
    /*
     * Add post comment
     */
    public function add_comment($user_id, $comment_author, $email, $post_id, $comment_content, $subject, $author_username)
    {
        $commentdata = array(
    'comment_post_ID' => $post_id, // to which post the comment will show up
    'comment_author' => $comment_author, //fixed value - can be dynamic 
       // 'author_username' => $author_username,
    'comment_author_email' => $email, //fixed value - can be dynamic 
    'comment_author_url' => '', //fixed value - can be dynamic 
    'comment_content' => $comment_content, //fixed value - can be dynamic 
    'comment_date' => date("Y-m-d H:i:s"), //empty for regular comments, 'pingback' for pingbacks, 'trackback' for trackbacks
        'comment_date_gmt' => date("Y-m-d H:i:s"),
        'comment_approved' => 0,
    'comment_parent' => 0, //0 if it's not a reply to another comment; if it's a reply, mention the parent comment ID here
        'comment_subject' => $subject,
    'user_id' => $user_id, //passing current user ID or any predefined as per the demand
);
      //  get_error_message();
//print_r($commentdata);

//Insert new comment and get the comment ID
   $this->data->insert(
            'wp_comments',
            $commentdata
            );
    //wp_new_comment($commentdata);
    
    $comment_id = $this->data->insert_id;
    $res = array();
    if(!empty($comment_id)){
    $res = array('error_codes' => 0,'id' => $comment_id);
    }
    else{
         $res = array('error_codes' => -1);
    }
    
    echo json_encode($res);
    }
    
    /*
     * Get post comments
     */
    public function get_comments($post_id, $user_id)
    {
       $comments = $this->get_comments_data($post_id, $user_id)  ;
        $res = array();
         if(count($comments > 0)){
             
         $res['error_codes'] = 0;
         $res['data'] = $comments;
         
         }
         else{
              $res['error_codes'] = -1003;
         }
         
       echo json_encode($res);
    }

    public function get_public_comments($post_id, $user_id = 0)
    {
        /*
       $comments = $this->get_comments_data($post_id, $user_id)  ;
        $res = array();
         if(count($comments > 0)){
             
         $res['error_codes'] = 0;
         $res['data'] = $comments;
         
         }
         else{
              $res['error_codes'] = -1003;
         }
         */
         $res['data'] = 'testong';
       echo json_encode($res);
    }
    
    public function get_comments_data($post_id, $user_id)
    {
     // echo $user_id;
       $comments = get_comments( array( 'number' => '', 'post_id' => $post_id) );
         $comment_arr = array();
         $i = 0;
         if(count($comments > 0)){
         foreach ($comments as $comment_data) {
            
             $isApproved = $comment_data->comment_approved;
            $author_user_id = $comment_data->user_id;
             $username = $this->getTableFieldData('users', 'username', 'id', $author_user_id);
             if($isApproved == 1 OR $author_user_id == $user_id){
              //  echo $author_user_id.'yes';
                 
             $data['formatted_date'] = get_day_name($comment_data->comment_date);
             $data['content'] = $comment_data->comment_content;
             $data['id'] = $comment_data->comment_ID;
             $data['user_id'] = $comment_data->user_id;
             $data['author'] = $comment_data->comment_author;
             $data['author_username'] = $username;
            // $data['subject'] = $comment_data->comment_subject;
             $data['date'] = $comment_data->comment_date;
              $data['time'] = date("H:i",  strtotime($comment_data->comment_date));
             
             // $report_status = $comment_data->isReported;
               $data['status'] = $isApproved;
             // if($report_status == 1){
               if($isApproved == 0){
                 
                  $data['content'] = "Your comment is being reviewed by Grapevine and will be posted shortly";
                  $data['subject'] = "";
                }
              
             array_push($comment_arr, $data);
             }
             //print_r($data);
         }
          return $comment_arr; 
         }
         else{
             return array();
         }
         
         
         
        
         
       
    }
    /*
     * Report abusive comments
     */
    public function report_comment($comment_id, $post_id, $author_name, $comment_reported_by_id, $comment_reported_by_name, $author_id)
    {
        
        $this->update_data('wp_comments', array('comment_ID' => $comment_id), array('isReported' =>1));
        
       $this->data->insert(
            'reported_comments',
            array(
                'comment_id' => $comment_id,
                'comment_post_id' => $post_id,
                'coment_author_id' => $author_id,
                'comment_author' => $author_name,
                'comment_reported_by_id' => $comment_reported_by_id,
                'comment_reported_by_name' => $comment_reported_by_name
            )
            ); 
       
       $report_id = $this->data->insert_id;
    if(!empty($report_id)){
    $res = array('error_codes' => 0,'id' => $report_id);
    }
    else{
        $res['error_codes'] = -1;
    }
     echo json_encode($res);
    }
    
    
    public function get_reported_comments()
    {
       $results = $this->data->get_results("SELECT rc.comment_reported_by_id, rc.comment_reported_by_name, c. *, p.post_title FROM wp_comments c, reported_comments rc, wp_posts p WHERE c.comment_ID = rc.comment_id AND c.comment_post_ID = p.ID AND rc.isApproved = 0", ARRAY_A); 
       $res = array();
      if(count($results) > 0){
          $res['error_codes'] = 0;
          $res['data'] = $results;
      }
      else{
           $res['error_codes'] = -1003;
      }
      
      echo json_encode($res);
    }
    
    /*
     * Update the comment status
     */
    public function update_comment_status($comment_id, $status)
    {
        if($status == 0){
           $this->update_data('wp_comments', array('comment_ID' => $comment_id), array('comment_approved' =>1));   
        }
         $this->update_data('wp_comments', array('comment_ID' => $comment_id), array('isReported' =>$status));
         $this->update_data('reported_comments', array('comment_id' => $comment_id), array('isApproved' => 1));
    }
    
    /*
     * Delete Comment
     */
    
    public function delete_comment($comment_id)
    {
        $this->data->delete( 'wp_comments', array( 'comment_ID' => $comment_id ) );  
    }
    
    /*
     * Get all users who have interests that match with the article tags
     */
    public function get_users_by_interests($interests)
    {
        
        $results = $this->data->get_results("SELECT interests, device_token, id, device_type FROM users WHERE device_token IS NOT NULL", ARRAY_A); 
        //echo '<pre>';
        //print_r($results);
        //echo '</pre>';
        $res = array();
        //print_r($results);
        //die();

        if(count($results) > 0){
            foreach($results as $user_data){
                $user_interest_data = json_decode($user_data['interests'], true);
                $user_interests = array_shift($user_interest_data);
                $data['device_token'] = $user_data['device_token'];
                $data['user_id'] = $user_data['id'];
                $data['device_type'] = $user_data['device_type'];
                $match = array_intersect($interests, $user_interests);
                
                if(count($match) > 0){
                    array_push($res, $data);
                } 
                else {
                    //echo '<pre>';
                    //print_r($user_interest_data);
                    //echo '</pre>';
                    //echo '<pre>';
                    //print_r($user_interests);
                    //echo '</pre>';
                    //echo '<pre>';
                    //print_r($interests);
                    //echo '</pre>';
                    //echo '<pre>';
                    //print_r($data);
                    //echo '</pre>';
                    //echo '<pre>';
                    //print_r($match);
                    //echo '</pre>';
                    //die();
                }
              
            }
        }
        
        //echo '<pre>';
        //($interests);
        //echo '</pre>';
        //echo '<pre>';
        //print_r($res);
        //echo '</pre>';
        //die(); 
        return $res;
    }
    
    public function send_push_by_interest($interests, $message){

        echo "in send_push_by_interest - ";
        //die();

        $users_data = $this->get_users_by_interests($interests);
        
        if(empty($users_data)){
            echo ' - no users - ';
            die();
        }

        foreach($users_data as $user_data) {
            $device_type = $user_data['device_type']; 
            $device_token = $user_data['device_token'];
            if(!empty($device_token)){
                //echo  $request = get_data(get_site_url().'/API/push.php', array('device_token' => $device_token, 'message' => $message));
                echo ' - send push to: '.$device_token.' - ';
                $this->push->sendPushNotification($device_token, $message, $device_type); 
            } else {
                echo ' - no device_token - ';
                die();
            }
        }
      
      //print_r($request);
    }
    
    public function send_push_by_location($location, $distance, $message)
    {
        $users_data = $this->get_users_by_location($location, $distance);
        
        foreach ($users_data as $user_data) {
            $device_type = $user_data['device_type']; 
            $device_token = $user_data['device_token'];
            // echo  $request = get_data(get_site_url().'/API/push.php', array('device_token' => $device_token, 'message' => $message));
                
            $this->push->sendPushNotification($device_token, $message, $device_type);
        }
        
      
    }
    
    public function get_users_by_location($selected_location, $distance)
    {
        $results = $this->data->get_results("SELECT home_town_coordinates, device_token, id, device_type FROM users", ARRAY_A); 
        $res = array();
       
        $location = $this->get_coordinates($selected_location);
       
        //print_r($location);
        if(count($results) > 0){
            foreach($results as $user_data){
                $user_location_data = json_decode($user_data['home_town_coordinates'], true);
                
                $range_check = $this->check_location_iswithin_range($user_location_data, $location, $distance);
                
                if($range_check == 1){
                 $data['device_type'] = $user_data['device_type'];
                $data['device_token'] = $user_data['device_token'];
                 $data['user_id'] = $user_data['id'];
                 
                 array_push($res, $data);
                
                }
              
              
            }
        }
        
        return $res;
    }
    
    /*
     * Function to check if a location is within a certain distance of another location
     */
    
    public function check_location_iswithin_range($user_location_data, $event_location_data, $distance = 0)
    {
         $radius = 6000.16;
         $lat = 0;
         $lng = 0;
         $ev_lat = 0;
         $ev_lng = 0;
         
         if(count($user_location_data) > 0){
         $lat = $user_location_data['lat'];
         $lng = $user_location_data['lng'];
         }
         
         if(count($event_location_data) > 0){
         $ev_lat = $event_location_data['lat'];
         $ev_lng = $event_location_data['lng'];
         }


        $maxLat = (float) $lat + rad2deg($distance / $radius);
        $minLat = (float) $lat - rad2deg($distance / $radius);

        $maxLng = (float) $lng + rad2deg($distance / $radius / cos(deg2rad((float) $lat)));
        $minLng = (float) $lng - rad2deg($distance / $radius / cos(deg2rad((float) $lat)));


        if ($ev_lng >= $minLng AND $ev_lng <= $maxLng AND $ev_lat >= $minLat AND $ev_lat <= $maxLat) {

            return 1;
        }
        else{
            return 0;
        }
    }
    
    
    
    public function send_campain_data($post_params, $post_data) {

        $url = 'https://grapevinesocial.api-us1.com';


        $params = $post_params;

// here we define the data we are posting in order to perform an update
        $post = $post_data;

// This section takes the input fields and converts them to the proper format
        $query = "";
        foreach ($params as $key => $value)
            $query .= $key . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

// This section takes the input data and converts it to the proper format
        $data = "";
        foreach ($post as $key => $value)
            $data .= $key . '=' . urlencode($value) . '&';
        $data = rtrim($data, '& ');

// clean up the url
        $url = rtrim($url, '/ ');

// This sample code uses the CURL library for php to establish a connection,
// submit your request, and show (print out) the response.
        if (!function_exists('curl_init'))
           // die('CURL not supported. (introduced in PHP 4.0.2)');

// If JSON is used, check if json_decode is present (PHP 5.2.0+)
        if ($params['api_output'] == 'json' && !function_exists('json_decode')) {
            //die('JSON not supported. (introduced in PHP 5.2.0)');
        }

// define a final API request - GET
        $api = $url . '/admin/api.php?' . $query;

        $request = curl_init($api); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_POSTFIELDS, $data); // use HTTP POST to send form data
//curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment if you get no gateway response and are using HTTPS
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

         $response = (string) curl_exec($request); // execute curl post and store results in $response
// additional options may be required depending upon your server configuration
// you can find documentation on curl options at http://www.php.net/curl_setopt
        curl_close($request); // close curl object

        if (!$response) {
          //  die('Nothing was returned. Do you have a connection to Email Marketing server?');
        }

// This line takes the response and breaks it into an array using:
// JSON decoder
        $result = json_decode($response, true);
        $contact_id = '';
       // print_r($result);
        if (count($result) > 0) {
            
            if (isset($result['subscriber_id'])) {
                $contact_id = $result['subscriber_id'];
            }

            return $contact_id;
        }
    }
    
    
    public function send_new_user_data($action, $post_data)
    {
      
        $params = array(
            // the API Key can be found on the "Your Settings" page under the "API" tab.
            // replace this with your API Key
            'api_key' => 'acfd1498952a893c9a7b79bf3c7b4066bcb3ce5b0bb37c33c74ce8e5e526d4845ccbfd3f',
            // this is the action that adds a contact
            'api_action' => $action,
            'api_output' => 'json',
        );



        $contact_id = $this->send_campain_data($params, $post_data);
       
         
        
        
        if ($action == 'contact_add') {
            
            $this->data->update('users', 
                array('active_campaign_id' => $contact_id),
                array('email' => $post_data['email']));
        }
    }
    
    /*
     * Verify username uniqueness
     */
    
    public function validate_username($username)
    {
        $results = $this->data->get_results("SELECT * FROM users WHERE username = '$username'", ARRAY_A);
        
   
        
     
        $res  = array();
        if(count($results) > 0){
            
            $res['error_codes'] = -1;
            $res['message'] = 'Username already exists';
           
        }else{
            
             $res['error_codes'] = 0;
        }
        
        echo json_encode($res);
    }
    

    public function update_device_credentials($user_id, $device_token, $device_type)
    {
        $this->data->update(
                'users',
                array(
                    'device_token' => $device_token,
                    'device_type' => $device_type
                ),
                array(
                    'id' => $user_id
                )
                ) ;
    }

}


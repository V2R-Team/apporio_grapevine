
<?php

/**
 * Description of main_interface
 *
 * @author baldesalame
 */
require_once 'Validation.php';
 
class Main_Interface {

    public $response;
    public $request;
    public $webservice;
   
    

    public function __construct() {
       // error_reporting(E_ALL);
        $this->webservice = new Webservice();
      //  error_log("_REQUEST: " . print_r($_REQUEST, true));
        $this->validation = new Validation();

        $exclude = array("register_user", "user_login", "forgot_password", "reset_forgot_password", "send_interest_based_push", "send_location_based_push", "validate_username", "update_facebook_id", "get_post_preview", "get_facebook_id", "get_comments", "get_public_comments","get_article");

        $this->request = json_decode(stripslashes(str_replace("\\\'", "'", $_REQUEST['json'])), true);

        //print_r($this->request);
        $method = $this->request['method'];
        $user_id = $this->request['user_id'];
        $token   = $this->request['token'];
        $email   = $this->request['email'];
        $origin  = $this->request['origin'];
        
        if($method == "get_feeds" OR $method == "get_events"){
            
             if( $user_id == 'public' && $token == 'public' ){
                 array_push($exclude, 'get_feeds', 'get_events');
             }
            
        }



        //$this->$method();
        if (in_array($method, $exclude)) {
            //Do not check for tokens
            
            $this->$method();

        } else {

           /*$user_id = $this->request['user_id'];
            $token   = $this->request['token'];
            $email   = $this->request['email'];
            $origin  = $this->request['origin'];*/

            // IF the user hasn;t registered yet then id and token will be 'public'
            // if( $user_id == 'public' && $token == 'public' ){

            //     if( $this->request['method'] == 'get_feeds' || $this->request['method'] == 'get_events' || $this->request['method'] == 'get_public_comments'){

            //         $validation_check = 1;

            //     }

            // }else{

            //     $validation_check = $this->validation->validate_token($user_id, $email, $token, $origin);

            // }
            if( $this->request['method'] == 'get_feeds' || $this->request['method'] == 'get_events' || $this->request['method'] == 'get_public_comments'){

                    $validation_check = 1;

                }
            // print_r($validation_check);
            if ($validation_check == 1) {
                $this->$method();
            } else {

                $res = array("error_codes" => -1000, "message" => "Invalid Token");

                echo json_encode($res);
            }
        }

     
    }

    /*
     * Register users
     */

    public function register_user() {
        $gender = '';
        $username = $this->request['username'];
        $user_location = $this->request['location'];
        $f_name = $this->request['first_name'];
        $l_name = $this->request['last_name'];
        $fb_id = $this->request['fb_id'];
        $email = $this->request['email'];
        $pass = $this->request['user_pass'];
        $origin = $this->request['origin'];
         $region = $this->request['region'];
        $phone_number = $this->request['tel_number'];
        $validation_field = '';
        
        if($origin == 'web'){
            $validation_field = 'token';
        }else if($origin == 'mobile'){
            $validation_field = 'm_token';
        }else{
             $validation_field = 'token';
        }
        
        if (isset($this->request['gender'])) {
            $gender = $this->request['gender'];
        }

        if (isset($this->request['interests'])) {
            $interest_data = $this->request['interests'];
            $interests = json_encode($interest_data);
        } else {
            $interests = '';
        }

        if (isset($this->request['current_location'])) {
            $current_location_data = $this->request['current_location'];
            $current_location = json_encode($current_location_data);
        } else {
            $current_location = '';
        }

        $this->webservice->register($email, $username, $f_name, $l_name, $user_location, $current_location, $pass, $phone_number, $region, $interests, $fb_id, $gender, $validation_field);
    }

    /*
     * User login
     */

    public function user_login() {
        // error_log("_LOGIN: " . print_r($this->request, true));
        $origin = $this->request['origin'];
        $validation_field = '';
        
        if($origin == 'web'){
            $validation_field = 'token';
        }else{
            $validation_field = 'm_token';
        }
        $fb_id = '';
        $email = $this->request['email'];
        $pass = $this->request['user_pass'];
        if (isset($this->request['fb_id'])) {
            $fb_id = $this->request['fb_id'];
        }
     $data =  $this->webservice->user_login($email, $pass, $fb_id, $validation_field);
       error_log("_LOGINRES: " . print_r(json_encode($data), true));
       
      //print_r($data);
       
      
    }

    /*
     * Update user details
     */

    public function update_user_details() {
        $user_id = $this->request['user_id'];

        if (!empty($this->request['region'])) {
            $coord_data = $this->webservice->get_coordinates($this->request['region']);

            $coordinates = array(
                'lat' => $coord_data['lat'],
                'lng' => $coord_data['lng']
            );
            $coord_obj = json_encode($coordinates);
        }

        if (isset($this->request['interests'])) {
            $interest_data = $this->request['interests'];
            $new_interest = array();
            if (count($interest_data) > 0) {
                foreach ($interest_data['interests'] as $key => $value) {


                    if ($value == "" || $value == null) {
                        unset($interest_data[$key]);
                    }else{
                        array_push($new_interest, trim($value));
                    }
                }
            }
            $cleanData['interests'] = $new_interest;
        // $clean_interests =  //array_values($interest_data);
            $interests = json_encode($cleanData);
        } else {
            $interests = '';
        }

        if (isset($this->request['current_location'])) {
            $current_location_data = $this->request['current_location'];
            $current_location = json_encode($current_location_data);
        } else {
            $current_location = '';
        }

        $pass = '';
        $fbid = '';
        $gender = '';

        if (isset($this->request['fb_id'])) {
            $fbid = $this->request['fb_id'];
        }

       
        
        if (isset($this->request['gender'])) {
            $gender = $this->request['gender'];
        }
        
        

        $data = array(
            'location' => $this->request['location'],
           // 'email' => $this->request['email'],
            'first_name' => $this->request['first_name'],
            'last_name' => $this->request['last_name'],
            'name' => $this->request['first_name'] . ' ' . $this->request['last_name'],
            //'user_password' => $pass,
            'region' => $this->request['region'],
            'home_town_coordinates' => $coord_obj,
            'tel_number' => $this->request['tel_number'],
            'facebook_id' => $fbid,
            'gender' => $gender,
            'interests' => $interests,
            'current_location' => $current_location
        );
        
         if (isset($this->request['user_pass'])) {
            $pass = $this->request['user_pass'];
            $data['user_password'] = $pass;
        }
        
   /*   $new_data = array();
        foreach ($data as $key => $value) {
            $check_value = trim($value);
            if($check_value != "" OR $check_value != null){
               
              $new_data[$key] = $value;
            }
        }
        
       

      

       // $this->webservice->update_data('users', array("id" => $user_id), array('log_data' => $interests));
        $res  = array();
        if (isset($this->request['email'])) {
           $email = $this->request['email'];
            $email_check = $this->webservice->getTableFieldData('users', 'email', 'id', $user_id);

            if($email_check != $email) {
                $email_second_check = $this->webservice->getTableFieldData('users', 'email', 'email', $email);
                if(empty($email_second_check)) {
                    $new_data['email'] = $email;
                    
                   
                   // $this->webservice->update_user_data($user_id, $new_data);
                } else {
                    $res['error_codes'] = -9999;
                    $res['message'] = 'Email already exits';
                    echo json_encode($res);
                   
                }
            }else{
               
                
               // $this->webservice->update_user_data($user_id, $new_data);
            }
        }
       /* } else {
            $this->webservice->update_user_data($user_id, $data);
        }*/
            
           //  error_log("_REQUEST: " . print_r($new_data, true));
             
             $this->webservice->update_user_data($user_id, $data);
    }

   

    public function get_user_data() {
        $email = '';
        $user_id = $this->request['user_id'];
        if (isset($this->request['email'])) {
            $email = $this->request['email'];
        }

        $this->webservice->get_user_data($user_id, $email);
    }

    /*
     * Get events based on location
     */

    public function get_events() {

      // print_r($this->request);
        if (isset($this->request['location'])) {
            $location = $this->request['location'];
        } else {
            $location = '';
        }
        $location = $this->request['location'];
        $dist = $this->request['distance'];
        $offset = $this->request['offset'];
        $orderby = $this->request['selected-date'];
        $selectdate = $this->request['selected_date'];
        $user_id = $this->request['user_id'];
        $selected_location = $this->request['selected_location'];
        $select_location = $this->request['selected-location'];
        //  $location = '';//array('lat'=> '52.30897', 'lng' => '-1.9409359999999651');
        if (!empty($dist) ) {
            $this->webservice->get_events($location, $selected_location,$dist, $orderby, $offset);
        } else {
            echo'{"error_codes": "-9999"}';
        }

    }

    public function get_event() {
        $event_id = $this->request['event_id'];
        $this->webservice->get_event($event_id);
    }

    public function get_article() {
        $artcle_id = $this->request['article_id'];
         $user_id = $this->request['user_id'];
        $this->webservice->get_article($artcle_id, $user_id);
    }
    
    /*
     * Get post preview
     */
    public function get_post_preview() {
        $post_id = $this->request['post_id'];
         
        $this->webservice->get_post_preview($post_id);
    }

    /*
     * Reset Password
     */

    public function reset_password() {
        $email = $this->request['reset-email'];
       
        $pass = $this->request['user_pass'];

        $this->webservice->reset_password($email, $pass);
    }

    /*
     * Get feeds depending on the user's interests
     */

    public function get_feeds() {
        $user_id = $this->request['user_id'];
        $offset = $this->request['offset'];
        //$limit = $this->request['limit'];
        $this->webservice->get_feeds($user_id, $offset);
    }

    /*
     * Get user home town coordinates
     */

    public function get_home_coordinates() {
        $user_id = $this->request['user_id'];

        $this->webservice->get_user_home_coordinates($user_id);
    }

    /*
     * Get user facebook id
     */

    public function get_facebook_id() {
        $email = $this->request['email'];

        $this->webservice->get_facebook_id($email);
    }

    /*
     * Add post comment
     */

    public function add_comment() {
        $subject = ' ';
        $user_id = $this->request['user_id'];
        $comment_author = $this->request['author'];
        $comment_author_username = $this->request['author_username'];
        $email = $this->request['email'];
        $post_id = $this->request['post_id'];
        $comment_content = $this->request['comment_content'];
        
        if(isset($this->request['subject'])){
        $subject = $this->request['subject'];
        }

      // print_r($this->request);
        $this->webservice->add_comment($user_id, $comment_author, $email, $post_id, $comment_content, $subject, $comment_author_username);
    }

    /*
     * Add post comment
     */

    public function get_comments() {
        $post_id = $this->request['post_id'];
         $user_id = $this->request['user_id'];
        $this->webservice->get_comments($post_id, $user_id);
    }

    /*
     * Report comments
     */

    public function report_comment() {
        $reporter_id = $this->request['reporter_id'];
        $reporter_name = $this->request['reporter_name'];
        $comment_id = $this->request['comment_id'];
        $post_id = $this->request['post_id'];
        $author_name = $this->request['comment_author'];
        $author_id = $this->request['comment_author_id'];

        $this->webservice->report_comment($comment_id, $post_id, $author_name, $reporter_id, $reporter_name, $author_id);
    }

    public function get_reported_comments() {
        //  echo uniqid();
        $this->webservice->get_reported_comments();
    }

    public function update_comment_status() {
        $comment_id = $this->request['comment_id'];
        $status = $this->request['status'];
        $this->webservice->update_comment_status($comment_id, $status);
    }

    public function delete_comment() {
        $comment_id = $this->request['comment_id'];

        $this->webservice->delete_comment($comment_id);
    }

    public function get_users_by_interests() {
        //  $comment_id = $this->request['comment_id'];

        $interests = $this->request['interests'];
        //  print_r($interests);
        $this->webservice->get_users_by_interests($interests);
    }

    public function send_interest_based_push() {
        $interests = $this->request['interests'];
        $message = $this->request['content'];
        
        // print_r($interests);
        $this->webservice->send_push_by_interest($interests, $message);
    }

    public function send_location_based_push() {
        $message = $this->request['content'];
        $location = $this->request['location'];
        $distance = $this->request['distance'];
        $this->webservice->send_push_by_location($location, $distance, $message);
    }

    public function reset_forgot_password() {
        $email = $this->request['email'];
        $pass = $this->request['user_pass'];
        $validation_token = $this->request['vt'];

        $this->webservice->reset_forgot_password($email, $pass, $validation_token);
    }

    public function forgot_password() {
        $email = $this->request['email'];
        $this->webservice->forgot_password_handler($email);
    }
    
    /*
     * Update user device credentials like device token
     */
    public function update_user_device_push_data()
    {
        $device_token = $this->request['device-token'];
        $user_id = $this->request['user_id'];
        $device_type = $this->request['device-type'];
      //  error_log("update_user_device_push_data: " . print_r($_REQUEST, true));
       $this->webservice->update_device_credentials($user_id, $device_token, $device_type);
        $this->webservice->update_data('users', array("id" => $user_id), array('log_second' => $this->request['token']));
    }
    
    /*
     * Verify username uniqueness
     */
    
    public function validate_username()
    {
        $username = $this->request['username'];
        $this->webservice->validate_username($username);
    }
    
    public function update_facebook_id(){
      //  print_r($this->request);
        $email = $this->request['email'];
        $fb_id = $this->request['fb_id'];
        $this->webservice->update_data('users', array('email' => $email), array('facebook_id' => $fb_id));
    }
}

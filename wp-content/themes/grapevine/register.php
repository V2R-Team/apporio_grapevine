<?php
/*
  Template Name: Register Page Template
 */
//unset($_SESSION['user']);

if(isset($_SESSION['user'])){
    wp_redirect(get_site_url().'/account');
    exit;
}
if(isset($_SESSION['register_data'])){
    $register_data = $_SESSION['register_data'];
    /*
    echo '<pre>';
    print_r($register_data);
    echo '</pre>';
    */
}
get_header();

$status = '';
/**
*
* moved the error code handling to bottom of page
* errors now appear in custom activated modal
*
*/
?>

<!--<span class="action-text">Sign In</span>-->
<section>
    <div class="container-fluid">
        <div class="row login-row">
            <div id="clear"></div>
            <form class="" role="form" action="<?php echo get_site_url() ?>/user-login" method="POST">
                
                <div class="col-md-1" style="padding-left: 0">
                    <p class="sign-text special-style-text"> Sign In</p>
                </div>
                <div class="col-md-11">
                    <div class="col-md-3 form-input-holder">
                        <div class="input-div">
                            <input type="email" name="email" placeholder="Enter Your Email" required="true" />
                        </div>
                        <div class="input-vertical-line"></div>
                        <div class="image-icon">
                               <img class="feature-img" src="<?php echo get_template_directory_uri() ?>/icons/email-icon.png">
                        </div>
                    </div>

                    <div class="col-md-3 form-input-holder common-holder last-col">
                         <div class="input-div">
                             <input type="password" name="user_pass" placeholder="Enter Your Password" required="true" />
                        </div>
                         <div class="input-vertical-line"></div>
                        <div class="lock-icon">
                           <img class="feature-img" src="<?php echo get_template_directory_uri() ?>/icons/lock-icon.png">
                        </div>
                    </div>
                    <div class="col-md-3 form-input-holder common-holder sign-btn">
                        <button type="submit">Sign In</button>
                    </div>
            </form>
                    <div class=" col-md-3 form-input-holder common-holder last-col fbsign-btn" onclick="login()">
                        <span class="fb-letter"><img class="feature-img" src="<?php echo get_template_directory_uri() ?>/icons/facebook.png"></span>&nbsp;&nbsp;
                        Sign In / Join
                        
                        <form class="facebook-login" id="fb-form" role="form" action="<?php echo get_site_url() ?>/control" method="POST">
                            <input type="hidden" id="fb-email" name="email"/>
                            <input type="hidden" id="fb-fname" name="first_name"/>
                            <input type="hidden" id="fb-lname" name="last_name"/>
                            <input type="hidden" id="fb-id" name="fb_id"/>
                            <input type="hidden" id="user-lat" name="lat"/>
                            <input type="hidden" id="user-lng" name="lng"/>
                            <input type="hidden" name="action" value="facebook-login"/>
                        </form> 
                    </div>
                    <div class="forgot-text"> &gt; Forgotten Password</div>
                </div>
                
                <div class="forgot-password-box">
                    <div class="change-pass-form">
                        
                        <p class="forgot-pass-text">
                            Please enter your email address and we will send you instructions
                            to reset your password
                        </p>
                                <form  role="form" action="<?php echo get_site_url() ?>/control" method="POST">
                                     <div class="form-group">

                                         <input type="email" class="form-control" id="reset-email" name="email" placeholder="Enter your email" required="true"/>
                                     </div>
                                     
                                     
                                     <input type="hidden" name="action" value="forgot-password"/>
                                     <div class="form-group">

                                         <button type="submit" class="save-change-btn">Submit</button>
                                     </div>
                                     
                                </form>

                               
                             </div>
                </div>
                <div id="clear"></div>

        </div>


         
         <div class="row register-field-row">
             
             <div class="container-fluid register-container">
                 <div class="row">
                     <!-- Register -->
                     <form role="form" action="<?php echo get_site_url() ?>/control" onsubmit="return validateDetails.checkRegister(this)" method="POST">

                        <input type="hidden" id="red" name="red" />
                        <input type="hidden" id="white" name="white"  />
                        <input type="hidden" id="rose" name="rose"  />
                        <input type="hidden" id="fizz" name="fizz"  />
                        <input type="hidden" id="dessert" name="dessert"  />
                        <input type="hidden" id="fortified" name="fortified"  />
                        <input type="hidden" id="connoisseur" name="connoisseur" />
                        <input type="hidden" id="travel" name="travel"v />
                        <input type="hidden" id="wine-school" name="wine-school" />
                        <input type="hidden" id="food" name="food" />
                        <input type="hidden" id="gallery" name="gallery"  />
                        <input type="hidden" id="social" name="social" />
                        <input type="hidden" id="" name="user_id" />
                        <input type="hidden" id="" name="action" value="register-user" />
                        <input type="hidden" id="current-lat" name="lat" />
                        <input type="hidden" id="current-lng" name="lng"/>
                        <input type="hidden" id="gender" name="gender" value="not-specified" />

                     <div class="col-md-6 col-holders" style="padding-left: 10px">
                          <p class="sign-text"> Register</p>
                          <div class="col-md-6">

                              <div class="form-group">
                                  <label class="purple-color">First Name</label>
                                  <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" tabindex="1" value="<?php if(isset($_SESSION['register_data'])){
                                      echo $_SESSION['register_data']->fname;
                                    }; ?>" required>
                              </div>
                              <div class="form-group">
                                  <label class="purple-color">Username</label>
                                  <input type="text" class="form-control" id="username" name="username" placeholder="Username" tabindex="3" value="<?php if(isset($_SESSION['register_data'])){
                                      echo $_SESSION['register_data']->username;
                                    }; ?>" required>
                              </div>
                              
                              <div class="form-group">
                                  <label class="purple-color">Password</label>
                                  <input type="password" class="form-control" id="pass1" name="user_pass" placeholder="Create Password" tabindex="5" required>
                              </div>
                              <div class="form-group">
                                  <label class="purple-color">Event Location</label>
                                  <input type="text" class="form-control" id="auto-complete" name="region" placeholder="Your Region" tabindex="7" value="<?php if(isset($_SESSION['register_data'])){
                                      echo $_SESSION['register_data']->region;
                                    }; ?>">
                                  <label class="purple-color region-text">Choose the area you would like to see events for....</label>
                              </div>

                          </div>

                          <div class="col-md-6">

                              <div class="form-group">
                                  <label class="purple-color">Last Name</label>
                                  <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Surname" tabindex="2" value="<?php if(isset($_SESSION['register_data'])){
                                      echo $_SESSION['register_data']->last;
                                    }; ?>">
                              </div>
                              
                              <div class="form-group">
                                  <label class="purple-color">Email</label>
                                  <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" tabindex="4" value="<?php if(isset($_SESSION['register_data'])){
                                      echo $_SESSION['register_data']->email;
                                    }; ?>" required>
                              </div>
                              <div class="form-group">
                                  <label class="purple-color">Confirm Password</label>
                                  <input type="password" class="form-control" id="pass2" name="user_pass2" placeholder="Confirm Password" tabindex="6" required>
                              </div>
                              <div class="form-group" id="register-last" >
                                  <label class="purple-color">Gender</label>
                                  <div class="selectDiv user-gender">
                                      <span class="selectDefault gender-select-default"></span>
                                      <select id="mySelect" name="gender-select" class="selectBox gender-select-box" tabindex="8">
                                          <option class="default-text gender-default-text" value="not-specified">Not Specified</option>
                                          <option value="male">Male</option>
                                          <option value="female">Female</option>
                                          <option value="other">Other</option>
                                      </select>
                                  </div>
                               </div>
                              <!--
                              <div class="form-group">
                                <div class="checkbox account-radio-holder">
                                    <input type="checkbox" id="myLocation" name="optradio" value="2" <?php if($location_to_show == 2){ ?> checked <?php }?>/>
                                    <label for="myLocation">
                                        <span></span>Show Events in My Current Location
                                    </label>
                                </div>
                              </div>
                              -->

                          </div>
                      </div>
                      <!-- Choose Interests -->
                      <div class="col-md-6 col-holders">
                          <p class="sign-text"> Choose Interests</p>
                          <div class="col-md-12" style="padding-left: 10px">
                              <div class="row f-row">
                              <div class="col-sm-3 col-md-3 col-box image-containerSquare" onclick="setPreferences('red','red', 'red-box')"><div class="click-icon red-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Red.png" ></div>
                              <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('white','white', 'white-box')"><div class="click-icon white-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/White.png"></div>
                              <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('rose','rose', 'rose-box')"><div class="click-icon rose-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Rose.png"></div>
                              <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('fizz','fizz', 'fizz-box')"><div class="click-icon fizz-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Fizz.png"></div>
                          </div>
                          <div class="row s-row f-row">
                              <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('dessert','dessert', 'dessert-box')"><div class="click-icon dessert-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Dessert.png"></div>
                              <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('fortified','fortified', 'fortified-box')"><div class="click-icon fortified-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Fortified.png"></div>
                              <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('connoisseur','connoisseur', 'connoisseur-box')"><div class="click-icon connoisseur-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Connoisseur.png"></div>
                              <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('social','social', 'social-box')"><div class="click-icon social-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Social.png"></div>
                          </div>
                          <div class="row s-row f-row">
                              <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('travel','travel', 'travel-box')"><div class="click-icon travel-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Travel.png"></div>
                              <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('wine-school','wine-school', 'wine-school-box')"><div class="click-icon wine-school-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Wine_School.png"></div>
                              <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('food','food', 'food-box')"><div class="click-icon food-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Food.png"></div>
                              <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('gallery','gallery', 'gallery-box')"><div class="click-icon gallery-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Gallery.png"></div>
                          </div>
                          <button type="submit" class="finish-btn">Finish</button>
                      </div>
                  </div>
              </div>
              </form>

         </div>
 
         <script>
             
             $(function () {
                   getLocation();
                   
                   $(".forgot-text").click(function(){
                       $(".forgot-password-box").toggle();
                       
                   });
                   
            });
            $(document).ready(function(){
                  
                  var genderText = $('.gender-default-text').text();

                  <?php if(isset($_SESSION['register_data'])){
                    echo 'genderText = "'. $_SESSION['register_data']->gender.'"';
                  }; ?>

                  $.each($('#mySelect option'), function(i, ele){
                    if($(this).val() == '<?php echo $_SESSION['register_data']->gender; ?>' ){
                      genderText = $(this).html();
                    }
                  });
                  $('.gender-select-default').text(genderText);

                    $('.gender-select-box').on('change',function(){
                        var defaulttext2 = $('.gender-select-box').find(":selected").text(); 
                        $('.gender-select-default').text(defaulttext2);
                        
                        var gender = $(this).val();
                        <?php if(isset($_SESSION['register_data'])){
                          echo 'genderText = "'. $_SESSION['register_data']->gender.'"';
                        } ?>
                          

                        
                            $("input#gender").val(gender);
                            
                  });
            })
             
              // This example displays an address form, using the autocomplete feature
              // of the Google Places API to help users fill in the information.

              // This example requires the Places library. Include the libraries=places
              // parameter when you first load the API. For example:
              // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

              var placeSearch, autocomplete;
              var componentForm = {
                street_number: 'short_name',
                route: 'long_name',
                locality: 'long_name',
                administrative_area_level_1: 'short_name',
                country: 'long_name',
                postal_code: 'short_name'
              };

              function initAutocomplete() {
                // Create the autocomplete object, restricting the search to geographical
                // location types.
                var options = {
                    types: ['geocode'],
                    //componentRestrictions: {country: 'GB'}
                }
                autocomplete = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */(document.getElementById('auto-complete')),
                    options);

                // When the user selects an address from the dropdown, populate the address
                // fields in the form.
                autocomplete.addListener('place_changed', fillInAddress);
              }

              function fillInAddress() {
                // Get the place details from the autocomplete object.
                var place = autocomplete.getPlace();

                for (var component in componentForm) {
                  document.getElementById(component).value = '';
                  document.getElementById(component).disabled = false;
                }

                // Get each component of the address from the place details
                // and fill the corresponding field on the form.
                for (var i = 0; i < place.address_components.length; i++) {
                  var addressType = place.address_components[i].types[0];
                  if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                  }
                }
              }

              // Bias the autocomplete object to the user's geographical location,
              // as supplied by the browser's 'navigator.geolocation' object.
              
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBv5rxWqJWNXg6R3VgwWKjmB0d4CsJgEAI&libraries=places&callback=initAutocomplete"async defer></script>

        <?php
          if(isset($_SESSION['register_data'])){
              $sessionPreferences;

              $sessionPreferences->rose = $_SESSION['register_data']->rose;
              $sessionPreferences->red = $_SESSION['register_data']->red;
              $sessionPreferences->food = $_SESSION['register_data']->food;
              $sessionPreferences->fortified = $_SESSION['register_data']->fortified;
              $sessionPreferences->dessert = $_SESSION['register_data']->dessert;
              $sessionPreferences->white = $_SESSION['register_data']->white;
              $sessionPreferences->travel = $_SESSION['register_data']->travel;
              $sessionPreferences->fizz = $_SESSION['register_data']->fizz;
              $sessionPreferences->connoisseur = $_SESSION['register_data']->conn;
              $sessionPreferences->social = $_SESSION['register_data']->social;
              $sessionPreferences->gallery = $_SESSION['register_data']->gallery;
              $sessionPreferences->wine_school = $_SESSION['register_data']->school;
              print_r($sessionPreferences);
        ?>
            <script>
              $(document).ready(function(){
                  var sessionPreferences =  <?php echo json_encode($sessionPreferences) ?>;
                  $.each(sessionPreferences, function(preference, value){

                      if(value != undefined && value != ''){
                          setPreferences(value, value, value+'-box');
                      }
                  });
              });
            </script>
        <?php

          }


        ?>

        <?php

            if(isset($_REQUEST['rgs'])){
    
                $rgs = $_REQUEST['rgs'];
                
                if($rgs == 1){
                    $errorTitle = "Registration Successful";
                    $errorMessage = "You are now registered. Please login";

                } else if($rgs == -8){
                    $errorTitle = "Username Error";
                    $errorMessage = "This username already exists";

                } else if($rgs == -1){
                    $errorTitle = "Email Error";
                    $errorMessage = "This email address already exists";

                }else if($rgs == 2){
                    $errorTitle = "Password Reset";
                    $errorMessage = "Instructions to reset your password have been sent to your email ";

                }else if($rgs == -2){
                    $errorTitle = "Email Error";
                    $errorMessage = "The email provided is not valid ";

                }else if($rgs == -3){
                    $errorTitle = "Login Error";
                    $errorMessage = "Incorrect Login Details";

                }else if($rgs == -5){
                    $errorTitle = "Password Reset";
                    $errorMessage = "There was a problem whilst trying to update your password. Please try again";

                }else if($rgs == 6){
                    $errorTitle = "Password Reset";
                    $errorMessage = "Your Password has been updated successfully";

                }
            // end of if below modal
        ?>


        <!-- Modal -->
        <div id="errorModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $errorTitle ?></h4>
              </div>
              <div class="modal-body">
                <p><?php echo $errorMessage ?></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function(){
                $('#errorModal').modal();
            });
        </script>

        <?php

          } 
          //end of error handling if statement
        ?>

        <!-- Modal -->
        <div id="registerModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="registerModalTitle">No Preferences Selected</h4>
              </div>
              <div class="modal-body">
                <p id="registerModalMessage">Please select your wine preferences</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>

        <!-- Modal -->
        <div id="forgottonPasswordModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Forgotten Password</h4>
              </div>
              <div class="modal-body">
                <p>Please enter your email address and we will send you instructions to reset your password</p>
                <form  role="form" action="<?php echo get_site_url() ?>/control" method="POST">
                  <div class="form-group">
                      <input type="email" class="form-control" id="reset-email" name="email" placeholder="Enter your email" required="true"/>
                  </div>             
                  <input type="hidden" id="" name="action" value="forgot-password"/>
                  <div class="form-group">
                      <button type="submit" class="save-change-btn">Submit</button>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
            
          </div>
        </div>
        <!-- end of modals -->
    </div>
    </div>

    </div>
</section>
<div
  class="fb-like"
  data-share="true"
  data-width="450"
  data-show-faces="true">
</div>
<script>
                
      window.fbAsyncInit = function() {
      FB.init({
        appId      : '477028962487682',
        cookie     : true,  // enable cookies to allow the server to access 
                            // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.6' // use graph api version 2.5
      });
                
      // This is called with the results from from FB.getLoginStatus().
      function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
          // Logged into your app and Facebook.
          //testAPI();
        } else if (response.status === 'not_authorized') {
          // The person is logged into Facebook, but not your app.
          document.getElementById('status').innerHTML = 'Please log ' +
            'into this app.';
        } else {
          // The person is not logged into Facebook, so we're not sure if
          // they are logged into this app or not.
          document.getElementById('status').innerHTML = 'Please log ' +
            'into Facebook.';
        }
      }

      // This function is called when someone finishes with the Login
      // Button.  See the onlogin handler attached to it in the sample
      // code below.
      function checkLoginState() {
        FB.getLoginStatus(function(response) {
          statusChangeCallback(response);
        });
      }

      

      // Now that we've initialized the JavaScript SDK, we call 
      // FB.getLoginStatus().  This function gets the state of the
      // person visiting this page and can return one of three states to
      // the callback you provide.  They can be:
      //
      // 1. Logged into your app ('connected')
      // 2. Logged into Facebook, but not your app ('not_authorized')
      // 3. Not logged into Facebook and can't tell if they are logged into
      //    your app or not.
      //
      // These three cases are handled in the callback function.

      FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
      });

      };
      
      function login(){
      FB.login(function(response) {
      if (response.status === 'connected') {
        // Logged into your app and Facebook.
        
         console.log('statusChangeCallback');
      
        sendFbData();
       
         console.log(JSON.stringify(response));
      } else if (response.status === 'not_authorized') {
        // The person is logged into Facebook, but not your app.
      } else {
        // The person is not logged into Facebook, so we're not sure if
        // they are logged into this app or not.
      }
    },{scope: 'email'});

      }

      // Load the SDK asynchronously
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));

      // Here we run a very simple test of the Graph API after login is
      // successful.  See statusChangeCallback() for when this call is made.
      function sendFbData() {
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me?fields=email, name, first_name, last_name', function(response) {
         // console.log('Successful login for: ' + response.name);
         
         $("#fb-email").val(response.email);
         $("#fb-fname").val(response.first_name);
         $("#fb-lname").val(response.last_name);
         $("#fb-id").val(response.id);
         var lat = $("#current-lat").val();
          var lng = $("#current-lng").val();
         $("#user-lat").val(lat);
         $("#user-lng").val(lng);
        $("#fb-form").submit();
         console.log(JSON.stringify(response));
        });
      }
    </script>
<?php
get_footer();


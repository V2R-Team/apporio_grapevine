
<?php
/*
  Template Name: News Feeds Template
 */

session_start();

$user_data = '';
$response = '';
if (isset($_SESSION['user'])) {
    $user_data = $_SESSION['user_data'];
   // print_r($user_data);
    $user_id = $user_data['id'];
    $token = $user_data['token'];

    $data = array('method' => 'get_feeds', 'user_id' => $user_id, 'token' => $token, 'offset' => '', 'origin' => 'web');

    $json = json_encode($data);

    $request = get_data(get_site_url() . '/API/serve-api.php', array('json' => $json));
    
    $response_data = json_decode($request, true);

    $url = get_site_url() . '/API/serve-api.php?json=' . $json;
  
    if($response_data['error_codes'] == -1000)
    {
        unset($_SESSION['user']);
        unset($_SESSION['user_data']);
      wp_redirect(get_site_url() . "/register");

    exit;
    }
    $response = $response_data['data'];
/*
    echo '<pre>';
    print_r($response);
    echo '</pre>;'; 
*/
    $slideCount = 5;
    if(count($response) < $slideCount){
        $slideCount = count($response);
    }

    $latest = '';
    if(count($response) > 0){
        $latest = array_slice($response, 0, $slideCount, true);
    }else{
        $latest = array();
    }
    $featured = array_shift($response_data['featured_articles']);

    if( has_post_thumbnail( $featured['id'] ) ){
                
        $featuredThumbnailId  = get_post_thumbnail_id( $featured['id'] );
        $featuredThumbnailUrl = wp_get_attachment_url( $featuredThumbnailId );
                                
    }

/*
    echo '<pre>';
    print_r($featured);
    echo '</pre>';
*/
    $featured_image   = $featuredThumbnailUrl;
    $slider_image     = $featuredThumbnailUrl;
    $featured_summary = $featured['summary'];
    if(strlen($featured_summary) > 45){
        $featured_summary = substr($featured_summary, 0, 100).'...';
    }
    $featured_title = $featured['title'];
    $featured_id    = $featured['id'];
    
    $current_date   = date("Y-m-d H:i:s");
    $featured_date  = $featured['post_date'];
    $fcdate_object  = strtotime($current_date);
    $fend_object    = strtotime($featured_date);
    $fdif           = $fcdate_object - $fend_object;
    $featured_tags  = $featured['tags'];
    
    $fdays          = floor($fdif / (60 * 60 * 24)); //seconds/minut
    $fhours         = round($fdif / (60 * 60));
    if($fdays > 0){
                                
        $featured_date = $fdays.' Days Ago';
    }
    else{
        $featured_date = $fhours.' Hours Ago';
    }

} else {
   // wp_redirect(get_site_url() . "/register");

  //  exit;
}

//print_r($response);

get_header();
?>
<section>

<?php /*
<div class="container-fluid">
<div class="col-lg-12 grid-holder">

        <!-- The Grid Plugin Version 1.0.0 --><!-- The Grid Wrapper Start --><div class="tg-grid-wrapper tg-nav-sqr-thick quito-grid tg-grid-loaded" id="grid-831"><!-- The Grid Styles -->
<style type="text/css" class="the_grid_styles">#grid-831 .tg-nav-border:hover,#grid-831 .tg-page-number.tg-page-current,#grid-831 .tg-filter.tg-filter-active:not(.tg-dropdown-item){border-color:#773a74}#grid-831 .tg-nav-border,#grid-831 .tg-dropdown-holder:hover,#grid-831 .tg-search-inner:hover,#grid-831 .tg-sorter-order:hover,#grid-831 .tg-disabled:hover i{border:2px solid #D1BDD0}#grid-831 .tg-search-clear,#grid-831 .tg-search-clear:hover{border:none;border-left:2px solid #D1BDD0}#grid-831 .tg-nav-color:not(.dots):not(.tg-dropdown-value):not(.tg-dropdown-title):hover,#grid-831 .tg-nav-color:hover .tg-nav-color,#grid-831 .tg-page-number.tg-page-current,#grid-831 .tg-filter.tg-filter-active span:not(.tg-filter-count){color:#773a74}.tg-nav-sqr-thick .tg-page-number.dots{border:none !important}.tg-nav-sqr-thick .tg-grid-area-left i,.tg-nav-sqr-thick .tg-grid-area-left i:before,.tg-nav-sqr-thick .tg-grid-area-right i,.tg-nav-sqr-thick .tg-grid-area-right i:before{line-height:38px}.tg-nav-sqr-thick input[type=text].tg-search{height:36px}.tg-nav-sqr-thick .tg-nav-font,.tg-nav-sqr-thick input[type=text].tg-search{font-size:13px;font-weight:600;line-height:36px}.tg-nav-sqr-thick .tg-search::-webkit-input-placeholder{font-size:13px;font-weight:600;line-height:36px}.tg-nav-sqr-thick .tg-search::-moz-placeholder{font-size:13px;font-weight:600;line-height:36px}.tg-nav-sqr-thick .tg-search:-ms-input-placeholder{font-size:13px;font-weight:600;line-height:36px}.tg-nav-sqr-thick .tg-page-number.dots,.tg-nav-sqr-thick .tg-slider-bullets{height:40px}.tg-nav-sqr-thick .tg-search-icon,.tg-nav-sqr-thick .tg-search-clear,.tg-nav-sqr-thick .tg-sorter-order,.tg-nav-sqr-thick .tg-page-number,.tg-nav-sqr-thick .tg-left-arrow i,.tg-nav-sqr-thick .tg-right-arrow i{min-width:40px}.tg-nav-sqr-thick .tg-search-icon,.tg-nav-sqr-thick .tg-sorter-order i{font-weight:100}.tg-nav-sqr-thick .tg-page-number.dots,.tg-nav-sqr-thick .tg-search-inner,.tg-nav-sqr-thick .tg-search-clear,.tg-nav-sqr-thick .tg-sorter-order,.tg-nav-sqr-thick .tg-left-arrow,.tg-nav-sqr-thick .tg-right-arrow{border:none}.tg-nav-sqr-thick .tg-dropdown-list{margin-top:2px}#grid-831 .tg-nav-color,#grid-831 .tg-search-icon:hover:before,#grid-831 .tg-search-icon:hover input,#grid-831 .tg-disabled:hover .tg-icon-left-arrow,#grid-831 .tg-disabled:hover .tg-icon-right-arrow,#grid-831 .tg-dropdown-title.tg-nav-color:hover{color:#773a74}#grid-831 input.tg-search:hover{color:#773a74 !important}#grid-831 input.tg-search::-webkit-input-placeholder{color:#773a74}#grid-831 input.tg-search::-moz-placeholder{color:#773a74;opacity:1}#grid-831 input.tg-search:-ms-input-placeholder{color:#773a74}#grid-831 .tg-slider-bullets li.tg-active-item span{background:#59585b}#grid-831 .tg-slider-bullets li span{background:#DDDDDD}#grid-831 .tg-grid-area-bottom1{text-align:center;margin-top:60px}#grid-831 .tg-item{margin-bottom:28px}#grid-831.tg-item{-webkit-transform-origin:50% 0%;-moz-transform-origin:50% 0%;-ms-transform-origin:50% 0%;-o-transform-origin:50% 0%;transform-origin:50% 0%}.quito a,.quito a:active,.quito a:focus,.quito .tg-share-icons i{text-decoration:none;border:none;-webkit-box-shadow:none;box-shadow:none;-webkit-transition:opacity 0.25s ease,color 0.25s ease;-moz-transition:opacity 0.25s ease,color 0.25s ease;-ms-transition:opacity 0.25s ease,color 0.25s ease;-o-transition:opacity 0.25s ease,color 0.25s ease;transition:opacity 0.25s ease,color 0.25s ease}.quito a:not(.mejs-volume-slider):hover{opacity:0.85}.quito .tg-center-inner{position:absolute}.quito .tg-overlay-image,.quito .tg-overlay-color{position:absolute;display:block;top:0;left:0;width:100%;height:100%;background-repeat:no-repeat;background-position:center center;-webkit-background-size:cover !important;-moz-background-size:cover !important;-o-background-size:cover !important;background-size:cover !important}.quito .tg-item-content-holder{position:relative;display:block;padding:0 24px 18px 24px;text-align:left}.quito .tg-cats-holder{position:relative;display:block;font-size:13px;font-weight:normal;line-height:16px;text-align:center}.quito .tg-item-date{position:relative;display:inline;padding:0 6px 0 0;font-size:13px;font-weight:normal;line-height:16px}.quito .tg-cats-holder span{position:relative;display:inline-block;padding:8px 14px;text-transform:uppercase;font-size:11px;font-weight:600;letter-spacing:1px}.quito .light .tg-cats-holder span{background:rgba(0,0,0,0.2)}.quito .dark .tg-cats-holder span{background:rgba(175,175,175,0.08)}.quito .tg-item-title,.quito .tg-item-title a{position:relative;display:block;font-size:18px;line-height:22px;letter-spacing:1px;font-weight:600;margin:0 !important;padding:22px 20px 0 20px !important;text-align:center}.quito .tg-item-title a{padding:0 !important}.quito .tg-item-excerpt{margin:22px 0;padding:0;font-size:14px;line-height:28px;text-align:center}.quito .tg-item-read-more{position:relative;display:block;font-size:13px;line-height:18px;font-weight:600;padding:0;margin:0 auto;text-align:center}.quito .tg-item-read-more a{position:relative;display:inline-block;border:2px solid;padding:8px 20px;text-transform:uppercase;font-size:12px}.quito .tg-item-overlay{position:absolute;top:0;left:0;width:100%;height:100%}.quito .tg-media-button,.quito .tg-link-button{position:relative;display:inline-block;margin:10px;cursor:pointer;opacity:0;padding:6px 14px;font-size:13px;font-weight:600;line-height:22px;text-transform:uppercase}.quito .tg-media-button i,.quito .tg-link-button i{position:absolute;display:block;height:40px;line-height:40px;text-align:center;width:40px}.quito .dark .tg-media-button,.quito .dark .tg-link-button{background:rgba(255,255,255,0.5)}.quito .dark .tg-media-button:hover,.quito .dark .tg-link-button:hover{background:rgba(255,255,255,0.85)}.quito .light .tg-media-button,.quito .light .tg-link-button{background:rgba(0,0,0,0.15)}.quito .light .tg-media-button:hover,.quito .light .tg-link-button:hover{background:rgba(0,0,0,0.35)}.quito .tg-item-footer{position:relative;display:inline-block;width:100%;margin:26px 0 0 0;line-height:25px;text-align:center;font-style:italic}.quito .quote-format .tg-item-footer,.quito .link-format .tg-item-footer{margin:42px 0 0 0}.quito .tg-quote-icon,.quito .tg-link-icon{position:relative;display:block;font-size:24px;margin:0;top:-24px;font-size:80px;opacity:0.14;text-align:center}.quito .quote-format,.quito .link-format{overflow:hidden}.quito .tg-link-content,.quito .tg-quote-content{margin:0 !important}.quito .tg-link-content,.quito .tg-link-content a,.quito .tg-quote-content,.quito .tg-quote-content a{position:relative;display:inline-block;margin:0 !important;padding:0 !important;text-transform:none;font-size:18px;font-style:italic;line-height:24px;letter-spacing:0.5px;text-align:center}.quito .tg-quote-author,.quito .tg-link-url{position:relative;display:block;margin:8px 0 0 0;font-size:13px;font-style:italic;text-align:center}.quito .tg-item-comment{position:relative;display:inline-block;font-size:13px;padding:0 0 0 6px}.quito .tg-link-button,.quito .tg-media-button{-webkit-transform:scale3d(0.8,0.8,0.8);-moz-transform:scale3d(0.8,0.8,0.8);-ms-transform:scale3d(0.8,0.8,0.8);-o-transform:scale3d(0.8,0.8,0.8);transform:scale3d(0.8,0.8,0.8)}.quito .tg-item-overlay,.quito .tg-media-button,.quito .tg-link-button{opacity:0;-webkit-transition:background 0.15s ease-in-out,opacity 0.3s ease-in-out,-webkit-transform 0.5s cubic-bezier(.39,1.89,.55,1.45);-moz-transition:background 0.15s ease-in-out,opacity 0.3s ease-in-out,-moz-transform 0.5s cubic-bezier(.39,1.89,.55,1.45);-ms-transition:background 0.15s ease-in-out,opacity 0.3s ease-in-out,-ms-transform 0.5s cubic-bezier(.39,1.89,.55,1.45);-o-transition:background 0.15s ease-in-out,opacity 0.3s ease-in-out,-o-transform 0.5s cubic-bezier(.39,1.89,.55,1.45);transition:background 0.15s ease-in-out,opacity 0.3s ease-in-out,transform 0.5s cubic-bezier(.39,1.89,.55,1.45)}.quito .tg-item-media-holder:hover .tg-item-overlay,.quito .tg-item-media-holder:hover .tg-media-button,.quito .tg-item-media-holder:hover .tg-link-button{opacity:1;-webkit-transform:scale3d(1,1,1);-moz-transform:scale3d(1,1,1);-ms-transform:scale3d(1,1,1);-o-transform:scale3d(1,1,1);transform:scale3d(1,1,1)}.quito.tg-is-playing .tg-item-overlay,.quito.tg-force-play .tg-item-overlay{pointer-events:none;opacity:0 !important}.quito .mejs-overlay-play{display:none !important}.tg-item .dark h2,.tg-item .dark h2 a,.tg-item .dark h3,.tg-item .dark h3 a,.tg-item .dark a,.tg-item .dark a.tg-link-url,.tg-item .dark i,.tg-item .dark .tg-media-button{color:#773a74;fill:#773a74;stroke:#773a74}.tg-item .dark p{color:#ffffff;fill:#ffffff;stroke:#ffffff}.tg-item .dark span,.tg-item .dark .no-liked .to-heart-icon path,.tg-item .dark .empty-heart .to-heart-icon path{color:#99489e;fill:#99489e;stroke:#99489e}.tg-item .light h2,.tg-item .light h2 a,.tg-item .light h3,.tg-item .light h3 a,.tg-item .light a,.tg-item .light a.tg-link-url,.tg-item .light i,.tg-item .light .tg-media-button{color:#ffffff;fill:#ffffff;stroke:#ffffff}.tg-item .light p{color:#f6f6f6;fill:#f6f6f6;stroke:#f6f6f6}.tg-item .light span,.tg-item .light .no-liked .to-heart-icon path,.tg-item .light .empty-heart .to-heart-icon path{color:#f5f5f5;fill:#f5f5f5;stroke:#f5f5f5}#grid-831 .tg-item-content-holder{background-color:#ffffff}#grid-831 .tg-item-overlay{background-color:rgba(119,58,116,0.75)}.mejs-controls .mejs-time-rail .mejs-time-current,.mejs-controls .mejs-volume-slider .mejs-volume-current,.mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-current,.mejs-controls .mejs-sourcechooser-button .mejs-sourcechooser-selector ul li input:checked + label,.mejs-controls .mejs-sourcechooser-button .mejs-sourcechooser-selector ul li:hover label,.mejs-controls .mejs-sourcechooser-button .mejs-sourcechooser-selector ul li label.active{background-color:#2fbfc1 !important;color:#ffffff !important}</style><!-- The Grid Item Sizer --><div class="tg-grid-sizer" style="width: 491px;"></div><!-- The Grid Items Holder --><div class="tg-grid-holder tg-layout-masonry" style="left: -1px; position: relative; height: 2460.41px; width: 1529px;"><!-- The Grid item #1 --><article class="tg-item tg-post-814 f3 f8 f5 f10 f13 f18 quito" style="width: 491px; position: absolute; left: 0px; top: 0px;"><div class="tg-item-inner"><div class="tg-item-media-holder light"><img class="tg-item-image" alt="" width="2457" height="3685" src="http://grapevine.dev.b60apps.co.uk/wp-content/uploads/2016/08/159A0534-1.jpg"><div class="tg-item-overlay"></div><div class="tg-center-outer"><div class="tg-center-inner"><a class="tg-link-button" href="http://grapevine.dev.b60apps.co.uk/wine-growingthe-latest-craze-in-celeb-world/" target="_self">Read More</a></div></div></div><div class="tg-item-content-holder dark image-format"><div class="tg-cats-holder"><a class="category" href="http://grapevine.dev.b60apps.co.uk/category/articles/" rel="category tag"><span>articles</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/connoisseur/" rel="category tag"><span>connoisseur</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/rose/" rel="category tag"><span>rose</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/social/" rel="category tag"><span>social</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/travel/" rel="category tag"><span>travel</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/wine-school/" rel="category tag"><span>wine-school</span></a></div><h2 class="tg-item-title"><a href="http://grapevine.dev.b60apps.co.uk/wine-growingthe-latest-craze-in-celeb-world/" target="_self">What’s the new must have in celeb world…a winery of course!</a></h2><p class="tg-item-excerpt">There seems to be a celebrity craze at the moment with producing, making, and applying their name to vino. Johnny Depp, Drew Barrymore and Kurt Russell to name a few have all done it. Angelina ...</p><div class="tg-item-read-more"><a href="http://grapevine.dev.b60apps.co.uk/wine-growingthe-latest-craze-in-celeb-world/" target="_self">Read More</a></div><div class="tg-item-footer"><span class="tg-item-date">August 26, 2016</span><span>/</span><a class="tg-item-comment" data-comment="{&quot;no&quot;:&quot;No comment&quot;,&quot;one&quot;:&quot;comment&quot;,&quot;plus&quot;:&quot;Comments&quot;}" data-comment-id="814" href="http://grapevine.dev.b60apps.co.uk/wine-growingthe-latest-craze-in-celeb-world/#respond" style="color:#99489e">No comment</a></div></div></div></article><!-- The Grid item #2 --><article class="tg-item tg-post-783 f3 f19 quito" style="width: 491px; position: absolute; left: 519px; top: 0px;"><div class="tg-item-inner"><div class="tg-item-media-holder light"><img class="tg-item-image" alt="" width="1300" height="1734" src="http://grapevine.dev.b60apps.co.uk/wp-content/uploads/2016/08/IMG_0218-e1470226400980.jpg"><div class="tg-item-overlay"></div><div class="tg-center-outer"><div class="tg-center-inner"><a class="tg-link-button" href="http://grapevine.dev.b60apps.co.uk/champagne-fashion/" target="_self">Read More</a></div></div></div><div class="tg-item-content-holder dark image-format"><div class="tg-cats-holder"><a class="category" href="http://grapevine.dev.b60apps.co.uk/category/articles/" rel="category tag"><span>articles</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/fizz/" rel="category tag"><span>fizz</span></a></div><h2 class="tg-item-title"><a href="http://grapevine.dev.b60apps.co.uk/champagne-fashion/" target="_self">Champagne Fashion</a></h2><p class="tg-item-excerpt">Despite a tight budget for most households at this time of year, it’s worth buying decent Champagne. Just like clothes, you get what you pay for. The supermarket own-label fizz, on deal at&nbsp;a ...</p><div class="tg-item-read-more"><a href="http://grapevine.dev.b60apps.co.uk/champagne-fashion/" target="_self">Read More</a></div><div class="tg-item-footer"><span class="tg-item-date">August 3, 2016</span><span>/</span><a class="tg-item-comment" data-comment="{&quot;no&quot;:&quot;No comment&quot;,&quot;one&quot;:&quot;comment&quot;,&quot;plus&quot;:&quot;Comments&quot;}" data-comment-id="783" href="http://grapevine.dev.b60apps.co.uk/champagne-fashion/#respond" style="color:#99489e">No comment</a></div></div></div></article><!-- The Grid item #3 --><article class="tg-item tg-post-774 f3 f12 f13 f6 quito" style="width: 491px; position: absolute; left: 1038px; top: 666px;"><div class="tg-item-inner"><div class="tg-item-media-holder light"><img class="tg-item-image" alt="" width="500" height="334" src="http://grapevine.dev.b60apps.co.uk/wp-content/uploads/2016/06/shutterstock_337134848.jpg"><div class="tg-item-overlay"></div><div class="tg-center-outer"><div class="tg-center-inner"><a class="tg-link-button" href="http://grapevine.dev.b60apps.co.uk/wine-holidays-in-tuscany/" target="_self">Read More</a></div></div></div><div class="tg-item-content-holder dark image-format"><div class="tg-cats-holder"><a class="category" href="http://grapevine.dev.b60apps.co.uk/category/articles/" rel="category tag"><span>articles</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/red/" rel="category tag"><span>red</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/travel/" rel="category tag"><span>travel</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/white/" rel="category tag"><span>white</span></a></div><h2 class="tg-item-title"><a href="http://grapevine.dev.b60apps.co.uk/wine-holidays-in-tuscany/" target="_self">Wine Holidays in Tuscany</a></h2><p class="tg-item-excerpt">If you are looking for the perfect wine tourism holiday,&nbsp;Tuscany it is. Florence&nbsp;in&nbsp;Tuscany is a beautiful&nbsp;Renaissance city, on the river Arno, with historical monuments and museums along with ...</p><div class="tg-item-read-more"><a href="http://grapevine.dev.b60apps.co.uk/wine-holidays-in-tuscany/" target="_self">Read More</a></div><div class="tg-item-footer"><span class="tg-item-date">July 27, 2016</span><span>/</span><a class="tg-item-comment" data-comment="{&quot;no&quot;:&quot;No comment&quot;,&quot;one&quot;:&quot;comment&quot;,&quot;plus&quot;:&quot;Comments&quot;}" data-comment-id="774" href="http://grapevine.dev.b60apps.co.uk/wine-holidays-in-tuscany/#respond" style="color:#99489e">No comment</a></div></div></div></article><!-- The Grid item #4 --><article class="tg-item tg-post-764 f3 f5 f13 quito" style="width: 491px; position: absolute; left: 1038px; top: 0px;"><div class="tg-item-inner"><div class="tg-item-media-holder light"><img class="tg-item-image" alt="" width="800" height="533" src="http://grapevine.dev.b60apps.co.uk/wp-content/uploads/2016/07/IMG_3387.jpg"><div class="tg-item-overlay"></div><div class="tg-center-outer"><div class="tg-center-inner"><a class="tg-link-button" href="http://grapevine.dev.b60apps.co.uk/bordeaux-rose/" target="_self">Read More</a></div></div></div><div class="tg-item-content-holder dark image-format"><div class="tg-cats-holder"><a class="category" href="http://grapevine.dev.b60apps.co.uk/category/articles/" rel="category tag"><span>articles</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/rose/" rel="category tag"><span>rose</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/travel/" rel="category tag"><span>travel</span></a></div><h2 class="tg-item-title"><a href="http://grapevine.dev.b60apps.co.uk/bordeaux-rose/" target="_self">Bordeaux Rosé</a></h2><p class="tg-item-excerpt">One of the best known wine regions is Bordeaux. If it were a fashion label it might be Margaret Howell: masculine, restrained, aloof and timeless. However,&nbsp;despite its burly, expensive ...</p><div class="tg-item-read-more"><a href="http://grapevine.dev.b60apps.co.uk/bordeaux-rose/" target="_self">Read More</a></div><div class="tg-item-footer"><span class="tg-item-date">July 27, 2016</span><span>/</span><a class="tg-item-comment" data-comment="{&quot;no&quot;:&quot;No comment&quot;,&quot;one&quot;:&quot;comment&quot;,&quot;plus&quot;:&quot;Comments&quot;}" data-comment-id="764" href="http://grapevine.dev.b60apps.co.uk/bordeaux-rose/#comments" style="color:#99489e">1 comment</a></div></div></div></article><!-- The Grid item #5 --><article class="tg-item tg-post-523 f3 f8 f18 quito" style="width: 491px; position: absolute; left: 0px; top: 1129px;"><div class="tg-item-inner"><div class="tg-item-media-holder light"><img class="tg-item-image" alt="" width="500" height="331" src="http://grapevine.dev.b60apps.co.uk/wp-content/uploads/2016/06/shutterstock_283952948.jpg"><div class="tg-item-overlay"></div><div class="tg-center-outer"><div class="tg-center-inner"><a class="tg-link-button" href="http://grapevine.dev.b60apps.co.uk/sicilys-indigenous-grapes/" target="_self">Read More</a></div></div></div><div class="tg-item-content-holder dark image-format"><div class="tg-cats-holder"><a class="category" href="http://grapevine.dev.b60apps.co.uk/category/articles/" rel="category tag"><span>articles</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/connoisseur/" rel="category tag"><span>connoisseur</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/wine-school/" rel="category tag"><span>wine-school</span></a></div><h2 class="tg-item-title"><a href="http://grapevine.dev.b60apps.co.uk/sicilys-indigenous-grapes/" target="_self">Sicily’s Indigenous Grapes</a></h2><p class="tg-item-excerpt">With a rich history in wine making dating back 2500 years, a unique mineral rich terroir and a Mediterranean climate you can see why Sicily is the ideal growing environment for some fine wines. ...</p><div class="tg-item-read-more"><a href="http://grapevine.dev.b60apps.co.uk/sicilys-indigenous-grapes/" target="_self">Read More</a></div><div class="tg-item-footer"><span class="tg-item-date">July 20, 2016</span><span>/</span><a class="tg-item-comment" data-comment="{&quot;no&quot;:&quot;No comment&quot;,&quot;one&quot;:&quot;comment&quot;,&quot;plus&quot;:&quot;Comments&quot;}" data-comment-id="523" href="http://grapevine.dev.b60apps.co.uk/sicilys-indigenous-grapes/#respond" style="color:#99489e">No comment</a></div></div></div></article><!-- The Grid item #6 --><article class="tg-item tg-post-513 f3 f17 f14 quito" style="width: 491px; position: absolute; left: 519px; top: 993px;"><div class="tg-item-inner"><div class="tg-item-media-holder light"><img class="tg-item-image" alt="" width="500" height="334" src="http://grapevine.dev.b60apps.co.uk/wp-content/uploads/2016/06/shutterstock_334264130-1.jpg"><div class="tg-item-overlay"></div><div class="tg-center-outer"><div class="tg-center-inner"><a class="tg-link-button" href="http://grapevine.dev.b60apps.co.uk/a-match-made-in-heaven/" target="_self">Read More</a></div></div></div><div class="tg-item-content-holder dark image-format"><div class="tg-cats-holder"><a class="category" href="http://grapevine.dev.b60apps.co.uk/category/articles/" rel="category tag"><span>articles</span></a><a class="category" href="http://grapevine.dev.b60apps.co.uk/category/featured/" rel="category tag"><span>featured</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/food/" rel="category tag"><span>food</span></a></div><h2 class="tg-item-title"><a href="http://grapevine.dev.b60apps.co.uk/a-match-made-in-heaven/" target="_self">A match made in heaven!!</a></h2><p class="tg-item-excerpt">Pairing two of life’s greatest pleasures cheese &amp; wine is something to get excited about, well at Grapevine we think so!! Its not all that difficult to match successfully and when you ...</p><div class="tg-item-read-more"><a href="http://grapevine.dev.b60apps.co.uk/a-match-made-in-heaven/" target="_self">Read More</a></div><div class="tg-item-footer"><span class="tg-item-date">July 20, 2016</span><span>/</span><a class="tg-item-comment" data-comment="{&quot;no&quot;:&quot;No comment&quot;,&quot;one&quot;:&quot;comment&quot;,&quot;plus&quot;:&quot;Comments&quot;}" data-comment-id="513" href="http://grapevine.dev.b60apps.co.uk/a-match-made-in-heaven/#comments" style="color:#99489e">1 comment</a></div></div></div></article><!-- The Grid item #7 --><article class="tg-item tg-post-368 f3 f18 quito" style="width: 491px; position: absolute; left: 519px; top: 1660px;"><div class="tg-item-inner"><div class="tg-item-media-holder light"><img class="tg-item-image" alt="" width="500" height="334" src="http://grapevine.dev.b60apps.co.uk/wp-content/uploads/2016/06/shutterstock_258710444.jpg"><div class="tg-item-overlay"></div><div class="tg-center-outer"><div class="tg-center-inner"><a class="tg-link-button" href="http://grapevine.dev.b60apps.co.uk/natural-wine-so-whats-the-hype/" target="_self">Read More</a></div></div></div><div class="tg-item-content-holder dark image-format"><div class="tg-cats-holder"><a class="category" href="http://grapevine.dev.b60apps.co.uk/category/articles/" rel="category tag"><span>articles</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/wine-school/" rel="category tag"><span>wine-school</span></a></div><h2 class="tg-item-title"><a href="http://grapevine.dev.b60apps.co.uk/natural-wine-so-whats-the-hype/" target="_self">Natural Wine..what’s the hype?</a></h2><p class="tg-item-excerpt">We have all heard the hype around the words “Natural” &amp; “Organic” when it comes to food but what about wine? do you know what natural wine is or do you really care? well I thought it was an ...</p><div class="tg-item-read-more"><a href="http://grapevine.dev.b60apps.co.uk/natural-wine-so-whats-the-hype/" target="_self">Read More</a></div><div class="tg-item-footer"><span class="tg-item-date">July 15, 2016</span><span>/</span><a class="tg-item-comment" data-comment="{&quot;no&quot;:&quot;No comment&quot;,&quot;one&quot;:&quot;comment&quot;,&quot;plus&quot;:&quot;Comments&quot;}" data-comment-id="368" href="http://grapevine.dev.b60apps.co.uk/natural-wine-so-whats-the-hype/#respond" style="color:#99489e">No comment</a></div></div></div></article><!-- The Grid item #8 --><article class="tg-item tg-post-459 f3 f6 quito" style="width: 491px; position: absolute; left: 1038px; top: 1333px;"><div class="tg-item-inner"><div class="tg-item-media-holder light"><img class="tg-item-image" alt="" width="500" height="375" src="http://grapevine.dev.b60apps.co.uk/wp-content/uploads/2016/06/shutterstock_132244739.jpg"><div class="tg-item-overlay"></div><div class="tg-center-outer"><div class="tg-center-inner"><a class="tg-link-button" href="http://grapevine.dev.b60apps.co.uk/grape-of-the-week-viognier/" target="_self">Read More</a></div></div></div><div class="tg-item-content-holder dark image-format"><div class="tg-cats-holder"><a class="category" href="http://grapevine.dev.b60apps.co.uk/category/articles/" rel="category tag"><span>articles</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/white/" rel="category tag"><span>white</span></a></div><h2 class="tg-item-title"><a href="http://grapevine.dev.b60apps.co.uk/grape-of-the-week-viognier/" target="_self">Grape of the week – Viognier</a></h2><p class="tg-item-excerpt">Let’s start off with the pronunciation (Vee-on-e-yay) The reason we have chosen this great variety as ‘grape of the week’ is twofold. Firstly, it is not particularly easy to grow, and then ...</p><div class="tg-item-read-more"><a href="http://grapevine.dev.b60apps.co.uk/grape-of-the-week-viognier/" target="_self">Read More</a></div><div class="tg-item-footer"><span class="tg-item-date">July 15, 2016</span><span>/</span><a class="tg-item-comment" data-comment="{&quot;no&quot;:&quot;No comment&quot;,&quot;one&quot;:&quot;comment&quot;,&quot;plus&quot;:&quot;Comments&quot;}" data-comment-id="459" href="http://grapevine.dev.b60apps.co.uk/grape-of-the-week-viognier/#comments" style="color:#99489e">1 comment</a></div></div></div></article><!-- The Grid item #9 --><article class="tg-item tg-post-379 f3 f18 quito" style="width: 491px; position: absolute; left: 0px; top: 1793px;"><div class="tg-item-inner"><div class="tg-item-media-holder light"><img class="tg-item-image" alt="" width="500" height="334" src="http://grapevine.dev.b60apps.co.uk/wp-content/uploads/2016/06/shutterstock_270584228.jpg"><div class="tg-item-overlay"></div><div class="tg-center-outer"><div class="tg-center-inner"><a class="tg-link-button" href="http://grapevine.dev.b60apps.co.uk/what-does-vintage-really-mean/" target="_self">Read More</a></div></div></div><div class="tg-item-content-holder dark image-format"><div class="tg-cats-holder"><a class="category" href="http://grapevine.dev.b60apps.co.uk/category/articles/" rel="category tag"><span>articles</span></a><a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/wine-school/" rel="category tag"><span>wine-school</span></a></div><h2 class="tg-item-title"><a href="http://grapevine.dev.b60apps.co.uk/what-does-vintage-really-mean/" target="_self">What does vintage really mean?</a></h2><p class="tg-item-excerpt">I’m afraid Its not because its old and sporting an inch of dust!!&nbsp; A wines vintage is simply the year the grapes were picked and this year will be displayed on the bottle. It can vary but in ...</p><div class="tg-item-read-more"><a href="http://grapevine.dev.b60apps.co.uk/what-does-vintage-really-mean/" target="_self">Read More</a></div><div class="tg-item-footer"><span class="tg-item-date">July 7, 2016</span><span>/</span><a class="tg-item-comment" data-comment="{&quot;no&quot;:&quot;No comment&quot;,&quot;one&quot;:&quot;comment&quot;,&quot;plus&quot;:&quot;Comments&quot;}" data-comment-id="379" href="http://grapevine.dev.b60apps.co.uk/what-does-vintage-really-mean/#comments" style="color:#99489e">1 comment</a></div></div></div></article><!-- The Grid Ajax Scroll --><div class="tg-ajax-scroll-holder"><div class="tg-ajax-scroll" data-no-more="No more post">Loading...</div></div></div><!-- The Grid Area bottom1 --><div class="tg-grid-area-bottom1"><!-- The Grid Ajax Button --><div class="tg-ajax-button-holder"><div class="tg-ajax-button tg-nav-color tg-nav-border tg-nav-font" data-item-tt="21" data-button="Load More" data-loading="Loading..." data-no-more="No more post" data-remain="true"><span class="tg-nav-color">Load More (12)</span></div></div></div></div><!-- The Grid Wrapper End -->
      </div>
</div>
*/ ?>

<input type="hidden" id="userId" value="<?php echo $_SESSION['user_data']['id']; ?>" />

<?php // echo '<h3>Count of feed query: '.count( $response_data['data'] ).'</h3>'; ?>




    <div class="container-fluid">

        <div class="row slider-row center-width-row">
            <div class="col-md-8 feed-slider ">
                <div id="myCarousel" class="carousel slide " data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <?php
                            for($i=0; $i<$slideCount; $i++){
                                if($i > 0){    
                        ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $i ?>"></li>
                        <?php }}?>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        
                    <?php 
                        if(strlen($latest[0]['title']) > 45){
                            $latest[0]['title'] = substr($latest[0]['title'], 0, 45).'...';
                        }

                        if (!empty($latest) AND count($latest) > 0) {
                            
                            $first_post_duration = get_diff_between_dates($latest[0]['post_date']);
                            $first_post_days     = $first_post_duration['days'];
                            $first_post_hours    = $first_post_duration['hours'];
                            
                            $fp_id = $latest[0]['id'];
                            $fdate = '';
                            if($first_post_days > 0){
                                
                                $fdate = $first_post_days.' Days Ago';
                            }
                            else{
                                $fdate = $first_post_hours.' Hours Ago';
                            }
                    ?>
                        
                        <!--div class="item active" onclick="redirect('index.php/article?aid=<?php echo $fp_id ?>')" style="cursor: pointer"-->
                        <div class="item active" onclick="redirect('/<?php echo str_replace(" ", "-", $latest[0]['title']); ?>')" style="cursor: pointer">
                            <p class="slider-title"><?php echo $latest[0]['title'] ?></p>
                            <!--<div class="item-img-overlay-purple"></div>-->
                            <div class="item-img-overlay"></div>
                            <?php

                                if( has_post_thumbnail( $latest[0]['id'] ) ){
                
                                    $latestThumbnailId  = get_post_thumbnail_id( $latest[0]['id'] );
                                    $latestThumbnailUrl = wp_get_attachment_url( $latestThumbnailId );
                                                            
                                }

                            ?>
                            <img class="" src="<?php echo $latestThumbnailUrl; ?>">
                            <div id="clear"></div>
                            <a href="index.php/article?aid=<?php echo $latest[0]['id'] ?>">
                                <div class="col-bottom-bar">
                                    <div class="read-more" >Read More...</div>
                                    <div class="time-rem-holder" >
                                        <span class="time-icon">
                                            <img class="" src="<?php echo get_template_directory_uri() ?>/icons/time-icon-white.png"/>
                                        </span>
                                        <?php echo $fdate ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                        
                    <?php
                            $i = 0;
                            foreach ($latest as $latest_feeds) {
                                
                                if($i >0){

                                    $feed_title = $latest_feeds['title'];
                                    
                                    if(strlen($feed_title) > 45){
                                        
                                        $feed_title = substr($feed_title, 0, 45).'...';

                                    }

                                    $latestThumbnailUrl = '';

                                    if( has_post_thumbnail( $latest_feeds['id'] ) ){
                
                                        $latestThumbnailId  = get_post_thumbnail_id( $latest_feeds['id'] );
                                        $latestThumbnailUrl = wp_get_attachment_url( $latestThumbnailId );
                                                                
                                    }

                                    $latest_thum          = $latestThumbnailUrl ;
                                    $latest_slider_image  = $latestThumbnailUrl ;
                                    $summary              = $latest_feeds['summary'];
                                    $latest_post_date     = $latest_feeds['post_date'];
                                    $lid                  = $latest_feeds['id'];
                                    
                                    $latest_post_duration = get_diff_between_dates($latest_post_date);
                                    $latest_post_days     = $latest_post_duration['days'];
                                    $latest_post_hours    = $latest_post_duration['hours'];
                                    
                                    $latest_date          = '';

                                    if($latest_post_days > 0){
                                        
                                        $latest_date = $latest_post_days.' Days Ago';
                                    
                                    }
                                    else{

                                        $latest_date = $latest_post_hours.' Hours Ago';
                                    
                                    }
                    ?>
                        

                        <!--div class="item" onclick="redirect('index.php/article?aid=<?php echo $lid ?>')" style="cursor: pointer"-->
                        <div class="item" onclick="redirect('/<?php echo str_replace(" ", "-", strtolower($feed_title)); ?>')" style="cursor: pointer">
                            <p class="slider-title"><?php echo $feed_title ?></p>
                            <!--<div class="item-img-overlay-purple"></div>-->
                            <div class="item-img-overlay-mesh"></div>
                            <img class="" src="<?php echo $latest_slider_image; ?>">
                            <div id="clear"></div>
                            <div class="col-bottom-bar" onclick="redirect('/<?php echo str_replace(" ", "-", strtolower($feed_title)); ?>')">
                                <a href="index.php/article?aid=<?php echo $lid ?>">
                                <div class="read-more slider-read-more" >Read More...</div></a>
                                <div class="time-rem-holder"><span class="time-icon"><img class="" src="<?php echo get_template_directory_uri() ?>/icons/time-icon-white.png"/></span><?php echo $latest_date ?></div>
                            </div>
                        </div>
                    <?php 
                                } 
                            $i++; }
                        }
                    ?>
                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    
                    </div>
                    <!-- end of carousel inner -->
                </div>
                <!-- end of carousel -->
                <div id="clear"></div>
            </div>
            <!-- end of feed-slider -->
            <div class="col-md-4 feed-featured">

                <article class="tg-item tg-post-774 f3 f12 f13 f6 quito" style="width: 100%;">
                    <div class="tg-item-inner">
                        <div class="tg-item-media-holder light">
                            <img class="tg-item-image" alt="" width="500" height="334" src="<?php echo $featured_image; ?>">
                            <div class="tg-item-overlay"></div>
                            <div class="tg-center-outer">
                                <div class="tg-center-inner">
                                    <a class="tg-link-button" href="<?php echo get_post_permalink($featured_id); ?>" target="_self">Read More</a>
                                </div>
                            </div>
                        </div>
                    <div class="tg-item-content-holder dark image-format">
                        <div class="tg-cats-holder">

                            <?php

                                echo '<div style="display: none">tags<pre>';
                                print_r($featured_tags);
                                echo '</pre></div>';

                                foreach($featured_tags as $tagObj){

                                    if( $tagObj['taxonomy'] == 'post_tag'){

                                        echo '<a href="' . get_tag_link($tagObj['term_id']) . '"><span>' . $tagObj['name'] . '</span></a>';

                                    }else{

                                        // echo category link get_category_link( int|object $category )
                                        echo '<a href="' . get_category_link($tagObj['term_id'], $tagObj['taxonomy']) . '"><span>' . $tagObj['name'] . '</span></a>';

                                    }

                                }

                            ?>
<?php /*
                            <a class="category" href="http://grapevine.dev.b60apps.co.uk/category/articles/" rel="category tag"><span>articles</span></a>
                            <a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/red/" rel="category tag"><span>red</span></a>
                            <a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/travel/" rel="category tag"><span>travel</span></a>
                            <a class="post_tag" href="http://grapevine.dev.b60apps.co.uk/tag/white/" rel="category tag"><span>white</span></a>
*/ ?>
                        </div>
                        <h2 class="tg-item-title">
                            <a href="<?php echo get_post_permalink($featured_id); ?>" target="_self"><?php echo $featured_title; ?></a>
                        </h2>
                        <p class="tg-item-excerpt"><?php echo $featured_summary; ?></p>
                        <div class="tg-item-read-more">
                            <a href="<?php echo get_post_permalink($featured_id); ?>" target="_self">Read More</a>
                        </div>
                        <div class="tg-item-footer"></div>
                    </div>
                </article>
                        


<?php /*
                <a href="/<?php echo str_replace(" ", "-", strtolower($featured_title)); ?>">
                <div class="top-thumb-holder">
                    <div class="feed-title-text"><?php echo $featured_title ?></div>
                    <img class="img-responsive" src="<?php echo $featured_image ?>">
                </div>
                
                <div id="clear"></div>
                
                <div class="feed-info-holder top-feed-info">
                    <div class="speech-box">
                        <img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/icons/web-speech.png">
                    </div>
                     <div class="speech-text">
                        <?php echo $featured_summary ?>  
                     </div>
                </div>

                <div id="clear"></div>
                
                
                 <div class="col-bottom-bar">
                    <div class="read-more" >Read More...</div>
                    <div class="time-rem-holder" ><span class="time-icon"><img class="" src="<?php echo get_template_directory_uri() ?>/icons/time-icon-white.png"/></span><?php echo $featured_date ?></div>
                </div>
                </a>
*/ ?>
            </div>
            <!-- end of featured -->
        </div>
        <!-- end of row-slider -->
        
        <div id="clear"></div>

        <?php /*
        <div class="row feed-rows center-width-row">
        <?php 
            if(!empty($response) AND count($response) > 0){
                
                //if(!$response['featured_articles']){
                foreach ($response as $data_feeds) {
                    // print_r($data_feeds);
                    if(!$data_feeds[0]){

                        $feed_title = $data_feeds['title'];
                        if(strlen($feed_title) > 45){
                            $feed_title = substr($feed_title, 0, 45).'...';
                        }

                        $thum = $data_feeds['featured_image'];
                        $summary = $data_feeds['summary'];

                        if(strlen($summary) > 45){
                            $summary = substr($summary, 0, 145).'...';
                        }

                        $post_date = $data_feeds['post_date'];
                        $id = $data_feeds['id'];

                        $current_date = date("Y-m-d H:i:s");
                        $cdate_object = strtotime($current_date);
                        $end_object = strtotime($post_date);
                        $dif = $cdate_object - $end_object ;
                        $days = floor($dif / (60 * 60 * 24)); 
                        $hours = round($dif / (60 * 60));
                        
            
                        $article_date = '';
                        if($days > 0){
                            
                            $article_date = $days.' Days Ago';
                        }
                        else{
                            $article_date = $hours.' Hours Ago';
                        }
        ?>
            <div class="col-md-4 feed-column">
                <a href="index.php/article?aid=<?php echo $id ?>">
                    <div class="col-top-info">
                        <div class="feed-info-holder">
                            <p class="event-title-text"><?php echo $feed_title ?></p>
                            <div class="speech-box"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/icons/web-speech.png"></div>
                            <div class="speech-text"><?php echo $summary ?></div>
                        </div>
                        <div id="clear"></div>
                        <div class="thumb-holder"><img class="img-responsive" src="<?php echo $thum ?>"/></div>
                    </div>
                    <div class="col-bottom-bar">
                        <div class="read-more">Read More...</div>
                        <div class="time-rem-holder" ><span class="time-icon"><img class="" src="<?php echo get_template_directory_uri() ?>/icons/time-icon-white.png"/></span><?php echo $article_date ?></div>
                    </div>
                </a>
            </div>
            <!-- end of feed column -->

        <?php
                    }
                }
            }
                   
        ?>
             
        </div>
        <!-- end fo feed rows -->
        */ ?>
       
    </div><!-- /.container-fluid -->

    <div class="container-fluid">

    <div class="row slider-row center-width-row">
      
        <div class="col-lg-12 grid-holder">

            <?php echo do_shortcode('[the_grid name="Quito"]'); ?>
        
        </div><!-- /.col-lg-12 .grid-holder -->

    </div><!-- /.row -->

</div><!-- /.container-fluid -->

</section>
<script>
     $(document).ready(function(){
        /*$('.col-md-2').click(
            function(e)
            {*/
               // alert();
                $('.nav-event-holder').removeClass('active');
                $(".newsfeed-holder").addClass('active');
            /*}
        );*/
         });              
    function redirect(page){
        var siteurl = "<?php echo get_site_url() ?>";
        
        window.location = siteurl+"/"+page;
    }

</script>
<?php
get_footer();


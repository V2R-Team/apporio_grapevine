<?php
/*
  Template Name: News Feeds Template
 */
$user_data = '';
$response = '';
if (isset($_SESSION['user'])) {
    $user_data = $_SESSION['user_data'];

    $user_id = $user_data['id'];
    $token = $user_data['token'];

    $data = array('method' => 'get_feeds', 'user_id' => $user_id, 'token' => $token, 'offset' => '', 'origin' => 'web');


    $json = json_encode($data);

    $request = get_data(get_site_url() . '/API/serve-api.php', array('json' => $json));
    
    $response_data = json_decode($request, true);

    $url = get_site_url() . '/API/serve-api.php?json=' . $json;
  
    if($response_data['error_codes'] == -1000)
    {
        unset($_SESSION['user']);
        unset($_SESSION['user_data']);
     // wp_redirect(get_site_url() . "/register");

    exit;
    }
    $response = $response_data['data'];

    $slideCount = 5;
    if(count($response) < $slideCount){
        $slideCount = count($response);
    }

    $latest = '';
    if(count($response) > 0){
        $latest = array_slice($response, 0, $slideCount, true);
    }else{
        $latest = array();
    }
    $featured = array_shift($response_data['featured_articles']);

    if( has_post_thumbnail( $featured['id'] ) ){
                
        $featuredThumbnailId  = get_post_thumbnail_id( $featured['id'] );
        $featuredThumbnailUrl = wp_get_attachment_url( $featuredThumbnailId );
                                
    }

    $featured_image   = $featuredThumbnailUrl;
    $slider_image     = $featuredThumbnailUrl;
    $featured_summary = $featured['summary'];
    if(strlen($featured_summary) > 45){
        $featured_summary = substr($featured_summary, 0, 100).'...';
    }
    $featured_title = $featured['title'];
    $featured_id    = $featured['id'];
    
    $current_date   = date("Y-m-d H:i:s");
    $featured_date  = $featured['post_date'];
    $fcdate_object  = strtotime($current_date);
    $fend_object    = strtotime($featured_date);
    $fdif           = $fcdate_object - $fend_object;
    $featured_tags  = $featured['tags'];
    
    $fdays          = floor($fdif / (60 * 60 * 24)); //seconds/minut
    $fhours         = round($fdif / (60 * 60));
    if($fdays > 0){
                                
        $featured_date = $fdays.' Days Ago';
    }
    else{
        $featured_date = $fhours.' Hours Ago';
    }

} else {
   
}

//print_r($response);

get_header();
?>
<section>

<input type="hidden" id="userId" value="<?php echo $_SESSION['user_data']['id']; ?>" />

    <div class="container-fluid">

        <div class="row slider-row center-width-row">
            <div class="col-md-8 feed-slider ">
                <div id="myCarousel" class="carousel slide " data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <?php
                         $arrr = array(
                            'post_type'=>'post',
                            'post_status'=>'publish',
                            'posts_per_page'=>5
                         );
                         $query = new WP_Query($arrr);
                         $countd = 0 ;
                         while ($query->have_posts()) { $query->the_post();  $url_fea = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
                         $countd++ ;
                        ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $countd;?>"></li>
                        <?php } wp_reset_query();  ?>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php
                         $ar_fea = array(
                            'post_type'=>'post',
                            'post_status'=>'publish',
                            'posts_per_page'=>5
                         );
                         $ar1_fea = new WP_Query($ar_fea);
                         $count = 0 ;
                         while ($ar1_fea->have_posts()) { $ar1_fea->the_post();  $url_fea = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
                         $count++;
                          if ($count==1) {
                              $class = 'active';
                          }else
                          {
                            $class = '';
                          }
                        ?>

                     
                        <div class="item <?php echo $class;?>">
                            <p class="slider-title"><?php the_title();?></p>
                            <!--<div class="item-img-overlay-purple"></div>-->
                            <div class="item-img-overlay-mesh"></div>
                            <?php if ($url_fea!="") {?>
                                <img class="" src="<?php echo $url_fea ?>">    
                            <?php }else{?>
                                <img class="" src="<?php the_field('top_image'); ?>">    
                            <?php }?>
                            
                            <div id="clear"></div>
                            <div class="col-bottom-bar">
                                <a href="#">
                                <div class="read-more slider-read-more" >Read More...</div></a>
                                <div class="time-rem-holder"><span class="time-icon"><img class="" src="<?php echo get_template_directory_uri() ?>/icons/time-icon-white.png"/></span><?php echo get_the_date(); ?></div>
                            </div>
                        </div>
                        <?php } wp_reset_query();  ?>
                        
                   
                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    
                    </div>
                    <!-- end of carousel inner -->
                </div>
                <!-- end of carousel -->
                <div id="clear"></div>
            </div>



            <!-- end of feed-slider -->
            <div class="col-md-4 feed-featured">

              <?php
                  $farrr = array(
                            'post_type'=>'post',
                            'post_status'=>'publish',
                            'posts_per_page'=>1,
                            'tax_query'=>array(
                                  array('taxonomy' =>'category' ,
                                    'terms'=>17
                                   )

                                )
                         );
                         $fquery = new WP_Query($farrr);
                        

                       while ( $fquery->have_posts()) {
                             $fquery->the_post();
                             $url_fea1 = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
                         
              ?>
                <article class="tg-item tg-post-774 f3 f12 f13 f6 quito" style="width: 100%;">
                    <div class="tg-item-inner">
                        <div class="tg-item-media-holder light">
                            <img class="tg-item-image" alt="" width="500" height="334" src="<?php echo $url_fea1; ?>">
                            <div class="tg-item-overlay"></div>
                            <div class="tg-center-outer">
                                <div class="tg-center-inner">
                                    <a class="tg-link-button" href="<?php echo the_permalink(); ?>" target="_self">Read More</a>
                                </div>
                            </div>
                        </div>
                    <div class="tg-item-content-holder dark image-format">
                        <div class="tg-cats-holder">

                            <?php
                              $categories= get_the_category($post->ID );
                             // echo "<pre>";
                               // print_r($categories);
                               // echo "</pre>";
                                echo '<div style="display: none">tags<pre>';
                                print_r($featured_tags);
                                echo '</pre></div>';

                                foreach($categories as $tagObj){
                               echo '<a href="' . get_category_link($tagObj->term_id, $tagObj->taxonomy) . '"><span>' . $tagObj->name . '</span></a>';
                                    // if( $tagObj['taxonomy'] == 'post_tag'){

                                    //     echo '<a href="' . get_tag_link($tagObj['term_id']) . '"><span>' . $tagObj['name'] . '</span></a>';

                                    // }else{

                                    //     // echo category link get_category_link( int|object $category )
                                    //     echo '<a href="' . get_category_link($tagObj['term_id'], $tagObj['taxonomy']) . '"><span>' . $tagObj['name'] . '</span></a>';

                                    // }

                                }
                                $cataad=get_the_tags( $post->ID);
                              foreach ($cataad as $tagObj) {
                                 echo '<a href="' . get_tag_link($tagObj->term_id) . '"><span>' . $tagObj->name . '</span></a>';
                              }

                            ?>

                        </div>
                        <h2 class="tg-item-title">
                            <a href="<?php echo the_permalink(); ?>" target="_self"><?php  the_title(); ?></a>
                        </h2>
                        <p class="tg-item-excerpt"><?php echo substr(get_the_excerpt(),0,100); ?></p>
                        <div class="tg-item-read-more">
                            <a href="<?php echo the_permalink(); ?>" target="_self">Read More</a>
                        </div>
                        <div class="tg-item-footer"></div>
                    </div>
                </article>
                       <?php } ?> 



            </div>
            <!-- end of featured -->
        </div>
        <!-- end of row-slider -->
        
        <div id="clear"></div>

        
       
    </div><!-- /.container-fluid -->

    <div class="container-fluid">

    <div class="row slider-row center-width-row">
      
        <div class="col-lg-12 grid-holder">

            <?php echo do_shortcode('[the_grid name="Quito"]'); ?>
        
        </div>

    </div>

</div>
</section>
<script>
     $(document).ready(function(){
        /*$('.col-md-2').click(
            function(e)
            {*/
               // alert();
                $('.nav-event-holder').removeClass('active');
                $(".newsfeed-holder").addClass('active');
            /*}
        );*/
         });              
    function redirect(page){
        var siteurl = "<?php echo get_site_url() ?>";
        
        window.location = siteurl+"/"+page;
    }

</script>
<?php
get_footer();


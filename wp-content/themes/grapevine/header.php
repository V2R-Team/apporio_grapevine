<?php
session_start();

$account_page = 'register';
//$account_text = 'Sign In / Register';
$account_text = 'Sign In';
 if(isset($_SESSION['user'])){ 
     
     $account_page = 'account';
     $account_text = 'My Account';
 }
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <title> The Grapevine Social </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width">

        <!-- Link to store or app -->
        <!--<meta name="apple-itunes-app" content="app-id=585027354, app-argument=grapevine://">-->


        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">       
       
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/bootstrap/js/bootstrap.min.js"></script>
        
        <script src="<?php echo get_template_directory_uri() ?>/js/jscript.js"></script>

        <script>

        
        //auto click the smart banner to redirect
        //$('div#branch-banner a#branch-mobile-action').trigger('click');
        </script>

        <script>
        if(navigator.userAgent.match(/Mobile/i) && !navigator.userAgent.match(/iPad/i)){
            (function(b,r,a,n,c,h,_,s,d,k){if(!b[n]||!b[n]._q){for(;s<_.length;)c(h,_[s++]);d=r.createElement(a);d.async=1;d.src="https://cdn.branch.io/branch-latest.min.js";k=r.getElementsByTagName(a)[0];k.parentNode.insertBefore(d,k);b[n]=h}})(window,document,"script","branch",function(b,r){b[r]=function(){b._q.push([r,arguments])}},{_q:[],_v:1},"addListener applyCode banner closeBanner creditHistory credits data deepview deepviewCta first getCode init link logout redeem referrals removeListener sendSMS setIdentity track validateCode".split(" "), 0);
            branch.init('key_live_onm9Sj4DzC5KiCLPRoXwHfekwxfEAcA5');

            var icon = '<?php echo get_template_directory_uri() ?>/icons/grapevine-icon.png';
            var smartBanner = false;
            var isHome = '<?php echo is_front_page(); ?>';
            branch.addListener('didShowBanner', function(){
                if(isHome == '1' && $(window).width() < 600){
                    console.log('Attempting to open app');
                    branch.deepviewCta();
                }
                smartBanner = true;
            });

            branch.banner({
                icon: icon,
                title: 'Grapevine',
                description: 'The Wine Lovers App',
                openAppButtonText: 'Open App',              // Text to show on button if the user has the app installed
                downloadAppButtonText: 'Download',
                showiOS: true,                          // Should the banner be shown on iOS devices?
                showAndroid: true,                      // Should the banner be shown on Android devices?
                showDesktop: false,
                iframe: true,
                disableHide: true, 
                mobileSticky: false,
            }, {});
            // if will show banner (mobile only) will attempt to open banner
            branch.deepview(
                {},
                {
                  'open_app': true  // If true, Branch attempts to open your app immediately when the page loads. If false, users will need to press a button. Defaults to true
                }
            );       
        }
        </script>
        
        <script>
            $(document).ready(function(){
                $('.col-md-2').click(
                    function(e)
                    {
                       // alert();
                        $('.col-md-2').removeClass('active');
                      //  $(this).addClass('active');
                    }
                );
            });

            </script>
        <?php wp_head(); ?>

    </head>

    <body  <?php body_class(); ?> >
   
    <div id="mobile-landing-image" style="display:none;"><img src="<?php echo get_template_directory_uri() ?>/images/mobileLandingImage.jpg"></div>

        <header class="site-header">
            <div class="container-fluid header-container" style="padding: 0">
                <div class="row header-row">
                    <div class="header-box logo-box">
                        <a href="<?php echo site_url(); ?>">
                            <div class="header-sub-box">
                            <div class="logoclass">
                                <p class="text-center">The</p></div>
                                <p class="logo-title">GRAPEVINE</p>
                                <p class="logo-tagline text-center">Social </p>
                            </div>
                        </a>
                    </div>
                    <div class="header-box newsfeed">
                        <?php //if(isset($_SESSION['user'])){ ?>
                            <a href="index.php/feeds">
                                <div class="header-sub-box">
                                    <div class="header-icon"></div>
                                    <p>Newsfeed</p>
                                </div>
                            </a>
                        <?php //}?>
                    </div>
                    <div class="header-box events">
                        <?php // if(isset($_SESSION['user'])){ ?>
                            <a href="index.php/events">
                                <div class="header-sub-box">
                                    <div class="header-icon"></div>
                                    <p>Events </p>
                                </div>
                            </a>
                        <?php //}?>
                    </div>
                    <div class="header-box account">
                        <a href="index.php/<?php echo $account_page ?>">
                            <div class="header-sub-box">
                                <div class="header-icon"></div>
                                <p><?php echo $account_text ?></p>
                            </div>
                        </a>
                    </div>
                    <div class="header-box menu">
                        <div class="hangdown" style="display:none;">
                            <a href="/get-the-app">
                                <div class="header-sub-box download">
                                    <p>DOWNLOAD<span> THE APP</span></p>
                                </div>
                            </a>
                            <a href="https://www.instagram.com/thegrapevinesocial/" target="_blank">
                                <div class="header-sub-box social insta">
                                    <div class="header-icon"></div>
                                    <p>Instagram</p>
                                </div>
                            </a>
                            <a href="https://twitter.com/Grapevine_App_" target="_blank">
                                <div class="header-sub-box social twitter">
                                    <div class="header-icon"></div>
                                    <p>Twitter</p>
                                </div>
                            </a>
                            <a href="https://www.facebook.com/Grapevine-The-Wine-Lovers-App-483960915120128/" target="_blank">
                                <div class="header-sub-box social facebook">
                                    <div class="header-icon"></div>
                                    <p>Facebook</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
                
            <div id="clear"></div>
            

<?php

/*
  Template Name: Control Template
 */

$action = $_POST['action'];

if ($action == "logout") {
    session_start();
    unset($_SESSION['user']);
    unset($_SESSION['page']);
    unset($_SESSION['user_data']);
    unset($_SESSION['admin']);
    wp_redirect(get_site_url());
    exit;
} else if ($action == "register-user") {
    session_start();
    unset($_SESSION['register_data']);
    
    $lat = $_POST['lat'];
    $lng = $_POST['lng'];
    $username = $_POST['username'];
    $fname = $_POST['first_name'];
    $last = $_POST['last_name'];
    $gender = $_POST['gender'];
    $email = $_POST['email'];
    $pass1 = $_POST['user_pass'];
    $pass2 = $_POST['user_pass2'];
    $region = $_POST['region'];
    $phone = $_POST['tel_number'];
    $rose = $_POST['rose'];
    $red = $_POST['red'];
    $food = $_POST['food'];
    $fortified = $_POST['fortified'];
    $dessert = $_POST['dessert'];
    $white = $_POST['white'];
    $travel = $_POST['travel'];
    $fizz = $_POST['fizz'];
    $conn = $_POST['connoisseur'];
    $social = $_POST['social'];
    $gallery = $_POST['gallery'];
    $school = $_POST['wine-school'];

    $show_in_current_location = '';
    if (isset($register_data['optradio'])) {
        $show_in_current_location = $register_data['optradio'];
    }

    //store certain information in the registerdata session
    //store everything posted in a common object     
    $register_data;
    $register_data->username = $username;
    $register_data->fname = $fname;
    $register_data->last = $last;
    $register_data->gender = $gender;
    $register_data->email = $email;
    $register_data->region = $region;
    $register_data->phone = $phone;
    $register_data->rose = $rose;
    $register_data->red = $red;
    $register_data->food = $food;
    $register_data->fortified = $fortified;
    $register_data->dessert = $dessert;
    $register_data->white = $white;
    $register_data->travel = $travel;
    $register_data->fizz = $fizz;
    $register_data->conn = $conn;
    $register_data->social = $social;
    $register_data->gallery = $gallery;
    $register_data->school = $school;

    //store a temporary session for registration data
    $_SESSION['register_data'] = $register_data;

    //check if the username already exists
    $post_data = array(
        'method' => 'validate_username',
        'username' => $username,
    );

    $json = json_encode($post_data);

    $request = get_data(get_site_url() . '/API/serve-api.php', array('json' => $json));

    $response = json_decode($request, true);
    //print_r($response);
    //echo $response['error_codes'];
    //exit;
    if ($response['error_codes'] == 0) {

        $interests = array($white,$red,$rose,$fizz,$conn,$gallery,$school,$social, $travel, $dessert, $fortified, $food);
       
        // $interest_data = array('white'=>$white, 'red'=>$red, 'rose'=>$rose, 'fizz'=>$fizz, 'connaiseur'=>$conn, 'gallery'=>$gallery, 'wine-school'=>$school, 'social'=>$social);
        $new_interest = array();
        
        $location = array('lat' => $lat, 'lng' => $lng);

        foreach ($interests as $key => $value) {

                $cleanvalue = trim($value);
                    if ($cleanvalue == "" OR $cleanvalue == null) {
                        unset($interests[$key]);
                    }
                }
        
        $user_interests = array_values($interests);
         
        // print_r($user_interests);
        $user_preferences = array('interests' => $user_interests);

        $post_data = array(
            'method' => 'register_user',
            'origin' => 'web',
            'first_name' => $fname,
            'last_name' => $last,
            'username' => $username,
            'email' => $email,
            'region' => $region,
            'user_pass' => sha1($pass1),
            'tel_number' => $phone,
            'gender' => $gender,
            'current_location' => $location,
            'interests' => $user_preferences,
            'location' => $show_in_current_location
        );

        $json = json_encode($post_data);

       $request = get_data(get_site_url() . '/API/serve-api.php', array('json' => $json));

        $response = json_decode($request, true);
        //print_r($response);
        //echo $response['error_codes'];
        if ($response['error_codes'] == 0) {
            
            unset($_SESSION['register_data']);
            unset($_SESSION['user']);
            unset($_SESSION['admin']);
            unset($_SESSION['user_data']);
            $_SESSION['user_data'] = $response;
            
            $_SESSION['user'] = 'user';
            
           // print_r($_SESSION);
           wp_redirect(get_site_url() . "/feeds");

        } else if ($response['error_codes'] == -1) {
           wp_redirect(get_site_url() . "/register?rgs=-1");

        }
    } else if ($response['error_codes'] == -1){
        wp_redirect(get_site_url() . "/register?rgs=-8");
    }
} else if ($action == "update-user-data") {

    //print_r($_POST);
    $show_in_current_location = '';
    if (isset($_POST['optradio'])) {
        $show_in_current_location = $_POST['optradio'];
    }

    $lat = $_POST['lat'];
    $lng = $_POST['lng'];
    $user_id = $_POST['user_id'];
    $fname = $_POST['first_name'];
    $last = $_POST['last_name'];
    $email = $_POST['email'];
    $username = $_POST['username'];
    $token  = $_POST['token'];
    $region = $_POST['region'];
    $gender = $_POST['gender'];
    $phone = $_POST['tel_number'];
    $rose = $_POST['rose'];
    $red = $_POST['red'];
    $food = $_POST['food'];
    $fortified = $_POST['fortified'];
    $dessert = $_POST['dessert'];
   
    $travel = $_POST['travel'];
    $white = $_POST['white'];
    $fizz = $_POST['fizz'];
    $conn = $_POST['connoisseur'];
    $social = $_POST['social'];
    $gallery = $_POST['gallery'];
    $school = $_POST['wine-school'];

   $interests = array($white, $red, $rose, $fizz, $conn, $gallery, $school, $social, $travel, $dessert, $fortified, $food);
    // $interest_data = array('white'=>$white, 'red'=>$red, 'rose'=>$rose, 'fizz'=>$fizz, 'connaiseur'=>$conn, 'gallery'=>$gallery, 'wine-school'=>$school, 'social'=>$social);
    $location = array('lat' => $lat, 'lng' => $lng);

   foreach ($interests as $key => $value) {

            $cleanvalue = trim($value);
                if ($cleanvalue == "" OR $cleanvalue == null) {
                    unset($interests[$key]);
                }
            }
    
     $user_interests = array_values($interests);
     
     //print_r($user_interests);
    $user_preferences = array('interests' => $user_interests);

    $post_data = array(
        'method' => 'update_user_details',
        'origin' => 'web',
        'first_name' => $fname,
        'token' => $token,
        'last_name' => $last,
        'email' => $email,
        'region' => $region,
        'user_id' => $user_id,
        'tel_number' => $phone,
        'gender' => $gender,
       // 'username' => $username,
        'interests' => $user_preferences,
        'current_location' => $location,
        'location' => $show_in_current_location
    );

   //print_r($post_data);

    $json = json_encode($post_data);

   $request = get_data(get_site_url() . '/API/serve-api.php', array('json' => $json));
    // print_r($request);
    $response = json_decode($request, true);

    if ($response['error_codes'] == 0) {

      wp_redirect(get_site_url() . "/account");

        exit;
    } else if ($response['error_codes'] == -1) {
       wp_redirect(get_site_url() . "/account?rs=-2");

        exit;
    }else if ($response['error_codes'] == -9999) {
       wp_redirect(get_site_url() . "/account?rs=-4");

        exit;
    }
    else{
        wp_redirect(get_site_url() . "/account");


        exit;
    }
} else if ($action == "facebook-login") {
  // print_r($_POST) ; 
 session_start();
    $lat = $_POST['lat'];
    $lng = $_POST['lng'];
    $fname = $_POST['first_name'];
    $last = $_POST['last_name'];
    $email = $_POST['email'];
    $fb_id = $_POST['fb_id'];
    
   

// print_r($_POST);
    $location = array('lat' => $lat, 'lng' => $lng);

    /*$check_data = array('method' => 'get_facebook_id', 'email' => $email);
    $checkjson = json_encode($check_data);
    $check = get_data(get_site_url() . '/API/serve-api.php', array('json' => $checkjson));

    $check_response = json_decode($check, true);*/

    /*
     * Check if the user exists
     */

    $user_check_data = array('method' => 'get_user_data', 'email' => $email, 'user_id' => '', 'origin' => 'web');
    $user_checkjson = json_encode($user_check_data);
    $user_check = get_data(get_site_url() . '/API/serve-api.php', array('json' => $user_checkjson));

    $user_check_response_data = json_decode($user_check, true);
    $user_check_response = $user_check_response_data['data'];
   // print_r($user_check_response_data);
    
    
    if ($user_check_response_data['error_codes'] == -1003 OR $user_check_response_data['error_codes'] == -1000) {
        /*
         * Register the user and them log them in
         */
        $post_data = array(
            'method' => 'register_user',
            'origin' => 'web',
            'first_name' => $fname,
            'last_name' => $last,
            'email' => $email,
            'fb_id' => $fb_id,
            'current_location' => $location
        );
        // print_r($post_data);
        $register_json = json_encode($post_data);

      $register_user = get_data(get_site_url() . '/API/serve-api.php', array('json' => $register_json));
       $reg_response = json_decode($register_user, true);
       // print_r($register_user);
       // print_r($reg_response);
        if ($reg_response['error_codes'] == 0) {
            unset($_SESSION['user']);
            unset($_SESSION['user_data']);
            $_SESSION['user'] = 'user';
            $_SESSION['user_data'] = $reg_response;
                    /* array(
                'first_name' => $fname,
                'last_name' => $last,
                'email' => $email,
                'fb_id' => $fb_id,
                'id'=> $reg_response['id'],
                'token' =>$reg_response['token'],
                'current_location' => $location
            );*/
            wp_redirect(get_site_url() . "/index.php/feeds");

            exit;
        }
    } else if ($user_check_response['error_codes'] == 0) {

        if (empty($user_check_response[0]['facebook_id'])) {

            $update_data = array(
                'method' => 'update_facebook_id',
                'email' => $email,
                'fb_id' => $fb_id
            );

            // print_r($post_data);

            $update_json = json_encode($update_data);

           $update_request = get_data(get_site_url() . '/API/serve-api.php', array('json' => $update_json));
            // print_r($request);
            $update_response = json_decode($update_request, true);
        }
            /*
             * Log the user in using the facebook credentials
             */
            $data = array('method' => 'user_login', 'email' => $email, 'fb_id' => $fb_id);
            $loginjson = json_encode($data);
           $login = get_data(get_site_url() . '/API/serve-api.php', array('json' => $loginjson));

            $response = json_decode($login, true);
            
            if ($response['error_codes'] == 0) {
                unset($_SESSION['user']);
                unset($_SESSION['user_data']);
                $_SESSION['user'] = 'user';
                $_SESSION['user_data'] = $response['user_data'][0];
                wp_redirect(get_site_url() . "/feeds");

                exit;
            } else if ($response['error_codes'] == -1) {
                wp_redirect(get_site_url() . "/register?rgs=-3");

                exit;
            }
        
    }
} else if ($action == "reset-password") {
  
    $email = $_POST['reset-email'];
     $user_id = $_POST['user_id'];
    $pass = $_POST['user_pass'];
    $token = $_POST['token'];
    $enc_pass = sha1($pass);
    $data = array('method' => 'reset_password', 'reset-email' => $email, 'user_pass' => $enc_pass, 'user_id' => $user_id, "token" =>$token, 'origin' => 'web');
            $json = json_encode($data);
       $reset = get_data(get_site_url() . '/API/serve-api.php', array('json' => $json));

            $response = json_decode($reset, true);
            
             echo $response['error_codes'];
} else if ($action == "add-comment") {
    
     $email           = $_POST['email'];
     $post_id         = $_POST['post_id'];
     $comment         = $_POST['comment_content'];
     $user_id         = $_POST['user_id'];
     $token           = $_POST['token'];
     $author          = $_POST['author'];
     $author_username = $_POST['author_username'];
     $subject         = $_POST['subject'];
     
     $data            = array('method' => 'add_comment', 'email' => $email, 'post_id' => $post_id, 'comment_content' => $comment, 'author' => $author,'author_username'=> $author_username, 'subject' => $subject, 'user_id' => $user_id, 'token' => $token, 'origin' => 'web');
     $json            = json_encode($data);
     $add_comment     = get_data(get_site_url() . '/API/serve-api.php', array('json' => $json));
     
     $response        = json_decode($add_comment, true);

     $permalink = get_permalink($post_id);
     
    //wp_redirect(get_site_url() . "/article?aid=$post_id");

    wp_redirect( $permalink );
    
    exit();
    
}else if ($action == "forgot-password") {
    $email = $_POST['email'];
   // print_r($email);
    $data = array('method' => 'forgot_password', 'email' => $email);
            $json = json_encode($data);
            print_r($json);
           $reset = get_data(get_site_url() . '/API/serve-api.php', array('json' => $json));
           
           echo get_site_url();
           print_r($reset);
            $response = json_decode($reset, true);
            
            if($response['error_codes'] == 0){
                
                wp_redirect(get_site_url() . "/register?rgs=2");
    
                exit();
            }else{
                wp_redirect(get_site_url() . "/register?rgs=-2");
    
                exit(); 
            }
}else if ($action == "reset-forgot-password") {
    print_r($_POST);
    $email = $_POST['email'];
    $pass = $_POST['user_pass'];
    $vt = $_POST['vt'];
    $enc_pass = sha1($pass);
    $data = array('method' => 'reset_forgot_password', 'email' => $email, 'user_pass' => $enc_pass, 'vt' => $vt);
    
    //print_r($data);
            $json = json_encode($data);
        echo $reset = get_data(get_site_url() . '/API/serve-api.php', array('json' => $json));
         
            $response = json_decode($reset, true);
            //print_r($response);
            if($response['error_codes'] == 0){
                
                wp_redirect(get_site_url() . "/register?rgs=6");
    
                exit();
            }else{
                wp_redirect(get_site_url() . "/register?rgs=-5");
    
                exit(); 
            }
}
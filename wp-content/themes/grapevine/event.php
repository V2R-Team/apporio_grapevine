<?php
/*
  Template Name: Single Event Template
 */

get_header();
?>
<section>
    <div class="container-fluid">
    <?php
                         $ar_fea = array(
                            'post_type'=>'post',
                            'post_status'=>'publish',
                            'p' => $_GET['pid']
                         );
                         $ar1_fea = new WP_Query($ar_fea);

                        
                        
                          while ($ar1_fea->have_posts()) { $ar1_fea->the_post();  $url_fea = wp_get_attachment_url( get_post_thumbnail_id($post->ID)); ?>
                         <div class="row single-event-header">
                                

 <?php if ($url_fea!="") {?>
                                <img class="img-responsive single-img-top" src="<?php echo $url_fea ?>">    
                            <?php }else{?>
                                <img class="img-responsive single-img-top" src="<?php the_field('top_image'); ?>">    
                            <?php }?>

                                <div class="single-evt-info">
                                    <p class="single-evt-title"><?php the_title(); ?></p>
                                    <div class="single-evt-loc-info">
                                       <?php echo get_the_date(); ?>  | <?php the_field('time'); ?>   &nbsp;&nbsp; &nbsp;&nbsp;<span><img class="pin-img" src="<?php echo get_template_directory_uri() ?>/icons/location-pin.png">&nbsp;&nbsp;</span> <?php echo the_field('address') .' , '. the_field('post_code') ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row single-event-data center-width-row">
                                <div class="col-md-8 single-evt-text"><?php echo the_content() ?></div>
                                <div class="col-md-4 single-evt-video">
                                    <div class="evt-video-holder">
                                        <iframe width="100%" height="300px"
                                                src="<?php echo $event_video ?>">
                                        </iframe>
                                    </div>
                                </div>
                            </div>
                         <?php } wp_reset_query();  ?>

        

    </div>
</section>
        

<?php
    get_footer();


<?php

/*
  Template Name: My Account Template
 */

session_start();
//$interests = array($white, $red, $rose, $fizz, $conn, $gallery, $school, $social);
//print_r($_SESSION['user_data']);
//unset($_SESSION['user']);

$status = '';
if(isset($_REQUEST['rs'])){
    
    $rgs = $_REQUEST['rs'];
    
    if($rgs == 1){
        $status = "Your password has been updated";
    }
    else if($rgs == -1){
         $status = "There was problem updating your password. Try again";
    }else if($rgs == -2){
         $status = "There was problem updating your data. Try again";
    }
    else if($rgs == -4){
         $status = "The new email provided already exists. Please try a different email";
    }
}
$response = '';
if(isset($_SESSION['user']))
{
    $user_data = $_SESSION['user_data'];
    
  
    //print_r($user_data);
     $user_id = $user_data['id'];
    $token = $user_data['token'];
    $request_data = array('method' => 'get_user_data','user_id' => $user_id, 'token' => $token, 'origin' => 'web',);
    $json = json_encode($request_data);
  $request = get_data(get_site_url().'/API/serve-api.php', array('json' => $json));
    
   
     $response_data = json_decode($request, true);
     
     if($response_data['error_codes'] == -1000)
    {
        unset($_SESSION['user']);
        unset($_SESSION['user_data']);
      wp_redirect(get_site_url() . "/register");

    exit;
    }
    
    $response = $response_data['data'];
    
    $username = $response[0]['username'];
    $region = $response[0]['region'];
    $location_to_show = $response[0]['location'];
    $email = $response[0]['email'];
    $fname = $response[0]['first_name'];
    $lname = $response[0]['last_name'];
    $phone = $response[0]['tel_number'];
    $gender = $response[0]['gender'];
    
    $interests = json_decode($response[0]['interests'], true);

    //print_r($interests['interests']);
    $interest_arr = $interests['interests'];
   // $interest_object = json_encode($interest_arr, stripslashes);
   // echo array_search('rose', $interests['interests'], false);
 // echo get_elem_pos('gallery', $interests['interests']);
 
    $red = get_elem_pos("red",$interest_arr);
    $white  = get_elem_pos("white", $interest_arr);
    $rose = get_elem_pos("rose", $interest_arr);
    $fizz = get_elem_pos("fizz", $interest_arr);

    $dessert = get_elem_pos("dessert", $interest_arr);
    $fortified = get_elem_pos("fortified", $interest_arr);
    $conn = get_elem_pos("connoisseur", $interest_arr);
    $travel = get_elem_pos("travel", $interest_arr);

   $school = get_elem_pos("wine-school", $interest_arr);
    $food = get_elem_pos("food", $interest_arr);
    $gallery = get_elem_pos("gallery", $interest_arr);
    $social = get_elem_pos("social", $interest_arr);
}
 else {
    wp_redirect(get_site_url()."/register");
       
       exit;
}
//print_r($response);
//print_r($user_data);



get_header();

?>
<section>
    <div class="container-fluid">
        <div class="row account-icon-row">
            <form action="<?php echo get_site_url() ?>/control" method="POST">
                <input type="hidden" name="action" value="logout" />
                <button class="logoutbtn-btn" type="submit"><span class="purple-color logout-btn">Sign Out</span></button>
            </form> 
        </div>

        <div class="row register-field-row">
            <div class="container-fluid register-container">
               
                <div class="row">
                    <!-- Register -->
                    <form role="form" action="<?php echo get_site_url() ?>/control" onsubmit="return validateDetails.checkAccount(this)" method="POST">
                      <input type="hidden" id="red" name="red" value="<?php echo $red ?>"/>
                      <input type="hidden" id="white" name="white" value="<?php echo $white ?>" />
                      <input type="hidden" id="rose" name="rose" value="<?php echo $rose ?>" />
                      <input type="hidden" id="fizz" name="fizz" value="<?php echo $fizz ?>" />
                      <input type="hidden" id="dessert" name="dessert" value="<?php echo $dessert ?>" />
                      <input type="hidden" id="fortified" name="fortified" value="<?php echo $fortified ?>" />
                      <input type="hidden" id="connoisseur" name="connoisseur"value="<?php echo $conn ?>" />
                      <input type="hidden" id="travel" name="travel"value="<?php echo $travel ?>" />
                      <input type="hidden" id="wine-school" name="wine-school" value="<?php echo $school ?>"/>
                      <input type="hidden" id="food" name="food"value="<?php echo $food ?>" />
                      <input type="hidden" id="gallery" name="gallery" value="<?php echo $gallery ?>" />
                      <input type="hidden" id="social" name="social" value="<?php echo $social ?>"/>
                      <input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id ?>"/>
                      <input type="hidden" id="token" name="token" value="<?php echo $token ?>"/>
                      <input type="hidden" id="current-lat" name="lat" />
                      <input type="hidden" id="current-lng" name="lng"/>
                      <input type="hidden" name="action" value="update-user-data" />
                      <input type="hidden" id="gender" name="gender" value="<?php echo $gender ?>" />

                      <div class="col-md-6 col-holders" style="padding-left: 10px">
                          <span class="confirmation-span"><?php echo $status ?></span>
                          <p class="sign-text"> </p>
                          <div class="col-md-6" style="padding-left: 0">
                              <div class="form-group">
                                  <label class="purple-color">First Name</label>
                                  <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="<?php echo $fname ?>"/>
                              </div>

                              <div class="form-group">
                                  <label class="purple-color">Username</label>
                                  <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?php echo $username ?>"/>
                              </div>
                               <!--
                               <div class="form-group">
                                 <input type="text" class="form-control" id="tel_number" name="tel_number" placeholder="Telephone Number" value="<?php echo $phone ?>"/>
                               </div>
                               -->
                              <div class="form-group">
                                 <label class="purple-color">Event Location</label>
                                 <input type="text" class="form-control" id="auto-complete" name="region" placeholder="Your Region" value="<?php echo $region ?>" />
                                 <label class="purple-color region-text">Choose the area you would like to see events for....</label>
                              </div>

                              <div class="form-group">
                                 <button type="button" class="save-change-btn change-pass-btn">Change Password</button>
                              </div>

                              <div class="change-password-box">
                                 <div class="change-pass-form">
                                   <div class="form-group">
                                     <input type="email" class="form-control" id="reset-email" name="reset-email" placeholder="Enter your email"/>
                                   </div>
                                   <div class="form-group">
                                     <input type="password" class="form-control" id="new-pass" name="user_pass" placeholder="New Password" />
                                   </div>
                                   <div class="form-group">
                                     <input type="password" class="form-control" id="conf-pass" name="user_pass2" placeholder="Confirm Password"/>
                                   </div>
                                   <input type="hidden" id="baseurl" name="" value="<?php echo get_site_url() ?>"/>
                                   <div class="form-group">
                                     <button type="button" class="save-change-btn" onclick="changePassword()">Save Changes</button>
                                   </div>
                                 </div>
                              </div>
                   
                          </div>
                          <!-- end of md-6 -->
                          <div class="col-md-6">
                       
                              <div class="form-group">
                                  <label class="purple-color">Last Name</label>
                                  <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Surname" value="<?php echo $lname ?>"/>
                              </div>

                              <div class="form-group">
                                  <label class="purple-color">Email</label>
                                  <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="<?php echo $email ?>"/>
                              </div>
                       
                              <div class="form-group" id="account-last">
                                  <label class="purple-color">Gender</label>
                                  <div class="selectDiv user-gender">
                                      <span class="selectDefault gender-select-default"></span>
                                      <select name="gender-select" class="selectBox gender-select-box">
                                          <option class="default-text gender-default-text" value="not-specified">Not Specified</option>
                                          <option value="male">Male</option>
                                          <option value="female">Female</option>
                                          <option value="other">Other</option>
                                      </select>
                                  </div>
                              </div>
                              <!--
                              <div class="form-group">             
                                  <div class="checkbox account-radio-holder">
                                      <input type="checkbox" id="myLocation" name="optradio" value="2" <?php if($location_to_show == 2){ ?> checked <?php }?>/>
                                      <label for="myLocation">
                                          <span></span>Show Events in My Current Location
                                      </label>
                                  </div>
                              </div>
                              -->
                          </div>
                        <!-- end of md-6 -->
                      </div>
                      <!-- end of holders -->

                      <!-- Choose Interests -->
                      <div class="col-md-6 col-holders">
                          <p class="sign-text"></p>
                          <div class="col-md-12" style="padding-left: 10px">

                              <div class="row f-row">
                                 <div class="col-sm-3 col-md-3 col-box image-containerSquare" onclick="setPreferences('red','red', 'red-box')"><div class="click-icon red-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Red.png" ></div>
                                 <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('white',' white', 'white-box')"><div class="click-icon white-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/White.png"></div>
                                 <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('rose',' rose', 'rose-box')"><div class="click-icon rose-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Rose.png"></div>
                                 <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('fizz',' fizz', 'fizz-box')"><div class="click-icon fizz-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Fizz.png"></div>
                              </div>
                              <div class="row s-row f-row">
                                 <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('dessert',' dessert', 'dessert-box')"><div class="click-icon dessert-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Dessert.png"></div>
                                 <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('fortified',' fortified', 'fortified-box')"><div class="click-icon fortified-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Fortified.png"></div>
                                 <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('connoisseur',' connoisseur', 'connoisseur-box')"><div class="click-icon connoisseur-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Connoisseur.png"></div>
                                 <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('social',' social', 'social-box')"><div class="click-icon social-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Social.png"></div>
                              </div>
                              <div class="row s-row f-row">
                                  <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('travel',' travel', 'travel-box')"><div class="click-icon travel-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Travel.png"></div>
                                  <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('wine-school',' wine-school', 'wine-school-box')"><div class="click-icon wine-school-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Wine_School.png"></div>
                                  <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('food',' food', 'food-box')"><div class="click-icon food-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Food.png"></div>
                                  <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('gallery',' gallery', 'gallery-box')"><div class="click-icon gallery-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Gallery.png"></div>      
                              </div>

                              <button type="submit" class="finish-btn">Save Changes</button>
                          </div>
                          <!-- end of col -->
                      </div>
                      <!-- end of holders -->
                    </form>
                </div>

            </div>
            <!-- end of container fluid -->
        </div>
        <!-- end of field row -->
    </div>
</section>
<script>
    
    $(function () {
         $(document).ready(function(){
                    /*$('.col-md-2').click(
                        function(e)
                        {*/
                           // alert();
                           // $('.newsfeed-holder').removeClass('active');
                            // $('.newsfeed-holder').removeClass('active');
                            $(".signin-holder").addClass('active');
                        /*}
                    );*/
                     });

        
           getLocation();
         //  var jsonObject = "<?php echo $interest_object ?>";
           var arr = ['red', 'rose', 'white', 'fizz', 'dessert', 'fortified', 'connoisseur', 'travel', 'wine-school', 'food', 'gallery', 'social'];
           console.log(arr);
           var len = arr.length;
           
           for(x = 0; x < len; x++){
               
               var target = arr[x];
               var tv = $("#"+target).val();
               console.log(tv);
               if(tv == "" || tv == undefined || tv == null){
                   
               }else{
                   $("."+target+"-box").show();
               }
               
           }
           
           $(".change-pass-btn").click(function(){
               $(".change-password-box").toggle();
               
           });
          var userGender = '<?php echo $gender ?>';
          var genders = $('.gender-select-box').children();
          var genderText;
          for(var i=0; i<genders.length; i++){
              if(genders[i].value == userGender){
                  genderText = genders[i].text;
              }
          }
          $('.gender-select-default').text(genderText);

            $('.gender-select-box').on('change',function(){
                var defaulttext2 = $('.gender-select-box').find(":selected").text(); 
                $('.gender-select-default').text(defaulttext2);
                
                var gender = $(this).val();
                    $("input#gender").val(gender);
                    
          });
           
    });
    
    
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('auto-complete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBv5rxWqJWNXg6R3VgwWKjmB0d4CsJgEAI&libraries=places&callback=initAutocomplete"
        async defer></script>
<!-- Modal -->
<div id="registerModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">No Preferences Selected</h4>
            </div>
            <div class="modal-body">
                <p>Please select your wine preferences</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-- Modal End-->
<!-- Modal -->
<div id="forgottonPasswordModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Forgotten Password</h4>
            </div>
            <div class="modal-body">
                <p>Please enter your email address and we will send you instructions to reset your password</p>
                <form  role="form" action="<?php echo get_site_url() ?>/control" method="POST">
                <div class="form-group">
                    <input type="email" class="form-control" id="reset-email" name="email" placeholder="Enter your email" required="true"/>
                </div>             
                <input type="hidden" id="" name="action" value="forgot-password"/>
                <div class="form-group">
                    <button type="submit" class="save-change-btn">Submit</button>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
       
    </div>
</div>
<!-- Modal End-->
<?php
get_footer();

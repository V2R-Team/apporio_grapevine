<?php
/* 
  Template Name: Comment Admin Template
 */

session_start();
$user_data = '';
$response = '';
if(isset($_SESSION['admin']))
{
    $user_data = $_SESSION['user_data'];
    $user_id = $user_data[0]['id'];
    
    /*$request_data = array('method' => 'get_user_data','user_id' => $user_id);
    $json = json_encode($request_data);
    $request = get_data(get_site_url().'/API/serve-api.php', array('json' => $json));
    
   
     $response_data = json_decode($request, true);
    
    $response = $response_data['data'];
    
    $username = $response[0]['username'];
    $region = $response[0]['region'];
    $email = $response[0]['email'];
    $fname = $response[0]['first_name'];
    $lname = $response[0]['last_name'];
    $phone = $response[0]['tel_number'];*/
    
    $request_data = array('method' => 'get_reported_comments');
$json = json_encode($request_data);
$request = get_data(get_site_url() . '/API/serve-api.php', array('json' => $json));
$response_data = json_decode($request, true);
$response = $response_data['data'];
    
   
    
    
}
 else {
    //wp_redirect(get_site_url()."/register");
       
       exit;
}




//print_r($response);


get_header();


include 'admin-menu.php';
?>

<div class="container comment-admin-holder">
    
    <input type="hidden" id="baseurl" value="<?php echo get_site_url() ?>" />
    <!--<div class="row comment-admin-row">
        <div class="col-md-3 comment-admin-auth-col">
            
        </div>
        
        <div class="col-md-5 comment-admin-text-col">
            <div class="admin-comment-text">
                
            </div>
            <div id="clear"></div>
            
            <span class="admin-action-buttons">
                <span class="delete-btn">Delete</span>
                <span class="approve-btn">Approve</span>
                <span class="delete-btn">No action</span>
            </span>
            
            
        </div>
        
        <div class="col-md-2 comment-admin-article-col">
            
        </div>
        
        <div class="col-md-2 comment-admin-date-col">
            
        </div>
    </div>-->
    
    <div class="table-responsive">          
  <table class="table table-responsive comment-table">
    <thead>
        <tr class="comment-table-row">
        <th>Author</th>
        <th>Comments</th>
       <th>Comment Reporter</th>
        <th>Article</th>
        <th>Date</th>
        
      </tr>
    </thead>
    <tbody>
        <?php
        if(count($response) > 0){
            
            foreach ($response as $data) {
                
                $author = $data['comment_author'];
                $reporter = $data['comment_reported_by_name'];
                $article = $data['post_title'];
                $comment_content = $data['comment_content'];
               $post_date = date("Y-m-d",  strtotime($data['comment_date'])); 
               $comment_author_email = $data['comment_author_email'];
               $comment_id = $data['comment_ID'];
               $time = date("H:i",  strtotime($data['comment_date']));
        ?>
        <tr class="comment-row<?php echo $comment_id ?>">
        <td><?php echo $author ?><br><?php echo $comment_author_email ?></td>
        <td>
            <div class="admin-comment-text"><?php echo $comment_content ?></div>
            
            <div id="clear"></div>
            
            <span class="admin-action-buttons">
                
                <span class="approve-btn" style="color: green; cursor: pointer;" onclick="approveComment('0','<?php echo $comment_id ?>')">Approve</span>&nbsp;|&nbsp;
                <span class="delete-btn" style="color: red; cursor: pointer;" onclick="deleteComment('<?php echo $comment_id ?>')">Delete</span><!--&nbsp;|&nbsp;
                <span class="delete-btn" style="color: grey; cursor: pointer;">No action</span>-->
            </span>
        </td>
        <td><?php echo $reporter ?></td>
        <td><?php echo $article ?></td>
        <td><?php echo $post_date. ' @ '. $time ?></td>
      </tr>
      
            <?php }}?>
    </tbody>
  </table>
  </div>
</div>
    
    
</div>



<?php

get_footer();


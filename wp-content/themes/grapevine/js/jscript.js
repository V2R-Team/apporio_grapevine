$(window).load(function(){
    resizeImages();
    resizeColumns();
    if($(window).width() < 753){
        $('body').children().hide();
        $('#mobile-landing-image').show();
    } else {
        $('#mobile-landing-image').hide();
    }
    if($(window).width() < 1161){
        $('header .hangdown').hide();
        $('header .header-box.menu').on('click', function(){
          console.log('menu clicked');
          $('header .hangdown').toggle(200);
          
      });
    } else {
        $('header .hangdown').show();
    }
    
});

// header moves respective to smart banner when banbner is shown
$(window).on("scroll", function () {
    
    if(smartBanner){
        if ($(this).scrollTop() <= 76) {
            console.log($(this).scrollTop());
            $('header').css({'top': (76 - $(this).scrollTop() )+ 'px' });
        }
        else {
            $('header').css({'top': '0' });
        }
    }
    
});

$(window).resize(function(){
    resizeImages();
    resizeColumns();
    if($(window).width() < 1161){
        $('header .hangdown').hide();
    } else {
        $('header .hangdown').show();
    }

});


resizeImages = function(){
    console.log('resizeImages Fired!');
    var imageContainers = $('body .image-containerSquare, body .image-containerRectangle');
    $.each(imageContainers, function(element, value){
        if($(value).hasClass('image-containerSquare')){
            $(value).css({'height': $(value).width() + 'px'})
        } else if($(value).hasClass('image-containerRectangle')){
            var width = $(value).width();
            $(value).css({'height': ((width/40) * 21) + 'px'})
        }
    });
};
resizeColumns = function(){
    console.log('resizeColumns Fired!');
    if($(window).width() > 990){
        var column = $('.register-container .col-md-6.col-holders:last-of-type');
        if(column.length > 0){
            var columnHeight = $(column[0]).height();
            $('.register-container, .col-md-6.col-holders:first-of-type').css({'height': columnHeight + 'px'});
        }
    } else {
      var column = $('.register-container .col-md-6.col-holders:last-of-type');
        if(column.length > 0){
            $('.register-container, .col-md-6.col-holders:first-of-type').css({'height': 'auto'});
        }
    }    
};

function showDropDown(container)
{
    $("."+container).toggle();
}

function hideDropDown(container)
{
    $("."+container).hide();
}

function hideTick(container)
{
  $("."+container).hide();
}

function showTick(container)
{
  $("."+container).show();
}

function setPreferences(holder, preference, box)
{
   // alert(holder);
   var target = box;
   var pref = $("#"+holder).val();
   // showtick('click-icon');
    
    if(pref == "" || pref == null){
        $("#"+holder).val(preference);
        showTick(target);
    }
    else{
        $("#"+holder).val('');
        hideTick(target);
    }
}

function getLocation() {
    
   // var loc;
    if (navigator.geolocation) {
     navigator.geolocation.getCurrentPosition(showPosition);
     
    
    } else {
        alert("Geolocation is not supported by this browser.");
    }
    
    
}
function showPosition(position) {
    var lat = position.coords.latitude;
    var lng =  position.coords.longitude; 
   
    $("#current-lat").val(lat);
    $("#current-lng").val(lng);
    
  
}



function buildEvents()
{
  
    var latitude;
    var longitude;
    var order = $("#date-selected").val();
    var imurl = $("#imurl").val(); 
    var token = $("#token").val(); 
    var baseurl = $("#baseurl").val(); 
    var selected_location  = $("#selected-location").val();
    var user_id  = $("#user_id").val();
    var  distance  = $("#selected-distance").val();

     if(selected_location == 1)
   {
     latitude =   $("#lat").val();
     longitude =   $("#lng").val();
     // alert(1);
   }
   else if(selected_location == 2)
   {
      latitude =   $("#current-lat").val();
     longitude =   $("#current-lng").val(); 
     
    
      //alert(2);
   }
   else{
        latitude =   $("#lat").val();
     longitude =   $("#lng").val();
      //alert(3);
   }
   
     console.log('current latitude '+latitude);
    console.log('current longitude '+longitude);
    
    
    
   var data = {"method":"get_events","user_id": user_id,"distance":distance,"token":token, "origin":"web", "selected-date":order, "selected_location":selected_location, "location":{"lat":latitude, "lng":longitude}};
   var json  = JSON.stringify(data);
   $.post(baseurl + "/API/serve-api.php",
   {
      json:json
   },     
    function (data, status) {
    console.log(data);
   
   var responseData = JSON.parse(data);
   var dataObject = responseData['data'];
   // console.log(dataObject[0]['title']);
   var x;
    $(".colum-holder").html("");
   for(x in dataObject)
   {
       var id= dataObject[x]['id'];
       var title = dataObject[x]['title'];
       var address = dataObject[x]['address'];
       var date = dataObject[x]['formated_date'];
       var postcode = dataObject[x]['post_code'];
       var featured_image = dataObject[x]['featured_image'];
       var time = dataObject[x]['time'];
   
    $(".colum-holder").append(
          '<div class="col-md-4 feed-column">'+
            '<a href="index.php/event?pid='+id+'">'+
                '<div class="col-top-info">'+
                    '<div class="feed-info-holder">'+
                      '<p class="event-title-text">'+title+'</p>'+
                        '<div class="speech-text">'+
                           ' '+address+',  '+ postcode+''+
                       '</div>'+
                    '</div>'+
                    ' <div id="clear"></div>'+
                    '<div class="thumb-holder">'+
                       '<img class="img-responsive" src="'+featured_image+'"/>' +
                    '</div>'+
              '</div>'+
              '<div class="col-bottom-bar">'+
                  '<div class="read-more" >'+date+'  |  '+ time+'</div>'+
              '</div>'+
          '</a>' +
       ' </div>'
                
       );
      }
      
   });
}


function changePassword(){
        var baseurl = $("#baseurl").val(); 
        var token = $("#token").val(); 
        var user_id = $("#user_id").val(); 
        var user_email = $("#reset-email").val();
        var newpass = $("#new-pass").val();
        var confirmpass = $("#conf-pass").val();
        console.log(user_id+ token);
        
    if( newpass == "" || confirmpass == "" || user_email == ""){
         alert("All the fields are required");
    } else  if(validateDetails.validatePassword(newpass)) {
        if(newpass === confirmpass){
            
           $.post(baseurl + "/control",
            {
               "reset-email":user_email, "user_pass":newpass, "action":"reset-password", "user_id":user_id, "token":token
            },     
             function (data, status) {
                 console.log(data);
                if(data == 0){
                    $(".confirmation-span").html("Your password has been reset");
                } else {
                     $(".confirmation-span").html("There was problem updating your password. Try again");
                }
                 $("#reset-email").val("");
                 $("#new-pass").val("");
                 $("#conf-pass").val("");
                $(".change-password-box").hide();
             });
   
        }
        else{
            alert("The passwords do not match");
        }
    } else {
        alert("Your password must contain 8 characters and at least one uppercase letter and one number!");
    }
       
    
}


function check_form(evt)
{
   var pass = $("#pass1").val();
   var confirmpass = $("#pass2").val(); 
   
   if(pass == confirmpass){
       
       return true;
   }else{
       
       evt.preventDefault();
       return false;
   }
}


function reportAbuse(comment_id, comment_author, comment_author_id)
{
   var reporter_id = $("#user_id").val();
    var reporter_name = $("#current_user_name").val();
     var post_id = $("#post_id").val();
     var baseurl = $("#baseurl").val();
    
    var data = {"method":"report_comment","reporter_id": reporter_id,"reporter_name":reporter_name,"comment_id":comment_id,"comment_author_id": comment_author_id, "comment_author": comment_author, "post_id": post_id};
   var json  = JSON.stringify(data);
   $.post(baseurl + "/API/serve-api.php",
   {
      json:json
   },     
    function (data, status) {
       console.log(data);
       
       //var dataObject = JSON.parse(data);
       var t =  $("#commentpara"+comment_id).html();
        console.log(t);
       $("#commentpara"+comment_id).html("This comment is being reviewed by the administrator");
       $("#subject"+comment_id).html("");
       
       alert("Your report has been recived");
   });
   
}

function hideReportButton(container, status)
{
    if(status == 1){
    $("."+container).hide();
    }
}

function approveComment(status, comment_id)
{
  var data = {"method":"update_comment_status","status": status,"comment_id":comment_id};
   var json  = JSON.stringify(data);
   
    var baseurl = $("#baseurl").val();
   $.post(baseurl + "/API/serve-api.php",
   {
      json:json
   },     
    function (data, status) {
      // console.log(data);
       
       
       alert("You have approved this comment");
       $(".comment-row"+comment_id).fadeOut();
   });  
}


function deleteComment(comment_id)
{
   var data = {"method":"delete_comment","comment_id":comment_id};
   var json  = JSON.stringify(data);
   
    var baseurl = $("#baseurl").val();
   $.post(baseurl + "/API/serve-api.php",
   {
      json:json
   },     
    function (data, status) {
      
      $(".comment-row"+comment_id).fadeOut();
       
       
      // alert("You have approved this comment");
   });   
}

function sendPush()
{
    var w = $("#white").val();
    var re = $("#red").val();
    var ro = $("#rose").val();
    var ws = $("#wine-school").val();
    var co = $("#connoisseur").val();
    var g = $("#gallery").val();
    var s = $("#social").val();
    var f = $("#fizz").val();
    var fo = $("#fortified").val();
    var des = $("#dessert").val();
    var tr = $("#travel").val();
    var fd = $("#food").val();
    var content = $("#comment_content").val();
    /*
      API method is added to exclusions in top of main_interface
    */

    var data = {"method":"send_interest_based_push","content": content,"interests":[re,w, ro, ws, co, g, s, f, fo, des, tr, fd]};
    var json = JSON.stringify(data);
   
    var baseurl = $("#baseurl").val();
    console.log('interest based push about to fire!');
    $.post(baseurl + "/API/serve-api.php",
    {
      json:json
    },     
    function (data, status) {
      
      //JSON.parse(data);
      if('error_codes' in data){
          alert(data.errorMessage);
      } else {
          alert("Message sent");
      }

      //status always returns 'success' if the call recieved a response. NOT if the server responded with an error


      /*
      if(status === 'success'){
          alert("Message Sent");
      }
      */

   });  
}


function sendLocationBasedPush()
{
    var location = $("#auto-complete").val();
    //console.log(location);
    var distance = $("#selected-distance").val();
    var content = $("#message_content").val();

    /*
      API method is added to exclusions in top of main_interface
    */
    var data = {"method":"send_location_based_push","location": location,"distance":distance, "content":content};
    var json  = JSON.stringify(data);

    var baseurl = $("#baseurl").val();
    console.log('location based push about to fire!');
    $.post(baseurl + "/API/serve-api.php",
    {
      json:json
    },     
    function (data, status) {
        debugger;
    });  
}


function toggleOptions(current, old)
{
    $("."+old).hide();
   $("."+current).show();
}


function validate_input(input1, input2, form_id)
{
   var first_input = $("#"+input1).val();
   var second_input = $("#"+input2).val();
   var email = $("#reset-email").val();
   if(first_input == "" || second_input == "" || email == "" || first_input == undefined || second_input == undefined || email == undefined)
   { 
       alert("All the fields are required");
    }else{
        if(first_input == second_input){

            $("#"+form_id).submit();
        }else{
            alert("The passwords do not match");
        }
   }
}


function check_form(evt)
{
   var pass = $("#pass1").val();
   var confirmpass = $("#pass2").val(); 
   
   if(pass == confirmpass){
       
       return true;
   }else{
       
       evt.preventDefault();
       return false;
   }
}

function ValidationError(errorCode, errorMessage){
    this.errorCode = errorCode;
    this.errorMessage = errorMessage;
}

validateDetails = new function(){
    var emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    //var passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    var passwordRegex = /^(?=.*\d)(?=.*[a-z]).{6,}$/;
    var preferenceCheckCount = 0;

    this.checkAccount = function(event){
        var submit;
        
        //var lat = $(event).find('#current-lat').val();
        //var lng = $(event).find('#current-lat').val();
        //var username = $(event).find('#username').val();
        //var first_name = $(event).find('#first_name').val();
        //var last_name = $(event).find('#last_name').val();
        var email = $(event).find('#email').val();

        //var user_pass = $(event).find('#pass1').val();
        //var user_pass2 = $(event).find('#pass2').val();
        //var region =  $(event).find('#current-lat').val();
        //var tel_number = $(event).find('#tel_number').val();
        var rose = $(event).find('#rose').val();
        var red = $(event).find('#red').val();
        var food = $(event).find('#food').val();
        var fortified = $(event).find('#fortified').val();
        var dessert = $(event).find('#dessert').val();
        var white = $(event).find('#white').val();
        var travel = $(event).find('#travel').val();
        var fizz = $(event).find('#fizz').val();
        var connoisseur = $(event).find('#connoisseur').val();
        var social = $(event).find('#social').val();
        var gallery = $(event).find('#gallery').val();
        var wine_school = $(event).find('#wine_school').val();

        /* VALIDATE THAT THE EMAIL IS REAL */
        var validateEmail = emailRegex.test(email);

        /* minimum security of Password1 */
        //var validatePassword = passwordRegex.test(user_pass);
        if(email == ''){
            submit = false;
            alert('Please enter an email');
        } else if(!validateEmail){
            alert('Please enter a valid email!');
            submit = false;
        } else if(rose == '' && red == '' && food == '' && fortified == '' && dessert == '' && white == '' && travel == '' && fizz == '' && connoisseur == '' && social == '' && gallery == '' && wine_school == '' && preferenceCheckCount == 0){
            submit = false;
            //alert('No Preferences Selected!');
            $('#registerModal').modal();
            preferenceCheckCount++;
        } else {
            submit = true;
        }
        console.log(submit);
        /* action
        <?php echo get_site_url() ?>/control
        POST fields
        */
        if(submit){
          preferenceCheckCount = 0;
        }
        return submit;
    }

    this.checkRegister = function(event){
        var submit;
        
        //var lat = $(event).find('#current-lat').val();
        //var lng = $(event).find('#current-lat').val();
        var username = $(event).find('#username').val();
        var first_name = $(event).find('#first_name').val();
        var last_name = $(event).find('#last_name').val();
        var email = $(event).find('#email').val();

        var user_pass = $(event).find('#pass1').val();
        var user_pass2 = $(event).find('#pass2').val();
        var region =  $(event).find('#auto-complete').val();
        //var tel_number = $(event).find('#tel_number').val();
        var rose = $(event).find('#rose').val();
        var red = $(event).find('#red').val();
        var food = $(event).find('#food').val();
        var fortified = $(event).find('#fortified').val();
        var dessert = $(event).find('#dessert').val();
        var white = $(event).find('#white').val();
        var travel = $(event).find('#travel').val();
        var fizz = $(event).find('#fizz').val();
        var connoisseur = $(event).find('#connoisseur').val();
        var social = $(event).find('#social').val();
        var gallery = $(event).find('#gallery').val();
        var wine_school = $(event).find('#wine_school').val();

        /* VALIDATE THAT THE EMAIL IS REAL */
        var validateEmail = emailRegex.test(email);

        /* minimum security of Password1 */
        try{
            if(username == ''){
                throw new ValidationError('Username Error', 'Please enter a username');
            }
            if(first_name == ''){
                throw new ValidationError('First Name Error', 'Please enter a first name');
            }
            if(last_name == ''){
                throw new ValidationError('Last Name Error', 'Please enter a last name');
            }
            if(region == ''){
                throw new ValidationError('Region Error', 'Please enter an region');
            }
            if(!this.validatePassword(user_pass)){
                throw new ValidationError('Password Error', 'Your password must contain 8 characters and at least one uppercase letter and one number!');
            }
            if(email == ''){
                throw new ValidationError('Email Error', 'Please enter an email');
            } 
            if(!validateEmail){
                throw new ValidationError('Email Error', 'Please enter a valid email!');
            } 
            if(rose == '' && red == '' && food == '' && fortified == '' && dessert == '' && white == '' && travel == '' && fizz == '' && connoisseur == '' && social == '' && gallery == '' && wine_school == '' && preferenceCheckCount == 0){
                throw new ValidationError('Register Error', 'No Preferences Selected');
                preferenceCheckCount++;
            } 
            if(user_pass !== user_pass2){
                throw new ValidationError('Password Error', 'Passwords Do Not Match!');
            }
            submit = true;

        } catch (error){
            submit = false;
            if(Object.getPrototypeOf(error) == ValidationError.prototype){
          
              $('#registerModalTitle').html(error.errorCode);
              $('#registerModalMessage').html(error.errorMessage);
              $('#registerModal').modal();
            } else {
              alert('An unknown error occured!');
            }

        }
        
        console.log(submit);
        /* action
        <?php echo get_site_url() ?>/control
        POST fields
        */
        if(submit){
          preferenceCheckCount = 0;
        }
        return submit;
    };

    this.validatePassword = function(password){

        var validatePassword = passwordRegex.test(password);

        if(!validatePassword){
            return false
        } else {
            return true;
        }
    };

};


$(document).ready(function(){

  $('.comment-form-box form').on('submit', function(e){

    e.preventDefault();

    if( $.trim($('#comment').val()) == '' ){

      alert('Please enter a comment.');

      return false;

    }

    var author_username = $('#author_username').val();

    $.post( 
      "/control", 
      $( ".comment-form-box form" ).serialize()
    )
    .done(function() {
    
      $('#notificationModal h4.modal-title').html('Success');
      $('#notificationModal .modal-body').html('Your comment is being reviewed by Grapevine and will be posted shortly.');

      // get date and time
      var d = new Date(); // 1 Sept 2016 at 09:30
      var time = d.getHours() + ':' + d.getMinutes();

      $('.commentlist').append(
        $('<div />')
          .addClass('comments-wrapper')
          .append(
            $('<p />')
              .html( 'Today at ' + time)
          )
          .append(
            $('<div />')
              .addClass('comment-text-area')
              .append(
                $('<p />')
                  .html('Your comment is being reviewed by Grapevine and will be posted shortly.')
              )
          )
          .append(
            $('<span />')
              .html( author_username )
          )
      );

      $('#comment').val('').html('');

    })
    .fail(function( data ) {

      console.log('data',data);
      
      $('#notificationModal h4.modal-title').html('Error!');
      $('#notificationModal .modal-body').html( data );

    })
    .always(function(){

      $('#notificationModal').modal();

    });

  });



  $('#signupBtn').on('click', function(e) { 
    
    e.preventDefault();

    $('#notificationModal .modal-dialog').css({'margin-top':'10%'});
    $('#notificationModal .modal-footer').css({'border-top':'1px solid #e5e5e5'});

    $('#notificationModal h4.modal-title').html('Sign In Required');
    $('#notificationModal .modal-body').html( 'Sign up now to post comments, be part of the grapevine community and view articles and events tailored to you.' );

    var btn = 
    $('<button />')
      .addClass('cta-btn purple')
      .html('Sign In')
      .on('click', function(event) {
        
        window.location.href = '/register/';

      });


    $('#notificationModal .modal-footer')
      .empty()
      .append( btn )
      .append(
        $('<div />')
          .html('Follow our community')
      )
      .append(
        $('<a />')
          .attr({
            href: 'https://twitter.com/Grapevine_App_'
          })
          .append(
            $('<img />')
              .attr({
                src: '/wp-content/themes/grapevine/images/twitter.png'
              })
          )
      )
      .append(
        $('<a />')
          .attr({
            href: 'https://www.facebook.com/GrapevineApp/'
          })
          .append(
            $('<img />')
              .attr({
                src: '/wp-content/themes/grapevine/images/facebook.png'
              })
          )
      )
      .append(
        $('<a />')
          .attr({
            href: 'https://www.instagram.com/grapevine_app'
          })
          .append(
            $('<img />')
              .attr({
                src: '/wp-content/themes/grapevine/images/instagram.png'
              })
          )
      );

    $('#notificationModal').modal();

  });

});




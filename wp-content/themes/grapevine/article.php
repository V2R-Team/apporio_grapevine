
<?php

/*
  Template Name: Single Article Template
 */

session_start();

$post_id = 16;
$post_data = '';
if(isset($_SESSION['user']))
{
    $user_data = $_SESSION['user_data'];
    $user_id = $user_data['id'];
    $token = $user_data['token'];
    $post_id = $_REQUEST['aid'];
  
    $request_data = array('method' => 'get_article','article_id' => $post_id, 'user_id' =>$user_id, 'token' => $token, 'origin' => 'web');
    $json = json_encode($request_data);
    $request = get_data(get_site_url().'/API/serve-api.php', array('json' => $json));
    $response_data = json_decode($request, true);
    /*
     * Logout user if they are logged in with another device
     */
   if($response_data['error_codes'] == -1000)
    {
        unset($_SESSION['user']);
        unset($_SESSION['user_data']);
        wp_redirect(get_site_url() . "/register");

        exit;
    }
    
    $response = $response_data['data'];
     
    
    $request_arr = array('method' => 'get_user_data','user_id' => $user_id, 'token' => $token, 'origin' => 'web');
    $userjson = json_encode($request_arr);
    $user_request = get_data(get_site_url().'/API/serve-api.php', array('json' => $userjson));
    
   
    $user_response_data = json_decode($user_request, true);
    $user_response = $user_response_data['data'];
    
    
    $username = $user_response[0]['username'];
  
    $email = $user_response[0]['email'];
    $fname = $user_response[0]['first_name'];
    $lname = $user_response[0]['last_name'];
    
    
    $comment_data = array('method' => 'get_comments','post_id' => $post_id, 'user_id' =>$user_id, 'token' => $token, 'origin' => 'web');
    $commentjson = json_encode($comment_data);
    $comment_request = get_data(get_site_url().'/API/serve-api.php', array('json' => $commentjson));
    
   
    $comment_response_data = json_decode($comment_request, true);
    $comment_response = $comment_response_data['data'];
    
//echo count($comment_response);
     
}
 else {
    wp_redirect(get_site_url()."/register");
       
    exit;
}

if(count($response) > 0){
    $title = $response['title'];
    $content = $response['content'];
    $f_date = $response['formatted_date'];
    $top_image = $response['side_image'];
    $time = $response['time'];
    $author = $response['author'];
   // $author = $response['author'];
}

if(count($comment_response) > 0){
    $comment_count = count($comment_response);
    $comment = $comment_count;
    if($comment_count == 0){
        $comment = 'Be the first to comment';
    }
}

get_header();
?>
<section>
    <div class="container-fluid">

        <input type="hidden" id="post_id" name="post_id" value="<?php echo $post_id ?>" />
        <input type="hidden" id="baseurl" name="user_id" value="<?php echo get_site_url() ?>" />
        <input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id ?>" />
        <input type="hidden" id="current_user_name" name="user_id" value="<?php echo $fname.' '.$lname ?>" />

        <div class="row center-padding-row article-row">
            <p class="article-title"><?php echo $title ?></p>
            <div class="article-date-time">
                <span class=""><a href="#add-comment-area"><img class="" src="<?php echo get_template_directory_uri() ?>/icons/comment-icon.png"></a></span>&nbsp;<span style="color:#95277e; font-weight: bold;"><?php echo $comment ?></span> &nbsp; | &nbsp;
                <span class=""><img class="" src="<?php echo get_template_directory_uri() ?>/icons/time-color.png"></span>&nbsp;
                <?php echo $f_date ?> &nbsp;|&nbsp; <?php echo $time ?> Written by <?php echo $author ?>
            </div>
            <span class="article-image"><img class="feature-img" src="<?php echo get_template_directory_uri() ?>/icons/divider-icon.png"></span>
        </div>
        
        <div class="row center-padding-row article-data">
            <?php echo $content ?>
        </div>
        <span id="add-comment-area"></span>
        
        <div class="row center-padding-row article-comment-box">
            <!--
            <div class="article-date-time">
                <span class=""><a href="#add-comment-area"><img class="" src="<?php echo get_template_directory_uri() ?>/icons/comment-icon.png"></a></span>&nbsp;<span style="color:#95277e; font-weight: bold;"><?php echo $comment ?></span> &nbsp; | &nbsp;
                <span class=""><img class="" src="<?php echo get_template_directory_uri() ?>/icons/time-color.png"></span>&nbsp;
                <?php echo $f_date ?> &nbsp;|&nbsp; <?php echo $time ?> Written by <?php echo $author ?>
            </div>
           
            <span class=""><img class="feature-img" src="<?php echo get_template_directory_uri() ?>/icons/divider-icon.png"></span>
             -->
             <?php   if(count($comment_response) > 0){
           foreach ($comment_response as $commentData) {
               
               $author = $commentData['author'];
               $author_username = $commentData['author_username'];
               $time = $commentData['time'];
               $formated_date =$commentData['formatted_date'];
               $comment_content = $commentData['content'];
               //$subject = $commentData['subject'];
               $comment_id = $commentData['id'];
               $comment_author_id = $commentData['user_id'];
               
               $status = $commentData['status'];
           
          ?>
            
            <div class="comment-text-area">
                <input type="hidden" id="comment_author" name="" value="<?php echo $author ?>" />
                <input type="hidden" id="comment_id" name="" value="<?php echo $comment_id ?>" />
                <p class="comment-title"><span style="color:#480f56"><?php echo $author_username ?></span></p>
                <p>
                    <?php echo $formated_date ?> &nbsp;| &nbsp;<?php echo $time ?>
                </p>
                <!--
                <p class="comment-title" id="subject<?php echo $comment_id ?>">
                    <?php echo $subject ?>
                </p>
                -->
                <p id="commentpara<?php echo $comment_id ?>">
                    
                    <?php echo $comment_content ?>
                </p>
                <!--
                <div class="report-abuse rep-btn<?php echo $comment_id ?>" onclick="reportAbuse('<?php echo $comment_id ?>', '<?php echo $author ?>', '<?php echo $comment_author_id ?>')">
                    <img class="feature-img" src="<?php echo get_template_directory_uri() ?>/icons/report-icon.png">
                </div>
                -->
                <script>
                    hideReportButton('rep-btn<?php echo $comment_id ?>', '<?php echo $status ?>');
                </script>
            </div>
            <?php }} ?>
            <div class="comment-form-box">
                <form role="form" action="<?php echo get_site_url() ?>/control" method="POST">
                    <div class="form-group">
                        <textarea class="form-control" rows="5" id="comment" name="comment_content" placeholder="Write Your comments here" required="true"></textarea>
                    </div>
                    <input type="hidden" name="post_id" value="<?php echo $post_id ?>" />
                    <input type="hidden" name="author" value="<?php echo $author ?>" />
                    <input type="hidden" name="author_username" value="<?php echo $username ?>" />
                    <input type="hidden" name="email" value="<?php echo $email ?>" />
                    <input type="hidden" name="user_id" value="<?php echo $user_id ?>" />
                    <input type="hidden" name="token" value="<?php echo $token ?>" />
                    <input type="hidden" name="action" value="add-comment" />
                    
                    <button type="submit" class="write-comment-btn">Post Comment</button>
                </form>
            </div>
        </div>

    </div>
</section>

<?php

get_footer();

<?php

/* 
  Template Name: Home Page Template
 */

get_header();
//echo sha1('56f176ffb674d');
?>
<section>
    <div class="containter-fluid">
        <div class="row home-slider">
            <div class="home-container">
                <div class="col-md-6 slider-desc-holder">
                    <p class="app-value">Grapevine The Wine Lovers App</p>
                     <p class="app-value-desc" style="font-style:italic; text-align:center;">Personalised to your wine interests</p>
                    <p class="app-value-desc">Whether your a complete novice or an advanced enthusiast our mission is to deliver engaging and interesting content in a fun and light hearted way. You tell us your location and what interests you about the world of wine and the app will automatically deliver articles and events that are specific to you.</p>
                    <p class="app-value-desc">Don't miss out and join our exciting wine community today!!</p>
                    
                    <div class="download-stores">
                        <div class="download-slider-area">
                            <p>DOWNLOAD THE APP</p>
                        </div>
                        <div class="store-holder">
				<a href="https://play.google.com/store/apps/details?id=com.b60apps.grapevine" target="_blank">
					<img class="feature-img" src="<?php echo get_template_directory_uri() ?>/icons/google-play.png">
				</a>
                        </div>
                        <div class="store-holder apple-logo">
				<a href="https://itunes.apple.com/us/app/grapevine-the-wine-lovers-app/id1076998722?ls=1&amp;mt=8" target="_blank">
					<img class="feature-img" src="<?php echo get_template_directory_uri() ?>/icons/apple-store.png">
				</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-5 slider-phone-img">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/slider-phonev2.png">
                </div>

            </div>
        </div>

        <div class="row plain-row">
            <div class="container first-desc top-desc">
                <!--
                <p class="feature-title">Wine Events specific to your Palate</p>
                <div class="purple-line"></div>
                <p class="feature-text">Do you really enjoy drinking wine but you just stick to one or two types?</p>
                -->
                <p class="feature-title">Do you enjoy drinking wine but just stick to one or two types?</p>
                <p class="feature-title">Would you like to expand your knowledge of wine in a fun and casual way?</p>
                <p class="feature-title">Are you interested in local wine events while at home or away?</p>
                <div class="feature-news-letter">
                    <a href="index.php/register">JOIN OUR COMMUNITY</a>
                    
                </div>
            </div>
            <div class="container feature-desc">
                <img class="feature-img" src="<?php echo get_template_directory_uri() ?>/icons/home-logo.png">
            </div>
            
        </div>

        <div class="row event-area">
             
            <div class="container first-desc">
                <p class="feature-title">The Wine Lovers Newsletter…</p>
                <div class="white-line"></div>
                <p class="feature-text">Bringing you all the best content through our fortnightly newsletter. Tipping you off on the best wine offers of the moment and not to be missed events in your area.</p>
                 <p class="feature-text">Sign up to the newsletter and be part of our Wine Lovers community!</p>
                <div class="feature-create-account">
                    <a href="#" type="button" data-toggle="modal" data-target="#signUpModal">SIGN UP NOW!</a>
                </div>
            </div>
            
            <div class="container phone-desc">
                <img class="feature-i feature-phone" src="<?php echo get_template_directory_uri() ?>/icons/home-logo-phone.png">
                <img class="feature-is feature-phone" src="<?php echo get_template_directory_uri() ?>/icons/home-logo-phones.png">
            </div>
            
            
        </div>
    </div>
</section>

<!-- Modal -->
<div id="signUpModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form method="POST" action="https://grapevinesocial.activehosted.com/proc.php" id="_form_1_" class="_form _form_1 _inline-form  _dark" novalidate>
          <input type="hidden" name="u" value="1" />
          <input type="hidden" name="f" value="1" />
          <input type="hidden" name="s" />
          <input type="hidden" name="c" value="0" />
          <input type="hidden" name="m" value="0" />
          <input type="hidden" name="act" value="sub" />
          <input type="hidden" name="v" value="2" />
          <div class="_form-content">
            <div class="_form_element _x70768551 _full_width _clear" >
              <div class="_form-title">
                newsletter sign-up
              </div>
            </div>
            <div class="_form_element _x83561039 _full_width _clear" >
              <div class="_html-code">
                <p>
                  SIGN UP FOR THE LATEST WINE NEWS AND INSPIRATION
                </p>
              </div>
            </div>
            <div class="_form_element _x35649496 _full_width " >
              <label class="_form-label">
                Full Name
              </label>
              <input type="text" name="fullname" placeholder="Type your name" />
            </div>
            <div class="_form_element _x40396162 _full_width " >
              <label class="_form-label">
                Email*
              </label>
              <input type="text" name="email" placeholder="Type your email" required/>
            </div>
            <div class="_button-wrapper _full_width">
              <button id="_form_1_submit" class="_submit" type="submit">
                Submit
              </button>
            </div>
            <div class="_clear-element">
            </div>
          </div>
          <div class="_form-thank-you" style="display:none;">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- End of Modal -->

</div>

<script type="text/javascript">
/* active campaign sign up form */
window._show_thank_you = function(id, message, trackcmp_url) {
  var form = document.getElementById('_form_' + id + '_'), thank_you = form.querySelector('._form-thank-you');
  form.querySelector('._form-content').style.visibility = 'hidden';
  thank_you.innerHTML = message;
  thank_you.style.display = 'block';
  if (typeof(trackcmp_url) != 'undefined' && trackcmp_url) {
    // Site tracking URL to use after inline form submission.
    _load_script(trackcmp_url);
  }
  if (typeof window._form_callback !== 'undefined') window._form_callback(id);
};
window._show_error = function(id, message, html) {
  var form = document.getElementById('_form_' + id + '_'), err = document.createElement('div'), button = form.querySelector('button');
  err.innerHTML = message;
  err.className = '_error-inner _form_error _no_arrow';
  var wrapper = document.createElement('div');
  wrapper.className = '_form-inner';
  wrapper.appendChild(err);
  button.parentNode.insertBefore(wrapper, button);
  if (html) {
    var div = document.createElement('div');
    div.className = '_error-html';
    div.innerHTML = html;
    err.appendChild(div);
  }
};
window._load_script = function(url, callback) {
    var head = document.querySelector('head'), script = document.createElement('script'), r = false;
    script.type = 'text/javascript';
    script.charset = 'utf-8';
    script.src = url;
    if (callback) {
      script.onload = script.onreadystatechange = function() {
      if (!r && (!this.readyState || this.readyState == 'complete')) {
        r = true;
        callback();
        }
      };
    }
    head.appendChild(script);
};
(function() {
  var getCookie = function(name) {
    var match = document.cookie.match(new RegExp('(^|; )' + name + '=([^;]+)'));
    return match ? match[2] : null;
  }
  var setCookie = function(name, value) {
    var now = new Date();
    var time = now.getTime();
    var expireTime = time + 1000 * 60 * 60 * 24 * 365;
    now.setTime(expireTime);
    document.cookie = name + '=' + value + '; expires=' + now + ';path=/';
  }
      var addEvent = function(element, event, func) {
    if (element.addEventListener) {
      element.addEventListener(event, func);
    } else {
      var oldFunc = element['on' + event];
      element['on' + event] = function() {
        oldFunc.apply(this, arguments);
        func.apply(this, arguments);
      };
    }
  }
  var _removed = false;
  var form_to_submit = document.getElementById('_form_1_');
  var allInputs = form_to_submit.querySelectorAll('input, select, textarea'), tooltips = [], submitted = false;
  var remove_tooltips = function() {
    for (var i = 0; i < tooltips.length; i++) {
      tooltips[i].tip.parentNode.removeChild(tooltips[i].tip);
    }
      tooltips = [];
  };
  var remove_tooltip = function(elem) {
    for (var i = 0; i < tooltips.length; i++) {
      if (tooltips[i].elem === elem) {
        tooltips[i].tip.parentNode.removeChild(tooltips[i].tip);
        tooltips.splice(i, 1);
        return;
      }
    }
  };
  var create_tooltip = function(elem, text) {
    var tooltip = document.createElement('div'), arrow = document.createElement('div'), inner = document.createElement('div'), new_tooltip = {};
    if (elem.type != 'radio' && elem.type != 'checkbox') {
      tooltip.className = '_error';
      arrow.className = '_error-arrow';
      inner.className = '_error-inner';
      inner.innerHTML = text;
      tooltip.appendChild(arrow);
      tooltip.appendChild(inner);
      elem.parentNode.appendChild(tooltip);
    } else {
      tooltip.className = '_error-inner _no_arrow';
      tooltip.innerHTML = text;
      elem.parentNode.insertBefore(tooltip, elem);
      new_tooltip.no_arrow = true;
    }
    new_tooltip.tip = tooltip;
    new_tooltip.elem = elem;
    tooltips.push(new_tooltip);
    return new_tooltip;
  };
  var resize_tooltip = function(tooltip) {
    var rect = tooltip.elem.getBoundingClientRect();
    var doc = document.documentElement, scrollPosition = rect.top - ((window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0));
    if (scrollPosition < 40) {
      tooltip.tip.className = tooltip.tip.className.replace(/ ?(_above|_below) ?/g, '') + ' _below';
    } else {
      tooltip.tip.className = tooltip.tip.className.replace(/ ?(_above|_below) ?/g, '') + ' _above';
    }
  };
  var resize_tooltips = function() {
    if (_removed) return;
    for (var i = 0; i < tooltips.length; i++) {
      if (!tooltips[i].no_arrow) resize_tooltip(tooltips[i]);
    }
  };
  var validate_field = function(elem, remove) {
    var tooltip = null, value = elem.value, no_error = true;
    remove ? remove_tooltip(elem) : false;
    if (elem.type != 'checkbox') elem.className = elem.className.replace(/ ?_has_error ?/g, '');
    if (elem.getAttribute('required') !== null) {
      if (elem.type == 'radio' || (elem.type == 'checkbox' && /any/.test(elem.className))) {
        var elems = form_to_submit.elements[elem.name];
        no_error = false;
        for (var i = 0; i < elems.length; i++) {
          if (elems[i].checked) no_error = true;
        }
        if (!no_error) {
          tooltip = create_tooltip(elem, "Please select an option.");
        }
      } else if (elem.type =='checkbox') {
        var elems = form_to_submit.elements[elem.name], found = false, err = [];
        no_error = true;
        for (var i = 0; i < elems.length; i++) {
          if (elems[i].getAttribute('required') === null) continue;
          if (!found && elems[i] !== elem) return true;
          found = true;
          elems[i].className = elems[i].className.replace(/ ?_has_error ?/g, '');
          if (!elems[i].checked) {
            no_error = false;
            elems[i].className = elems[i].className + ' _has_error';
            err.push("Checking %s is required".replace("%s", elems[i].value));
          }
        }
        if (!no_error) {
          tooltip = create_tooltip(elem, err.join('<br/>'));
        }
      } else if (elem.tagName == 'SELECT') {
        var selected = true;
        if (elem.multiple) {
          selected = false;
          for (var i = 0; i < elem.options.length; i++) {
            if (elem.options[i].selected) {
              selected = true;
              break;
            }
          }
        } else {
          for (var i = 0; i < elem.options.length; i++) {
            if (elem.options[i].selected && !elem.options[i].value) {
              selected = false;
            }
          }
        }
        if (!selected) {
          no_error = false;
          tooltip = create_tooltip(elem, "Please select an option.");
        }
      } else if (value === undefined || value === null || value === '') {
        elem.className = elem.className + ' _has_error';
        no_error = false;
        tooltip = create_tooltip(elem, "This field is required.");
      }
    }
    if (no_error && elem.name == 'email') {
      if (!value.match(/^[\+_a-z0-9-'&=]+(\.[\+_a-z0-9-']+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i)) {
        elem.className = elem.className + ' _has_error';
        no_error = false;
        tooltip = create_tooltip(elem, "Enter a valid email address.");
      }
    }
    if (no_error && /date_field/.test(elem.className)) {
      if (!value.match(/^\d\d\d\d-\d\d-\d\d$/)) {
        elem.className = elem.className + ' _has_error';
        no_error = false;
        tooltip = create_tooltip(elem, "Enter a valid date.");
      }
    }
    tooltip ? resize_tooltip(tooltip) : false;
    return no_error;
  };
  var needs_validate = function(el) {
    return el.name == 'email' || el.getAttribute('required') !== null;
  };
  var validate_form = function(e) {
    var err = form_to_submit.querySelector('._form_error'), no_error = true;
    err ? err.parentNode.removeChild(err) : false;
    if (!submitted) {
      submitted = true;
      for (var i = 0, len = allInputs.length; i < len; i++) {
        var input = allInputs[i];
        if (needs_validate(input)) {
          if (input.type == 'text') {
            addEvent(input, 'input', function() {
              validate_field(this, true);
            });
          } else if (input.type == 'radio' || input.type == 'checkbox') {
            (function(el) {
              var radios = form_to_submit.elements[el.name];
              for (var i = 0; i < radios.length; i++) {
                addEvent(radios[i], 'click', function() {
                  validate_field(el, true);
                });
              }
            })(input);
          } else if (input.tagName == 'SELECT') {
            addEvent(input, 'change', function() {
              validate_field(input, true);
            });
          }
        }
      }
    }
    remove_tooltips();
    for (var i = 0, len = allInputs.length; i < len; i++) {
      var elem = allInputs[i];
      if (needs_validate(elem)) {
        validate_field(elem) ? true : no_error = false;
      }
    }
    if (!no_error && e) {
      e.preventDefault();
    }
    resize_tooltips();
    return no_error;
  };
  addEvent(window, 'resize', resize_tooltips);
  addEvent(window, 'scroll', resize_tooltips);
  var form_submit = function(e) {
    e.preventDefault();
    if (validate_form()) {
            var serialized = _form_serialize(document.getElementById('_form_1_'));
      _load_script('https://grapevinesocial.activehosted.com/proc.php?' + serialized + '&jsonp=true');
    }
    return false;
  };
  addEvent(form_to_submit, 'submit', form_submit);
  window._old_serialize = null;
  if (typeof serialize !== 'undefined') window._old_serialize = window.serialize;
  _load_script("//d3rxaij56vjege.cloudfront.net/form-serialize/0.3/serialize.min.js", function() {
    window._form_serialize = window.serialize;
    if (window._old_serialize) window.serialize = window._old_serialize;
  });
})();
</script>

<?php
get_footer();
<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */


get_header(); ?>

<div class="containter-fluid blog">
    
    <div class="home-slider">
      
    	<?php if( !isset($_SESSION['user_data']['id']) ): ?>      
      
    		<div class="home-container">

        		<div class="row">
                
			        <div class="col-md-12">
			          
			          <h1>Grapevine The Wine Lovers App</h1>
			          <h2>Personalised to your wine interests</h2>

			        </div><!-- /.col-md-12 -->

			    </div><!-- /.row -->

		        <div class="row">

		          <div class="col-md-6 text-right">

		            <p class="cta-btn white" role="button">
		              <a href="#" type="button" data-toggle="modal" data-target="#signUpModal">
		                Join Our Newsletter
		              </a>
		            </p><!-- /.cta-btn .white -->

		          </div><!-- /.col-sm-6 -->

		          <div class="col-md-6 text-left">

		            <p class="cta-btn white" role="button">
		              <a href="/register" type="button">
		                <?php /*Get Your Tailored Articles */ ?>
		                Customise Your Aritcles 
		              </a>
		            </p><!-- /.cta-btn .white -->

		          </div><!-- /.col-sm-6 -->

		        </div><!-- /.row -->

    		</div><!-- /.home-container -->

    	<?php else: ?>

    		<div class="row single-blog-header">

    			<?php 

    				while( have_posts() ) : the_post();

    					if( has_post_thumbnail( $post -> ID ) ){
				
							$postThumbnailId  = get_post_thumbnail_id( $post -> ID );
							$postThumbnailUrl = wp_get_attachment_url( $postThumbnailId );
							
							echo '<img class="img-responsive single-img-top" src="' . $postThumbnailUrl . '">';
								
						}

						echo '<h1 class="entry-title">' . get_the_title() . '</h1>';

    				endwhile;

    			?>

	        </div><!-- /.row single-event-header -->
    	
    	<?php endif; ?>

    </div><!-- /.home-slider -->

<article>

	<div class="row">

		<div class="col-sm-7 article-content">
			<?php



			
				// Start the Loop.
				while ( have_posts() ) : the_post();

					$user_id = isset($_SESSION['user_data']['id'])    ? $_SESSION['user_data']['id'] : 'public' ;
					$token   = isset($_SESSION['user_data']['token']) ? $_SESSION['user_data']['token'] : 'public' ;

						$comment_data          = array('method' => 'get_comments','post_id' => $post->ID, 'user_id' => $user_id, 'token' => $token, 'origin' => 'web');
						$commentjson           = json_encode($comment_data);
						$comment_request       = get_data(get_site_url().'/API/serve-api.php', array('json' => $commentjson));
						$comment_response_data = json_decode($comment_request, true);
						$comment_response      = $comment_response_data['data'];
					

					if( !isset( $_SESSION['user_data']['id'] ) ){

						echo '<h1 class="entry-title">' . get_the_title() . '</h1>';

					}
				
					the_content();
					
					echo '<div class="clearfix"></div> <!-- New code added for bug fix content overlapping comment -->';
					echo '<hr />';					
/*				
					if ( comments_open() || get_comments_number() ) {

						//comments_template();
						

						echo '<div id="comments" class="comments-area">';

							if ( have_comments() ) : 
								
								echo '<h2 class="comments-title">';
								
								printf( _nx( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'grapevine' ),
											number_format_i18n( get_comments_number() ), get_the_title() );
								
								echo '</h2>';
								echo '<div class="clearfix"></div> <!-- New code added for bug fix content overlapping comment -->';
								echo '<ol class="commentlist">';
								
										wp_list_comments( array(
											'style'       => 'ol',
											'short_ping'  => true,
											'avatar_size' => 56,
											'callback' => 'grapevine_custom_comment' //if( !isset( $_SESSION['user_data']['id'] ) ){
										) );
								
								echo '</ol><!-- .comment-list -->';

							//endif; // have_comments() 

						
								// If comments are closed and there are comments, let's leave a little note, shall we?
								//if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
						
								//echo '<p class="no-comments">' . _e( 'Comments are closed.', 'grapvine' ) . '</p>';
								
							endif;
*/
							//comment_form(); //- only show if logged in 

						//echo '</div><!-- .comments-area -->';

					
						/*
						if( isset($_SESSION['user_data']['id']) ){

							comments_template();

						}else{

							$msg = get_comments_number() >= 1 ? 'Sign in to view comments or add your own' : 'Sign in to be the first to comment';

							echo '<div class="alert alert-purple"><strong>You are not logged in.</strong> <p>' . $msg . '</p></div>';

						}
						*/

					//}
					
					$post_id = $post->ID;
					
				endwhile;
			 ?>
		
		

		<div class="row center-padding-row article-comment-box"> 
            <div class="commentlist">
             <?php   

             	if(count($comment_response) > 0) : 
		          
		           foreach ($comment_response as $commentData) :
		           
		           /*
		           Array
					(
						[formatted_date]  => 1 Sep 2016
						[content]         => On our way to France, we'll see if we can check it out!
						[id]              => 623
						[user_id]         => 196
						[author]          => Pip Barnes
						[author_username] => Slurp
						[date]            => 2016-09-01 09:13:34
						[time]            => 09:13
						[status]          => 1
					)


					Array
					(
					    [user_data] => Array
					        (
								[id]                    => 167
								[email]                 => dave@b60apps.co.uk
								[first_name]            => Dave
								[last_name]             => Henderson
								[name]                  => Dave Henderson
								[username]              => DaveB60
								[user_password]         => 70ccd9007338d6d81dd3b6271621b9cf9a97ea00
								[region]                => London, United Kingdom
								[home_town_coordinates] => {"lat":51.5073509,"lng":-0.1277583}
								[tel_number]            => 
								[interests]             => {"interests":["red","fizz"]}
								[facebook_id]           => 
								[location]              => 1
								[current_location]      => {"lat":"52.307631699999995","lng":"-1.946405"}
								[device_token]          => 1b41fc7d54b3be8cac4ed8386c5ff59c2f0d2b5ce32cb9fe2dc90472a7f863c9
								[device_type]           => iOS
								[token]                 => 57f39e63c0366
								[validation_token]      => 
								[active_campaign_id]    => 166
								[isAdmin]               => 
								[log_data]              => result token578f98c8e520f new token578f98c8e520f
								[gender]                => not-specified
								[country]               =>  United Kingdom
								[m_token]               => 57c54f4043e19
								[log_second]            => 5791de7acb4a5
					        )

					    [user] => user
					)

		            */
		          ?>
            		<div class="comments-wrapper">

            			<p>
			                <?php echo $commentData['formatted_date']; ?> at <?php echo $commentData['time'] ?>
			            </p>
			            <div class="comment-text-area">

			                <p id="commentpara<?php echo $comment_id ?>">
			                    
			                    <?php echo $commentData['content']; ?>
			                </p>

			            </div>

			            <span><?php echo $commentData['author_username']; ?></span>
			    
			    </div>

		    <?php 

		            endforeach;

		        endif;
		    ?>
		    </div><!-- /.commentlist -->

		    <?php 

		        if( isset($_SESSION['user_data']['id']) ){
            	
            		//comment_form();
            		
            ?>

            	<!--h3 id="comments">Leave a reply</h3-->
            		
		        	<div class="comment-form-box">
		                <form role="form" action="<?php /*echo get_site_url() ?>/control */ ?>" method="POST">
		                    <div class="form-group">
		                        <textarea class="form-control" rows="5" id="comment" name="comment_content" placeholder="Write Your comments here" required="true"></textarea>
		                    </div>
		                    <input type="hidden" name="post_id" value="<?php echo $post_id ?>" />
		                    <input type="hidden" name="author" value="<?php echo $_SESSION['user_data']['name']; ?>" />
		                    <input type="hidden" name="author_username" id="author_username" value="<?php echo $_SESSION['user_data']['username']; ?>" />
		                    <input type="hidden" name="email" value="<?php echo $_SESSION['user_data']['email']; ?>" />
		                    <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_data']['id']; ?>" />
		                    <input type="hidden" name="token" value="<?php echo $_SESSION['user_data']['token']; ?>" />
		                    <input type="hidden" name="action" value="add-comment" />
		                    
		                    <button type="submit" class="cta-btn purple">Post Comment</button>


		                </form>

		                
		            </div>

		    <?php

            	}
            
            ?>

            <?php 

            	//trsW04AMDmpd5JuDg64BHXp_zwFUtdF6KZcDarMPPPbj3W2P1eCi3ShVy2ukKs-IZBf0tFVgXNDF8c4D-Cf09A

            	if( !isset($_SESSION['user_data']['id']) ){ // Rja_ISipGgiBK0WQQ5jsd8XSlDbIr1kigEND62uR2Udkeah80SmKlw9eyrWe9bhvFeEnB_iGeuLtSoGYHvCNhQ

						echo '
						<div class="row">

							<div class="col-lg-12">

								<p class="cta-btn purple" role="button" id="signupBtn">
									POST A COMMENT
									<span class="glyphicon glyphicon-menu-right"></span>
								</p>

							</div><!-- /.col-lg-12 -->

						</div><!-- /.row -->
						';

					}

			?>

        </div>

		</div><!-- /.col-sm-7 -->

		<div class="col-sm-5">

			<?php 

				if( is_active_sidebar( 'blog-sidebar' ) ){
				
					dynamic_sidebar( 'blog-sidebar' );  
				
				}

			?>

		</div><!-- /.col-sm-5 -->

	</div><!-- /.row -->

	<div class="modal fade" id="notificationModal" tabindex="-1" role="dialog">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title"></h4>
					      </div>
					      <div class="modal-body">
					        <p></p>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="cta-btn purple" data-dismiss="modal">Continue</button>
					        <!--button type="button" class="btn btn-primary">Save changes</button-->
					      </div>
					    </div><!-- /.modal-content -->
					  </div><!-- /.modal-dialog -->
					</div><!-- /.modal -->


</article>

<?php

get_footer();
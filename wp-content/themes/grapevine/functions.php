<?php


// Add WP 3+ Functions & Theme Support
if( !function_exists( "wp_theme_support" ) ) {  
  function wp_theme_support() {
    add_theme_support( 'post-thumbnails' );      // wp thumbnails (sizes handled in functions.php)
    add_theme_support( 'automatic-feed-links' ); // rss
  }
}
// launching this stuff after theme setup
add_action( 'after_setup_theme','wp_theme_support' );


if( function_exists('register_sidebar') ){
    register_sidebar(
        array(
            'name'          => __( 'Blog Sidebar', 'grapevine' ),
            'id'            => 'blog-sidebar',
            'before_widget' => '<div class="blog-widget">',
            'after_widget'  => '</div>',
            'before_title'  => '<h4>',
            'after_title'   => '</h4>',
        )
    );
}


add_action('wp_enqueue_scripts', 'loadStyles');

function loadStyles(){

    wp_enqueue_style( 'activeCampaignContact', get_template_directory_uri().'/activeCampaignContact.css' );
    
}

function after_setup_theme(){

}

function get_data($url, $request_data)
{
    $response = wp_remote_post($url, array(
	'method' => 'POST',
	'timeout' => 45,
	'redirection' => 5,
	'httpversion' => '1.0',
	'blocking' => true,
	'headers' => array(),
	'body' => $request_data,
	'cookies' => array()
    )
);
    if(count($response) > 0){
    return $response['body'];
    }
 else {
       return array(); 
    }
}

function get_diff_between_dates($since_date) {
    //$ldate_object = strtotime($current_date);
    $lend_object = strtotime($since_date);
    //$lrem = $ldate_object - $lend_object;

    $ldif = time() - $lend_object; //time returns current time in seconds
    $ldays = floor($ldif / (60 * 60 * 24)); //seconds/minut
    $lhours = round(($ldif - $ldays * 60 * 60 * 24) / (60 * 60));
    
    $res = array(
        'days' => $ldays,
        'hours' => $lhours
    );
    
    return $res;
}

function get_day_name($post_date) {
    $date = date("Y-m-d",strtotime($post_date));
   $timestamp = strtotime($date);
    $today = strtotime(date('Y-m-d'));
    $yesterday = strtotime(date('Y-m-d',time() - (24 * 60 * 60)));
    if($timestamp == $today) {
      $date = 'Today';
    } 
    else if($timestamp == $yesterday) {
      $date = 'Yesterday';
    }else{
        $date = date("j M Y", strtotime($post_date));
    }
    return $date;
}


function myprint_r($my_array) {
    $reutrn = '';
    
    $return .= "<table border=1 cellspacing=0 cellpadding=3 width=100%>";
        $return .= '<tr><td colspan=2 style="background-color:#333333;"><strong><font color=white>ARRAY</font></strong></td></tr>';
        foreach ($my_array as $k => $v) {
                $return .= '<tr><td valign="top" style="width:40px;background-color:#F0F0F0;">';
                $return .= '<strong>' . $k . "</strong></td><td>";
                myprint_r($v);
                $return .= "</td></tr>";
        }
        $return .= "</table>";
    
    return $retrun ;
}
/*
 * Send push to all users whose interests match the article tags
 */
function post_published_notification( $post ) {

    //auto-draft_to_publish
/*
    $to = 'loki9421@gmail.com';
            $subject = 'The post was created for the first time!';
            //$body = '<p>post status: <strong>' . $post_obj->post_status . '</strong></p><p>Update (false/true): <strong>' . $update1 . '</strong></p><p>Update (0/1): <strong>' . $update2 . '</strong></p>';
            $body = 'Post created for the first time via auto-draft_to_publish, post id is: ' . $post->ID;
            $headers = array('Content-Type: text/html; charset=UTF-8');
             
            wp_mail( $to, $subject, $body, $headers );
*/

    /*
    Possible values are:
    --------------------
        'publish'    - A published post or page
        'pending'    - post is pending review
        'draft'      - a post in draft status
        'auto-draft' - a newly created post, with no content
        'future'     - a post to publish in the future
        'private'    - not visible to users who are not logged in
        'inherit'    - a revision. see get_children.
        'trash'      - post is in trashbin. added with Version 2.9.
     */

 //  $update1 = $update == false ? 'false' : 'true' ;
 //   $update2 = $update == 0 ? '0' : '1' ;
/*
    $thePost = get_post( $post_id );

    if( $post_obj != null && !empty($post_obj) ){

        if( $post_obj->post_status == 'publish' ){

            
        }

    }
    //echo '<script>alert("post_published_notification running, ID: '.$post_id.'");</script>';

    // $webservice = new Webservice();

*/

    //get_post( $post_id ) == null checks if the post is not yet in the database
    //$thePost = get_post( $post_id );
   

        // send notification as this is the first instance
        $tags       = wp_get_post_tags($post->ID, array('fields' => 'names'));
        $users_data = get_users_by_interests($tags);
      
        foreach ($users_data as $user_data) {
                
            $device_token = $user_data['device_token'];
            $device_type  = $user_data['device_type'];      

            if(!empty($device_token)){
/*
                $to = 'loki9421@gmail.com';
            $subject = 'Push notification would be sent!';
            //$body = '<p>post status: <strong>' . $post_obj->post_status . '</strong></p><p>Update (false/true): <strong>' . $update1 . '</strong></p><p>Update (0/1): <strong>' . $update2 . '</strong></p>';
            $body = 'This would be a push notification';
            $headers = array('Content-Type: text/html; charset=UTF-8');
             
            wp_mail( $to, $subject, $body, $headers );
*/       
                $result = get_data(get_site_url().'/API/push.php', array('device_token' => $device_token, 'device_type' => $device_type, 'message' => 'A new post that matches your interests has been added'));
                
            } 
           
        }

    


}
//add_action( 'save_post', 'post_published_notification', 10, 3 );
//add_action( 'publish_post', 'post_published_notification', 10, 3 );
//add_action('edit_post', 'post_published_notification', 10, 3 );
add_action('draft_to_publish', 'post_published_notification');

/*
function wpse77561_mail_on_publish( $new_status, $old_status, $post ) {
  if (get_post_type($post) !== 'post')
        return;    //Don't touch anything that's not a post (i.e. ignore links and attachments and whatnot )


    $to = 'loki9421@gmail.com';
            $subject = 'The post was created for the first time!';
            //$body = '<p>post status: <strong>' . $post_obj->post_status . '</strong></p><p>Update (false/true): <strong>' . $update1 . '</strong></p><p>Update (0/1): <strong>' . $update2 . '</strong></p>';
            $body = 'New status: ' . $new_status . ', old status: ' . $old_status . ' ';
            $headers = array('Content-Type: text/html; charset=UTF-8');
             
            wp_mail( $to, $subject, $body, $headers );

    //If some variety of a draft is being published, dispatch an email
    if( ( 'draft' === $old_status || 'auto-draft' === $old_status ) && $new_status === 'publish' ) {
        
        

    }
}
*/
//add_action('transition_post_status', 'wpse77561_mail_on_publish');


function get_users_by_interests($interests)
    {
        global $wpdb;

        $results = $wpdb->get_results("SELECT interests, device_token, id, device_type FROM users WHERE device_token IS NOT NULL", ARRAY_A); 
        $res = array();
        
        if(count($results) > 0){
            foreach($results as $user_data){
                $user_interest_data = json_decode($user_data['interests'], true);
                $user_interests = array_shift($user_interest_data);
                $data['device_token'] = $user_data['device_token'];
                $data['user_id'] = $user_data['id'];
                $data['device_type'] = $user_data['device_type'];
                $match = array_intersect($interests, $user_interests);
                
                if(count($match) > 0){
                    array_push($res, $data);
                } 
              
            }
        }
        //echo '<pre>';
        //print_r($res);
        //echo '</pre>';
        //wp_die();
        return $res;

    }
    
    function get_elem_pos($element, $data_arr)
    {
        $pos = search_array_pos($element, $data_arr);
            
        $elem_data = $data_arr[$pos];
        
        return $elem_data;
    }
    
    function search_array_pos($element, $data_arr)
    {
        
        $total = count($data_arr);
        for($x = 0; $x < $total; $x++){
            
            $second_elem = $data_arr[$x];
            
            if($element == trim($second_elem)){
                
            
                return $x;
               
               
            } 
        }
    }
    
    function plugin_add_comment_columns($columns) 
    {
        $columns['username_custom_column'] = __('Username');
        return $columns;
    }

add_filter('manage_edit-comments_columns', 'plugin_add_comment_columns');

function plugin_populate_comment_column( $column, $comment_ID )
{
	if ( 'username_custom_column' == $column ) {
		echo $author = get_comment_author_username( $comment_ID );
	}
}
add_filter( 'manage_comments_custom_column', 'plugin_populate_comment_column', 10, 2 );


function get_comment_author_username($id)
{
    $comment = get_comment($id);
    
    $user_id = $comment->user_id;
    
   $username =  get_username($user_id);
   
   return $username;
}

function get_username($user_id)
{
    global $wpdb;
    
    $res = $wpdb->get_results("SELECT username FROM users WHERE id = '$user_id'", ARRAY_A);
    
    if(count($res) > 0){
        
        $username = $res[0]['username'];
        
        return $username;
    }
}

function get_username_by_email( $user_email ){

    global $wpdb;
    
    $res = $wpdb->get_results("SELECT username FROM users WHERE email = '" . trim($user_email) . "'", ARRAY_A);
    
    if(count($res) > 0){
        
        $username = $res[0]['username'];
        
        return $username;
    }

}




//if ( ! function_exists( 'grapevine_custom_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own twentytwelve_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Twenty Twelve 1.0
 */

function grapevine_custom_comment( $comment, $args, $depth ) { 
  
    $GLOBALS['comment'] = $comment;
    switch ( $comment->comment_type ) :
        case 'pingback' :
        case 'trackback' :
        // Display trackbacks differently than normal comments.
        
        // get the user info from the database because the user data isn't stored in the Wordpress environment.
        $username = get_username_by_email( $comment->comment_author_email );
    ?>

<?php 
    echo 'comment:<pre>';
    print_r($comment);
    echo '</pre>';

    echo ':<pre>';
    print_r($post);
    echo '</pre>';

    echo 'args:<pre>';
    print_r($args);
    echo '</pre>';

    echo 'depth:<pre>';
    print_r($depth);
    echo '</pre>';
 ?>

    <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
        <p><?php _e( 'Pingback:', 'grapevine' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'grapevine' ), '<span class="edit-link">', '</span>' ); ?></p>
    <?php
            break;
        default :
        // Proceed with normal comments.
        global $post;
    ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
        <article id="comment-<?php comment_ID(); ?>" class="comment">
            <header class="comment-meta comment-author vcard">
                <?php
                    echo get_avatar( $comment, 44 );
                    printf( '<cite><b class="fn">%1$s</b> %2$s</cite>',
                        get_comment_author_link(),
                        // If current post author is also comment author, make it known visually.
                        ( $comment->user_id === $post->post_author ) ? '<span>' . __( 'Post author', 'grapevine' ) . '</span>' : ''
                    );
                    printf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
                        esc_url( get_comment_link( $comment->comment_ID ) ),
                        get_comment_time( 'c' ),
                        // translators: 1: date, 2: time 
                        sprintf( __( '%1$s at %2$s', 'grapevine' ), get_comment_date(), get_comment_time() )
                    );
                ?>
            </header><!-- .comment-meta -->

            <?php if ( '0' == $comment->comment_approved ) : ?>
                <p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'grapevine' ); ?></p>
            <?php endif; ?>

            <section class="comment-content comment">
                <?php comment_text(); ?>
                <?php edit_comment_link( __( 'Edit', 'grapevine' ), '<p class="edit-link">', '</p>' ); ?>
            </section><!-- .comment-content -->

            <div class="reply">
                <?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'grapevine' ), 'after' => ' <span>&darr;</span>', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
            </div><!-- .reply -->
        </article><!-- #comment-## -->
    <?php
        break;
    endswitch; // end comment_type check
}

// endif;

//Comments
//add_filter( 'login_url', 'my_login_linkchanger');
/*
function my_login_linkchanger( $link ) {

    return home_url( '/register/' );

}
*/


/*
function customCommentForm($comment, $args, $depth) {

    $username = get_username_by_email( $comment->comment_author_email );

    if ( 'div' === $args['style'] ) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    }
    ?>
    <<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>
    <div class="comment-author vcard">
        <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
        <?php printf( __( '<cite class="fn">'.$username.'</cite> <span class="says">says:</span>' ), get_comment_author_link() ); ?>
    </div>
    <?php if ( $comment->comment_approved == '0' ) : ?>
         <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em>
          <br />
    <?php endif; ?>

    <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
        <?php
        // translators: 1: date, 2: time 
        printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)' ), '  ', '' );
        ?>
    </div>

    <?php comment_text(); ?>

    <div class="reply">
        <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
    </div>
    <?php if ( 'div' != $args['style'] ) : ?>
    </div>
    <?php endif; ?>
    <?php
}
*/

 register_sidebar(
        array(
            'name'          => __( 'News Letter', 'grapevine' ),
            'id'            => 'news-letter',
            'before_widget' => '<div class="blog-widget">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="_form-title">',
            'after_title'   => '</div>',
        )
    );

<?php
/*
  Template Name: Push Notification Template
 */
session_start();
$user_data = '';
if(isset($_SESSION['admin']))
{
    $user_data = $_SESSION['user_data'];
    $user_id = $user_data[0]['id'];
    
    /*$request_data = array('method' => 'get_user_data','user_id' => $user_id);
    $json = json_encode($request_data);
    $request = get_data(get_site_url().'/API/serve-api.php', array('json' => $json));
    
   
     $response_data = json_decode($request, true);
    
    $response = $response_data['data'];
    
    $username = $response[0]['username'];
    $region = $response[0]['region'];
    $email = $response[0]['email'];
    $fname = $response[0]['first_name'];
    $lname = $response[0]['last_name'];
    $phone = $response[0]['tel_number'];*/
    
   
    
    
}
 else {
   // wp_redirect(get_site_url()."/register");
       
       exit;
}

get_header();

include 'admin-menu.php';
?>

<div class="container push-admin-holder">
    
    <ul class="nav nav-tabs">
        <li class="" onclick="toggleOptions('interest-column', 'location-column')"><a href="#">Interests</a></li>
    
    <li onclick="toggleOptions('location-column', 'interest-column')"><a href="#">Location</a></li>
  </ul>
    
    
    <form class="" role="form" action="<?php echo get_site_url() ?>/control" method="POST">
        <input type="hidden" id="red" name="red" value="<?php echo $red ?>"/>
          <input type="hidden" id="white" name="white" value="<?php echo $white ?>" />
          <input type="hidden" id="rose" name="rose" value="<?php echo $rose ?>" />
          <input type="hidden" id="fizz" name="fizz" value="<?php echo $fizz ?>" />
          <input type="hidden" id="dessert" name="dessert" value="<?php echo $dessert ?>" />
          <input type="hidden" id="fortified" name="fortified" value="<?php echo $fortified ?>" />
          <input type="hidden" id="connoisseur" name="connoisseur"value="<?php echo $conn ?>" />
          <input type="hidden" id="travel" name="travel"value="<?php echo $travel ?>" />
          <input type="hidden" id="wine_school" name="wine-school" value="<?php echo $school ?>"/>
          <input type="hidden" id="food" name="food"value="<?php echo $food ?>" />
          <input type="hidden" id="gallery" name="gallery" value="<?php echo $gallery ?>" />
          <input type="hidden" id="social" name="social" value="<?php echo $social ?>"/>
        <input type="hidden" id="baseurl" value="<?php echo get_site_url() ?>" />
        
        <input type="hidden" id="selected-distance" value="20"/>

        <div class="col-md-6 col-holders interest-column" style="padding-left: 0">
            <p class="sign-text"> Send Push by Interests</p>
            <!--<div class="col-md-12 col-pref-container">-->
        <div class="col-md-12" style="padding-left: 10px">
        <div class="row f-row">
         <div class="col-sm-3 col-md-3 col-box image-containerSquare" onclick="setPreferences('red','red', 'red-box')"><div class="click-icon red-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Red.png" ></div>
         <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('white','white', 'white-box')"><div class="click-icon white-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/White.png"></div>
         <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('rose','rose', 'rose-box')"><div class="click-icon rose-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Rose.png"></div>
         <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('fizz','fizz', 'fizz-box')"><div class="click-icon fizz-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Fizz.png"></div>
       </div>
       <div class="row s-row f-row">
         <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('dessert','dessert', 'dessert-box')"><div class="click-icon dessert-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Dessert.png"></div>
         <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('fortified','fortified', 'fortified-box')"><div class="click-icon fortified-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Fortified.png"></div>
         <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('connoisseur','connoisseur', 'connoisseur-box')"><div class="click-icon connoisseur-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Connoisseur.png"></div>
         <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('social','social', 'social-box')"><div class="click-icon social-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Social.png"></div>
       </div>
       <div class="row s-row f-row">
        <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('travel','travel', 'travel-box')"><div class="click-icon travel-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Travel.png"></div>
        <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('wine-school','wine-school', 'wine-school-box')"><div class="click-icon wine-school-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Wine_School.png"></div>
        <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('food','food', 'food-box')"><div class="click-icon food-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Food.png"></div>
        <div class="col-sm-3 col-md-3 image-containerSquare" onclick="setPreferences('gallery','gallery', 'gallery-box')"><div class="click-icon gallery-box"></div><img class="preference-img" src="<?php echo get_template_directory_uri() ?>/preferences/Gallery.png"></div>      
      </div>
     
    </div>

            <div id="clear"></div>
            <div class="form-group push-text-box">

                <textarea class="form-control" id="comment_content" rows="5" style="max-width: 100%" id="comment" name="comment_content" placeholder="Write Your message here" required="true"></textarea>
            </div>

            <button type="button" class="finish-btn" onclick="sendPush()">Send Push</button>
            
    </form>
     <div id="clear"></div>

</div>

<div class="col-md-6 col-holders location-column" style="padding-left: 0">
    <div class="location-box-holder">
        <div class="form-group">
            <label>
                <span class="purple-color"> Please enter the location</span>
            </label>

            <input type="text" class="form-control" id="auto-complete" name="region" placeholder="Your Region"/>
        </div>
        
        <div class="form-group">
        
        <div class="selectDi">
                <!--<span class="selectDefault dist-selectDefault"></span>-->
                <select name="distance-selected" class="form-control selectBox dist-selectBox">
                    <!--<option class="defualt-text dist-defualt-text">Select Distance</option>-->
                    <option value="0">Select Distance</option>
                    <option value="5">Within 5 miles</option>
                    <option value="10">Within 10 miles</option>
                    <option value="20">Within 20 miles</option>
                    <option value="30">Within 30 miles</option>
                    <option value="40">Within 40 miles</option>
                    <option value="50">Within 50 miles</option>
                    <option value="60">Within 60 miles</option>
                </select>
            </div>
            
        </div>
        
        
        <div id="clear"></div>
            <div class="form-group push-text-box">

                <textarea class="form-control" id="message_content" rows="5" style="max-width: 100%" id="comment" name="comment_content" placeholder="Write Your message here" required="true"></textarea>
            </div>
        
        

        <button type="button" class="finish-btn" onclick="sendLocationBasedPush()">Send Push</button>

    </div>

</div>
</div>

<script>
     
     $(function () {
         var defaulttext = $('.dist-defualt-text').text();

    $('.dist-selectDefault').text(defaulttext);

    $('.dist-selectBox').on('change',function(){
       var defaulttext2 = $('.dist-selectBox').find(":selected").text(); 
        $('.dist-selectDefault').text(defaulttext2);
        
        var dist = $(this).val();
            
            $("#selected-distance").val(dist);
           //  buildEvents();
            
    });
           
    });
     
     
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('auto-complete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBv5rxWqJWNXg6R3VgwWKjmB0d4CsJgEAI&libraries=places&callback=initAutocomplete"async defer></script>






<?php

get_footer();
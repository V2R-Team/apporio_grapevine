<?php
/*
  Template Name: Events Template
 */

session_start();

$user_data = '';
$response = '';

$dist = 20;
if(isset($_SESSION['user']))
{
    $user_data = $_SESSION['user_data'];
    $user_id = $user_data['id'];
    $token = $user_data['token'];
    
    $request_data = array('method' => 'post', 'token' => $token, 'origin' => 'web', 'user_id' => $user_id);
    $json = json_encode($request_data);
    $user_request = get_data(get_site_url().'/API/serve-api.php', array('json' => $json));
     $user_response_data = json_decode($user_request, true);
     
     /*
     * Logout user if they are logged in with another device
     */
    if($user_response_data['error_codes'] == -1000)
    {
        
      
    }
     
     $user_response = $user_response_data['data'];
     $coord = $user_response[0]['home_town_coordinates'];
     $coord_data = json_decode($coord, true);
     
     $lat = $coord_data['lat'];
     $lng = $coord_data['lng'];
    
}   
 else {
   
}


get_header();
?>
<section>
    <div class="container-fluid">

        <div class="row event-settings plain-row center-padding-row">

            <input type="hidden" id="user_id" value="<?php echo $user_id ?>" />
            <input type="hidden" id="token" value="<?php echo $token ?>" />
            <input type="hidden" id="lat" value="<?php echo $lat ?>" />
            <input type="hidden" id="lng" value="<?php echo $lng ?>" />
            <input type="hidden" id="current-lat" />
            <input type="hidden" id="current-lng"/>
            <input type="hidden" id="selected-distance" value="20"/>
            <input type="hidden" id="selected-date" value="DESC"/>
            <input type="hidden" id="imurl" value="<?php echo get_template_directory_uri() ?>"/>
            <input type="hidden" id="baseurl" value="<?php echo get_site_url() ?>"/>

            <div class="col-sm-4 col-md-4">
                <div class="radio event-radio-select">
                <input class="placepicker form-control selectBox dist-selectBox" id="selected-location" name="selected-location" placeholder="Enter a location" />
                <!-- <input type="radio" id="home-selection" name="optradio" value="1">
                    <label for="home-selection">
                        <span></span>Home Town
                    </label> -->
                </div>
            </div>
            <!-- 
            <div class="col-sm-2 col-md-2">
                <div class="radio event-radio-select">
                <input type="radio" id="home-selection" name="optradio" value="1">
                    <label for="home-selection">
                        <span></span>Home Town
                    </label>
                </div>
            </div>
            <div class="col-sm-2 col-md-2">
                <div class="radio event-radio-select">
                    <input type="radio" id="current-location-selection" name="optradio" value="2">
                    <label for="current-location-selection">
                        <span></span>Current Location
                    </label>
                </div>
            </div>
            -->
            <div class="col-sm-4 col-md-4 dist-holder">
                <div class="selectDiv">
                    <span class="selectDefault dist-selectDefault"></span>
                    <select name="distance-selected" class="selectBox dist-selectBox">
                        <option class="defualt-text dist-defualt-text">Select Distance</option>
                        <option value="5">Within 5 miles</option>
                        <option value="10">Within 10 miles</option>
                        <option value="15">Within 15 miles</option>
                        <option value="20">Within 20 miles</option>
                        <option value="30">Within 30 miles</option>
                        <option value="40">Within 40 miles</option>
                        <option value="50">Within 50 miles</option>
                        <option value="60">Within 60 miles</option>
                        <option value="70">Within 60 miles</option>
                        <option value="80">Within 80 miles</option>
                        <option value="90">Within 90 miles</option>
                        <option value="100">Within 100 miles</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-4 col-md-4 date-select-holder">
                <div class="selectDiv">
                    <span class="selectDefault date-selectDefault"></span>
                    <select name="date-selected" id="date-selected" class="selectBox date-selectBox">
                        <option class="defualt-text date-defualt-text">Select Date</option>
                        <option value="ASC">Sort by soonest First</option>
                        <option value="DESC">Sort by latest First</option>
                    </select>
                </div>
            </div>

        </div>
        <!-- end of event settings row -->
 


        <div class="row feed-rows center-width-row">
            <div class="colum-holder">
             <?php
                         $ar_fea = array(
                            'post_type'=>'post',
                            'post_status'=>'publish',
                             'tax_query'=>array(
                                  array('taxonomy' =>'category' ,
                                    'terms'=>2
                                   )

                                )
                            
                         );
                         $ar1_fea = new WP_Query($ar_fea);
                         $count = 0 ;
                           // echo "<pre>";
                           //  print_r($ar1_fea);
                           //    echo "</pre>";
                         while ($ar1_fea->have_posts()) { $ar1_fea->the_post();  $url_fea = wp_get_attachment_url( get_post_thumbnail_id($post->ID ));
  
                          ?>
     <div class="col-md-4 feed-column"><a href="event/?pid=<?php echo get_the_id(); ?>">
        <div class="col-top-info"><div class="feed-info-holder">
          <p class="event-title-text"><?php the_title(); ?></p>
             <div class="speech-text"> <?php the_field('location'); ?></div>
             </div>
         <div id="clear"></div>
     <div class="thumb-holder">

      
                            <?php if ($url_fea!="") {?>
                                <img class="" src="<?php echo $url_fea ?>">    
                            <?php }else{?>
                                <img class="" src="<?php the_field('top_image'); ?>">    
                            <?php }?>


     </div>
    </div>
   <div class="col-bottom-bar">
 <div class="read-more"><?php echo date("M d,Y", strtotime(get_field('date'))); ?>  | <?php the_field('time'); ?></div></div></a> 
 </div>
                 <?php } wp_reset_query();  ?>
            </div>
        </div>

    </div>
  
</section>
<script>
    
    
            $(document).ready(function(){
                    /*$('.col-md-2').click(
                        function(e)
                        {*/
                           // alert();
                            $('.newsfeed-holder').removeClass('active');
                            $(".nav-event-holder").addClass('active');
                        /*}
                    );*/
                     });

     $(function () {
           getLocation();
           buildEvents();
         
    var defaulttext = $('.dist-defualt-text').text();

    $('.dist-selectDefault').text(defaulttext);

    $('.dist-selectBox').on('change',function()

    {
        
       var defaulttext2 = $('.dist-selectBox').find(":selected").text(); 
        $('.dist-selectDefault').text(defaulttext2);
        
        var dist = $(this).val();
            
            $("#selected-distance").val(dist);
             buildEvents();
            
    });
    
    
    var date_defaulttext = $('.date-defualt-text').text();

    $('.date-selectDefault').text(date_defaulttext);

    $('.date-selectBox').on('change',function(){
       var date_defaulttext2 = $('.date-selectBox').find(":selected").text(); 
        $('.date-selectDefault').text(date_defaulttext2);
          var selected_date = $(this).val();
            
            $("#selected-date").val(selected_date);
            
             buildEvents();
          
    });
    
    
    $("#home-selection").on('change',function(){
        var location = $(this).val();
     $("#selected-location").val(location);
     
      buildEvents();
    
    });
    
    $("#current-location-selection").on('change',function()

    {

        var location = $(this).val();
     $("#selected-location").val(location);
     
      buildEvents();
    
    });
    
    
   
    
    
    
    });
   // alert(cu);
</script>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>

    <script src="http://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyBjaadb0aRThyQsptY3o_oyUYQMXwMleOc"></script>  
    <script src="<?php echo get_template_directory_uri() ?>/js/jquery.placepicker.js"></script>
 <script>

      $(document).ready(function() {
        // Basic usage
        jQuery(".placepicker").placepicker();

        // Advanced usage
        $("#advanced-placepicker").each(function() {
          var target = this;
          var $collapse = $(this).parents('.form-group').next('.collapse');
          var $map = $collapse.find('.another-map-class');

          var placepicker = $(this).placepicker({
            map: $map.get(0),
            placeChanged: function(place) {
              console.log("place changed: ", place.formatted_address, this.getLocation());
            }
          }).data('placepicker');
        });

      }); // END document.ready

    </script>


<?php
    get_footer();

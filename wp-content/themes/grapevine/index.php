<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>


    <?php if ( have_posts() ) : ?>

        <div class="containter-fluid blog">
    
            <div class="home-slider">
                    
              <div class="home-container">

                <div class="row">
                        
                <div class="col-md-12">
                  
                  <h1>Grapevine The Wine Lovers App</h1>
                  <h2>Personalised to your wine interests</h2>

                </div><!-- /.col-md-12 -->

                </div><!-- /.row -->

                <?php if( !isset($_SESSION['user_data']['id']) ): ?>

                    <div class="row">

                      <div class="col-md-6 text-right">

                        <p class="cta-btn white" role="button">
                          <a href="#" type="button" data-toggle="modal" data-target="#signUpModal">
                            Join Our Newsletter
                          </a>
                        </p><!-- /.cta-btn .white -->

                      </div><!-- /.col-sm-6 -->

                      <div class="col-md-6 text-left">

                        <p class="cta-btn white" role="button">
                          <a href="/register" type="button">
                            <?php /*Get Your Tailored Articles */ ?>
                            Customise Your Aritcles 
                          </a>
                        </p><!-- /.cta-btn .white -->

                      </div><!-- /.col-sm-6 -->

                    </div><!-- /.row -->

                <?php else: ?>

                    <div class="row">

                      <div class="col-lg-12 text-center">

                        <p class="cta-btn white" role="button">
                          <a href="#" type="button" data-toggle="modal" data-target="#signUpModal">
                            Join Our Newsletter
                          </a>
                        </p><!-- /.cta-btn .white -->

                      </div><!-- /.col-sm-12 -->

                    </div><!-- /.row -->

                <?php endif; ?>

              </div><!-- /.home-container -->
        
            </div><!-- /.home-slider -->

            <div class="row default">

                <div class="col-sm-7">

                    <?php /* Start the Loop */ ?>
                    <?php while ( have_posts() ) : the_post(); ?>

                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <header class="entry-header">
                                <h3 class="entry-title">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_title(); ?>
                                    </a>
                                </h3>
                            </header><!-- .entry-header -->

                            <div class="entry-content">
                                <?php the_excerpt(); ?>
                                <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'grapevine' ) . '</span>', 'after' => '</div>' ) ); ?>
                            </div><!-- .entry-content -->
                            <footer class="entry-meta">
                                <?php edit_post_link( __( 'Edit', 'grapevine' ), '<span class="edit-link">', '</span>' ); ?>
                            </footer><!-- .entry-meta -->
                        </article>

                    <?php endwhile; ?>

                </div><!-- /.col-sm-7 -->

                <div class="col-sm-5">

                    <?php 

                        if( is_active_sidebar( 'blog-sidebar' ) ){
                        
                            dynamic_sidebar( 'blog-sidebar' );  
                        
                        }

                    ?>

                </div><!-- /.col-sm-5 -->

            </div><!-- .row -->

            <?php else : ?>

            <div class="row default">

                <div class="col-sm-12">
                    
                    <header class="entry-header">
                        <h1 class="entry-title"><?php _e( 'Nothing Found', 'grapevine' ); ?></h1>
                    </header><!-- .entry-header -->

                    <h2><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'grapvine' ); ?></h2>

                    <div class="searchFrom">
                        <?php get_search_form(); ?>
                    </div><!-- /.searchForm -->

                </div><!-- /.col-sm-12 -->

            </div><!-- .row -->
                        
            <?php endif; ?>

    

<?php get_footer(); ?>






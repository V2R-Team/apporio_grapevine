<?php


/*
  Template Name: Forgot Password Template
 */

$vt = '';
if(isset($_REQUEST['vt'])){
    $vt = $_REQUEST['vt'];
}

$status = '';
if(isset($_REQUEST['rgs'])){
    
    $rgs = $_REQUEST['rgs'];
    
    if($rgs == 1){
        $status = "Your Password has been updated successfully";
    }
    else if($rgs == -1){
         $status = "There was a problem whilst updating your password. Try again";
    }
}
get_header()


?>


<div class="forgot-pass-form">
    <div style="text-align: center; margin-bottom: 10px;"><?php echo $status ?></div>
    <div class="change-pass-form">
        <form  role="form" id="forgot-password-form" action="<?php echo get_site_url() ?>/control" method="POST">
        <div class="form-group">

            <input type="email" class="form-control" id="reset-email" name="email" placeholder="Enter your email" required/>
        </div>
        
        <div class="form-group">

            <input type="password" class="form-control" id="pass1" name="user_pass" placeholder="New Password" required/>
        </div>
        
        <div class="form-group">

            <input type="password" class="form-control" id="pass2" placeholder="Confirm Password" required/>
        </div>


        <input type="hidden" id="" name="action" value="reset-forgot-password"/>
        <input type="hidden" id="" name="vt" value="<?php echo $vt ?>"/>
        <div class="form-group">

            <button type="button" class="save-change-btn" onclick="validate_input('pass1', 'pass2', 'forgot-password-form')">Submit</button>
        </div>

    </form>
    </div>
</div>


<?php
get_footer();

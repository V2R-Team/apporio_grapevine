<?php
/**
 * @package   the_b60_grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 */

// Exit if accessed directly
if (!defined( 'ABSPATH')) { 
	exit;
}

$banner  = '<div id="tg-banner-holder" class="tg-banner-settings tg-banner-sticky">';
	$banner .= '<h2><span>The Grid</span>'.__( 'Global Settings', 'tg-text-domain').'</h2>';
	$banner .= '<div id="tg-buttons-holder">';
		$banner .= '<a class="tg-button" id="tg_settings_save"><i class="dashicons dashicons-yes"></i>'. __( 'Save Settings', 'tg-text-domain') .'</a>';
		$banner .= '<a class="tg-button reset" id="tg_settings_reset"><i class="dashicons dashicons-update"></i>'. __( 'Reset', 'tg-text-domain') .'</a>';
	$banner .= '</div>';
$banner .= '</div>';

echo $banner;
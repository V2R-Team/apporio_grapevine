<?php
/**
 * @package   the_b60_grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 */

// Exit if accessed directly
if (!defined( 'ABSPATH')) { 
	exit;
}

$banner  = '<div id="tg-banner-holder">';
	$banner .= '<h2><span>The Grid</span>'.__( 'Import & Export Settings', 'tg-text-domain').'</h2>';
$banner .= '</div>';

echo $banner;
<?php
/**
 * @package   the_b60_grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 */

// Exit if accessed directly
if (!defined( 'ABSPATH')) { 
	exit;
}

$banner  = '<div id="tg-banner-holder" class="tg-banner-post tg-banner-sticky">';
	$banner .= '<h2><span>The Grid</span>'. __( 'Grid Settings', 'tg-text-domain') .'</h2>';
	$banner .= '<div id="tg-buttons-holder">';
		$banner .= '<a class="tg-button" id="tg_post_save"><i class="dashicons dashicons-yes"></i>'. __( 'Save', 'tg-text-domain') .'</a>';
		$banner .= '<a class="tg-button" id="tg_post_preview"><i class="dashicons dashicons-welcome-view-site"></i>'. __( 'Preview', 'tg-text-domain') .'</a>';
		$banner .= '<a class="tg-button" id="tg_post_delete"><i class="dashicons dashicons-trash"></i>'. __( 'Delete', 'tg-text-domain') .'</a>';
		$banner .= '<a class="tg-button" id="tg_post_close" href="'.admin_url( 'admin.php?page=the_b60_grid').'"><i class="dashicons dashicons-no-alt"></i>'. __( 'Close', 'tg-text-domain') .'</a>';
	$banner .= '</div>';
$banner .= '</div>';

echo $banner;
/*global jQuery:false*/
/*global ajaxurl:false*/
/*global TOMB_JS:false*/
/*global TG_metaData*/

jQuery.noConflict();

(function($) {
				
	"use strict";
	
	$(document).ready(function() {
		
		// ======================================================
		// Export grid select2
		// ======================================================
		
		$('#tg-list-export').select2({
			allowClear: true,
			placeholder: $(this).data('placeholder'),
			width: $(this).data('width'),
			theme: 'classic'
		}).hide();
		
		// ======================================================
		// Close info boxes
		// ======================================================
		
		$(document).on('click','.tg-info-inner, .tg-button.tg-close-infox-box',function(e) {
			if ($('.tg-box-msg').is(':visible') && ($(e.target).is('.tg-info-inner') || $(e.target).is('.tg-close-infox-box'))) {
				$('#tg-info-box').removeClass('tg-box-loading');
				setTimeout(function() {
					$('.tg-info-box-msg').removeClass('tg-box-msg');
				}, 300);
			}
		});
		
		// ======================================================
		// Sticker banner/header
		// ======================================================
		
		var $tg_sticky = $('.tg-banner-sticky');
		if ($tg_sticky.length) {
			var position = $tg_sticky.offset().top-$('#wpadminbar').height();
			var variable = ($tg_sticky.is('.tg-banner-settings')) ? position : 4;
			var $child   = ($tg_sticky.is('.tg-banner-settings')) ? $('.metabox-holder.tg-settings') : $('#the_b60_grid_metabox');
			$(document).scroll(function() {
				if (position <= $(document).scrollTop()) {
					$child.css('margin-top',$tg_sticky.outerHeight(true)+variable+2);
					$tg_sticky.addClass('fixed-tg-banner').width($tg_sticky.parent().width()-2);
					
				} else {
					$tg_sticky.removeClass('fixed-tg-banner').width('');
					$child.css('margin-top', '');
				}
			});
		}
		
    });
	
	// ======================================================
	// Export clear/all select fields
	// ======================================================
	
	$(document).on('click','.tg-list-export-all',function(){
		$('#tg-list-export option').each(function(){
			$(this).prop('selected', true);
		});
		$('#tg-list-export').trigger("change");
	});
	$(document).on('click','.tg-list-export-clear',function(){
		$('#tg-list-export option').each(function(){
			$(this).prop('selected', '');
		});
		$('#tg-list-export').trigger("change");
	});
	
	// ======================================================
	// Import demo
	// ======================================================
	
	var box_load = 'tg-box-loading',
		box_msg  = 'tg-box-msg';

	$(document).on('click','#tg-import-demo', function() {
		var $this    = $(this),
			info_box = '#tg-info-box-general',
			$diving  = $('#tg-info-box-importing-demo'),
			$dived   = $('#tg-info-box-imported-demo'),
			$error   = $('#tg-info-box-import-error');

		var file = $this.data('demo-content');

		if (file) {
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: {
					nonce: tg_admin_global_var.nonce,
					action : 'grid_import_demo',
					file : file,
				},
				beforeSend: function(){
					$('#tg-info-box-general').addClass(box_load);
					$diving.addClass(box_msg);
				},
				error: function() {
					$diving.removeClass(box_msg);
					$error.addClass(box_msg);
					setTimeout(function() {
							$(info_box).removeClass(box_load);
							setTimeout(function() {
								$error.removeClass(box_msg);
							}, 800);
					}, 800);
				},
				success: function(data){
					var list = $(data).find('#tg-grid-list-holder').html();	
					$diving.removeClass('tg-box-msg');
					$dived.addClass('tg-box-msg');
					setTimeout(function() {
							$(info_box).removeClass(box_load);
							$('#tg-grid-list-holder').fadeOut(350);
							setTimeout(function() {
								$('#tg-grid-list-holder').html(list);
								$('#tg-grid-list-holder').fadeIn(350);
							}, 400);	
							setTimeout(function() {
								$dived.removeClass('tg-box-msg');
							}, 300);
					}, 700);
				}
			});
		}
	});

	// ======================================================
	// Import functionnality
	// ======================================================		
		
	$(document).on('click','#tg_post_import', function() {
		var info_box = '#tg-info-box-general',
			$diving  = $('#tg-info-box-importing'),
			$dived   = $('#tg-info-box-imported'),
			$error   = $('#tg-info-box-import-error');

		var fd = new FormData();
		var file = $(document).find('#tg-import-file[type="file"]');
		var individual_file = file[0].files[0];
		fd.append('file', individual_file);
		fd.append('action', 'grid_import');
		fd.append('nonce', tg_admin_global_var.nonce);  
		if (individual_file) {
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: fd,
				contentType: false,
				processData: false,
				beforeSend: function(){
					$(info_box).addClass(box_load);
					$diving.addClass(box_msg);
					$('.tg-import-error').remove();
				},
				error: function() {
					$diving.removeClass(box_msg);
					$error.addClass(box_msg);
					setTimeout(function() {
							$(info_box).removeClass(box_load);
							setTimeout(function() {
								$error.removeClass(box_msg);
							}, 800);
					}, 2000);
				},
				success: function(response){
					var error = $(response).is('.tg-import-error');
					if (error === true) {
						$('#tg-import-file').after($(response));
						$diving.removeClass(box_msg);
						$error.addClass(box_msg);
						setTimeout(function() {
								$(info_box).removeClass(box_load);
								setTimeout(function() {
									$error.removeClass(box_msg);
								}, 800);
						}, 2000);
					} else {
						$diving.removeClass(box_msg);
						$dived.addClass(box_msg);
						setTimeout(function() {
								$(info_box).removeClass(box_load);
								setTimeout(function() {
									$dived.removeClass(box_msg);
								}, 800);
						}, 800);
					}
				}
			});
		}
	});

	// ======================================================
	// Export Grid(s) functionnality
	// ======================================================	
	
	$(document).on('click','#tg_post_export', function() {
		var info_box = '#tg-info-box-general',
			$diving  = $('#tg-info-box-exporting'),
			$dived   = $('#tg-info-box-exported'),
			$error   = $('#tg-info-box-export-error');
		
		var post_ID = []; 
		$('#tg-list-export :selected').each(function(i, selected){ 
			post_ID[i] = $(selected).val(); 
		});

		if (post_ID.length > 0) {
			$.ajax({
				url: ajaxurl,
				type: 'GET',
				contentType: 'application/json',
				dataType : 'json',
				data: {
					nonce: tg_admin_global_var.nonce,
					action: 'grid_export',
					post_ID : post_ID
				},
				beforeSend: function(){
					$(info_box).addClass(box_load);
					$diving.addClass(box_msg);
				},
				error: function() {
					$diving.removeClass(box_msg);
					$error.addClass(box_msg);
					setTimeout(function() {
							$(info_box).removeClass(box_load);
							setTimeout(function() {
								$error.removeClass(box_msg);
							}, 800);
					}, 2000);
				},
				success: function(data){
					$diving.removeClass(box_msg);
					$dived.addClass(box_msg);
					setTimeout(function() {
							$(info_box).removeClass(box_load);
							setTimeout(function() {
								$dived.removeClass(box_msg);
							}, 800);
					}, 800);
					
					data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(data));
					
					var fullDate      = new Date();
					var twoDigitMonth = fullDate.getMonth()+"";if(twoDigitMonth.length==1)  twoDigitMonth="0" +twoDigitMonth;
					var twoDigitDate  = fullDate.getDate()+"";if(twoDigitDate.length==1) twoDigitDate="0" +twoDigitDate;
					var currentDate   = twoDigitDate + "_" + twoDigitMonth + "_" + fullDate.getFullYear();
					
					$('#download-json-grid').attr({'download':'the_b60_grid_'+currentDate+'.json','href':'data:' + data}).get(0).click();
				}
			});
			return false;
		}
	});

	// ======================================================
	// WPML language switcher
	// ======================================================
	
	var info_box   = '#tg-info-box';
	
	$(document).on('click','.the_b60_grid_language span', function() {	
		var $this = $(this);
		$this.closest('.tomb-field').find('.tomb-image-holder, input, img').removeAttr('data-checked checked');
		$this.attr('data-checked',1);
		$this.find('img').attr('data-checked',1);
		$this.find('input').attr('checked',1);
			
		var lang = $this.find('input').val();
		$('#tg_post_save').click();
		$(document).ajaxSuccess(function() {
			setGetParameter('lang',lang);
		});
	});

	function setGetParameter(paramName, paramValue) {
		var url = window.location.href;
		if (url.indexOf(paramName + "=") >= 0) {
			var prefix = url.substring(0, url.indexOf(paramName));
			var suffix = url.substring(url.indexOf(paramName));
			suffix = suffix.substring(suffix.indexOf("=") + 1);
			suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
			url = prefix + paramName + "=" + paramValue + suffix;
		} else {
			if (url.indexOf("?") < 0) {
				url += "?" + paramName + "=" + paramValue;
			} else {
       		 url += "&" + paramName + "=" + paramValue;
			}
    	}
		window.location.href = url;
	}

	// ======================================================
	// Clear Grid Cache
	// ======================================================	
	
	$(document).on('click','#tg_clear_cache', function() {

		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: {
				nonce: tg_admin_global_var.nonce,
				action: 'grid_clear_cache',
			},
			beforeSend: function(){
				$('#tg_clearing_cache_done').hide();
				$('#tg_clearing_cache').show();
			},
			error: function() {
			},
			success: function(){
				$('#tg_clearing_cache').hide();
				$('#tg_clearing_cache_done').show();
			}
		});
		
		return false;
		
	});

	// ======================================================
	// GRID post page save and delete functionnality
	// ======================================================	
	
	$(document).on('click','#tg_post_save, #tg_post_delete', function() {

		var $this = $(this),
			name  = $this.attr('id'),
			break_save = false,
			$diving,
			$dived,
			action;
		
		var post_ID   = $('#post_ID').attr('value');
		
		var meta_data = {},
			meta_ID,
			meta_val;
		$('#the_b60_grid_metabox .tomb-row').each(function() {
			var $this = $(this),
				meta_ID = $this.find('[name]').attr('name');
			meta_ID = (meta_ID) ? meta_ID : '';
			meta_ID = meta_ID.replace('[]', '');
			if ($this.is('.tomb-type-radio')) {
				meta_val = $this.find('[name]:checked').val();
			} else if ($this.is('.tomb-type-image_select')) {
				meta_val = $this.find('input:checked').val();
			} else if ($this.is('.tomb-type-checkbox_list')) {
				meta_val = [];
				$this.find('.tomb-checkbox-list:checked').each(function() {
    				meta_val.push($(this).val());
				});
				meta_val = (meta_val.length === 0) ? null : meta_val;
			} else {
				meta_val = $this.find('[name]').val();
			}	
		
			if (meta_ID !== '') {
				meta_data[meta_ID] =  meta_val;
			}
        });

		if (name.indexOf('save')  > -1) {
			action  = 'save_the_b60_grid';
			$diving = $('#tg-info-box-saving');
			$dived  = $('#tg-info-box-saved');
			if (!$('input#the_b60_grid_name').val().length) {
				$(info_box).addClass(box_load);
				$('#tg-info-box-grid-empty').addClass(box_msg);
				return false;
			}
			
			name  = $('input#the_b60_grid_name').val();
			var grids = $('*[data-grid-name]').data('grid-name').split(',');
			var obj   = {};
			for (var i = 0; i < grids.length; i++) {
				var split = grids[i].split(':');
				obj[split[0]] = split[1];
			}
			$.each(obj, function(key, value) {
				if (key === name &&  value !==  post_ID) {
					$(info_box).addClass(box_load);
					$('#tg-info-box-grid-name').addClass(box_msg);
					break_save = true;
					return false;
				}
			});
			if (break_save === true) {
				return false;
			}
			
			
		} else {
			action = 'delete_the_b60_grid';
			$diving = $('#tg-info-box-deleting');
			$dived  = $('#tg-info-box-deleted');
		}
		
		var $error   = $('#tg-info-box-error');

		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: {
				nonce: tg_admin_global_var.nonce,
				action: action,
				meta_data : meta_data,
				post_ID : post_ID
			},
			beforeSend: function(){
				$(info_box).addClass(box_load);
				$diving.addClass(box_msg);
			},
			error: function() {
				$diving.removeClass(box_msg);
				$error.addClass(box_msg);
				setTimeout(function() {
						$(info_box).removeClass(box_load);
						setTimeout(function() {
							$error.removeClass(box_msg);
						}, 800);
				}, 2000);
			},
			success: function(data){
				if (data && action == 'save_the_b60_grid') {
					$('#post_ID').attr('value',data);
				} else if (action == 'delete_the_b60_grid') {
					$('#tg_post_close')[0].click();
				}
				$diving.removeClass(box_msg);
				$dived.addClass(box_msg);
				setTimeout(function() {
						$(info_box).removeClass(box_load);
						setTimeout(function() {
							$dived.removeClass(box_msg);
						}, 800);
				}, 800);
				
				if ($('#original_post_status').val() === 'auto-draft') {
					var href = window.location.href;
					var lastIndex = href.substr(href.lastIndexOf('/') + 1);
					href = href.replace(lastIndex, 'post.php?post='+$('#post_ID').val()+'&action=edit');
					if (history.pushState) {
						history.pushState(null, null, href);
					} else {
						window.location.href = href;
					}
				}
			}
		});
	
		return false;
	
	});

	// ======================================================
	// GRID list overview page functionnality to sort (ajax)
	// ======================================================
	
	$(document).on('change','.tg-list-number', function(e) {
		e.preventDefault();
		triggerAjax($(this));
	});
	
	$(document).on('click','#tg-grid-list-holder .tg-clone,#tg-grid-list-holder .tg-delete,#tg-grid-list-holder .page-numbers,#tg-grid-list-holder .tg-sort-table span,#tg-grid-list-holder .tg-grid-list-favorite .dashicons', function(e) {	
		e.preventDefault();
		triggerAjax($(this));	
	});
	
	function triggerAjax(el) {
		var $this = el,
			name = $this.attr('class'),
			page_nb = null,
			$diving,
			$dived,
			action;
		// get action name & set main div
		if (name.indexOf('clone')  > -1) {
			action  = 'clone_the_b60_grid';
			$diving = $('#tg-info-box-cloning');
			$dived  = $('#tg-info-box-cloned');
		} else if (name.indexOf('delete')  > -1) {
			action = 'delete_the_b60_grid';
			$diving = $('#tg-info-box-deleting');
			$dived  = $('#tg-info-box-deleted');
			var msg    = $('#tg-info-box-confirm').html();
			var result = confirm(msg);
			if (!result) {
				return false;
			}
		} else if (name.indexOf('numbers')  > -1) {
			action = 'pagenb_the_b60_grid';
			$diving = $('#tg-info-box-refreshing');
			$dived  = $('#tg-info-box-refreshed');
			page_nb   = ($this.attr("href")) ? $this.attr("href").match(/pagenum=([0-9]+)/)[1] : $this.html();
			page_nb   = (!parseInt(page_nb)) ? parseInt($this.prev('.page-numbers').attr("href").match(/pagenum=([0-9]+)/)[1])+1 : page_nb;
		} else if (name.indexOf('number')  > -1) {
			action = 'number_the_b60_grid';
			$diving = $('#tg-info-box-refreshing');
			$dived  = $('#tg-info-box-refreshed');
		} else if (name.indexOf('sort')  > -1) {
			action = 'order_the_b60_grid';
			$diving = $('#tg-info-box-refreshing');
			$dived  = $('#tg-info-box-refreshed');
		} else if (name.indexOf('favorite')  > -1) {
			action = 'favorite_the_b60_grid';
			$diving = $('#tg-info-box-unfavoriting');
			$dived  = $('#tg-info-box-unfavorited');
		} else if (name.indexOf('dashicons')  > -1) {
			action = 'favorite_the_b60_grid';
			$diving = $('#tg-info-box-favoriting');
			$dived  = $('#tg-info-box-favorited');
		} 
		
		var $error   = $('#tg-info-box-error');
		
		// get main info
		var post_ID   = $this.data('grid-id');
		var meta_data = $this.data('favorite');
		var number    = $this.val();
		var order     = $this.data('order');
		var orderby   = $this.closest('th').data('orderby');

		// start ajax request
		$.ajax({
			url: ajaxurl,
			type: 'POST',
			dataType: 'html',
			data: {
				nonce     : tg_admin_global_var.nonce,
				action    : action,
				meta_data : meta_data,
				post_ID   : post_ID,
				page_nb   : page_nb,
				number    : number,
				orderby   : orderby,
				order     : order,
			},
			beforeSend: function(){
				$('#tg-info-box').addClass('tg-box-loading');
				$diving.addClass('tg-box-msg');
			},
			error: function() {
				$diving.removeClass(box_msg);
				$error.addClass(box_msg);
				setTimeout(function() {
						$(info_box).removeClass(box_load);
						setTimeout(function() {
							$error.removeClass(box_msg);
						}, 800);
				}, 2000);
			},
			success: function(data){
				var list = $(data).find('#tg-grid-list-holder .wp-list-table').html();
				if (!list) {
					list = $(data).html();
					$('#tg-grid-list-wrap').fadeOut(350);
					setTimeout(function() {
						$('#tg-grid-list-wrap').html(list).fadeIn(350);
					}, 400);
				} else {
					var page = $(data).find('#tg-grid-list-holder #tg-pagination-holder').html();
					$('#tg-grid-list-holder .wp-list-table').html(list);	
					$('#tg-grid-list-holder #tg-pagination-holder').html(page);
					if (name.indexOf('clone')  > -1) {
						$('#tg-grid-list-holder .wp-list-table tbody tr').first().addClass('cloned');
					}
					$diving.removeClass('tg-box-msg');
					$dived.addClass('tg-box-msg');
					setTimeout(function() {
							$(info_box).removeClass('tg-box-loading');	
							setTimeout(function() {
								$dived.removeClass('tg-box-msg');
							}, 300);
					}, 700);
				}
			}
		});
	}

	// ======================================================
	// Global settings page save settings functionnality
	// ======================================================	
	
	$(document).on('click','#tg_settings_save, #tg_settings_reset', function() {
			
		var $diving  = $('#tg-info-box-settings-saving'),
			$dived   = $('#tg-info-box-settings-saved'),	
			$error   = $('#tg-info-box-settings-error'),
			info_box = '#tg-info-box-general';
		
		var reset = $(this).is('.reset');
		
		if (!reset) {
			$diving  = $('#tg-info-box-settings-saving');
			$dived   = $('#tg-info-box-settings-saved');	
			$error   = $('#tg-info-box-settings-error');
		} else {
			$diving  = $('#tg-info-box-resetting-saving');
			$dived   = $('#tg-info-box-resetting-saved');	
			$error   = $('#tg-info-box-resetting-error');
		}
		
		var setting_data = {},
			setting_val;
		$('.tomb-row').each(function() {
			var $setting     = $(this).find('[name]');
			var setting_name = $setting.attr('name');
			if (reset) {
				setting_val  = $setting.data('default');
			} else {
				setting_val  = $setting.val();
			}
            setting_data[setting_name] =  setting_val;
        });	

		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: {
				nonce: tg_admin_global_var.nonce,
				action: 'save_settings',
				setting_data : setting_data
			},
			beforeSend: function(){
				$(info_box).addClass(box_load);
				$diving.addClass(box_msg);
			},
			error: function() {
				$diving.removeClass(box_msg);
				$error.addClass(box_msg);
				setTimeout(function() {
						$(info_box).removeClass(box_load);
						setTimeout(function() {
							$error.removeClass(box_msg);
						}, 800);
				}, 2000);
			},
			success: function(data){
				$diving.removeClass(box_msg);
				$dived.addClass(box_msg);
				setTimeout(function() {
						$(info_box).removeClass(box_load);
						if (reset) {
							var settings = $(data).html();
							$('.metabox-holder.tg-settings').html(settings);
							TOMB_JS.init();
						}
						setTimeout(function() {
							$dived.removeClass(box_msg);
						}, 800);
				}, 800);
			}
		});
	
		return false;
	
	});

	// ======================================================
	// Grid preview functionnality
	// ======================================================	
	
	var tg_ww = 0,
		tg_colw;
		
	$(document).on('click','.tg-item a, .tg-item-button-play, [data-tolb-src]', function(e) {
		e.preventDefault();
		e.stopPropagation();
		return false;
	});
	
	window.TG_excludeItem = function(url, gkey){ 
		var not_in = $('#the_b60_grid_post_not_in').val();
		var not_arr = not_in.split(', ');
		$('#tg-grid-preview .tg-item-exclude').each(function() {
			var ID = $(this).data('id').toString();
			if($.inArray(ID, not_arr) > -1){
				$(this).prevAll('.tg-item-hidden-overlay').addClass('tg-item-hidden');
				$(this).addClass('tg-item-excluded');
			}
		});
	};
	
	window.TG_metaData = function(url, gkey){ 
		var meta_data = {},
			meta_ID,
			meta_val;
		$('#the_b60_grid_metabox .tomb-row').each(function() {
			var $this = $(this),
				meta_ID = $this.find('[name]').attr('name');
			meta_ID = (meta_ID) ? meta_ID : '';
			meta_ID = meta_ID.replace('[]', '');
			if ($this.is('.tomb-type-radio')) {
				meta_val = $this.find('[name]:checked').val();
			} else if ($this.is('.tomb-type-image_select')) {
				meta_val = $this.find('input:checked').val();
			} else if ($this.is('.tomb-type-checkbox_list')) {
				meta_val = [];
				$this.find('.tomb-checkbox-list:checked').each(function() {
    				meta_val.push($(this).val());
				});
				meta_val = (meta_val.length === 0) ? null : meta_val;
			} else {
				meta_val = $this.find('[name]').val();
			}	
			if (meta_ID !== '') {
				meta_data[meta_ID] =  meta_val;
			}
		});
		meta_data.the_b60_grid_info = $('#post_ID').val(); // for new empty grid!
		meta_data.the_b60_grid_post_not_in = null;
		meta_data.the_b60_grid_video_lightbox = false;
		meta_data.the_b60_grid_ajax_pagination = true;
		return meta_data;
	};
	
	var tg_grid_xhr = null;
	
	$(document).on('click','#tg_post_preview, #tg-grid-preview-refresh', function() {
		
		$.TG_media_destroy($('#tg-grid-preview-inner'));
		$('#tg-grid-preview-loading').show();
		$('body').css('overflow','hidden');
		$('#tg-grid-preview-inner').html('');
		$('.tg-filter-tooltip').remove();
		var meta_data = new TG_metaData();
		
		if (tg_grid_xhr && (tg_grid_xhr.readyState == 3 || tg_grid_xhr.readyState == 2 || tg_grid_xhr.readyState == 1)) {
			return false;
		}

		tg_grid_xhr = $.ajax({
			url: ajaxurl,
			type: 'POST',
			datatype : 'html',
			data: {
				nonce: tg_admin_global_var.nonce,
				action: 'grid_preview',
				meta_data: meta_data
			},
			beforeSend: function(){
				$('#tg-grid-preview').addClass('tg-show-preview');
			},
			error: function(xhr, status, error) {
				console.log(xhr + " :: " + status + " :: " + error);
			},
			success: function(data){
				$('#tg-grid-preview-loading').hide();
				if (data) {
					$('#tg-grid-preview-inner').html(data);
					tg_colw = $('#tg-grid-preview-inner').find('.tg-grid-holder').data('cols');
					if (tg_colw) {
						tg_colw.sort(function(a, b){return b[0]-a[0];});
						$('.tg-grid-preview-mobile').data('val',tg_colw[5][0]);
						$('.tg-grid-preview-tablet-small').data('val',tg_colw[4][0]);
						$('.tg-grid-preview-tablet').data('val',tg_colw[3][0]);
						$('.tg-grid-preview-desktop-small').data('val',tg_colw[2][0]);
						$('.tg-grid-preview-desktop-medium').data('val',tg_colw[1][0]);
						$('.tg-grid-preview-desktop-large').data('val','100%');
					}
					exclude_item();
					grid_preview_width();
					setTimeout(function(){
						$('.preloader-styles').removeAttr('scoped');
						$('.tg-grid-holder').the_b60_grid();
						$.TG_media_init();
					}, 10);
				}
			}
		});
		
	});
	
	$(window).resize(function() {
		grid_preview_width();
	});
	
	function grid_preview_width() {	
		var ww = $(window).width(),
			width,
			view;

		var current_w = $('#tg-grid-preview-viewport .tg-viewport-active').data('val');
			current_w = (current_w == '100%') ? ww : current_w;

		if (tg_ww !== ww && tg_colw) {
			tg_colw[0][0] = tg_ww = ww;
			$.each(tg_colw, function(i, col) {
				if (col[0] >= ww) {
					view  = 6-1-i;
					width = col[0];	
				} else {
					return false;
				}
			});
			if (current_w >= ww) {
				width = (width > tg_colw[1][0] && width > tg_ww) ? tg_ww : width;
				$('#tg-grid-preview-viewport div').removeClass('tg-viewport-active');
				$('#tg-grid-preview-viewport').children().eq(view).show().addClass('tg-viewport-active');
				$('#tg-grid-preview-inner').width(width);
			}
			$('#tg-grid-preview-viewport div').show();
			view = (!view) ? 0 : view;
			if (view+1 < 6) {
				for (var i = view+1; i <= 6; i++) {
					$('#tg-grid-preview-viewport').children().eq(i).hide();
				}
			}
		}
	}

	var tg_debounce_resize = (typeof tg_global_var !== 'undefined' && tg_global_var.debounce) ? 'debouncedresize' : 'resize'; // debounce resize functionnality

	$(document).on('click','#tg-grid-preview-viewport div', function() {
		if ($('#tg-grid-preview-inner .tg-grid-loaded').length > 0) {
			var size = $(this).data('val');
			$('#tg-grid-preview-viewport div').removeClass('tg-viewport-active');
			$(this).addClass('tg-viewport-active');
			$('#tg-grid-preview-inner').width(size);
			$(window).trigger(tg_debounce_resize);
		}
	});
	
	$(document).on('click','#tg-grid-preview-close', function() {
		$('#tg-grid-preview').removeClass('tg-show-preview');
		$('body').css('overflow','visible');
		setTimeout(function() {
			$('#tg-grid-preview-settings').removeClass('loaded');
			$('#tg-grid-preview-inner').html('');
			$('.tg-filter-tooltip').remove();
		}, 400);
	});
	
	$(document).on('click','.tg-page-number', function(e) {
		e.preventDefault();
		e.stopPropagation();
	});
	
	// ======================================================
	// Item preview settings popup (meta data)
	// ======================================================	

	var $item_settings;

	$(document).ready(function() {
    	$item_settings = $('#tg-grid-preview-settings');    
    });
	
	$(document).on('click','.tg-item-settings', function() {
		
		var $this  = $(this);	
		var action = $this.data('action');

		$.ajax({
			type:'GET',
			url: action,
			beforeSend: function(){
				$this.addClass('loading');
				$item_settings.removeClass('loaded');
			},
			success: function(data){
				$this.removeClass('loading');
				data = $(data).find('#the_b60_grid_item_formats').html();
				$('#tg-grid-preview-settings-save').data('id',$this.data('id'));
				$item_settings.find('>*').not('#tg-grid-preview-settings-footer').remove();
				$item_settings.prepend(data);
				$item_settings.find('.hndle').append('<div id="tg-grid-preview-settings-close" class="dashicons dashicons-no-alt"></div>');
				$item_settings.draggable();
				$item_settings.addClass('loaded');
				TOMB_JS.init();
			}
		});
	});
	
	$(document).on('click','#tg-grid-preview-settings-close', function() {
		$item_settings.removeClass('loaded');
	});
	
	$(document).on('click','#tg-grid-preview-settings-save', function() {
		
		var $this     = $(this);
		var post_ID   = $this.data('id');

		var meta_data = {},
			meta_ID,
			meta_val;
		
		$this.closest('#tg-grid-preview-settings').find('.tomb-row').each(function() {
			var $this = $(this);
			meta_ID = $this.find('[name]').attr('name');
			meta_ID = (meta_ID) ? meta_ID : '';
			meta_ID = meta_ID.replace('[]', '');
			if ($this.is('.tomb-type-radio')) {
				meta_val = $this.find('[name]:checked').val();
			} else if ($this.is('.tomb-type-image_select')) {
				meta_val = $this.find('input:checked').val();
			} else {
				meta_val = $this.find('[name]').val();
			}
						
			if (meta_ID !== '') {
				meta_data[meta_ID] =  meta_val;
			}
        });
		
		$.ajax({
			url: ajaxurl,
			type: 'POST',
			dataType: 'html',
			data: {
				nonce     : tg_admin_global_var.nonce,
				action    : 'save_the_b60_grid_item',
				meta_data : meta_data,
				post_ID   : post_ID
			},
			beforeSend: function(){
				$item_settings.addClass('saving');
			},
			success: function(){
				$item_settings.removeClass('saving');
			}
		});
		
	});

	// ======================================================
	// Item Exclude functionnality
	// ======================================================

	function exclude_item() {
		var not_in = $('#the_b60_grid_post_not_in').val();
		var not_arr = not_in.split(', ');
		$('#tg-grid-preview .tg-item-exclude').each(function() {
			var ID = $(this).data('id').toString();
			if($.inArray(ID, not_arr) > -1){
				$(this).prevAll('.tg-item-hidden-overlay').addClass('tg-item-hidden');
				$(this).addClass('tg-item-excluded');
			}
		});
	}
	
	$(document).on('click', '.tg-item-exclude', function() {
		var ID = $(this).data('id').toString();
		var not_in = $('#the_b60_grid_post_not_in').val();
		var not_arr = not_in.split(', ');
		if($.inArray(ID, not_arr) == -1){
			$(this).prevAll('.tg-item-hidden-overlay').addClass('tg-item-hidden');
			$(this).addClass('tg-item-excluded');
			var separator = (not_in === '') ? '' : ', ';
			$('#the_b60_grid_post_not_in').val(not_in+separator+ID);
			
		} else {
			$(this).prevAll('.tg-item-hidden-overlay').removeClass('tg-item-hidden');
			$(this).removeClass('tg-item-excluded');
			not_arr = jQuery.grep(not_arr, function(value) {
				return value != ID;
			});
			not_arr = not_arr.join(', ');
			$('#the_b60_grid_post_not_in').val(not_arr);
		}
	});
	
	// ======================================================
	// Skin Preview Functionnality
	// ======================================================
	
	$(document).on('ready', function() {
		
		$('.tomb-tab-content.Skins').find('#section_skins_start').addClass('has-grid-skin');
		
		var first_maso_skin,
			first_grid_skin,
			std_grid_skin  = 'brasilia',
			std_maso_skin  = 'kampala',
			grid_grid_skin = '#tg-grid-grid-skin',
			maso_grid_skin = '#tg-grid-masonry-skin',
			dft_post_style = $('.the_b60_grid_style input:checked').val(),
			dft_skin_name  = (dft_post_style === 'grid') ?  std_grid_skin : std_maso_skin,
			cur_skin_post  = $('select.tomb-post-type-skin ').val(),
			skin_data_arr  = $.parseJSON($('.tomb-grid-skins').val());
		skin_data_arr  = (skin_data_arr && !$.isEmptyObject(skin_data_arr)) ? skin_data_arr : '';
		
		if ($('#tg-grid-skins').length > 0) {

			$.ajax({
				url: ajaxurl,
				type: 'POST',
				cache: false,
				data: {
					nonce: tg_admin_global_var.nonce,
					action: 'grid_skin_selector'
				},
				error: function(xhr, status, error) {
					console.log(xhr + " :: " + status + " :: " + error);
				},
				success: function(data){
					if (data) {
						$('#tg-grid-skins-loading').remove();
						$('#tg-grid-skins').append(data);
						// check & set std grid/masonry skins
						set_default_skin();
						// set the default skin
						set_post_skin();
						// run post skin event
						post_skin_event();
						$('#tg-grid-grid-skin').find('.tg-grid-holder').the_b60_grid();
						$('#tg-grid-masonry-skin').find('.tg-grid-holder').the_b60_grid();
						hideGridSkin($('.the_b60_grid_style input:checked'));
						var interval = setInterval(function(){ 
							if ($('#tg-grid-skins .tg-grid-loaded').length === 2) {
								$(window).trigger(tg_debounce_resize);
								clearInterval(interval);
							}
						}, 50);
					} else {
						$('#tg-grid-skins-loading').removeClass('loading-anim').text($('#tg-grid-skins-loading').data('no-found'));
					}
				}
			});
			
		}
		
		function post_skin_event() {
			$('.the_b60_grid_style input').on('change', function() {
				hideGridSkin($(this));
				set_post_skin();
			});
			
			$('#the_b60_grid_post_type').on('change', function() {
				update_post_skin($(this));
				set_post_skin();
			});
			
			$('select.tomb-post-type-skin').on('change', function() {
				update_selected_skin($(this).val());
			});
		}
		
		$(document).on('click','#tg-grid-skins .tg-item, #tg-grid-skins .tg-item *, .tg-item-skin-name', function() {
			var $this = $(this);
			if (!$this.hasClass('tg-item-skin-name')) {
				$this = $this.closest('.tg-item').find('.tg-item-skin-name');
			}
			select_post_skin($this);
		});
				
		function hideGridSkin(el) {
			if (el.val() === 'masonry') {
				$('#tg-grid-grid-skin').addClass('skin-hidden');
				$('#tg-grid-masonry-skin').removeClass('skin-hidden');
			} else {
				$('#tg-grid-grid-skin').removeClass('skin-hidden');
				$('#tg-grid-masonry-skin').addClass('skin-hidden');		
			}
		}
		
		function set_default_skin() {
			first_maso_skin = $(maso_grid_skin).find('.tg-item-skin-name').first().data('slug');
			first_grid_skin = $(maso_grid_skin).find('.tg-item-skin-name').first().data('slug');
			std_maso_skin   = ($(maso_grid_skin).find('.tg-item-skin-name[data-slug="'+std_maso_skin+'"]').length > 0) ? std_maso_skin : first_maso_skin;
			std_grid_skin   = ($(grid_grid_skin).find('.tg-item-skin-name[data-slug="'+std_grid_skin+'"]').length > 0) ? std_grid_skin : first_grid_skin;
		}
		
		function set_post_skin() {
			dft_post_style = $('.the_b60_grid_style input:checked').val();
			dft_skin_name  = (dft_post_style === 'grid') ?  std_grid_skin : std_maso_skin;
			
			if (!skin_data_arr) {
				skin_data_arr = {};
				$('.tg-item-skin-name[data-slug="'+std_grid_skin+'"]').closest('.tg-item').addClass('selected');
				$('.tg-item-skin-name[data-slug="'+std_maso_skin+'"]').closest('.tg-item').addClass('selected');
				dft_post_style = $('.the_b60_grid_style input:checked').val();
				$('select.tomb-post-type-skin option').each(function() {
					skin_data_arr[$(this).val()] = dft_skin_name;
				});
				
			} else {
				
				$.each(skin_data_arr, function(key,value){
					if ($('.tg-grid-wrapper:not(.skin-hidden) .tg-item-skin-name[data-slug="'+value+'"]').length === 0) {
						skin_data_arr[key] = dft_skin_name;
					}
				});	
				
			}
			
			$('input.tomb-grid-skins').val(JSON.stringify(skin_data_arr));
			update_selected_skin($('select.tomb-post-type-skin').val());
		}
		
		function select_post_skin($this) {
			var selected = $this.data('slug');
			$('#tg-grid-skins .tg-item').removeClass('selected');
			$this.closest('.tg-item').addClass('selected');
			cur_skin_post = $('select.tomb-post-type-skin').val();
			skin_data_arr[cur_skin_post] = selected;
			$('.tomb-grid-skins').val(JSON.stringify(skin_data_arr));
		}
		
		function update_selected_skin(post_type) {
			var skin = skin_data_arr[post_type];
			$('.tg-item-skin-name[data-slug="'+skin+'"]').trigger('click');
			$('#tg-grid-skins .tg-grid-wrapper:not(.skin-hidden) .tg-filter-active').click();
		}

		function update_post_skin($this) {
			var post_type = ($this.val()) ? $this.val() : ['post'],
				post_name = [],
				data_new_arr = {},
				skin_post_opt = null;

			dft_skin_name = ($('.the_b60_grid_style input:checked').val() === 'grid') ?  std_grid_skin : std_maso_skin;
			for (var i = 0; i < post_type.length; i++) {
				post_name.push($('#the_b60_grid_post_type [value="'+post_type[i]+'"]').text());
			}

			$('select.tomb-post-type-skin option').remove();
			for (var i = 0; i < post_type.length; i++) {
				data_new_arr[post_type[i]] = (skin_data_arr[post_type[i]]) ? skin_data_arr[post_type[i]] : dft_skin_name;
				skin_post_opt += '<option value="'+post_type[i]+'" selected="selected">'+post_name[i]+'</option>';
			}
			skin_data_arr = data_new_arr;
			
			$('.tomb-grid-skins').val(JSON.stringify(skin_data_arr));
			$('select.tomb-post-type-skin').append(skin_post_opt).trigger('change');		
		}
		
	});
	
})(jQuery);
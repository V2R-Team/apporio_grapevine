/*global jQuery:false*/
/*global tinymce:false*/
/*global tg_names:false*/
/*global tg_sc_title:false*/
/*global tg_but_tooltip:false*/
/*global tg_but_url:false*/
/*global tg_but_text:false*/
/*global tg_but_label:false*/
/*global tg_list_tooltip:false*/
/*global tg_list_label:false*/
/*global tg_sc_tooltip:false*/

jQuery.noConflict();

(function() {
	
	"use strict";
	
	jQuery(document).ready(function() {
	
		if (typeof(tinymce) !== 'undefined') {
		
			tinymce.PluginManager.add('the_b60_grid', function(editor) {
	
				var sh_tag = 'the_b60_grid';
			
				//add popup
				editor.addCommand('the_b60_grid_panel_popup', function() {
					//setup defaults
					if (tg_names === '') {
						editor.windowManager.open({
							title      : tg_sc_title,
							fixedWidth : false,
							width      : 550,
							height     : 205,
							popup_css  : false,
							resizable  : false,
							inline     : false,
							autoScroll : false,
							id         : 'tg-shortcode-panel',
							body: [
								{
									type: 'container',
									tooltip: tg_but_tooltip,
									html: '<a class="tg-button" href="'+tg_but_url+'"><i class="dashicons dashicons-plus"></i>'+tg_but_text+'</a>',
									label: tg_but_label,
								}
							]
						});
					} else {
						editor.windowManager.open({
							title      : tg_sc_title,
							fixedWidth : false,
							width      : 550,
							height     : 205,
							popup_css  : false,
							resizable  : false,
							inline     : false,
							autoScroll : false,
							id         : 'tg-shortcode-panel',
							body: [
								{
									type: 'listbox',
									name: 'name',
									icon: 'list-grid',
									label: tg_list_label,
									value: tg_names[0].value,
									'values': tg_names,
									tooltip: tg_list_tooltip
								}
							],
							onsubmit: function( e ) {
								var shortcode_str = '[' + sh_tag + ' name="'+e.data.name+'"]';
								//insert shortcode to tinymce
								editor.insertContent( shortcode_str);
							}
						});
					}
				});
			
				//add button
				editor.addButton('the_b60_grid', {
					text    : 'The Grid',
					tooltip : tg_sc_tooltip,
					icon    : 'icon dashicons-before dashicons-screenoptions',
					onclick : function() {
						editor.execCommand('the_b60_grid_panel_popup','',{
							name   : '',
						});
					}
				});
			
				//open popup on placeholder double click
				editor.on('DblClick',function(e) {
					if (e.target.nodeName == 'IMG' && e.target.className.indexOf('wp-the_b60_grid_panel') > -1) {
						editor.execCommand('the_b60_grid_panel_popup','',{
							name: name
						});
					}
				});
				
			});
		
		}
	
	});

})();

<?php
/**
 * @package   the_b60_grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 *
 * @wordpress-plugin
 * Plugin Name:       The B60 Grid
 * Plugin URI:        http://www.theme-one.com/the-grid/
 * Description:       The B60 Grid - Create advanced grids for any post type with endless possibilities (no programming knowledge required)
 * Version:           1.0.5
 * Author:            Themeone
 * Author URI:        http://www.theme-one.com/
 * Text Domain:       tg-text-domain
 * Domain Path:       /langs
 */
 
// Exit if accessed directly
if (!defined( 'ABSPATH')) { 
	exit;
}

if(!class_exists('The_B60_Grid_Plugin')) {

	class The_B60_Grid_Plugin {
		
		/**
		* @vars string
		* @since 1.0.0
		*/
		public $plugin_version = '1.0.0';
		public $plugin_slug    = 'the_b60_grid';
		public $plugin_prefix  = 'the_b60_grid_';
		
		/**
		 * Cloning disabled
		 * @since 1.0.0
		 */
		private function __clone() {
		}
	
		/**
		 * Serialization disabled
		 * @since 1.0.0
		 */
		private function __sleep() {
		}
	
		/**
		 * De-serialization disabled
		 * @since 1.0.0
		 */
		private function __wakeup() {
		}
	
		/**
	 	* The Grid Constructor
		* @since 1.0.0
	 	*/
		public function __construct() {
			$this->localize_plugin();
			$this->define_constants();
			$this->includes();
			$this->init_hooks();
		}
		
		/**
		* Localize_plugin
		* @since 1.0.0
		*/
		public function localize_plugin() {
			
			load_plugin_textdomain(
				'tg-text-domain',
				FALSE,
				dirname( plugin_basename( __FILE__ ) ) . '/langs'
			);
			
		}
		
		/**
		* Define The Grid Constants
		* @since 1.0.0
		*/
		public function define_constants() {
			define('TG_PLUGIN_PATH', plugin_dir_path( __FILE__ ));
			define('TG_PLUGIN_URL', str_replace('index.php','',plugins_url( 'index.php', __FILE__ )));
			define('TG_VERSION', $this->plugin_version);
			define('TG_SLUG',$this->plugin_slug);
			define('TG_PREFIX', $this->plugin_prefix);
		}
		
		/**
		* Include required core files for Backend/Frontend.
		* @since 1.0.0
		*/
		public function includes() {
			// Update notification
			//require_once(TG_PLUGIN_PATH . '/includes/update/wp_autoupdate.php');
			// Aqua Resizer Class
			require_once(TG_PLUGIN_PATH . '/includes/aqua-resizer.class.php');
			// Attachment taxonomy
			require_once(TG_PLUGIN_PATH . '/includes/media-taxonomies.php');
			// Grid base Class (main functionnalities)
			require_once(TG_PLUGIN_PATH . '/includes/the-grid-base.class.php');
			// Load skins classes
			require_once(TG_PLUGIN_PATH . '/includes/item-skin.class.php');
			require_once(TG_PLUGIN_PATH . '/includes/preloader-skin.class.php');
			require_once(TG_PLUGIN_PATH . '/includes/navigation-skin.class.php');
			require_once(TG_PLUGIN_PATH . '/includes/item-animation.class.php');
			// post like class
			require_once(TG_PLUGIN_PATH . '/includes/post-like/post-like.php');
			// Load frontend classes
			require_once(TG_PLUGIN_PATH . '/frontend/the-grid.class.php');
			require_once(TG_PLUGIN_PATH . '/includes/first-media.class.php');
			require_once(TG_PLUGIN_PATH . '/frontend/the-grid-style.class.php');
			require_once(TG_PLUGIN_PATH . '/frontend/the-grid-content.class.php');
			// Load backend classes
			if (is_admin()) {
				require_once(TG_PLUGIN_PATH . '/includes/default-post.class.php');
				require_once(TG_PLUGIN_PATH . '/includes/custom-fields.class.php');
				require_once(TG_PLUGIN_PATH . '/backend/the-grid-admin.php');
				require_once(TG_PLUGIN_PATH . '/includes/wpml.class.php');	
			}
			// Register shortcode & add Tinymce button/popup & add Visual Composer element
			require_once(TG_PLUGIN_PATH . '/backend/the-grid-shortcode.php');
		}
		
		/**
		* Hook into actions and filters
		* @since 1.0.0
		*/
		public function init_hooks() {
			add_action( 'after_setup_theme', array( &$this, 'add_image_size' ) );
			add_action( 'init', array( &$this, 'register_post_type' ) );
			add_action( 'current_screen', array( &$this, 'post_formats' ) );
			//add_filter( 'update_post_metadata', array( &$this, 'disable_PostLock'), 10, 5 );
			add_filter( 'plugin_action_links_'. plugin_basename(__FILE__), array( &$this, 'action_links') );
		}
		
		/**
		* Add image sizes to Wordpress
		* @since 1.0.0
		*/
		public function add_image_size() {
			// get images size from global settings
			$s1_w = get_option('the_b60_grid_size1_width', 500);
			$s1_h = get_option('the_b60_grid_size1_height', 500);
			$s1_c = get_option('the_b60_grid_size1_crop', true);
			$s2_w = get_option('the_b60_grid_size2_width', 500);
			$s2_h = get_option('the_b60_grid_size2_height', 1000);
			$s2_c = get_option('the_b60_grid_size2_crop', true);
			$s3_w = get_option('the_b60_grid_size3_width', 1000);
			$s3_h = get_option('the_b60_grid_size3_height', 500);
			$s3_c = get_option('the_b60_grid_size3_crop', true);
			$s4_w = get_option('the_b60_grid_size4_width', 1000);
			$s4_h = get_option('the_b60_grid_size4_height', 1000);
			$s4_c = get_option('the_b60_grid_size4_crop', true);
			$s5_w = get_option('the_b60_grid_size5_width', 500);
			$s5_h = get_option('the_b60_grid_size5_height', 99999);
			$s5_c = get_option('the_b60_grid_size5_crop', '');
			// settings images size from global settings
			add_image_size( 'the_b60_grid_size1', $s1_w, $s1_h, $s1_c );
			add_image_size( 'the_b60_grid_size2', $s2_w, $s2_h, $s2_c );
			add_image_size( 'the_b60_grid_size3', $s3_w, $s3_h, $s3_c );  
			add_image_size( 'the_b60_grid_size4', $s4_w, $s4_h, $s4_c );
			add_image_size( 'the_b60_grid_size5', $s5_w, $s5_h, $s5_c );
		}
		
		/**
		* Register post type
		* @since 1.0.0
		*/
		public function register_post_type() {		
			$labels = array(
				'name'          => __( 'The B60 Grid', 'taxonomy general name', 'tg-text-domain'),
				'singular_name' => __( 'the_b60_grid', 'tg-text-domain'),
				'search_items'  =>  __( 'Search the_b60_grid', 'tg-text-domain'),
				'all_items'     => __( 'All the_b60_grid', 'tg-text-domain'),
				//'parent_item'   => __( 'Parent the_b60_grid', 'tg-text-domain'),
				'edit_item'     => false,
				'update_item'   => false,
				'add_new_item'  => false,
				'menu_name'     => __( 'The B60 Grid', 'tg-text-domain')
			 );
			 	 
			 $args = array(
					'labels'          => $labels,
					'singular_label'  => __('The B60 Grid', 'tg-text-domain'),
					'public'          => true,
					'capability_type' => 'post',
					'query_var'       => true,
					'rewrite'         => true,
					'show_ui'         => true,
					'show_in_menu'    => false,
					'hierarchical'    => false,
					'menu_position'   => 10,
					'menu_icon'       => 'dashicons-slides',
					'supports'        => false,
					'rewrite'         => array(
						'slug' => $this->plugin_slug,
						'with_front' => FALSE
					),
			);
			register_post_type( $this->plugin_slug, $args );
			remove_post_type_support( $this->plugin_slug, 'title' );
			remove_post_type_support( $this->plugin_slug, 'editor' );
		}
		
		/**
		* Add post formats to any post types
		* @since 1.0.5
		*/
		public function post_formats() {
			$post_format = get_option('the_b60_grid_post_formats', false);
			// add post formats support if option enable in global settings
			if ($post_format == true) {
				// post formats supported by The Grid Plugin
				add_theme_support('post-formats', array('gallery', 'video', 'audio', 'quote', 'link'));
				// retireve all post types
				$post_types = the_b60_grid_Base::get_all_post_types();
				// remove post format for attacment post type
				unset($post_types['attachment']);
				foreach ($post_types as $slug => $name) {
					add_post_type_support($slug, 'post-formats');
				}
			}
		}
		
		/**
		* Add edit link on plugin activation
		* @since 1.0.0
		*/
		public function action_links($links) {
			$mylinks = array(
 				'<a href="' . admin_url( 'admin.php?page=the_b60_grid' ) . '">Edit</a>',
 			);
			return array_merge( $links, $mylinks );
		}
		
		/**
		* Disable post_lock for the_b60_grid
		* @since 1.0.0
		*/
		public function disable_PostLock() {
			if (get_current_screen() && get_current_screen()->post_type == $this->plugin_slug) {
        		return false;
			}
		}

	}
	
	new The_B60_Grid_Plugin;

}
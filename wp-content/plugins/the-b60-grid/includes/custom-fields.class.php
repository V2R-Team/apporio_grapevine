<?php
/**
 * @package   the_b60_grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 */

// Exit if accessed directly
if (!defined( 'ABSPATH')) { 
	exit;
}

class the_b60_grid_Custom_Fields {
	
	/**
	* Get post ID
	* @since 1.0.0
	*/
	public static function get_post_ID() {
		$post_ID = isset($_GET['post']) ? $_GET['post'] : get_the_ID();
		return $post_ID;
	}
	
	/**
	* Build preloader preview for grid settings
	* @since 1.0.0
	*/
	public static function preloader_config() {
		
		$post_ID = self::get_post_ID();
		
		$prefix = TG_PREFIX;
		
		$preloader_base  = new the_b60_grid_preloader_skin();
		
		$preloader_skins = $preloader_base->get_preloader_name();
		$preloader_curr  = get_post_meta($post_ID, $prefix.'preloader_style', true);
		$preloader_curr  = (!empty($preloader_curr)) ? $preloader_curr : 'square-grid-pulse';
		$preloader_color = get_post_meta($post_ID, $prefix.'preloader_color', true);
		$preloader_color = (!empty($preloader_color)) ? $preloader_color : '#34495e';
		$preloader_color = '.the_b60_grid_preloader_preview .tg-grid-preloader-inner>div{background:'.$preloader_color.';border-color:'.$preloader_color.';}';
		$preloader_size  = get_post_meta($post_ID, $prefix.'preloader_size', true);
		$preloader_size  = (!empty($preloader_size)) ? $preloader_size : 1;
		$preloader_size  = '.the_b60_grid_preloader_preview .tg-grid-preloader-scale{transform:scale('.$preloader_size.')}';
		
		$preloader = null;
		$preloader_css  = null;
		foreach ($preloader_skins as $preloader_skin => $param) {
			$preloader_name  = $preloader_skin;
			$preloader_skin  = $preloader_base->$preloader_name();
			$preloader_css  .= $preloader_skin['css'];
			$preloader_show = ($preloader_name == $preloader_curr) ? ' show' : ' hide';
			$preloader .= '<div class="tg-grid-preloader-inner '.$preloader_name.$preloader_show.'">';	
			$preloader .= $preloader_skin['html'];
			$preloader .= '</div>';
		}
		$preloader_html  = '<style class="preloader-styles" type="text/css" scoped>'.$preloader_css.$preloader_color.$preloader_size.'</style>';
		$preloader_html .= '<div class="tg-grid-preloader ">';
			$preloader_html .= '<div class="tg-grid-preloader-holder">';
				$preloader_html .= '<div class="tg-grid-preloader-scale">';
					$preloader_html .= $preloader;
				$preloader_html .= '</div>';
			$preloader_html .= '</div>';
		$preloader_html .= '</div>';
		
		return $preloader_html;
	}
	
	/**
	* Build filter section for grid settings
	* @since 1.0.0
	*/
	public static function available_filters_config() {
		
		$post_ID = self::get_post_ID();
		
		$prefix = TG_PREFIX;
		
		$number = get_post_meta($post_ID, $prefix.'filters_number', true);
		
		$filters = '<div id="tg-available-filters-holder" data-delete-msg="'. __( 'Are you sure you want to delete this filter?', 'tg-text-domain' ) .'">';
			$filters .= '<h3 class="tg-filter-title">'. __( 'Available Filter item(s)', 'tg-text-domain' ) .'</h3>';
			$filters .= '<div id="tg-available-filters">';
				$filters .= '<ul id="tg-filter-sort1" class="connectedSortable"></ul>';
			$filters .= '</div>';
		$filters .= '</div>';
		$filters .= '<input type="hidden" name="the_b60_grid_filters_number" value=\''.$number.'\'>';
		
		$filters .= '<div class="tg-filter-holder-number">';
		for ($i = 2; $i <= $number; $i++) {
			$filters .= '<div class="tg-filter-holder-number-'.$i.'">';
				$filters .= '<input type="hidden" data-input="the_b60_grid_filters_order_'.$i.'" value=\''.get_post_meta($post_ID, $prefix.'filters_order_'.$i, true).'\'>';
				$filters .= '<input type="hidden" data-input="the_b60_grid_filter_type_'.$i.'" value=\''.get_post_meta($post_ID, $prefix.'filter_type_'.$i, true).'\'>';
				$filters .= '<input type="hidden" data-input="the_b60_grid_filter_dropdown_title_'.$i.'" value=\''.get_post_meta($post_ID, $prefix.'filter_dropdown_title_'.$i, true).'\'>';
				$filters .= '<input type="hidden" data-input="the_b60_grid_filter_all_text_'.$i.'" value=\''.get_post_meta($post_ID, $prefix.'filter_all_text_'.$i, true).'\'>';
				$filters .= '<input type="hidden" data-input="the_b60_grid_filter_count_'.$i.'" value=\''.get_post_meta($post_ID, $prefix.'filter_count_'.$i, true).'\'>';
				$filters .= '<input type="hidden" data-input="the_b60_grid_filters_'.$i.'" value=\''.get_post_meta($post_ID, $prefix.'filters_'.$i, true).'\'>';
			$filters .= '</div>';
		}
		$filters .= '</div>';
		
		return $filters;
	}
	
	/**
	* Build active filter section for grid settings
	* @since 1.0.0
	*/
	public static function active_filters_config() {
		
		$post_ID = self::get_post_ID();
		
		$prefix = TG_PREFIX;
		
		$filters = '<div id="tg-active-filters">';
			$filters .= '<label class="tomb-label">'.__( 'Active Filter(s)', 'tg-text-domain' ).'</label>';
			$filters .= '<ul class="tg-filter-sort2 connectedSortable"></ul>';
			$filters .= '<span class="tg-filter-button-remove">'.__( 'Remove all', 'tg-text-domain' ).'</span> / ';
			$filters .= '<span class="tg-filter-button-add">'.__( 'Add all', 'tg-text-domain' ).'</span>';
		$filters .= '</div>';
		$filters .= '<input type="hidden" name="the_b60_grid_filters_1" value=\''.get_post_meta($post_ID, $prefix.'filters_1', true).'\'>';
		
		return $filters;
	}
	
	/**
	* Build drag/drop layout builder
	* @since 1.0.0
	*/
	public static function grid_layout_config() {
		
		$post_ID = self::get_post_ID();
		
		$prefix = TG_PREFIX;
	
		$meta_area_top1    = get_post_meta($post_ID, $prefix.'area_top1', true);
		$meta_area_top2    = get_post_meta($post_ID, $prefix.'area_top2', true);
		$meta_area_left    = get_post_meta($post_ID, $prefix.'area_left', true);
		$meta_area_right   = get_post_meta($post_ID, $prefix.'area_right', true);
		$meta_area_bottom1 = get_post_meta($post_ID, $prefix.'area_bottom1', true);
		$meta_area_bottom2 = get_post_meta($post_ID, $prefix.'area_bottom2', true);
		
		$grid_layout_buts  = '<div class="tg-layout-setting-buttons">';
			$grid_layout_buts .= '<div class="tg-layout-align-left dashicons dashicons-editor-alignleft" data-align="txt-left" data-val="left"></div>';
			$grid_layout_buts .= '<div class="tg-layout-align-center dashicons dashicons-editor-aligncenter" data-align="txt-center" data-val="center"></div>';
			$grid_layout_buts .= '<div class="tg-layout-align-right dashicons dashicons-editor-alignright" data-align="txt-right" data-val="right"></div>';
			$grid_layout_buts .= '<div class="tg-layout-area-settings dashicons dashicons-admin-generic"></div>';
		$grid_layout_buts .= '</div>';
		
		$grid_layout_buts2  = '<div class="tg-layout-setting-buttons tg-simple-setting">';
			$grid_layout_buts2 .= '<div class="tg-layout-area-settings dashicons dashicons-admin-generic"></div>';
		$grid_layout_buts2 .= '</div>';
		
		$grid_layout  = '<div id="tg-layout-wrapper">';
		
			$grid_layout .= '<div class="tg-layout-blocs-wrapper">';
				$grid_layout .= '<label class="tomb-label">'.__( 'Available blocs', 'tg-text-domain' ).'</label>';
				$grid_layout .= '<ul id="tg-layout-blocs-holder" class="tg-layout-connected">';
					$grid_layout .= '<li class="tg-layout-bloc tg-layout-search dashicons-search" data-func="the_b60_grid_get_search_field">'.__( 'Search', 'tg-text-domain' ).'</li>';
					$grid_layout .= '<li class="tg-layout-bloc tg-layout-filter dashicons-admin-settings" data-func="the_b60_grid_get_filters_1">'.__( 'Filter', 'tg-text-domain' ).' - <span class="tg-filter-func-nb">1</span></li>';
					$grid_layout .= '<li class="tg-layout-bloc tg-layout-sorter dashicons-randomize" data-func="the_b60_grid_get_sorters">'.__( 'Sort', 'tg-text-domain' ).'</li>';
					$grid_layout .= '<li class="tg-layout-bloc tg-layout-load-more dashicons-update" data-func="the_b60_grid_get_ajax_button">'.__( 'Load more', 'tg-text-domain' ).'</li>';
					$grid_layout .= '<li class="tg-layout-bloc tg-layout-pagination dashicons-media-default" data-func="the_b60_grid_get_pagination">'.__( 'Pagination', 'tg-text-domain' ).'</li>';
					$grid_layout .= '<li class="tg-layout-bloc tg-layout-arrow-left dashicons-arrow-left-alt2 tg-bloc-center" data-func="the_b60_grid_get_left_arrow"></li>';
					$grid_layout .= '<li class="tg-layout-bloc tg-layout-arrow-right dashicons-arrow-right-alt2 tg-bloc-center" data-func="the_b60_grid_get_right_arrow"></li>';
					$grid_layout .= '<li class="tg-layout-bloc tg-layout-slider-bullets dashicons-marker" data-func="the_b60_grid_get_slider_bullets">'.__( 'Slider bullets', 'tg-text-domain' ).'</li>';
				$grid_layout .= '</ul>';
			$grid_layout .= '</div>';
			
			$grid_layout .= '<div class="tg-layout-blocs-wrapper">';
			
				$grid_layout .= '<label class="tomb-label">'.__( 'Grid Layout', 'tg-text-domain' ).'</label>';
				
				$grid_layout .= '<div id="tg-layout-blocs-entries">';
				
					$grid_layout .= '<div id="tg-layout-top-area-1" class="tg-layout-area">';
						$grid_layout .= $grid_layout_buts;
						$grid_layout .= '<span class="tg-area-name">'.__( 'Top Area 1', 'tg-text-domain' ).'</span>';
						$grid_layout .= '<ul id="tg-layout-top-area-1-holder" class="tg-layout-connected"></ul>';
						$grid_layout .= '<div class="tomb-row" id="the_b60_grid_area_top1">';
							$grid_layout .= '<input type="hidden" name="the_b60_grid_area_top1" data-value=\''.$meta_area_top1.'\' value=\''.$meta_area_top1.'\'>';
						$grid_layout .= '</div>';
					$grid_layout .= '</div>';
					
					$grid_layout .= '<div id="tg-layout-top-area-2" class="tg-layout-area">';
						$grid_layout .= $grid_layout_buts;
						$grid_layout .= '<span class="tg-area-name">'.__( 'Top Area 2', 'tg-text-domain' ).'</span>';
						$grid_layout .= '<ul id="tg-layout-top-area-2-holder" class="tg-layout-connected"></ul>';
						$grid_layout .= '<div class="tomb-row" id="the_b60_grid_area_top2">';
							$grid_layout .= '<input type="hidden" name="the_b60_grid_area_top2" data-value=\''.$meta_area_top2.'\' value=\''.$meta_area_top2.'\'>';
						$grid_layout .= '</div>';
					$grid_layout .= '</div>';
					
					$grid_layout .= '<div id="tg-layout-center-area">';
					
						$grid_layout .= '<div id="tg-layout-center-bloc-holder">';
							$grid_layout .= '<div class="tg-layout-center-bloc"></div>';
							$grid_layout .= '<div class="tg-layout-center-bloc"></div>';
							$grid_layout .= '<div class="tg-layout-center-bloc"></div>';
							$grid_layout .= '<div class="tg-layout-center-bloc"></div>';
							$grid_layout .= '<div class="tg-layout-center-bloc"></div>';
							$grid_layout .= '<div class="tg-layout-center-bloc"></div>';
						$grid_layout .= '</div>';
						
						$grid_layout .= '<div id="tg-layout-center-left" class="tg-layout-area">';
							$grid_layout .= $grid_layout_buts2;
							$grid_layout .= '<span class="tg-area-name">'.__( 'Left Area', 'tg-text-domain' ).'</span>';
							$grid_layout .= '<ul id="tg-layout-center-left-holder" class="tg-layout-connected tg-exclude"></ul>';
							$grid_layout .= '<div class="tomb-row" id="the_b60_grid_area_left">';
								$grid_layout .= '<input type="hidden" name="the_b60_grid_area_left" data-value=\''.$meta_area_left.'\' value=\''.$meta_area_left.'\'>';
							$grid_layout .= '</div>';
						$grid_layout .= '</div>';
						
						$grid_layout .= '<div id="tg-layout-center-right" class="tg-layout-area">';
							$grid_layout .= $grid_layout_buts2;
							$grid_layout .= '<span class="tg-area-name">'.__( 'Right Area', 'tg-text-domain' ).'</span>';
							$grid_layout .= '<ul id="tg-layout-center-right-holder" class="tg-layout-connected tg-exclude"></ul>';
							$grid_layout .= '<div class="tomb-row" id="the_b60_grid_area_right">';
								$grid_layout .= '<input type="hidden" name="the_b60_grid_area_right" data-value=\''.$meta_area_right.'\' value=\''.$meta_area_right.'\'>';
							$grid_layout .= '</div>';
						$grid_layout .= '</div>';
	
					$grid_layout .= '</div>';
				
					$grid_layout .= '<div id="tg-layout-bottom-area-1" class="tg-layout-area">';
						$grid_layout .= $grid_layout_buts;
						$grid_layout .= '<span class="tg-area-name">'.__( 'Bottom Area 1', 'tg-text-domain' ).'</span>';
						$grid_layout .= '<ul id="tg-layout-bottom-area-1-holder" class="tg-layout-connected"></ul>';
						$grid_layout .= '<div class="tomb-row" id="the_b60_grid_area_bottom1">';
							$grid_layout .= '<input type="hidden" name="the_b60_grid_area_bottom1" data-value=\''.$meta_area_bottom1.'\' value=\''.$meta_area_bottom1.'\'>';
						$grid_layout .= '</div>';
					$grid_layout .= '</div>';
					
					$grid_layout .= '<div id="tg-layout-bottom-area-2" class="tg-layout-area">';
						$grid_layout .= $grid_layout_buts;
						$grid_layout .= '<span class="tg-area-name">'.__( 'Bottom Area 2', 'tg-text-domain' ).'</span>';
						$grid_layout .= '<ul id="tg-layout-bottom-area-2-holder" class="tg-layout-connected"></ul>';
						$grid_layout .= '<div class="tomb-row" id="the_b60_grid_area_bottom2">';
							$grid_layout .= '<input type="hidden" name="the_b60_grid_area_bottom2" data-value=\''.$meta_area_bottom2.'\' value=\''.$meta_area_bottom2.'\'>';
						$grid_layout .= '</div>';
					$grid_layout .= '</div>';
				
				$grid_layout .= '</div>';
				
			$grid_layout .= '</div>';
		$grid_layout .= '</div>';
		
		$grid_layout .= '<div id="tg-layout-styles-box">';
			$grid_layout .= '<h3 id="tg-layout-styles-header"><i class="dashicons dashicons-art"></i>'.__('Styles', 'tg-text-domain' ).'<i class="dashicons dashicons-no-alt"></i></h3>';
			$grid_layout .= '<label class="tomb-label">'.__( 'Margin top:', 'tg-text-domain' ).'</label>';
			$grid_layout .= '<input type="number" class="tomb-text number mini" id="the-grid-area-margintop" data-name="margin-top" value="0" step="1" min="-200">';
			$grid_layout .= '<label class="tomb-number-label">px</label>';
			$grid_layout .= '<label class="tomb-label">'.__( 'Margin bottom:', 'tg-text-domain' ).'</label>';
			$grid_layout .= '<input type="number" class="tomb-text number mini" id="the-grid-area-marginbot" data-name="margin-bottom" value="0" step="1" min="-200">';
			$grid_layout .= '<label class="tomb-number-label">px</label>';
			$grid_layout .= '<label class="tomb-label">'.__( 'Margin left:', 'tg-text-domain' ).'</label>';
			$grid_layout .= '<input type="number" class="tomb-text number mini" id="the-grid-area-marginleft" data-name="margin-left" value="0" step="1" min="-200">';
			$grid_layout .= '<label class="tomb-number-label">px</label>';
			$grid_layout .= '<label class="tomb-label">'.__( 'Margin right:', 'tg-text-domain' ).'</label>';
			$grid_layout .= '<input type="number" class="tomb-text number mini" id="the-grid-area-marginright" data-name="margin-right" value="0" step="1" min="-200">';
			$grid_layout .= '<label class="tomb-number-label">px</label>';
			$grid_layout .= '<label class="tomb-label">'.__( 'Padding top:', 'tg-text-domain' ).'</label>';
			$grid_layout .= '<input type="number" class="tomb-text number mini" id="the-grid-area-paddingtop" data-name="padding-top" value="0" step="1" min="0">';
			$grid_layout .= '<label class="tomb-number-label">px</label>';
			$grid_layout .= '<label class="tomb-label">'.__( 'Padding bottom:', 'tg-text-domain' ).'</label>';
			$grid_layout .= '<input type="number" class="tomb-text number mini" id="the-grid-area-paddingbot" data-name="padding-bottom" value="0" step="1" min="0">';
			$grid_layout .= '<label class="tomb-number-label">px</label>';
			$grid_layout .= '<label class="tomb-label">'.__( 'Padding left:', 'tg-text-domain' ).'</label>';
			$grid_layout .= '<input type="number" class="tomb-text number mini" id="the-grid-area-paddingleft" data-name="padding-left" value="0" step="1" min="0">';
			$grid_layout .= '<label class="tomb-number-label">px</label>';
			$grid_layout .= '<label class="tomb-label">'.__( 'Padding right:', 'tg-text-domain' ).'</label>';
			$grid_layout .= '<input type="number" class="tomb-text number mini" id="the-grid-area-paddingright" data-name="padding-right" value="0" step="1" min="0">';
			$grid_layout .= '<label class="tomb-number-label">px</label>';
			$grid_layout .= '<label class="tomb-label">'.__( 'Background color:', 'tg-text-domain' ).'</label>';
			$grid_layout .= '<input class="tomb-colorpicker" data-alpha="1" id="the-grid-area-background" data-name="background" value="">';
			$grid_layout .= '<div id="tg-layout-styles-footer">';
				$grid_layout .= '<div class="tg-button" id="tg-button-save-styles"><i class="dashicons dashicons-yes"></i>'.__( 'Save Changes', 'tg-text-domain' ).'</div>';
			$grid_layout .= '</div>';
		$grid_layout .= '</div>';
		
		return $grid_layout;
	}
	
	/**
	* Build drag/drop meta query
	* @since 1.0.0
	*/
	public static function grid_meta_key_config() {
		
		$post_ID = self::get_post_ID();
		
		$prefix = TG_PREFIX;

		$meta_query  = '<div class="tg-button" id="tg-add-metakey"><i class="dashicons dashicons-plus"></i>'.__( 'Add a Meta Key', 'tg-text-domain' ).'</div>';
		$meta_query .= '<div class="tg-button" id="tg-add-relation"><i class="dashicons dashicons-plus"></i>'.__( 'Add relation', 'tg-text-domain' ).'</div>';
		$meta_query .= '<div class="tg-button" id="tg-preview-metakey-array"><i class="dashicons dashicons-search"></i>'.__( 'Check metakey array', 'tg-text-domain' ).'</div>';
		$meta_query .= '<div class="tomb-row" id="the_b60_grid_meta_query">';
			$meta_query .= '<input class="tg-hidden-meta-key" name="the_b60_grid_meta_query" data-metakey=\''.get_post_meta($post_ID, $prefix.'meta_query', true).'\' value=\''.get_post_meta($post_ID, $prefix.'meta_query', true).'\'>';
		$meta_query .= '</div>';
		
		return $meta_query;
	}
	
	/**
	* Build main setting resume
	* @since 1.0.0
	*/
	public static function grid_main_settings_config() {
		
		$post_ID = self::get_post_ID();
		
		$prefix = TG_PREFIX;
		
		$post_type_info     = get_post_meta($post_ID, $prefix.'post_type', true);
		$post_type_info     = (isset($post_type_info) && is_array($post_type_info)) ? ucwords(implode(", ", $post_type_info)) : ucfirst($post_type_info);
		$post_type_info     = (!empty($post_type_info)) ? $post_type_info : 'Post';
		$layout             = get_post_meta($post_ID, $prefix.'layout', true);
		$layout             = ($layout) ? ucfirst($layout) : __( 'Vertical', 'tg-text-domain' );
		$gutter             = get_post_meta($post_ID, $prefix.'gutter', true);
		$gutter             = ($gutter) ? $gutter : 0;
		$col_desktop_large  = get_post_meta($post_ID, $prefix.'desktop_large', true);
		$col_desktop_medium = get_post_meta($post_ID, $prefix.'desktop_medium', true);
		$col_desktop_small  = get_post_meta($post_ID, $prefix.'desktop_small', true);
		$col_tablet         = get_post_meta($post_ID, $prefix.'tablet', true);
		$col_tablet_small   = get_post_meta($post_ID, $prefix.'tablet_small', true);
		$col_mobile         = get_post_meta($post_ID, $prefix.'mobile', true);
		$col_desktop_large  = ($col_desktop_large) ? $col_desktop_large : 6;
		$col_desktop_medium = ($col_desktop_medium) ? $col_desktop_medium : 5;
		$col_desktop_small  = ($col_desktop_small) ? $col_desktop_small : 4;
		$col_tablet         = ($col_tablet) ? $col_tablet : 3;
		$col_tablet_small   = ($col_tablet_small) ? $col_tablet_small : 2;
		$col_mobile         = ($col_mobile) ? $col_mobile : 1;
		$columns            = $col_desktop_large.' / '.$col_desktop_medium.' / '.$col_desktop_small.' / '.$col_tablet  .' / '.$col_mobile;
		$item_skins         = json_decode(get_post_meta($post_ID, $prefix.'skins', true), TRUE);
		$item_skins         = (isset($item_skins) && is_array($item_skins)) ? ucwords(implode(', ', $item_skins)) : ucfirst($item_skins);
		
		$main_settings  = '<div><label class="tomb-label tomb-inline">'.__( 'Main Settings', 'tg-text-domain' ).'</label></div>';
		$main_settings .= '<div class="tomb-spacer" style="height: 15px"></div>';
		$main_settings .= '<div><label class="tomb-label tomb-inline">'.__( 'Post Type(s)', 'tg-text-domain' ).'&nbsp;:&nbsp;</label><span>'. $post_type_info .'</span></div>';
		$main_settings .= '<div class="tomb-spacer" style="height: 5px"></div>';
		$main_settings .= '<div><label class="tomb-label tomb-inline">'.__( 'Layout', 'tg-text-domain' ).'&nbsp;:&nbsp;</label><span>'. $layout .'</span></div>';
		$main_settings .= '<div class="tomb-spacer" style="height: 5px"></div>';
		$main_settings .= '<div><label class="tomb-label tomb-inline">'.__( 'Gutter', 'tg-text-domain' ).'&nbsp;:&nbsp;</label><span>'. $gutter .'px</span></div>';
		$main_settings .= '<div class="tomb-spacer" style="height: 5px"></div>';
		$main_settings .= '<div><label class="tomb-label tomb-inline">'.__( 'Columns', 'tg-text-domain' ).'&nbsp;:&nbsp;</label><span>'. $columns .'</span></div>';
		$main_settings .= '<div class="tomb-spacer" style="height: 5px"></div>';
		$main_settings .= '<div><label class="tomb-label tomb-inline">'.__( 'Skin(s)', 'tg-text-domain' ).'&nbsp;:&nbsp;</label>'. $item_skins .'</div>';
		
		return $main_settings;
	}
	
	/**
	* Build Select Post Type for skin
	* @since 1.0.0
	*/
	public static function grid_skin_post_type() {
		
		$post_ID = self::get_post_ID();
		$prefix  = TG_PREFIX;
		
		$post_types = get_post_meta($post_ID, $prefix.'post_type', true);
		$post_types = (isset($post_types) && is_array($post_types)) ? $post_types : array('post' => 'post');
		
		$options = null;
		foreach ($post_types as $post_type) {
			if (post_type_exists($post_type)) {
				$obj  = get_post_type_object($post_type);
				$name = $obj->labels->name;
				$options .= '<option value="'.$post_type.'" data-name="'.$name.'">'.$name.'</option>';
			}
		}
		
		$html  = '<label class="tomb-label">'.__( 'Select a skin for:', 'tg-text-domain' ).'</label>';
		$html.= '<div class="tomb-spacer" style="height: 5px"></div>';
		$html .= '<select class="tomb-select tomb-post-type-skin" data-width="180">';
		$html .= $options;
		$html .= '</select>';
		
		$skins  = get_post_meta($post_ID, $prefix.'skins', true);
		$html .= '<div class="tomb-row" id="the_b60_grid_skins">';
		$html .= '<input type="hidden" class="tomb-grid-skins" name="the_b60_grid_skins" value=\''.$skins.'\'>';
		$html .= '</div>';
		
		return $html;
		
	}
	
	/**
	* Get all skins
	* @since 1.0.0
	*/
	public static function get_all_grid_skins() {
			
		$item_base  = new the_b60_grid_Item_Skin();
		$item_skins = $item_base->get_skin_names();

		foreach($item_skins as $item_skin => $skin) {
			$skin_name = str_replace('-', ' ', $skin['name']);
			$skin_name = str_replace('_', ' ', $skin_name);
			$skin_name = ucwords(preg_replace('/[^A-Za-z0-9\-]/', ' ', $skin_name));
			$skins[$skin['type']][esc_attr($skin['slug'])] = esc_attr($skin_name);
		}
		
		$grid = array('grid-disabled' =>  __( 'Grid Skins', 'tg-text-domain' ));
		$maso = array('masonry-disabled' => __( 'Masonry Skins', 'tg-text-domain' ));
		$skin_list = $grid+$skins['grid']+$maso+$skins['masonry'];
		
		return $skin_list;
	}
	
	/**
	* Build Skin selector
	* @since 1.0.0
	*/
	public static function grid_skin_selector() {
			
		$item_base  = new the_b60_grid_Item_Skin();
		$item_skins = $item_base->get_skin_names();
		$item_css   = null;
		$item_html  = array();
		$item_filters = array();

		$virtual_base = new the_b60_grid_Skin_Post();
		$virtual_base->virtual_post(); 
		
		global $tg_skins_preview;
		$tg_skins_preview = true;

		uasort($item_skins,  function ($a,$b){ return strcmp($a['name'], $b['name']); });
		
		foreach($item_skins as $item_skin => $skin) {
						
			$css_file = $skin['css'];
			
			if (file_exists($css_file)) {
				$skin_css  = file_get_contents($css_file);	
				$item_css .= $skin_css;
			}
			
			if (file_exists($skin['php'])) {
				
				global $tg_grid_data;
				
				$tg_grid_data = array(
					'the_b60_grid_style' => $skin['type'],
					'the_b60_grid_item_ratio_x' => 1,
					'the_b60_grid_item_ratio_y' => 1,
					'the_b60_grid_default_image' => TG_PLUGIN_URL . 'backend/assets/images/skin-placeholder.jpg',
					'the_b60_grid_skin_content_background' => '#ffffff',
					'the_b60_grid_skin_overlay_background' => 'rgba(52, 73, 94, 0.75)',
					'the_b60_grid_skin_content_color' => 'dark',
					'the_b60_grid_skin_overlay_color' => 'light'
				);
				
				if (!isset($item_html[$skin['type']])) {
					$item_html[$skin['type']] = '';
				}
				
				if (!isset($item_filters[$skin['type']])) {
					$item_filters[$skin['type']] = array();
				}
				
				$item_filter = esc_attr($skin['filter']);
				if(!in_array($item_filter, $item_filters[$skin['type']], true)){
					array_push($item_filters[$skin['type']],$item_filter);
				}
				
				$skin_name = str_replace('-', ' ', $skin['name']);
				$skin_name = str_replace('_', ' ', $skin_name);
				$skin_name = ucwords(preg_replace('/[^A-Za-z0-9\-]/', ' ', $skin_name));

				ob_start();
				$skin_html  = include($skin['php']);	
				$item_html[$skin['type']] .= ob_get_contents();
				ob_end_clean();
				$item_html[$skin['type']] .= '<article class="tg-item '.esc_attr($item_skin).' '.strtolower(str_replace(' ', '-', $item_filter)).'" data-col="1" data-row="1">';
				$item_html[$skin['type']] .= '<div class="tg-item-skin-name" data-slug="'.esc_attr($item_skin).'"><i class="dashicons dashicons-yes"></i><span>'.esc_html($skin_name).'</span><span class="tg-select-skin">'.__( 'Select this skin', 'tg-text-domain' ).'</span></div>';
				$item_html[$skin['type']] .= '<div class="tg-item-inner">';
				$item_html[$skin['type']] .= $skin_html;
				$item_html[$skin['type']] .= '</div>';
				$item_html[$skin['type']] .= '</article>';
				
			}

		}

		$virtual_base->reset_virtual_post();
		$tg_skins_preview = false;
		
		$schemes    = array('dark','light');
		$title_tags = array('h2','h2 a','h3','h3 a','a','a.tg-link-url','i');
		$para_tags  = array('p');
		$span_tags  = array('span','.no-liked .to-heart-icon path','.empty-heart .to-heart-icon path');
		$tags = array(
			'title' => $title_tags,
			'text'  => $para_tags,
			'span'  => $span_tags
		);
		
		$default = array(
			'dark_title'  => '#444444',
			'dark_text'   => '#777777',
			'dark_span'   => '#999999',
			'light_title' => '#ffffff',
			'light_text'  => '#f5f5f5',
			'light_span'  => '#f6f6f6',
		);
		
		$colors = null;
		foreach ($schemes as $scheme) {
			foreach ($tags as $tag => $classes) {
				$classes   = implode(',.tg-item .'.$scheme.' ', $classes);
				$def_color = $default[$scheme.'_'.$tag];
				$color     = get_option('the_b60_grid_'.$scheme.'_'.$tag, $def_color);
				$colors   .= '#tg-grid-skins .tg-item .'.$scheme.' '.$classes.'{color:'.$color.';fill:'.$color.';stroke:'.$color.'}';
			}
		}
		
		$colors .= '#tg-grid-skins .tg-item .tg-item-content-holder {
						background-color: #ffffff;
				   }
				   #tg-grid-skins .tg-item .tg-item-overlay {
						background-color: rgba(52, 73, 94, 0.75);
				   }';
		
		if ($item_css) {
			$base = new the_b60_grid_Base();
			$item_css = $base->compress_css($colors.$item_css);
			$item_css = '<style id="tg-grid-skin-css">'.$item_css.'</style>';
		}

		$skin_grid = null;
		if ($item_html) {
			foreach($item_html as $type => $html) {
				$skin_grid .= self::grid_skin_markup($type,$html,$item_filters);
			}
		}
		
		echo $item_css.$skin_grid;
		
		die();
	}
	
	/**
	* Retrieve all skins
	* @since 1.0.0
	*/
	public static function grid_skin_markup($type,$html,$item_filters) {
		
		$rowNb   = ($type == 'grid') ? 2 : 1;
		$itemNav = ($type == 'grid') ? '' : 'basic';
		
		$data_attr  = ' data-name="tg-grid-'.$type.'-demo"';
		$data_attr .= ' data-style="'.$type.'"';
		$data_attr .= ' data-row="'.$rowNb.'"';
		$data_attr .= ' data-layout="horizontal"';
		$data_attr .= ' data-gutter="28"';
		$data_attr .= ' data-ratio="1.33"';
		$data_attr .= ' data-cols="[[320,1],[480,2],[768,2],[1200,2],[1480,3],[9999,4]]"';
		$data_attr .= ' data-transition="600ms"';
		$data_attr .= ' data-preloader="true"';
		$data_attr .= ' data-rtl="false"';
		$data_attr .= ' data-slider=\'{"itemNav":"'.$itemNav.'","swingSpeed":0.1,"cycleBy":"null","cycle":5000,"startAt":1}\'';
		
		$skin_grid  = '<div class="tomb-clearfix"></div>';
		$skin_grid .= '<div class="tg-grid-wrapper" id="tg-grid-'.$type.'-skin">';
		$skin_grid .= '<div class="tg-grid-skin-type">'.ucfirst($type).' '.__( 'Skins', 'tg-text-domain' ).'</div>';
		$skin_grid .= '<div class="tg-grid-sizer"></div>';
		
		$skin_grid .= '<div class="tg-filters-holder">';
		$skin_grid .= '<div class="tg-filter tg-show-filter tg-filter-active tg-filter-all" data-filter="*"><span class="tg-filter-name" data-count="true">'.__( 'All', 'tg-text-domain' ).'</span></div>';
		$skin_grid .= '<div class="tg-filter tg-show-filter" data-filter=".selected"><span class="tg-filter-name" data-count="true">'.__( 'Selected Skin', 'tg-text-domain' ).'</span></div>';
		if (count($item_filters[$type]) > 1) {
			foreach($item_filters[$type] as $item_filter) {	
				$skin_grid .= '<div class="tg-filter tg-show-filter" data-filter=".'.esc_attr(str_replace(' ', '-', strtolower($item_filter))).'"><span class="tg-filter-name" data-count="true">'.esc_attr(ucwords($item_filter)).'</span></div>';
			}
		}
		$skin_grid .= '</div>';
		
		$skin_grid .= '<div class="tg-grid-slider">';
		$skin_grid .= '<div class="tg-grid-holder tg-layout-'.$type.'" '.$data_attr.'>';
		$skin_grid .= $html;
		$skin_grid .= '</div>';
		$skin_grid .= '</div>';
		$skin_grid .= '<div class="tg-slider-bullets-holder">';
		$skin_grid .= '<div class="tg-slider-bullets"></div>';
		$skin_grid .= '</div>';
		$skin_grid .= '</div>';
		
		return $skin_grid;
	}
	
}
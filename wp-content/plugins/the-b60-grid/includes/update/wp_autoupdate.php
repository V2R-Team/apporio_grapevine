<?php
/**
 * @package   the_b60_grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 */

$plugin_current_version = TG_VERSION;
$plugin_slug  = 'the-grid/the-grid.php';
$license_user = '';
$license_key  = '';

//new WP_AutoUpdate ( $plugin_current_version, $plugin_remote_path, $plugin_slug, $license_user, $license_key );	

class WP_AutoUpdate {
	/**
	 * The plugin current version
	 * @var string
	 */
	private $current_version;

	/**
	 * The plugin remote update path
	 * @var string
	 */
	private $update_path;

	/**
	 * Plugin Slug (plugin_directory/plugin_file.php)
	 * @var string
	 */
	private $plugin_slug;

	/**
	 * Plugin name (plugin_file)
	 * @var string
	 */
	private $slug;

	/**
	 * License User
	 * @var string
	 */
	private $license_user;

	/**
	 * License Key 
	 * @var string
	 */
	private $license_key;

	/**
	 * Initialize a new instance of the WordPress Auto-Update class
	 * @param string $current_version
	 * @param string $update_path
	 * @param string $plugin_slug
	 */
	public function __construct( $current_version, $update_path, $plugin_slug, $license_user = '', $license_key = '' ) {
		// Set the class public variables
		$this->current_version = $current_version;
		$this->update_path = $update_path;

		// Set the License
		$this->license_user = $license_user;
		$this->license_key  = $license_key;

		// Set the Plugin Slug	
		$this->plugin_slug = $current_version;
		list ($t1, $t2) = explode( '/', $plugin_slug );
		$this->slug = str_replace( '.php', '', $t2 );		

		// define the alternative API for updating checking
		add_filter( 'pre_set_site_transient_update_plugins', array( &$this, 'check_update' ) );
		// Define the alternative response for information checking
		add_filter( 'plugins_api', array( &$this, 'check_info' ), 10, 3 );
		// add message for plugin update
		add_action( 'in_plugin_update_message-the-grid/the-grid.php' , array( &$this, 'addUpgradeMessageLink' ));
		
	}

	/**
	 * Add our self-hosted autoupdate plugin to the filter transient
	 *
	 * @param $transient
	 * @return object $ transient
	 */
	public function check_update( $transient ) {
		var_dump($transient);
		if ( empty( $transient->checked ) ) {
			return $transient;
		}
		
		// Get the remote version
		$remote_version = $this->getRemote_version();

		// If a newer version is available, add the update
		if ( version_compare( $this->current_version, $remote_version->new_version, '<' ) ) {
			$obj = new stdClass();
			$obj->slug = $this->slug;
			$obj->new_version = $remote_version->new_version;
			$obj->url = $remote_version->url;
			$obj->plugin = $this->plugin_slug;
			$obj->package = $remote_version->package;
			$transient->response[$this->plugin_slug] = $obj;
		}
		return $transient;
	}

	/**
	 * Add our self-hosted description to the filter
	 *
	 * @param boolean $false
	 * @param array $action
	 * @param object $arg
	 * @return bool|object
	 */
	public function check_info($false, $action, $arg) {
		if (isset($arg->slug) && $arg->slug === $this->slug) {
			$information = $this->getRemote_information();
			return $information;
		}
		return false;
	}

	/**
	 * Return the remote version
	 * @return string $remote_version
	 */
	public function getRemote_version() {
		$params = array(
			'body' => array(
				'action' => 'version',
				'license_user' => $this->license_user,
				'license_key' => $this->license_key,
			),
		);
		$request = wp_remote_post ($this->update_path, $params );
		if ( !is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) === 200 ) {
			return unserialize( $request['body'] );
		}
		return false;
	}

	/**
	 * Get information about the remote version
	 * @return bool|object
	 */
	public function getRemote_information() {
		$params = array(
			'body' => array(
				'action' => 'info',
				'license_user' => $this->license_user,
				'license_key' => $this->license_key,
			),
		);
		$request = wp_remote_post( $this->update_path, $params );
		if (!is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) === 200 ) {
			return unserialize( $request['body'] );
		}
		return false;
	}

	/**
	 * Return the status of the plugin licensing
	 * @return boolean $remote_license
	 */
	public function getRemote_license() {
		$params = array(
			'body' => array(
				'action' => 'license',
				'license_user' => $this->license_user,
				'license_key' => $this->license_key,
			),
		);
		$request = wp_remote_post( $this->update_path, $params );
		if ( !is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) === 200 ) {
			return unserialize( $request['body'] );
		}
		return false;
	}
	
	/**
	 * Shows message on Wp plugins page with a link for updating from envato.
	 */
	public function addUpgradeMessageLink() {

		$username = get_option('the_b60_grid_envato_username');
		$api_key  = get_option('the_b60_grid_envato_api_key');
		$purchase_code = get_option('the_b60_grid_purchase_code');

		if ( empty( $username ) || empty( $api_key ) || empty( $purchase_code ) ) {
			echo ' <a href="' . $this->url . '">' . __( 'Download new version from CodeCanyon.', 'tg-text-domain' ) . '</a>';
		} else {
			echo '<a href="' . wp_nonce_url( admin_url( 'update.php?action=upgrade-plugin&plugin=' . plugin_name() ), 'upgrade-plugin_' . plugin_name() ) . '">' . __( 'Update The Grid now.', 'tg-text-domain' ) . '</a>';
		}
	}
	
}



class TO_Envato {

    public static function verifyPurchase($userName, $apiKey, $purchaseCode, $itemId = false) {

        // Open cURL channel
        $ch = curl_init();

        // Set cURL options
        curl_setopt($ch, CURLOPT_URL, "http://marketplace.envato.com/api/edge/$userName/$apiKey/download-purchase:$purchaseCode.json");
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'ENVATO-PURCHASE-VERIFY'); //api requires any user agent to be set

        // Decode returned JSON
		$result = curl_exec($ch);
        $result = json_decode($result , true );
		
		// Close cURL channel
		curl_close($ch);
		
        //check if purchase code is correct
        if ( !empty($result['verify-purchase']['item_id']) && $result['verify-purchase']['item_id'] ) {
            //if no item name is given - any valid purchase code will work
            if ( !$itemId ) return true;
            //else - also check if purchased item is given item to check
            return $result['verify-purchase']['item_id'] == $itemId;
        }

        //invalid purchase code
        return false;

    }

}
$api = new TO_Envato();
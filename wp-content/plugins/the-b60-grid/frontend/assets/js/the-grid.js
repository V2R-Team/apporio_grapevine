/*!
 * The Grid – Core jQuery Plugin
 * Copyright © 2015 All Rights Reserved. 
 * @author Themeone [http://theme-one.com/the-grid/]
 */

/*global jQuery:false*/
/*global tg_global_var:false*/
/*global SC:false*/
/*global YT:false*/
/*global $f:false*/
/*global mejs*/
/*global TG_Slider:false*/
/*global TG_metaData*/
/*global TG_excludeItem*/
/*func = shorthand if function */

jQuery.noConflict();

// The Grid default settings
var the_b60_grid = {
    id      : 'the_b60_grid',
    version : '1.0',
	preview : '#tg-grid-preview-inner',
	wrapper : '.tg-grid-wrapper',
	slider  : '.tg-grid-slider',
	grid    : '.tg-grid-holder',
	loader  : '.tg-grid-preloader',
	ajax    : '.tg-ajax-button',
	ajaxMsg : '.tg-ajax-scroll-holder',
	sizer   : '.tg-grid-sizer',
	item    : '.tg-item',
	itemImg : '.tg-item-image',
	gallery : '.tg-item-gallery-holder',
	tooltip : '.tg-filter-count',
	filterH : '.tg-filters-holder',
	filter  : '.tg-filter, .tg-filters-holder select',
	search  : '.tg-search',
	clear   : '.tg-search-clear',
	sorter  : '.tg-sorters-holder',
	sorterBy: '.tg-sorter li, select.tg-sorter',
	sortASC : '.tg-sorter-order',
	arrLeft : '.tg-left-arrow',
	arrRight: '.tg-right-arrow',
	bullets : '.tg-slider-bullets',	
	pages   : '.tg-page-ajax',
	sortData: {
		excerpt : 'p',
		title   : function(elem) {
        	return jQuery(elem).data('title');
      	},
		id      : function(elem) {
        	return jQuery(elem).data('id');
      	},
		date    : function(elem) {
        	return jQuery(elem).data('date');
      	},
		author  : function(elem) {
        	return jQuery(elem).data('author');
      	},
		comment : function(elem) {
        	return jQuery(elem).data('comment');
      	},
		total_sales   : function(elem) {
        	return jQuery(elem).data('total-sales');
      	},
		regular_price : function(elem) {
        	return jQuery(elem).data('regular-price');
      	},
		sale_price : function(elem) {
        	return jQuery(elem).data('sale-price');
      	},
		featured   : function(elem) {
        	return jQuery(elem).data('featured');
      	},
		stock      : function(elem) {
        	return jQuery(elem).data('stock');
      	},
		sku        : function(elem) {
        	return jQuery(elem).data('sku');
      	}
	},
    defaults: {
		style      : 'grid',
		layout     : 'vertical',
		fullWidth  : null,
		fullHeight : null,
		rtl        : true,
		filterComb : false,
		filterLogic: 'AND',
		filterLoad : '',
		sortByLoad : '',
		orderLoad  : false,
		row        : 1,
		gutter     : 28,
		ratio      : 1,
        cols       : [[9999,4],[1200,3],[980,3],[768,2],[480,1],[320,1]],
		animation  : [{'name':'None'},{'visible':''},{'hidden':''}],
		transition : 0,
		itemNav    : null,
		swingSpeed : 500,
		cycleBy    : null,
		cycle      : 5000,
		startAt    : 0,
		ajaxMethod : null,
		ajaxDelay  : 0,
		preloader  : 0,
		itemDelay  : 0,
		galleryInt : false
    }
};

var tg_debounce_resize = (tg_global_var.debounce) ? 'debouncedresize' : 'resize'; // debounce resize functionnality
var tg_is_mobile = (tg_global_var.is_mobile); // check is we are on a mobile device

(function($) {
				
	"use strict";
	
	// the grid jquery plugin
	$.fn.the_b60_grid = function() {
		
		// Loop foreach grid (multi instance)
		return this.each(function () {
			
			// current grid
			var el = $(this);
			
			// main grid var
			var options,
				revealItems,
				buttonFilter,
				sortByValue,
				sortByName,
				rand1,
				index1,
				index2,
				gallery_imgs = [],
				sortOrder = true,
				item_nb = true,
				isInit = false,
				isAjax = false,
				page = 1,
				$frame,
				itemW,
				itemH,
				colR,
				colN,
				colW,
				colH,
				offs,
				realW,
				func,
				xhr = null;
			
			// define main DOM
			var ID        = el.closest(the_b60_grid.wrapper).attr('id'),
				$wrapper  = ($('[id="'+ID+'"]').length > 1) ? el.closest(the_b60_grid.wrapper) : $('#'+ID),
				$preview  = $wrapper.closest(the_b60_grid.preview),
				$slider   = $wrapper.find(the_b60_grid.slider),
				$loader   = $wrapper.find(the_b60_grid.loader),
				$ajax     = $wrapper.find(the_b60_grid.ajax),
				$ajaxMsg  = $wrapper.find(the_b60_grid.ajaxMsg),
				$sizer    = $wrapper.find(the_b60_grid.sizer),
				$item     = $wrapper.find(the_b60_grid.item),
				$filterH  = $wrapper.find(the_b60_grid.filterH),
				$filter   = $wrapper.find(the_b60_grid.filter),
				$search   = $wrapper.find(the_b60_grid.search),
				$clear    = $wrapper.find(the_b60_grid.clear),
				$sorter   = $wrapper.find(the_b60_grid.sorter),
				$sorterBy = $wrapper.find(the_b60_grid.sorterBy),
				$sortASC  = $wrapper.find(the_b60_grid.sortASC),
				$arrLeft  = $wrapper.find(the_b60_grid.arrLeft),
				$arrRight = $wrapper.find(the_b60_grid.arrRight),
				$bullets  = $wrapper.find(the_b60_grid.bullets),
				$pages    = $wrapper.find(the_b60_grid.pages);

			// retireve all data
			var data = el.data();
			
			// get grid settings (check for existing key in case php errors)
			var settings = {
				style       : (data.style)       && data.style,
				layout      : (data.layout)      && data.layout,
				fullWidth   : (data.fullwidth)   && data.fullwidth,
				fullHeight  : (data.fullheight)  && data.fullheight,
				rtl         : (data.rtl)         && data.rtl,
				filterComb  : (data.filtercomb)  && data.filtercomb,
				filterLogic : (data.filterlogic) && data.filterlogic,
				filterLoad  : (data.filterload)  && data.filterload,
				sortByLoad  : (data.sortbyload)  && data.sortbyload,
				orderLoad   : (data.orderload)   && data.orderload,
				row         : (data.row)         && data.row,
				ratio       : (data.ratio)       && data.ratio,
				gutter      : (data.gutter)      && data.gutter,
				cols        : (data.cols)        && data.cols,
				animation   : (data.animation)   && data.animation,
				transition  : (data.transition)  && data.transition,
				itemNav     : (data.slider)      && data.slider.itemNav,
				swingSpeed  : (data.slider)      && data.slider.swingSpeed,
				cycleBy     : (data.slider)      && data.slider.cycleBy,
				cycle       : (data.slider)      && data.slider.cycle,
				startAt     : (data.slider)      && data.slider.startAt,
				ajaxMethod  : (data.ajaxmethod)  && data.ajaxmethod,
				ajaxDelay   : (data.ajaxdelay)   && data.ajaxdelay,
				preloader   : (data.preloader)   && data.preloader,
				itemDelay   : (data.itemdelay)   && data.itemdelay
			};
			
			// check/set settings compare to default grid
			options = $.extend({},the_b60_grid.defaults, settings);
			// sort col array desc numeric (for futur column number detection)
			options.cols.sort(function(a, b){return b[0]-a[0];});
			// set correct value for RTL layout
			options.rtl = (options.rtl === true) ? false : true;
			// Ajax pagination get same delay on reveal
			options.ajaxDelay = ($pages.length > 0) ? options.itemDelay : options.ajaxDelay;
			// remove all data for security and confidentiality
			var keys = $.map(data, function(value, key) { return key; });
			for(var i = 0; i < keys.length; i++) {
				el.removeAttr('data-' + keys[i]);
			}
			// secure data
			cleanData($item);

			// build the grid
			full_width();      // 1. resize the grid wrapper at first
			columns(el);       // 2. then calculate the columns width based on the wrapper width
			item_size();       // 3. set the sizes to each item based on the column width			
			item_gallery();    // 4. init item gallery animation
			count_filter();    // 5. count and update filter number if necessary
			$.TG_Animations(); // 6. add animations plugins
			// set the grid size after everything loaded in masonry mode to relayout correctly
			if (options.style === 'masonry' || options.preloader === true || options.layout === 'horizontal') {
				$item.the_b60_grid_images_loaded({
					complete: function() {
						slider_size();   // 7. calculate slider height if necessary
						set_grid();      // 8. check grid parameters values
						init_slider();   // 9. init slider if available
						func = (options.preloader === true) && reveal_grid();
						// call on filtering, sorting, searching, and resizing  after complete
						func = (options.layout === 'horizontal') && el.TG_Layout('on', 'arrangeComplete', function(e) {onAnimationEnd(e);});  // filter	
					}
				});
			} else {
				set_grid();    // 7. check grid parameters values
				init_slider(); // 8. init slider if available
				// call on filtering, sorting, searching, and resizing  after complete
				func = (options.layout === 'horizontal') && el.TG_Layout('on', 'arrangeComplete', function(e) {onAnimationEnd(e);});  // filter	
			}
			
			// all events init
			isInit = (!options.preloader) &&  true;
			func = (isInit) && $wrapper.addClass('tg-grid-loaded');

			function onAnimationEnd(e) {
				var length = $item.not('.tg-item-hidden').length;
				if (e.length === length) {
					$frame.reload();
				}
			}
			
			// redefined and remove/clean data
			function cleanData(target) {
				for (var y = 0; y < $(target).length; y++) {
					var $this = $(target).eq(y), key, val,
						data  = $this.get(0).attributes;	
					for (var i = 0; i < data.length; i++) {
						key = data[i].name;
						val = data[i].value;
						if (key.indexOf('data-') === 0) {
							$this.removeAttr(key);
							$this.data(key.replace('data-',''),val);
							i--;
						}
					}
				}
			}
			
			// detect dropdown select list on touch devices
			$filter.on('click', function(e) {
				var $this = $(this);
				if (!$this.is('select')) {
					filterBy($this);
				} else {
					$filter.one('change', function() {
						filterBy($this.find('option:selected'));
					});
				}
			});
			
			function filterBy($this) {
				if (!isInit) {
					return false;
				}
				var filterValue;
				// remove class reveal and clear seach field before filtering
				$item.removeClass('tg-item-index');
				$search.val('');
				// check if filter combination or not
				if (options.filterComb === true) {
					filterValue = [];
					func = ($this.data('filter') === '*') ? $this.nextAll('[data-filter]').removeClass('tg-filter-active') : $this.prevAll('[data-filter="*"]').removeClass('tg-filter-active');
					func = ($this.data('filter') === '*') ? $this.closest('select').find('option').prop('selected', false) : $this.closest('select').find('[data-filter="*"]').prop('selected', true);	
					$this.toggleClass('tg-filter-active');
					$wrapper.find('.tg-filter-active').each(function(){
						if ($(this).data('filter') != '*') {
							filterValue.push($(this).data('filter'));
						}
					});
					filterValue = (options.filterLogic === 'AND') ? concatValues(filterValue) : filterValue.join(', ');
				} else {
					filterValue = $this.data('filter');
					$filter.removeClass('tg-filter-active');
					$this.addClass('tg-filter-active');
				}
				func = (!filterValue) && $wrapper.find('[data-filter="*"]').addClass('tg-filter-active');
				checkFilters();
				// apply filter(s)
				el.TG_Layout({filter: filterValue});
				// pause all videos
				$.TG_Pause_Players();		
			}
			
			function concatValues( obj ) {
				var value = '';
				for ( var prop in obj ) {
					if(obj.hasOwnProperty(prop)) {
						value += obj[ prop ];
					}
				}
				return value;
			}
			
			function checkFilters() {
				$filterH.each(function(index, element) {
                    var nbActive = $(this).find(the_b60_grid.filter+'.tg-filter-active').length;
					func = (nbActive === 0) && $(this).find('[data-filter="*"]').addClass('tg-filter-active');	
                });
			}

			if (!tg_is_mobile) {
				$sorterBy.on('click', function() {
					sortBy($(this));
				});
			} else {
				$sorterBy.on('change', function() {
					sortBy($(this).find('option:selected'));
				});
			}
			
			function sortBy(il) {
				if (!isInit) {
					return false;
				}
				sortByValue = (il.data('value') === 'none') ? '' : il.data('value');
				sortByName  = il.text();
				sortOrder   = $sortASC.data('asc');
				$item.removeClass('tg-item-index');
				$sorter.find('.tg-dropdown-value').text(sortByName);
				el.TG_Layout({
					sortAscending: sortOrder,
					sortBy: sortByValue 
				});
			}
			
			// TG_Layout sorter functionnality (ASC/DESC only)
			$sortASC.on('click', function() {
				if (!isInit) {
					return false;
				}
				var $this   = $(this);
				sortOrder = ($this.data('asc') === true) ? false : true;
				$this.data('asc',sortOrder).attr('data-asc',sortOrder);
				$item.removeClass('tg-item-index');
				el.TG_Layout({
					sortAscending: sortOrder
				});
			});
			
			// use value of search field to filter (regex in content)
			var $quicksearch = $search.keyup( debounce( function() {
				if (!isInit) {
					return false;
				}
				var qsRegex = new RegExp($quicksearch.val(), 'gi');
				$item.removeClass('tg-item-index');
				$filter.removeClass('tg-filter-active');
				el.TG_Layout({
					filter: function() { 
						var $this = $(this);
						var searchResult = qsRegex ? $this.text().match(qsRegex) : true;
						var buttonResult = buttonFilter ? $this.is(buttonFilter) : true;
						$('.tg-filter[data-filter="*"]').addClass('tg-filter-active');
						return searchResult && buttonResult;
					}
				});
				// pause all videos
				$.TG_Pause_Players();
			}, 200));
			
			$clear.on('click', function() {
				$search.val('').trigger('keyup');
			});
			
			// resize grids
			$(window).on(tg_debounce_resize, function() {
				full_width();         // 1. resize the grid wrapper at first
				columns();            // 2. then calculate the column width based on the wrapper width
				item_size();          // 3. set size to items based on the column width
				slider_size();        // 4. calculate slider height if necessary
				func = (isInit && el.closest('body').length > 0) && el.TG_Layout('layout'); // 5. Relayout the grid.
				// 6. Check animation end for horizontal layout to update slider
				func = (options.layout === 'horizontal') && el.TG_Layout('once', 'layoutComplete', function(e) {onAnimationEnd(e);});
			});
			
			// force full width
			function full_width() {
				if (options.fullWidth === true && $preview.length === 0) {
					$wrapper.css('left',0);
					var margL = parseInt($wrapper.css('margin-left'));
					var margR = parseInt($wrapper.css('margin-right'));
					var offset = $wrapper.offset().left-margL;
					$wrapper.width($(window).width()-(margL+margR));
					$wrapper.css('left',-offset);
				}
			}
			
			// random item gallery animation
			function item_gallery() {
				var gallery;
				update_gallery();
				if (!the_b60_grid.galleryInt && $(the_b60_grid.gallery).length > 0 ) {
					the_b60_grid.galleryInt = setInterval(function () {
						rand1   = Math.floor(Math.random()*gallery_imgs.length);
						index1  = (index1 === rand1 && gallery_imgs.length > 0) ? check_index(gallery_imgs,rand1+1) : rand1;
						gallery = $(the_b60_grid.gallery).eq(index1);
						index2  = (gallery_imgs.length > 0) ? check_index(gallery_imgs[index1],(gallery.find('.show').index()-1)+2) : 0;
						gallery.find(the_b60_grid.itemImg).removeClass('show');
						gallery.find(the_b60_grid.itemImg).eq(index2).addClass('show');
					},3500);
				}
			}
			
			// check image gallery index for unique random
			function check_index(array,index) {
				return (array.length + (index % array.length)) % array.length;
			}
			
			// get all galleries
			function update_gallery() {
				gallery_imgs = [];
				var galleries = $(the_b60_grid.gallery);
				for (var i = 0; i < galleries.length; i++) {
					var images = $(galleries[i]).find(the_b60_grid.itemImg);
					gallery_imgs[i] = [];
					for (var x = 0; x < images.length; x++) {
                     	gallery_imgs[i][x] = $(images[x]);   
                    }
				}
			}
			
			// count number per filter category/tag/term
			function count_filter() {
				for (var i = 0; i < $filter.length; i++) {
					var cat   = $filter.eq(i).data('filter');
					// if ajax page not count elements removing
					var not = ($pages.length > 0) ? '.tg-item-hidden' : null;
					var count = (cat !== '*') ? el.find(cat).not(not).length : $item.not(not).length;
					$filter.eq(i).find(the_b60_grid.tooltip).html(count);
					func = ($filter.eq(i).find('span:first-child').data('count')) && $filter.eq(i).find('span:first-child').data('tooltip',count);
					func = (count === 0) ? $filter.eq(i).removeClass('tg-show-filter') : $filter.eq(i).addClass('tg-show-filter');
				}
			}
			
			// count item to load more
			function count_items() {
				if ($ajax.length > 0) {
					item_nb = $ajax.data('item-tt')-$item.length;
					if (item_nb <= 0) {
						$ajax.addClass('tg-no-more');
						$ajax.find('span').text($ajax.data('no-more'));
						setTimeout(function(){
							$ajax.fadeOut(500);
						},3000);
					} else {
						var button_txt = $ajax.data('button');
						func = ($ajax.data('remain')) ? $ajax.find('span').text(button_txt+' ('+item_nb+')') : $ajax.find('span').text(button_txt);
					}
				}
			}	
			
			// reveal grid on load
			function reveal_grid() {
				var i = 0,
					interval,
					data = el.data('TG_Layout');
				func = ($pages.length === 0) ? $loader.remove() : $loader.hide();
				$wrapper.removeClass('tg-grid-loading');
				revealItems = data.filteredItems;
				if (revealItems.length > 0) {
					interval = window.tgInterval(function(){
						if (el.closest('body').length > 0) {
							$(revealItems[i].element).removeClass('tg-item-reveal');
							func = (options.itemDelay) && el.TG_Layout('reveal', [revealItems[i]]);	
							if (i === revealItems.length-1 || !options.itemDelay) {
								isInit = true;
								$item.removeClass('tg-item-reveal');
								$wrapper.addClass('tg-grid-loaded');
								func = (!options.itemDelay) && el.TG_Layout('reveal', revealItems);
								interval.clear();
							}
							i++;
						}
					},options.itemDelay);
				} else {
					isInit = true;
					$item.removeClass('tg-item-reveal');
					$wrapper.addClass('tg-grid-loaded');
				}
			}
			
			// Calculate column width number for current window width
			function columns() {
				var ww = viewport().width;
				var tw = $wrapper.width();
				for (var i = 0, l = options.cols.length; i < l; i++ ) {
					if (options.cols[i][0] >= ww) {
						colN = options.cols[i][1];
					} else {
						break;
					}
				}
				colW = (tw/colN) - options.gutter;
				if (options.layout === 'vertical') {
					el.width('');
					offs  = (colN-1)*options.gutter;
					colW  = (tw-offs)/colN;
					colW  = (colW % 1 !== 0) ? Math.ceil(colW) : colW;
					realW = colN*colW+offs;
					el.css('left', -(realW-el.width())/2+'px');
					el.width(realW);
				}
				if (options.fullHeight === true && options.layout === 'horizontal' && ww > 480) {
					var adminH = $('#wpadminbar').height();
					if ($preview.length === 0) {
						colH = (($(window).height()-adminH)-(options.gutter*(options.row-1)))/options.row;
					} else {
						colH = ($preview.height())/options.row;
					}
				} else {
					// round for accurate slider height (no need for width!)
					colH = Math.round(colW/options.ratio);
				}
			}
			
			// calculate window viewport (real width to match queries!)
			function viewport() {
				if ($preview.length === 0) {
					var e = window,
						a = 'inner';
					if (!('innerWidth' in window )) {
						a = 'client';
						e = document.documentElement || document.body;
					}
					return { width : e[ a+'Width' ]};
				} else {
					return { width : $preview.width()};
				}
			}
			
			// calculate item size and resize if necessary (unlimited size possibility!)
			function item_size() {
				// set gutters
				$sizer.width(colW);
				for (var i = 0; i < $item.length; i++) {
					var coli = $item.eq(i).data('col'),
						rowi = $item.eq(i).data('row');
						rowi = ((options.layout === 'horizontal') && rowi > options.row) ? options.row : rowi;
					if (colN === 1) {
						itemW = colW;
						itemH = colH;
					} else if (colN < coli) {
						colR  = Math.round(colN/(coli/rowi));
						colR  = (colR <= 1) ? 1 : colR;
						itemW = colN*colW+(colN-1)*options.gutter;
						itemH = (colR === 1) ? colH : colR*colH+(colR-1)*options.gutter;
					} else {
						itemW = coli*colW+(coli-1)*options.gutter;
						itemH = rowi*colH+(rowi-1)*options.gutter;
					}
					$item.eq(i).width(itemW);
					func = (options.style === 'grid') ? $item.eq(i).height(itemH) : null;
				}
			}
			
			// calculate slider inner height
			function slider_size() {
				if (options.layout === 'horizontal') {
					var sliderH;
					if (options.style === 'masonry') {
						$wrapper.removeClass('tg-grid-loading');
						// only when all images are loaded to get right height in masonry mode
						sliderH = Math.max.apply(null, $item.map(function () {
							return $(this).height();
						}).get());
						el.add($slider).height(sliderH);
					} else {
						sliderH = (colH)*options.row+options.gutter*(options.row-1);
						el.add($slider).height(sliderH);
					}
				}
			}
			
			// set the grid
			function set_grid() {
				var mode, param;
				var isHorizontal = false;
				if (options.layout === 'horizontal') {
					if (options.style === 'grid') {
						mode = param = 'packery';
						isHorizontal = true;
					} else {
						mode = 'horizontal';
						param = 'packery';
						isHorizontal = false;
					}
					
				} else if (options.style === 'grid') {
					mode = param = 'packery';
				} else {
					mode = param = 'masonry';
				}
				var layout = {};			
				layout[param] = {};
				layout.hiddenStyle = {};
				layout.visibleStyle = {};
				layout.layoutMode = mode;
				layout.filter = options.filterLoad;
				if (options.sortByLoad !== 'none') {
					layout.sortBy = options.sortByLoad;
				}
				layout.sortAscending = options.orderLoad;
				layout.isOriginLeft = options.rtl;
				layout.itemSelector = the_b60_grid.item;
				layout[param].gutter = options.gutter;
				layout[param].columnWidth = $sizer[0];
				layout[param].isHorizontal = isHorizontal;
				layout.hiddenStyle.opacity = 0;
				layout.visibleStyle.opacity = 1;
				layout.hiddenStyle.transform = options.animation[2].hidden;
				layout.visibleStyle.transform = options.animation[1].visible;
				layout.transitionDuration = options.transition;
				layout.getSortData = the_b60_grid.sortData;
				el.TG_Layout(layout);
			}

			// init horizontal slider
			function init_slider() {	
				if (options.layout === 'horizontal') {	
				
					var data  = el.data('TG_Layout'),
						filteredItems = data.filteredItems,
						index = options.startAt = (options.startAt-1 > filteredItems.length) ? filteredItems.length : options.startAt;	
					
					if (options.itemNav === 'forceCentered' && $bullets.length === 0) {
						$bullets = $($('<div class="tg-slider-bullets"></div>')).appendTo($wrapper).hide();
					}
								
					$frame = new TG_Slider($slider,{
						itemSelector  : '.tg-item:not(.tg-item-hidden)',
						cycleBy       : options.cycleBy,
						cycleInterval : options.cycle,
						pauseOnHover  : 1,
						itemNav       : options.itemNav,
						startAt       : options.startAt-1,
						smart         : 1,
						horizontal    : 1,
						easing        : 'easeOutExpo',
						speed         : 1000,
						swingSpeed    : options.swingSpeed,
						releaseSwing  : 1,
						mouseDragging : 1,
						touchDragging : 1,
						elasticBounds : 1,
						moveBy        : itemW,
						syncSpeed     : 0.8,
						keyboardNavBy : 'pages',
						activeClass   : 'tg-active-item',
						disabledClass : 'tg-disabled',
						draggedClass  : 'tg-slider-dragged',
						pageBuilder   : function () {
							return '<li><span></span></li>';
						},
						pagesBar      : $bullets,
						prevPage      : $arrLeft,
						nextPage      : $arrRight,
						activatePageOn: 'click'
					});
					
					$frame.init();
					
					// active on force centered nav
					if (options.itemNav === 'forceCentered') {
						$(filteredItems[index-1].element).addClass('tg-active-item');
						$frame.on('load activePage', function(index) {
							index = this.rel.activePage;
							data  = el.data('TG_Layout');
							filteredItems = data.filteredItems;
							$item.removeClass('tg-active-item');
							func = (filteredItems[index]) && $(filteredItems[index].element).addClass('tg-active-item');
						});
					}
					
				}
			}
			
			// load post with ajax
			$pages.on('click', function(e) {
				e.preventDefault();
				var $this = $(this);
				page = $this.data('page');
				if (!$this.is('.tg-page-current') && (!xhr || (xhr && xhr.readyState == 4)) && !isAjax && isInit) {
					$sorter.find('.tg-dropdown-value').text('');
					$search.val('');
					$filter.removeClass('tg-filter-active');
					$('.tg-filter[data-filter="*"]').addClass('tg-filter-active');
					$pages.removeClass('tg-page-current').addClass('tg-loading');
					$this.addClass('tg-page-current');
					// prevent showing filtered items when removing them
					$('.tg-item-hidden').addClass('tg-item-removed');
					// destroy all media (prevent issue with ajax)
					$.TG_media_destroy(el);
					// properly remove items, relayout and unfilter
					el.css('min-height',250).TG_Layout('remove', $item).TG_Layout({filter: '*'});
					// remove comments from DOM
					el.contents().each(function() {func = (this.nodeType == 8) ? $(this).remove() : null;});
					// add loading animation
					$loader.show();
					// load next page
					load_posts(el);
				}
			});
			
			// load post with ajax
			$ajax.on('click', function(e) {
				if (item_nb) {
					e.preventDefault();
					load_posts(el);
				}
			});

			// load more ajax on scroll
			if (options.ajaxMethod == 'on_scroll') {
				$(window).on('mousewheel resize scroll', function() {
					var visible = $wrapper.the_b60_grid_visible();
					func = (visible === true && isInit === true) && load_posts(el);
				});			
			}
			
			// load more post on ajax (on click and scroll)
			function load_posts(el){

				// check if ajax request not proceeded and finish before running another one
				if (xhr && (xhr.readyState == 3 || xhr.readyState == 2 || xhr.readyState == 1)) {
                	return false;
            	}

				var ajaxData = { 
					action     : 'the_b60_grid_load_more', 
					grid_nonce : tg_global_var.nonce, 
					grid_name  : el.data('name'),
					grid_page  : page,
					grid_data  : get_meta_data()
				};

				xhr = $.ajax({
					url: tg_global_var.url,
					type: 'post',
					datatype : 'json',
					data: ajaxData,
					beforeSend : function(){
						isAjax = true;
						page   = page + 1;
						func   = ($ajax.data('loading') && !$ajax.hasClass('tg-no-more')) && $ajax.find('span').text($ajax.data('loading'));
						func   = ($($ajaxMsg).length > 0 && $ajax.length === 0) && $($ajaxMsg).addClass('tg-loading');			
					},
					success : function(data){

						// retrieve ajax response
						var success = data.success,
							message = data.message,
							content = data.content;
						
						// if an error occur during retrieving content
						if (!success) {
							$ajax.add($($ajaxMsg)).add($pages).removeClass('tg-loading');
							$loader.find('> div').html(message);
							$ajax.find('span').text(message);
							$($ajaxMsg).children('div').text(message);
							page = page - 1;
							return false;
						}
						
						// ajax on scroll msg
						if (!content) {
							$($ajaxMsg).children('div').text($($ajaxMsg).children('div').data('no-more'));
							setTimeout(function(){
								$($ajaxMsg).fadeOut(400);
							}, 1000);
							page = page - 1;
							return false;
						}
						
						// get all items
						var $items = $(content);
						
						// prevent showing item if filtered and hidden
						func = ($pages.length === 0) && $items.addClass('tg-item-index');
						
						// Keep only item if unwanted wrapper exist (for grid cache system)
						if ($items.find(the_b60_grid.grid).length > 0) {
							$items = $items.find(the_b60_grid.item).removeClass('tg-item-reveal');
						}
						
						el.append($items);              // 01. append items
						$item = el.find(the_b60_grid.item); // 02. redefined $item DOM
						$items.hide();			        // 03. hide to append with delay
						item_size();					// 04. calculate size
						$.TG_media_init();			    // 05. Active media (visible/sized for mejs)
						cleanData($item);	            // 06. clean data
						count_filter();	                // 07. re-count filter nb              
						update_gallery();               // 08. update gallery dom
						item_gallery();                 // 09. update animated gallery
						exclude_item();                 // 10. only for preview mode
						$.TG_Animations();
						// Remove comment tags before items (otherwise it will count 2x and multiply delay)
						$items = $($.grep($items, function(e){ return typeof e.id !== 'undefined';}));
						var itemNb = $items.length-1;
						// wait all images load (mandatory for masonry layout)
						$items.the_b60_grid_images_loaded({
							complete: function() {
								var i = 0, interval;
								$loader.hide();
								func = (data) && $($ajaxMsg).removeClass('tg-loading');
								func = (options.layout === 'horizontal') && el.css('min-height','');
								func = (options.style === 'masonry' && options.layout === 'horizontal') && slider_size();
								interval = window.tgInterval(function(){
									// check if grid still exist during appending (prevent errors on destroy or ajax)
									if (el.closest('body').length > 0) {
										func = (options.ajaxDelay) && el.TG_Layout('appended',$items.eq(i));																	
										func = (options.layout === 'horizontal') && $frame.reload();
										// show ajax pages & reset ajax
										if (itemNb === i || !options.ajaxDelay) {
											count_filter(); // 11. re-count filter nb  
											count_items();  // 12. recount item for ajax button
											func = (!options.ajaxDelay) && el.TG_Layout('appended',$items);
											$pages.removeClass('tg-loading');
											isAjax = false;
											interval.clear();
											return false;
										}
										i++;
									}
								},options.ajaxDelay);
							}
						});
					},
					error : function(jqXHR, textStatus, errorThrown) {
						page = page - 1;
						console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
					}
				});
			}
			
			// only if in backend mode for preview
			function get_meta_data(){
				if ($preview.length > 0) {
					return new TG_metaData();
				}
			}
			
			// check hidden item in backend
			function exclude_item() {
				if ($preview.length > 0) {
					var EX = new TG_excludeItem();
				}
			}

		});

	};

})(jQuery);

// Custom script : wait images loaded for img src and/or background image
(function($) {
			
	"use strict";
	
	var tg_img_arr = [];
	
	$.fn.the_b60_grid_images_loaded = function() {
		
		// current element to search in
		var el = $(this);
		
		// extend the options to complete:
		var options = $.extend({
			complete: function() {}
		}, arguments[0] || {});
		
		// init global var
		var count = 0,
			preload = [],
			img_url;
		
		// search all image inside current selector
		el.find('*').filter(function() {
			img_url = $(this).css('background-image');
			img_url = /^url\((['"]?)(.*)\1\)$/.exec(img_url);
			img_url = img_url ? img_url[2] : null;
			img_url = (!img_url && $(this).is('img')) ? $(this).attr('src') : img_url;
			img_url = (img_url && img_url.match(/\.(jpg|jpeg|png|bmp|gif|tif|tiff|jif|jfif)/g)) ? img_url : null;
			if (img_url && $.inArray(img_url, tg_img_arr) == -1) {
				preload.push(img_url);
				tg_img_arr.push(img_url);			
			}
		});

		// load images
		var images = [];

		for (var i = 0; i < preload.length; i++) {
			images[i] = new Image();
			images[i].onload  = imgLoaded;
			images[i].onerror = imgLoaded;
			images[i].onabort = imgLoaded;
			images[i].src = preload[i];
		}
		
		// complete if no images
		if (!preload.length) {
			options.complete.call(el);
			return false;
		}
		
		// on load count and compare to array
		function imgLoaded() {
			count++;
			if(count >= preload.length){
				options.complete.call(el);
				return false;
			}
		}

	};

})(jQuery);

// Custom script : viewport check bottom grid visibility
(function($){
	
	"use strict";

    var $w = $(window);
	
    $.fn.the_b60_grid_visible = function(){

        if (this.length < 1) {
            return;
		}

        var $t        = this.length > 1 ? this.eq(0) : this,
            t         = $t.get(0),
            vpHeight  = $w.height(),
			hVisible  = false;

		if (typeof t.getBoundingClientRect === 'function'){

            // Use this native browser method, if available.
            var rec      = t.getBoundingClientRect();
                hVisible = rec.bottom <  vpHeight;
			
		} else {

            var viewTop    = $w.scrollTop(),
                viewBottom = viewTop + vpHeight,
                offset     = $t.offset(),
                top        = offset.top,
                bottom     = top + $t.height();
				hVisible   = viewBottom >  bottom;
		}
		
		return hVisible;
    };

})(jQuery);

// debounced resize
(function($) {
	
	"use strict";

	var $event = $.event,
		$special,
		func,
		resizeTimeout;
	
	$special = $event.special.debouncedresize = {
		setup: function() {
			$(this).on( "resize", $special.handler );
		},
		teardown: function() {
			$(this).off( "resize", $special.handler );
		},
		handler: function( event, execAsap ) {
			// Save the context
			var context = this,
				args = arguments,
				dispatch = function() {
					// set correct event type
					event.type = "debouncedresize";
					$event.dispatch.apply( context, args );
				};
	
			if ( resizeTimeout ) {
				clearTimeout( resizeTimeout );
			}
	
			func = execAsap ? dispatch() : resizeTimeout = setTimeout( dispatch, $special.threshold );
		},
		threshold: 100
	};

})(jQuery);

// throttle function to reduce calculations
function throttle(func, milliseconds) {
	var lastCall = 0;
	return function () {
		var now = Date.now();
		if (lastCall + milliseconds < now) {
			lastCall = now;
			return func.apply(this, arguments);
		}
	};
}

// debounce so filtering doesn't happen every millisecond
function debounce(fn, threshold) {
	
	"use strict";
	
	var timeout;
	
	return function debounced() {
		if (timeout) {
			clearTimeout(timeout);
		}
		function delayed() {
			fn();
			timeout = null;
		}
		setTimeout(delayed, threshold || 100);
	};
}

//RAF polyfill (http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/)
(function() {
    var lastTime = 0;
    var vendors = ['webkit', 'moz'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame =
          window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

// RAF interval for smooth animations
window.tgInterval = function(a, b) {
    var c = Date.now,
        d = window.requestAnimationFrame,
        e = c(),
        f, g = function() {
            c() - e < b || (e += b, a());
            f || d(g);
        };
    d(g);
    return {
        clear: function() {
            f = 1;
        }
    };
};

// Custom script : Simple DropDown list (to prevent overflow issue)
(function($) {
		
	$(document).ready(function() {

		var $filters,
			animation    = 'tg-dropdown-holder-animation',
			filteractive = 'li.tg-filter-active',
			filterspan   = 'li.tg-filter-active span';
			
		if (!tg_is_mobile) {
		
			$(document).on('mouseenter', '.tg-dropdown-holder', function(e) {
				e.preventDefault();
				e.stopPropagation();
				var $this     = $(this),
					listTimer = $($this.data('list-DOM')).data('list-timer');
				
				if (listTimer) {
					clearTimeout(listTimer);
					$this.data('list-DOM').addClass(animation);
					return false;
				}
	
				$filters = $this.find('ul');
				
				var $list = $('<ul class="tg-dropdown-list tg-list-appended"></ul>').appendTo('body');
				$this.add($list).data('list-DOM', $list);
				$list.data('filter-DOM', $this);
				
				var $items   = $filters.find('li').clone(),
					position = getListPos($this);
					
				$filters.hide();
	
				$list.html($items).css({
					'position'  : 'absolute',
					'z-index'   : 99999,
					'width'     : position.width,
					'top'       : position.top,
					'left'      : position.left
				}).addClass(animation);
	
				dropDownColors($list.find('.tg-filter-active'), 'hover-');
				
			}).on('mouseleave', '.tg-dropdown-holder', function(e) {
				removeList($(this));
			});
			
			$(document).on('mouseenter touchstart', '.tg-list-appended', function(e) {
				var $this = $(this);
				if ($this.data('list-DOM')) {
					clearTimeout($this.data('list-timer'));
					$this.data('list-DOM').addClass(animation);
					return false;
				}
			}).on('mouseleave touchend', '.tg-list-appended', function(e) {
				removeList($(this));
			});	
			
			$(document).on('click touchstart', '.tg-list-appended li', function(){
				
				var $this    = $(this).closest('ul'),
					$filters = $this.data('filter-DOM');
					
				$filters.find('[data-filter="'+$(this).data('filter')+'"]').trigger('click');
				$filters.find('[data-value="'+$(this).data('value')+'"]').trigger('click');
				$this.width($filters.outerWidth());
				$this.css('left',$filters.offset().left);
				
				var position = getListPos($filters);
	
				if (position.top !== $this.position().top) {
					$this.css('top',position.top);
				} else {
					$this.find('li').removeClass('tg-filter-active');
					$filters.find('.tg-filter-active').each(function(i) {
						$this.find('li').eq($(this).index()).addClass('tg-filter-active');
					});
					dropDownColors($this.find('li'), '');
					dropDownColors($this.find('.tg-filter-active'), 'hover-');
				}
	
			});
			
			$(document).on('mouseenter mouseover', '.tg-list-appended li:not(.tg-filter-active)', function(){
				dropDownColors($(this), 'hover-');
			}).on('mouseleave', '.tg-list-appended li:not(.tg-filter-active)', function(){
				dropDownColors($(this), '');
			});

		}
		
		function dropDownColors(el, state) {
			var bkg   = el.data(state+'bg'),
				color = el.data(state+'color');
			el.css({
				'color': color,
				'background': bkg
			});	
		}
			
		function removeList($this) {
			var $list = $this.data('list-DOM').removeClass(animation);
			var timer = setTimeout(function() {
				$list.remove();
				$list = null;
				$this.data('list-DOM', $list);
			},400);
			$($this.data('list-DOM')).data('list-timer', timer);
			return false;
		}
			
		function getListPos(el) {	
			var offset  = el.parent().offset(),
				height  = el.parent().height(),
				body    = ($('body').css('position') === 'relative') ?  $(window).scrollTop()+$('body')[0].getBoundingClientRect().top : null,
				margin  = parseInt(el.css('margin-bottom')),
				width   = el.outerWidth(),
				left    = offset.left,
				top     = offset.top+height-body-margin;
	
			var position   = [];
			position.top   = top;
			position.left  = left;
			position.width = width;
			return position;
		}
		
	});
	
})(jQuery);

// Custom script : Simple Tooltip (prevent overflow issue)
(function($) {
		
	$.fn.TG_ToolTip = function(options) {
		
		var settings = $.extend({
            data        : 'tooltip', // Effect Tooltip class name.
			zindex      : 99999,     // z-index css
			place       : 'top',     // Default place.
			appendClass : 'tooltip', // Class to append
			hoverClass  : null,      // Hover class
			spacing     : 0          // Gap between target and Tooltip.
        }, options);
		
		
		var toolTip_Data  = 'tooltip-DOM',
			toolTip_Timer = 'tooltip-timer';
		
		$(document).on('mouseenter', $(this).selector, function() {
			
			var $this = $(this);
			
			if (!$this.data(options.data)) {
				return false;
			}

			if ($this.data(toolTip_Data)) {
				clearTimeout($this.data(toolTip_Timer));
				$this.data(toolTip_Data).addClass(options.hoverClass.split('.').join(''));
				return false;
			}
				
			var $tooltip = $('<div class="'+options.appendClass.split('.').join('')+'"></div>').appendTo('body');
			$this.data(toolTip_Data, $tooltip);

			var data   = $this.data(options.data),
				offset = $this.offset(),
				body    = ($('body').css('position') === 'relative') ? $(window).scrollTop()+$('body')[0].getBoundingClientRect().top : null,
				top    = offset.top - body,
				left   = offset.left,
				width  = $this.outerWidth(true);
				
			$tooltip.html(data).css({
				'position'  : 'absolute',
				'z-index'   : options.zindex,
				'width'     : width,
				'top'       : top+options.spacing-$tooltip.outerHeight(true),
				'left'      : left+width/2
			}).addClass(options.hoverClass.split('.').join(''));
			
		}).on('mouseleave', $(this).selector, function() {
			
			var $this = $(this);
			
			if (!$this.data(options.data)) {
				return false;
			}
			
			var $tooltip = $this.data(toolTip_Data).removeClass(options.hoverClass.split('.').join(''));
			var timer    = setTimeout(function() {
				$tooltip.remove();
				$tooltip = null;
				$this.data(toolTip_Data, $tooltip);
			},400);
			$this.data(toolTip_Timer, timer);
			
		});
		
	};
	
	$(document).ready(function() {
		$('.tg-filter-name').TG_ToolTip({
			data        : 'tooltip',
			zindex      : 99999,
			place       : 'top',
			appendClass : '.tg-filter-tooltip',
			hoverClass  : '.tg-tooltip-hover',
			spacing     : -2
		});	
	});
			
})(jQuery);

// Custom script : handle all media content in the grid
(function($) {
	
	"use strict";
	
	var tg_media_init = 'tg-media-init',
		tg_media_hold = '.tg-item',
		ie = (function() { // ie version detection
			var v = 3,
				div = document.createElement('div'),
				all = div.getElementsByTagName('i');
			do {div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->';}
			while (all[0]);
			return v > 4 ? v : document.documentMode; 		
		}());

	if (ie) {
		$('body').addClass('is-ie');
	}
	
	// SoundCloud players (API)
	$.fn.TG_SoundCloud = function() {
		
		return this.each(function () {
			
			var $this = $(this).closest(tg_media_hold);
			
			if (!$this.hasClass(tg_media_init)) {

				var player = SC.Widget($(this).attr('id'));
				
				$.TG_Media_Ready($this,player,'STD');
			
				player.bind(SC.Widget.Events.PLAY,function() {
					$.TG_Media_Play($this);
				});
					
				player.bind(SC.Widget.Events.PAUSE,function() {
					$.TG_Media_Pause($this);
				});
					
				player.bind(SC.Widget.Events.FINISH,function() {
					$.TG_Media_Pause($this);
				});
			
			}
			
		});
	};
	
	//object to hold all players on the page
	var TG_YT_players = {};
	// Youtube players (API)
	$.fn.TG_Youtube = function() {
		
		return this.each(function () {
	
			var $this = $(this).closest(tg_media_hold);
			
			if (!$this.hasClass(tg_media_init)) {
	
				var player,
					playerID = this.id;

				TG_YT_players[playerID] = new YT.Player(playerID, {
					events: {
						'onReady':function(){
							$.TG_Media_Ready($this,TG_YT_players[playerID],'YT');
						},
						'onStateChange': function(event){
							if (event.data === 1) {
								$.TG_Media_Play($this);
							}
							if (event.data === 2 ||  event.data === 0) {
								$.TG_Media_Pause($this);
							}
						}
					}
				});
											
			}
			
		});
	};
	
	// Vimeo players (API)
	$.fn.TG_Vimeo = function() {
		
		return this.each(function () {

			var $this = $(this).closest(tg_media_hold);
			
			// Vimeo not supported with IE <= 9, so remove iframe & play button
			if (ie <= 9) {
				$(this).remove();
				$this.find('.tg-media-button').remove();
			}
				
			if (!$this.hasClass(tg_media_init)) {
				
				// reset the src of the iframe because of a cache issue from VIMEO API
				$(this).attr('src', $(this).attr('src'));
				
				var player = $f(this),
					firstPause = false,
					VMtimer;
				
				player.addEvent('ready', function() {
					$.TG_Media_Ready($this,player,'VM');
					player.addEvent('play', function(){
						$.TG_Media_Play($this);
					});
					player.addEvent('pause', function(){
						$.TG_Media_Pause($this);
						firstPause = true;
					});
					player.addEvent('playProgress', function(data){
						clearInterval(VMtimer);
						if (!firstPause && parseInt(data.seconds) < 5) {
							VMtimer = setInterval(wasPlayed, 300);
						}
					});
					player.addEvent('finish', function(){
						$.TG_Media_Pause($this);
					});
					function wasPlayed() {
						clearInterval(VMtimer);
						$this.removeClass('tg-force-play tg-is-playing');
					}
				});
				
				
			}
				
		});
	};

	// HTML5 video/audio players
	$.fn.TG_HTML_Player = function() {
		
		return this.each(function () {
			
			var $this = $(this).closest(tg_media_hold);
			
			if (!$this.hasClass(tg_media_init)) {
				
				var player = $(this)[0];
				
				if (player.addEventListener) {
					$.TG_Media_Ready($this,player,'STD');
					player.addEventListener('play', function(){
						$.TG_Media_Play($this);
					});
					player.addEventListener('pause', function(){
						$.TG_Media_Pause($this);
					});
					player.addEventListener('ended', function(){
						$.TG_Media_Pause($this);
					});
				}
			}
			
		});
		
	};
	
	// Set player on ready
	$.TG_Media_Ready = function(el,player,method) {
		el.data('pause-method',method)
		  .data('media-player',player)
		  .addClass(tg_media_init);
	};
	
	// handle Play on player
	$.TG_Media_Play = function(el) {
		if (!el.hasClass('tg-force-play')) {
			$.TG_Pause_Players();
		}
		el.addClass('tg-is-playing');
		$(tg_media_hold).removeClass('tg-force-play');
	};
	
	// handle Pause on player
	$.TG_Media_Pause = function(el) {
		el.removeClass('tg-is-playing tg-force-play');
	};
	
	// Play button
	$(document).on('click', '.tg-item-button-play', function(e) {
		
		e.preventDefault();
		
		var $this  = $(this).closest('.tg-item'),
			method = $this.data('pause-method'),
			player = $this.data('media-player');
			
		// check if player & init
		if (player && $this.hasClass(tg_media_init)) {
			$this.find('.tg-item-media').show();
			$.TG_Pause_Players();
			$(tg_media_hold).removeClass('tg-force-play tg-play-error');
			$this.addClass('tg-force-play');
			switch(method) {
				case 'STD':
					player.play();
					break;
				case 'YT':
					player.playVideo();
					break;
				case 'VM':
					player.api('play');
					break;
			}
		}
		
	});

	// Pause all kind of video/audio media
	$.TG_Pause_Players = function() {
		$('.tg-item.tg-is-playing, .tg-item.tg-force-play').each(function() {
			var $this  = $(this),
				method = $this.data('pause-method'),
				player = $this.data('media-player');
			if (player && $this.hasClass(tg_media_init)) {
				switch(method) {
					case 'STD':
						player.pause();
						break;
					case 'YT':
						player.pauseVideo();
						break;
					case 'VM':
						player.api('pause');
						break;
				}
				$this.closest(tg_media_hold).removeClass('tg-is-playing tg-force-play');
			}
		});
	};

	// API scripts (Youtube, Vimeo, Soundclound)
	$.TG_media_init = function() {
		
		var tag,
			API,
			url,
			type,
			loaded,
			script,
			scripts = [
				{'ID':'youtube', 'url':'//www.youtube.com/iframe_api'},
				{'ID':'vimeo', 'url':'//a.vimeocdn.com/js/froogaloop2.min.js'},
				{'ID':'soundcloud', 'url':'//w.soundcloud.com/player/api.js'}
			];
		
		if ($('.tg-item-media').length === 0) {
			return false;
		}
		
		API = {
			youtube: function () {
				// because of futur ajax events
				if (typeof YT == 'undefined' || YT.loaded === 0) {
					window.onYouTubeIframeAPIReady = function() {
						$('.tg-item-youtube').TG_Youtube();
					};
				} else {
					$('.tg-item-youtube').TG_Youtube();
				}
			},
			vimeo: function() {
				$('.tg-item-vimeo').TG_Vimeo();
			},
			soundcloud: function() {
				$('.tg-item-soundcloud').TG_SoundCloud();
			}
		};
		
		/* jshint loopfunc:true */
		// auto & smart load API scripts (only when necessary)
		for (var i = 0; i < scripts.length; i++) {
			type = scripts[i].ID;
			if ($('.tg-item-'+type).length > 0) {
				url  = scripts[i].url;
				if (checkScripts(url) !== true) {
					tag = document.createElement('script');
					tag.src = scripts[i].url;
					tag.id  = 'tg-'+type+'-api';
					script = document.getElementsByTagName('script')[0];
					script.parentNode.insertBefore(tag, script);
					(function(tag,type){
						tag.onload = function() {
							API[type]();
						};
					})(tag,type);
				} else {
					API[type]();
				}
			}
		}
		
		function checkScripts(url) {
			// script position in DOM will depend of OS & browser (win7 above head, win10 in head,...)
			$('head script').add($('head').prevAll('script')).each(function(){
				var src = this.src;
				if (src.indexOf(url) !== -1) {
					loaded = true;
					return false;
				} else {
					loaded = false;
				}
			});
			return loaded;
		}
		
		// HTML5 Player events
		$(document).ready(function() {
			$('.tg-item .tg-item-audio-player').attr('width','100%');
			$('.tg-item-video-player,.tg-item-audio-player').TG_HTML_Player();
			if (tg_global_var.mediaelement) {
				$('.tg-item-video-player:not(.tg-mediaelement-init), .tg-item-audio-player:not(.tg-mediaelement-init)').mediaelementplayer({
					audioVolume: 'vertical',
					videoVolume: 'vertical',
					startVolume: 0.8,
					success: function(media, domObject) {
						$(domObject).TO_MediaRail();
					}
				});	
				$('.tg-item-video-player, .tg-item-audio-player').addClass('tg-mediaelement-init');
			}
		});

	};
	
	// destroy all video/audio players 
	$.TG_media_destroy = function(el) {

		var vimeo = (el) ? el : $('.tg-item');
		// destroy Vimeo, needed for IE
		vimeo.find('.tg-item-vimeo').each(function() {
			var $this = $(this),
				player = $this.closest(tg_media_hold).data('media-player');
			if (player) {
				player.api('pause');
				$this.attr('src','about:blank');
			}
		});
		
		el = (el) ? el.find('.tg-item video, .tg-item audio') : $('.tg-item video, .tg-item audio');
		// destroy media player
		el.each(function() {
						
			var $media = $(this),
				player = $media.data('mediaelementplayer');
				
			$media.closest(tg_media_hold).removeClass('tg-force-play tg-is-playing');
				
			if ($media.length) {
				if (player) {
					player = $media.data('mediaelementplayer');
					player.pause();
					player.setSrc('about:blank');
					$media.children('source').prop('src', '');
					player.remove();
				} else {
					$media[0].pause();
					$media[0].src = 'about:blank';
					$media.children('source').prop('src', '');
					$media.remove().length = 0;
				}
			}
						
		});
		
		// delete all instances of mediaelement.js
		if (mejs) {
			mejs.players = [];
		}
			
	};
	
})(jQuery);

(function($) {
	
	"use strict";
	
	$.fn.TO_MediaRail = function() {
		var dom   = this;
		var ctrl  = dom.closest('.mejs-container').find('.mejs-controls');
		if (ctrl.length > 0) {
			dom.closest('.tg-item').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e) {
				if (!dom.closest('.tg-item').hasClass('tg-item-hidden')) {
					if (dom[0].player) {
						dom[0].player.setControlsSize();
					}
				}
			});
		}
	};

})(jQuery);

// Custom script : image/video lightbox
(function($) {
	
	"use strict";

	$.TO_Lightbox = function() {
	
		var TO_LB_Markup = '<div class="tolb-holder">'+
								'<div class="tolb-loader"></div>'+
									'<div class="tolb-inner">'+
									'<figure>'+
										'<div class="tolb-close tg-icon-close"></div>'+
										'<div class="tolb-content"></div>'+
										'<figcaption>'+
											'<div class="tolb-title"></div>'+
											'<div class="tolb-counter"></div>'+
										'</figcaption>'+
									'</figure>'+
								'</div>'+
								'<div class="tolb-prev"><i class="tg-icon-arrow-prev-thin"></i></div>'+
								'<div class="tolb-next"><i class="tg-icon-arrow-next-thin"></i></div>'+
							'</div>',
			func,
			player,
			media_id,
			media_lb,
			media_src,
			media_alt,
			media_html,
			media_type,
			media_poster,
			media_length,
			media_arr  = [],
			media_data = '[data-tolb-src]:not(.tolb-disabled)',
			holder  = '.tolb-holder',
			inner   = '.tolb-inner',
			content = '.tolb-content',
			video   = '.tolb-video',
			image   = '.tolb-img',
			title   = '.tolb-title',
			counter = '.tolb-counter',
			next    = '.tolb-next',
			prev    = '.tolb-prev',
			close   = '.tolb-close',
			index   = 'tolb-index',
			open    = 'tolb-open',
			ready   = 'tolb-ready',
			loading = 'tolb-loading',
			iframe  = 'tolb-iframe';

		function checkLightbox() {
			media_length = $(media_data).length;
			for (var i = 0; i < media_length; i++) {
				var el = $(media_data).eq(i);
				media_arr[i] = {};
				media_arr[i].type   = el.data('tolb-type');
				media_arr[i].src    = el.data('tolb-src');
				media_arr[i].alt    = el.data('tolb-alt');
				media_arr[i].poster = el.data('tolb-poster');
				el.data(index,i);
			}
			if (media_length > 1) {
				$(next+','+prev).show();
			} else {
				$(next+','+prev).hide();
			}
		}
		
		function getMedia(el) {
			$(holder).addClass(open+' '+loading);
			media_id   = el.data(index);
			media_type = media_arr[media_id].type;
			media_src  = media_arr[media_id].src;
			media_alt  = media_arr[media_id].alt;
			updateIndex();
			switch(media_type) {
				case 'image':
					media_html = $('<img class="tolb-img" src="'+media_src+'" alt="'+media_alt+'"></img>');
					media_lb = new Image();
					media_lb.onload  = load_image;
					media_lb.onerror = load_image;
					media_lb.src = media_src;
					break;
				case 'youtube':
					media_src  = '//www.youtube.com/embed/'+media_src+'?html5=1&controls=1&autohide=1&rel=0&showinfo=0';
					media_html = $('<iframe class="tolb-video" src="'+media_src+'" allowfullscreen></iframe>');
					load_iframe();
					break;
				case 'vimeo':
					media_src  = '//player.vimeo.com/video/'+media_src+'?title=0&amp;byline=0&amp;portrait=0';
					media_html = $('<iframe class="tolb-video" src="'+media_src+'" allowfullscreen></iframe>');
					load_iframe();
					break;
				case 'video':
					var source = '';
					for(var i=0; i<media_src.length ;i++){
						source += '<source src="'+media_src[i][0].source+'" type="video/'+media_src[i][0].type+'" width="100%" height="100%"></source>';
					}
					media_poster = media_arr[media_id].poster;
					media_poster = (media_poster) ? ' poster="'+media_poster+'"' : null;
					media_html   = $('<video class="tolb-video" controls'+media_poster+'>'+source+'</video>');
					load_video();
					break;
			}
		}

		function unloadMedia() {
			var iframe = $(holder).find('iframe').not(media_html);
			if (iframe.length > 0) {
				iframe.attr('src','about:blank').one('load',function(){
					appendMedia();
				});
			} else {
				appendMedia();
			}
		}
		
		function appendMedia() {
			$(holder).addClass(ready);
			func = (media_type !== 'iframe') ?  $(content).html('') : $(content).find('*').not(media_html).remove();
			func = (media_type !== 'image') ? $(holder).addClass(iframe) : $(holder).removeClass(iframe);
			func = (media_type !== 'iframe') && $(content).append(media_html);
			maxHeight();
			updateCaption();
			media_html.show();
			$(holder).removeClass(loading);	
		}
		
		function load_image() {
			unloadMedia();
		}
		
		function load_video() {
			if (!tg_is_mobile) {
				media_html.one('loadeddata',function() {
					func = (player) && player.setSrc('about:blank');
					if (tg_global_var.mediaelement) {
						mediaelement();
					} else {
						unloadMedia();
					}
				});
				media_html[0].addEventListener('error', function(){
					updateCaption();
					$(holder).removeClass(loading);
				});
			} else {
				func = (player) && player.setSrc('about:blank');
				unloadMedia();
			}
		}
		
		function load_iframe() {
			media_type = 'iframe';
			$(content).append(media_html.hide());
			media_html.one('load',function (){
				unloadMedia();
			});
		}
		
		function mediaelement() {
			media_html.mediaelementplayer({
				features: ['playpause', 'stop', 'loop','current','progress','duration','volume', 'sourcechooser', 'fullscreen'],
				videoVolume: 'horizontal',
				startVolume: 0.8,
				success: function(media, domObject) {
					player = media;
					media_html = $(domObject).closest('.mejs-container');
					unloadMedia();
				}	
			});
		}
		
		function maxHeight() {
			$(image).css('max-height',$(window).height()-80-$('#wpadminbar').height());
		}
		
		function updateCaption() {
			$(title).text(media_alt);
			$(counter).text(media_id+1+'/'+media_length);
		}
		
		function updateIndex() {
			$(prev).data(index,checkIndex(media_id-1));
			$(next).data(index,checkIndex(media_id+1));
		}
		
		function checkIndex(media_id) {
			return (media_length + (media_id % media_length)) % media_length;
		}
		
		function close_lightbox() {
			$(holder).removeClass(open+' '+loading+' '+ready);
			setTimeout(function(){
				if ($(holder).find('iframe').length > 0) {
					$(holder).find('iframe').attr('src','about:blank').one('load', function(){
						$(content).html('');
					});
				} else {
					$(content).html('');
				}
			}, 300);
		}
		
		$(window).on('resize', function(){
			maxHeight();	
		});
		
		$(document).on('click','[data-tolb-src]:not(.tolb-disabled)', function() {	
			$.TG_Pause_Players();
			$(video+','+image).remove();
			checkLightbox();
			getMedia($(this));
			return false;
		});
	
		$(document).on('click touchend',next+','+prev, function() {
			getMedia($(this));
			return false;
		});
		
		$(document).on('keydown', throttle(function(e) {
			if ($(holder).hasClass(open)) {
				if (e.keyCode == 37) {
					$(prev).trigger('click');
				} else if(e.keyCode == 39) {
					$(next).trigger('click');
				} else if(e.keyCode == 27) {
					close_lightbox();
				}
			}
		},300));
		
		$(document).on('click touchend',inner+','+close, function(e) {
			e.stopPropagation();
			if ($(e.target).is(inner) || $(e.target).is(close)) {
				close_lightbox();
			}
			return false; 
		});
		
		$('body').append($(TO_LB_Markup));
	
	};

})(jQuery);

// custom plugin to panZoom image on mouseover
(function($) {
	
	"use strict";	
	
	$.fn.TG_PanZoom = function(options) {
		
		return this.each(function(options) {
			
			var settings = $.extend({
				target      : 'div:not(.tg-item-gallery-holder) > .tg-item-image, .tg-item-media-poster, .tg-item-audio-poster, .tg-item-gallery-holder',
				sensitivity : 12.6
			}, options);
			
			var el = $(this).closest('.tg-item');
			
			if (el.hasClass('to-panZ') || el.find(settings.target).length === 0) {
				return true;
			}
			
			var transiton = settings.transiton,
				target = el.find(settings.target),
				h,w,width,cx,x,y;
				
			el.css('overflow','hidden').addClass('to-panZ');
			el.on('mouseenter', function(e){
				if (w !== el.width()) {
					w = el.width();
					h = el.height();
					width = el.width() - settings.sensitivity;
					cx = (w-width)/width;
				}
			}).on('mousemove', function(e){
				x = (-cx*(e.pageX - target.offset().left));
				y = (-cx*(e.pageY - target.offset().top));
				transform(x,y);
			}).on('mouseleave', function(e){
				transform(0,0);
			});
						
			function transform(x,y) {
				target.css('-webkit-transform', 'matrix(1, 0, 0,1,'+x+','+y+')');
			}
		
		});
	
	};
	
	$.TG_Animations = function() {
		$(document).find('.tg-moving').TG_PanZoom();
	};

	
})(jQuery);

// social share link
(function($) {
	
	"use strict";
	
	$(document).on('click', '.tg-facebook', function(e){
		e.preventDefault();
		var data = data_share($(this));
		window.open('http://www.facebook.com/share.php?u=' + data.url + '&t=' + data.text, 'fbShareWin', data.props);
		return false;
	});
	
	$(document).on('click', '.tg-twitter', function(e){
		e.preventDefault();
		var data = data_share($(this));
		window.open('http://twitter.com/share?url=' + data.url + '&text=' + data.title, 'ttShareWin', data.props);
		return false;
	});
	
	$(document).on('click', '.tg-pinterest', function(e){
		e.preventDefault();
		var data = data_share($(this));
		window.open('http://pinterest.com/pin/create/button/?url='+data.url+'&description='+data.title+'&media='+data.img, 'ptShareWin', data.props);
		return false;
	});
	
	$(document).on('click', '.tg-google1', function(e){
		e.preventDefault();
		var data = data_share($(this));
		window.open('https://plus.google.com/share?url='+data.url, 'g1ShareWin', data.props) ;
		return false;
	});
	
	function data_share(el) {
		var data   = [],
			width  = 626,
			height = 436,
			l = Math.round(window.screenX + (window.outerWidth - width) / 2),
			t = Math.round(window.screenY + (window.outerHeight - height) / 2);

		data.url   = encodeURIComponent(el.closest('.tg-item').find('.tg-item-title a').attr('href'));
		data.title = encodeURIComponent(el.closest('.tg-item').find('.tg-item-title a').text());
		data.text  = encodeURIComponent(el.closest('.tg-item').find('.tg-item-excerpt').text());
		if(data.title.length > 140){ data.title = data.title.substring(0, 140); }
		if(data.text.length > 120){ data.text = data.text.substring(0, 120); }
		data.img   = encodeURIComponent(el.closest('.tg-item').find('img.tg-item-image').attr('src'));
		data.props = ['width='+width,'height='+height,'left='+l,'top='+t,'status=0','resizable=1', 'location=0', 'toolbar=0'].join(',');
		return data;
	}
	
})(jQuery);

// run the grid plugin if it match selector
(function($) {
	
	"use strict";
	$.TG_media_init();
	// Initializing the awesome
	$(document).ready(function() {
		// because of @keyframe and @font-face issue in Firefox
		$('.preloader-styles, .the_b60_grid_styles').removeAttr('scoped');
		$('.tg-grid-holder').the_b60_grid();
		$.TO_Lightbox();
	});

})(jQuery);

// mediaelement extended for loop and source button (The Grid lightbox only)
// Loop: http://mediaelementjs.com/examples/?name=loop
// source chooser: https://github.com/johndyer/mediaelement/blob/master/src/js/mep-feature-sourcechooser.js

if (tg_global_var.mediaelement_ex) {
	
!function(e){e.extend(mejs.MepDefaults,{sourcechooserText:"Source Chooser"}),e.extend(MediaElementPlayer.prototype,{buildsourcechooser:function(o,t,s,c){var r=this;o.sourcechooserButton=e('<div class="mejs-button mejs-sourcechooser-button"><button type="button" aria-controls="'+r.id+'" title="'+r.options.sourcechooserText+'" aria-label="'+r.options.sourcechooserText+'"></button><div class="mejs-sourcechooser-selector"><ul></ul></div></div>').appendTo(t).delegate("input[type=checkbox]","click",function(){var o=this.value;e(this).closest(".mejs-sourcechooser-selector").find("input").removeAttr("checked"),e(this).closest(".mejs-sourcechooser-selector").find("label").removeClass("active"),e(this).next("label").addClass("active"),c.currentSrc!=o&&(currentTime=c.currentTime,paused=c.paused,c.setSrc(o),c.load(),c.addEventListener("loadedmetadata",function(){this.currentTime=currentTime},!0),c.addEventListener("canplay",function(){paused||this.play()},!0))});for(var n in c.children){var i=c.children[n];"SOURCE"!==i.nodeName||"probably"!=c.canPlayType(i.type)&&"maybe"!=c.canPlayType(i.type)||o.addSourceButton(i.src,i.title,i.type,c.src==i.src)}},addSourceButton:function(o,t,s,c){var r=this;(""===t||void 0==t)&&(t=o),s=s.split("/")[1],r.sourcechooserButton.find("ul").append(e('<li><input type="checkbox" name="'+r.id+'_sourcechooser" id="'+r.id+"_sourcechooser_"+s+'" value="'+o+'" '+(c?'checked="checked"':"")+' /><label for="'+r.id+"_sourcechooser_"+s+'">'+s+"</label></li>")),r.adjustSourcechooserBox()},adjustSourcechooserBox:function(){var e=this;e.sourcechooserButton.find(".mejs-sourcechooser-selector").height(e.sourcechooserButton.find(".mejs-sourcechooser-selector ul").outerHeight(!0))}})}(mejs.$),function(e){e.extend(MediaElementPlayer.prototype,{buildloop:function(o,t){var s=this,c=e('<div class="mejs-button mejs-loop-button '+(o.options.loop?"mejs-loop-on":"mejs-loop-off")+'"><button type="button" aria-controls="'+s.id+'" title="Toggle Loop" aria-label="Toggle Loop"></button></div>').appendTo(t).click(function(){o.options.loop=!o.options.loop,o.options.loop?c.removeClass("mejs-loop-off").addClass("mejs-loop-on"):c.removeClass("mejs-loop-on").addClass("mejs-loop-off")})}})}(mejs.$);

}

/**** smart masonry height
var y = 0, pH = 0, ne = 0, cH, coli;
for (var i = 0; i <= $item.length; i++) {
	ne++;
	coli = parseInt($item.eq(i).data('col'));
	y    = (coli > 1) ? y+coli : y+1;
	cH   = $item.eq(i).height();
	if (y <= colN && cH > pH) {
		pH = cH;
	}
	if (y >= colN) {
		for (var c = 0; c < ne; c++) {
			$item.eq(i-c).height(pH);
		}
		pH = ne = y = 0;
	}
}
***/

// Custom swipe left/right
/*(function($) { 

	$.fn.tactile = function(swipe) {
    
		return this.each(function() {
			
			var $this = $(document),
				isTouching = false,
				debut;
	
				$this.on('touchstart', debutGeste);       
	
			function debutGeste() {
				if (event.touches.length == 1) {
					debut = event.touches[0].pageX;
					isTouching = true;
					$this.on('touchmove', geste);
				}
			}
		
			function finGeste() {
				$this.off('touchmove');
				isTouching = false;
				debut = null;
			} 
		
			function geste() {
				if(isTouching) {
					var actuel = event.touches[0].pageX,
					delta = debut - actuel;
		
					if (Math.abs(delta) >= 30) {
						if (delta > 0) {
							swipe.left();
						} else {
							swipe.right();
						}                       
						finGeste();
					}
				}
				event.preventDefault(); 
			}
		});
	};
	
})(jQuery);*/
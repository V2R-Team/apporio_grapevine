<?php
/**
 * @package   The_Grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 */

// Exit if accessed directly
if (!defined( 'ABSPATH')) { 
	exit;
}
 
class The_Grid_Admin {
	
	const VIEW_GRID_HEADER   = "grid-header";
	const VIEW_GRID_BANNER1  = "grid-banner1";
	const VIEW_GRID_BANNER2  = "grid-banner2";
	const VIEW_GRID_BANNER3  = "grid-banner3";
	const VIEW_GRID_BANNER4  = "grid-banner4";
	const VIEW_GRID_INFOBOX  = "grid-infobox";
	const VIEW_GRID_LIST     = "grid-list";
	const VIEW_GRID_SETTINGS = "grid-settings";
	const VIEW_GRID_CONFIG   = "grid-config";
	const VIEW_GRID_IMPORT   = "grid-import";
	const VIEW_GRID_EXPORT   = "grid-export";
	const VIEW_GRID_FOOTER   = "grid-footer";
	const VIEW_GRID_FORMAT   = "grid-format";
	
	// Instance of this class.
	protected $plugin_slug = TG_SLUG;
	protected $current_post;
	protected static $view;
	
	/**
	* Initialization (load admin scripts & styles and add main actions)
	* @since 1.0.0
	*/
	public function __construct() {
		// load metabox class for grid settings
		require_once(TG_PLUGIN_PATH . '/includes/metabox/tomb.php');
		// admin ajx
		$this->admin_ajax();
		// add custom css for admin menu icon
		add_action( 'admin_head', array( &$this, 'the_grid_admin_css'));
		// remove metaboxes
		add_action('add_meta_boxes', array( &$this, 'remove_meta_boxes'),999);
		// load grid config metabox
		add_action('add_meta_boxes', array($this, 'the_grid_metabox' ),1);
		// load metabox framework
		add_action('admin_menu', array($this, 'add_plugin_admin_menu'));
		// on admin init add extra field on post type grid
		add_action('admin_init', array($this, 'the_grid_admin_init' ) );
		// Load admin style sheet and JavaScript.
		add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_styles'));
		add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_scripts'));				
	}
	
	/**
	* Add ajax callback
	* @since 1.0.0
	*/
	public function admin_ajax(){
		// ajax save
		add_action('wp_ajax_save_the_grid', array($this, 'save_the_grid_callback'));
		// ajax save item preview
		add_action('wp_ajax_save_the_grid_item', array($this, 'save_the_grid_item_callback'));	
		// ajax clone post
		add_action('wp_ajax_clone_the_grid', array($this, 'clone_the_grid_callback'));	
		// ajax delete post
		add_action('wp_ajax_delete_the_grid', array($this, 'delete_the_grid_callback'));
		// ajax add to favorite
		add_action('wp_ajax_favorite_the_grid', array($this, 'favorite_the_grid_callback'));
		// ajax order the grid list
		add_action('wp_ajax_order_the_grid', array($this, 'order_the_grid_callback'));
		// ajax number item in the grid list
		add_action('wp_ajax_number_the_grid', array($this, 'number_the_grid_callback'));
		// ajax page nb in the grid list
		add_action('wp_ajax_pagenb_the_grid', array($this, 'pagenb_the_grid_callback'));
		// ajax export file
		add_action('wp_ajax_grid_export', array($this, 'grid_export_callback'));
		// ajax import file
		add_action('wp_ajax_grid_import', array($this, 'grid_import_callback'));
		// ajax import demo
		add_action('wp_ajax_grid_import_demo', array($this, 'grid_import_demo_callback'));
		// ajax save Global Settings
		add_action('wp_ajax_save_settings', array($this, 'save_settings_callback'));
		// Grid skins
		add_action('wp_ajax_grid_skin_selector', array(new The_Grid_Custom_Fields(), 'grid_skin_selector'));
		// Clear cache
		add_action('wp_ajax_grid_clear_cache', array($this, 'the_grid_clear_cache_callback'));
	}
	
	/**
	* Remove unecessary metaboxes (improve core query UPDATE metadata speed)
	* @since 1.0.0
	*/
	public function remove_meta_boxes(){
		$post_type = $this->get_current_post_type();
		if ($post_type == $this->plugin_slug) {
			global $wp_meta_boxes;
			foreach($wp_meta_boxes['the_grid'] as $context => $param){
				foreach($param as $priority => $meta_box){
					foreach($meta_box as $name => $value){					
						if ($name != 'the_grid_metabox') {
							unset($wp_meta_boxes['the_grid'][$context][$priority][$name]);
						}
					}
				}
			}	
		}
	}

	/**
	* Load metabox
	* @since 1.0.0
	*/
	public function the_grid_metabox($post) {
		global $post;
		$post_ID   = isset($_GET['post']) ? $_GET['post'] : 0;
		if ($post_ID) {
			$post_type = get_post_type($post_ID);
		} else {
			$post_type = $this->get_current_post_type();
		}
		if ($post_type == $this->plugin_slug) {
			
			require_once('views/'.self::VIEW_GRID_CONFIG.'.php');
		}	
	}
	
	/**
	* Check post type
	* @since 1.0.0
	*/
	public function get_current_post_type() {
		global $post, $typenow, $current_screen;
		if ($post && $post->post_type) {
			return $post->post_type;
		} else if ($typenow) {
			return $typenow;
		} else if ($current_screen && $current_screen->post_type) {
			return $current_screen->post_type;
		} else if (isset($_REQUEST['post_type'])) {
			return sanitize_key($_REQUEST['post_type']);
		}
		return null;
	}
	
	/**
	* Init the grid admin plugin settings for post only
	* @since 1.0.0
	*/
	public function the_grid_admin_init() {	
		global $typenow;
        // when editing pages, $typenow isn't set until later!
        if (empty($typenow)) {
            // try to pick it up from the query string
            if (isset($_GET['post']) && !empty($_GET['post'])) {
                $post = get_post($_GET['post']);
                $typenow = $post->post_type;
            }
            // try to pick it up from the quick edit AJAX post
            elseif (isset($_POST['post_ID']) && !empty($_POST['post_ID'])) {
                $post = get_post($_POST['post_ID']);
                $typenow = $post->post_type;
            }
        }
		// if post type the_grid add extra field after removed title
		if ($typenow == $this->plugin_slug) {
			add_action( 'edit_form_after_title', array($this, 'edit_form_after_title'));
			add_action( 'edit_form_advanced', array($this, 'edit_form_advanced'));
			add_filter( 'screen_options_show_screen', array($this, 'remove_screen_options'), 10, 2);
		}
		// load metabox class for grid settings (in eacg post type)
		require_once(TG_PLUGIN_PATH . '/backend/views/'.self::VIEW_GRID_FORMAT.'.php');
	}

	/**
	* Add title after title
	* @since 1.0.0
	*/
	public function edit_form_after_title() {
		require_once('views/'.self::VIEW_GRID_BANNER4.'.php');
	}
	
	/**
	* Add save, update information
	* @since 1.0.0
	*/
	public function edit_form_advanced() {
		require_once('views/'.self::VIEW_GRID_INFOBOX.'.php');
	}
	
	/**
	* Remove screen option tab
	* @since 1.0.0
	*/
	public function remove_screen_options($display_boolean, $wp_screen_object){
		$blacklist = array('post.php', 'post-new.php', 'index.php', 'edit.php');
		if (in_array($GLOBALS['pagenow'], $blacklist)) {
			$wp_screen_object->render_screen_layout();
			$wp_screen_object->render_per_page_options();
			return false;
		} else {
			return true;
		}
	}

	/**
	* Add css icon admin menu
	* @since 1.0.0
	*/
	public function the_grid_admin_css()  {
		echo '<style>.wp-has-submenu.toplevel_page_the_grid img{position: relative;top: -2px;}</style>';
	}

	/**
	* Register admin menu in Dashboard menu.
	* @since 1.0.0
	*/
	public function add_plugin_admin_menu() {
		global $typenow, $submenu;
		
		$icon_svg = 'data:image;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAEN0lEQVQ4y4WUW2xURRzGf3Pm7NnL2ft2W0tpUVFrbdCgUgRCCQYeJFEJisYg8U0gPCH4gpKYKPGCPmEMT5gYMDwo4YGEUKEBpQEKJFgxKWp6oy203a7dXnZPz54z40NLpSbgP5nkn8nMb2a+zPcJPv8OXnkL+sZAhUH4YACOBlfP9PA1IIEd3C2tIZYk21dkvfMVo83XKFzxZpc/uHYDO4HtQM2/sASJsRw7L54j4ZQoWhbAA4AagD3Al/fMbkRriCdJ/p1jxy8txFyHfNhGavU/QMEeNAfmH6JTxBKkRkd4t+0MsfI0w5HYHOz+QMX7BMUBLAFqnmY30vkRtre1EHenGbbj82D3A+4GvgAgLMAEfA3xVCGdHz6xva2FiFeOjNixyH9hM0DfAyEAMV8zPTvCArIpKkeHntnW9tO6sOe15iKxW1Krfi30eemZW003iDY0WmnMewR6D4wD4N/zdA3ZBIm/+le83XL6xYQdOHzbjiGVQgtNwA02a6GbJzLDzxqe3CUECM71QSCwlmKpFT8FWEB55nbZBKnuwaZNx3543lTqG1lXTSxo4voephskWLTpXHmGgSc6sP3MBuV4pwQ3NXT/2Y5kGRjgV4AKQDZCundw+aZjx5ss3z84GosifEUmEyEp44iJEJ0vnKWv8RqR8SSGNi+EHrJXG7jTEikbZ4zgg8xDNDhQ82vPhteO/vh6QHkHxxJxLABDMz7oo0YCdK06R2/jVcLjKYRvorV+2rCkYRAKasCdES0IGFTZneNLC+2XoqPO0inTRqBQQhOfTBKcDnW1LDsx/lvDJVITFRi+BKEBXLTGxEThlc9jBl9FS2rlEAsncw3daxdcLqSjKxaf7GkI3wm8KWWlzlfdOfX72p/fGVjU83JkOE5MKuLSZFprMMRF5fpKcDkHrldPyelcKHPUyTwlZeFLg1ImeDvZ5Syoa+2jELvOHytvbJu2pw5Fx5J4WqPQ1AYiRI0AKmw0Kce/YnDoM6iuupmVY1selqMUtYUSAqE00WGneqqmpnBxc6n+6urje4WhD0XzaTRgCoGBoL9cZEp5e+0K+8rQ+T5M+rvABEuo7z0MoRFHmP3mLhpdyMWXhZd09njLGSjdJGHVgmYO6mj1oaP9TwMBSbFvHINUBjIg6hYTqKw6ihBbhdaUtcbRinpL0RiuZlX2A9LWoxTcWwiMGahmn4xb++26BNHHU4QX2Eg5OoI4/RN0tBNMxQk1PNUh4oked3Ji45NmgEWZCkpGGRkK8Yi9mtuT15n0h4hUZz8yK8Mfl3snmDjZS/7sLYYuDCCsWZeVAYQktX4d9uY3eG7d+i2LPO8IgFKzWSdMhClondq/b7i945PpkzmmWgdxC8W5zBOBYOhuj+c4s7kKC5vX0PjShvra5jWHQ+l0k1bK11o5Y53duzJL6r89WP/Y7DYDM2jN2f8fKGLQEJGq4PoAAAAASUVORK5CYII=';

		add_menu_page(
			__('The Grid', 'tg-text-domain' ),
			__('The Grid', 'tg-text-domain' ),
			'manage_options',
			$this->plugin_slug,
			array($this, 'display_plugin_admin_page'),
			$icon_svg
		);
		// remove edit and add new button
		remove_submenu_page( 'edit.php?post_type='.$this->plugin_slug, 'post-new.php?post_type='.$this->plugin_slug );
		// add import/export submenu page
		add_submenu_page(
			$this->plugin_slug,
			'Import/Export',
			'Import/Export',
			'manage_options',
			'Import/Export',
			array($this, 'display_plugin_admin_export_import_page')
		);
		// add global settings submenu page
		add_submenu_page(
			$this->plugin_slug,
			'Global Settings',
			'Global Settings',
			'manage_options',
			'GlobalSettings',
			array($this, 'display_plugin_admin_settings_page')
		);
	}
		
	/**
	* Include admin page for layout
	* @since 1.0.0
	*/
	public function display_plugin_admin_page() {
		require_once('views/'.self::VIEW_GRID_HEADER.'.php');
		require_once('views/'.self::VIEW_GRID_BANNER1.'.php');
		require_once('views/'.self::VIEW_GRID_LIST.'.php');
		require_once('views/'.self::VIEW_GRID_FOOTER.'.php');
	}
	
	/**
	* Include admin page import/export for layout
	* @since 1.0.0
	*/
	public function display_plugin_admin_export_import_page() {
		require_once('views/'.self::VIEW_GRID_HEADER.'.php');
		require_once('views/'.self::VIEW_GRID_BANNER2.'.php');
		require_once('views/'.self::VIEW_GRID_EXPORT.'.php');
		require_once('views/'.self::VIEW_GRID_IMPORT.'.php');
		require_once('views/'.self::VIEW_GRID_FOOTER.'.php');
	}
	
	/**
	* Include admin page global settings for layout
	* @since 1.0.0
	*/
	public function display_plugin_admin_settings_page() {
		require_once('views/'.self::VIEW_GRID_HEADER.'.php');
		require_once('views/'.self::VIEW_GRID_BANNER3.'.php');
		require_once('views/'.self::VIEW_GRID_SETTINGS.'.php');
		require_once('views/'.self::VIEW_GRID_FOOTER.'.php');
	}
	
	/**
	* Check nonce in admin
	* @since 1.0.0
	*/
	public function the_grid_check_nonce() {
		// retrieve nonce
		$nonce  = (isset($_POST['nonce'])) ? $_POST['nonce'] : $_GET['nonce']; 
		// nonce action for the grid
		$action = 'tg_admin_nonce';
		// check ajax nounce
		if (!wp_verify_nonce($nonce, $action)) {
			die();
		}
	}
	
	/**
	* Clear cache
	* @since 1.0.0
	*/
	public function the_grid_clear_cache_callback() {
		
		// check the nonce
		$this->the_grid_check_nonce();
		
		global $wpdb;
		
		$sql = "SELECT `option_name` AS `name`, `option_value` AS `value`
				FROM  $wpdb->options
				WHERE `option_name` LIKE '%transient_%'
				ORDER BY `option_name`";

		$results = $wpdb->get_results( $sql );
		$transients = array();

		foreach ($results as $result) {
			if (strpos($result->name, 'tg_grid_transient_')) {
				$name = str_replace('_transient_','',$result->name);
				$name = str_replace('_transient_timeout_','',$result->name);
				delete_transient($name);
			}
		}
		
		die();
	
	}
	
	/**
	* Save meta box
	* @since 1.0.0
	*/
	public function save_the_grid_callback() {
		
		// check the nonce
		$this->the_grid_check_nonce();
		
		// retrieve data from jquery
		global $wpdb;
		$post_ID   = $_POST['post_ID'];
		$meta_data = $_POST['meta_data'];
		$favorite  = get_post_meta($post_ID, 'the_grid_favorite', true);
		$datetime  = current_time('Y-m-d\ H:i:s');
		// insert post if do not exist
		if ('publish' != get_post_status($post_ID)) {
			$post_ID = wp_insert_post( array(
				'import_id'    => $post_ID,
				'post_type'    => $this->plugin_slug,
				'post_title'   => $meta_data['the_grid_name'],
				'post_name'    => $meta_data['the_grid_name'],
				'post_content' => null,
				'post_status'  => 'publish',
				'post_author'  => ''
			));
			// set meta value favorite
			add_post_meta($post_ID, 'the_grid_favorite', '');
			// save post title if post exist
			$wpdb->query( "UPDATE `$wpdb->posts` SET `post_date` = '".$datetime." 'WHERE `ID` = '".$post_ID."'" );
		} else {
			// save post title if post exist
			$wpdb->update( $wpdb->posts, array( 'post_title' =>  $meta_data['the_grid_name'] ), array( 'ID' => $post_ID ) );
			$wpdb->query( "UPDATE `$wpdb->posts` SET `post_modified` = '".$datetime." 'WHERE `ID` = '".$post_ID."'" );
			
		}
		
		// add metadata
		foreach ($meta_data as $meta => $value) {
			update_post_meta($post_ID, $meta, $value );
		}
		
		// delete cache if grid change
		$grid_name = 'tg_grid_transient_'.$post_ID;
		global $wpdb;
		$sql = "SELECT `option_name` AS `name`, `option_value` AS `value`
				FROM  $wpdb->options
				WHERE `option_name` LIKE '%transient_%'
				ORDER BY `option_name`";
		$results = $wpdb->get_results( $sql );
		$transients = array();
		foreach ($results as $result) {
			if (strpos($result->name, $grid_name)) {
				$name = str_replace('_transient_','',$result->name);
				$name = str_replace('_transient_timeout_','',$result->name);
				delete_transient($name);
			}
		}
		
		// change post new post ID
		global $pagenow;
		if ($_POST['post_ID'] != $post_ID && 'edit.php' != $pagenow) {
			echo $post_ID;
		}

		die();
	}
	
	
	/**
	* Save item settings meta box value only
	* @since 1.0.0
	*/
	public function save_the_grid_item_callback() {
		
		// check the nonce
		$this->the_grid_check_nonce();
		
		// retrieve data from jquery
		global $wpdb;
		$post_ID   = $_POST['post_ID'];
		$meta_data = $_POST['meta_data'];
		// add metadata
		foreach ($meta_data as $meta => $value) {
			update_post_meta($post_ID, $meta, $value );
		}
		die();
	}
	
	/**
	* Clone grid list
	* @since 1.0.0
	*/
	public function clone_the_grid_callback($post, $status = '', $parent_id = '') {
		
		// check the nonce
		$this->the_grid_check_nonce();
		
		$post = get_post($_POST['post_ID']);
		
		$new_post = array(
			'menu_order'     => $post->menu_order,
			'comment_status' => $post->comment_status,
			'ping_status'    => 'default_ping_status',
			'post_author'    => '',
			'post_content'   => '',
			'post_excerpt'   => '',
			'post_parent'    => 0,
			'post_password'  => '',
			'post_status'    => 'publish',
			'post_title'     => $post->post_title,
			'post_name'      => $post->post_title,
			'post_type'      => $this->plugin_slug
		);

		$new_post_id = wp_insert_post($new_post);

		//unique post slug based on cloned post
		$post_title = $post->post_title.' '.$new_post_id;
		
		$new_post = array();
		$new_post['ID'] = $new_post_id;
		$new_post['post_title'] = $post_title;
		$new_post['post_name']  = $post_title;
		// Update the post into the database
		wp_update_post( $new_post );
		
		// get meta data and clone
		$this->duplicate_post_meta_info($new_post_id,$post);
		
		// add new post title to meta value
		update_post_meta($new_post_id, 'the_grid_name', $post_title);

		// sort to date and DSC in order to see the new one cloned
		update_option('the_grid_order', 'data');
		update_option('the_grid_orderby', 'DSC');

		$view = require_once('views/'.self::VIEW_GRID_LIST.'.php');
		echo $view;
		
		die();
	}
	
	/**
	* Delete grid list
	* @since 1.0.0
	*/
	public function delete_the_grid_callback() {
		
		// check the nonce
		$this->the_grid_check_nonce();
		
		$post_ID = $_POST['post_ID'];
		wp_delete_post( $post_ID, true);
		$view = require_once('views/'.self::VIEW_GRID_LIST.'.php');
		echo $view;
	}
	
	/**
	* Favorite grid list
	* @since 1.0.0
	*/
	public function favorite_the_grid_callback() {
		
		// check the nonce
		$this->the_grid_check_nonce();
		
		$post_ID  = $_POST['post_ID'];
		$favorite = $_POST['meta_data'];
		if ($favorite != 'favorite') {
			update_post_meta($post_ID, 'the_grid_favorite', 'favorite');
		} else {
			update_post_meta($post_ID, 'the_grid_favorite', '');
		}
		$view = require_once('views/'.self::VIEW_GRID_LIST.'.php');
		echo $view;
	}
	
	/**
	* Order grid list
	* @since 1.0.0
	*/
	public function order_the_grid_callback() {
		
		// check the nonce
		$this->the_grid_check_nonce();
		
		$order   = $_POST['order'];
		$orderby = $_POST['orderby'];
		update_option('the_grid_order', $order);
		update_option('the_grid_orderby', $orderby);
		$view = require_once('views/'.self::VIEW_GRID_LIST.'.php');
		echo $view;
	}
	
	/**
	* Item number in grid list
	* @since 1.0.0
	*/
	public function number_the_grid_callback() {
		
		// check the nonce
		$this->the_grid_check_nonce();
		
		$number = $_POST['number'];
		update_option('the_grid_number', $number);
		$view = require_once('views/'.self::VIEW_GRID_LIST.'.php');
		echo $view;
	}
	
	/**
	* Page number in grid list
	* @since 1.0.0
	*/
	public function pagenb_the_grid_callback() {
		
		// check the nonce
		$this->the_grid_check_nonce();
		
		$_GET['pagenum'] = $_POST['page_nb'];
		$view = require_once('views/'.self::VIEW_GRID_LIST.'.php');
		echo $view;
	}
	
	/**
	* Get all meta data / add them to new post
	* @since 1.0.0
	*/
	public function duplicate_post_meta_info($new_id, $post) {
		$post_meta_keys = get_post_custom_keys($post->ID);
		
		if (empty($post_meta_keys)) {
			return;
		}
	
		foreach ($post_meta_keys as $meta_key) {
			$meta_values = get_post_custom_values($meta_key, $post->ID);
			foreach ($meta_values as $meta_value) {
				$meta_value = maybe_unserialize($meta_value);
				add_post_meta($new_id, $meta_key, $meta_value);
			}
		}
	}
	
	/**
	* Export Grid(s) Settings as .json file
	* @since 1.0.0
	*/
	public function grid_export_callback() {
		
		// check the nonce
		$this->the_grid_check_nonce();
		
		$post_info = array();
		$grid_ids  = $_GET['post_ID'];
		foreach ($grid_ids as $grid_id) {	
			$grids['grids'][get_post_meta($grid_id, 'the_grid_name', true)] = $this->the_grid_export_settings($grid_id);
		}
		echo json_encode($grids);
		die;
	}
	
	/**
	* Save grid settings to export
	* @since 1.0.0
	*/
	public function the_grid_export_settings($post_ID) {
		
		$post_meta_keys = get_post_custom_keys($post_ID);

		if (empty($post_meta_keys)) {
			return;
		}

		foreach ($post_meta_keys as $meta_key) {	
			if (strpos($meta_key,$this->plugin_slug) !== false) {
				$meta_values = get_post_custom_values($meta_key, $post_ID);
				foreach ($meta_values as $meta_value) {
					$meta_value = maybe_unserialize($meta_value);
					$post_info[$meta_key] = $meta_value;
				}
			}
		}
		return $post_info;
	}
	
	/**
	* Import Grid(s) Settings as .json file
	* @since 1.0.0
	*/
	public function grid_import_callback() {
		
		// check the nonce
		$this->the_grid_check_nonce();
		
		$json = file_get_contents($_FILES['file']['tmp_name']);
		$obj = json_decode($json);
		if (!isset($obj->grids)) {
			echo '<span class="tg-import-error"><br>'. __( 'The file uploaded doesn\'t meet the standard grid settings.', 'tg-text-domain') .'</span>';
		} else {
			$grids = $obj->grids;
			foreach ($grids as $grid => $settings) {
				$new_grid_id = self::the_grid_import_settings($grid);
				foreach ($settings as $meta => $value) {
					add_post_meta($new_grid_id, $meta, $value);
				}
			}
		}
		die;
	}
	
	/**
	* Import Grid(s) Demo  Settings as .json file
	* @since 1.0.0
	*/
	public function grid_import_demo_callback() {
		
		// check the nonce
		$this->the_grid_check_nonce();
		
		$json = file_get_contents($_POST['file']);
		$obj = json_decode($json);
		if (!isset($obj->grids)) {
			echo '<span class="tg-import-error"><br>'. __( 'The file uploaded doesn\'t meet the standard grid settings.', 'tg-text-domain') .'</span>';
		} else {
			$grids = $obj->grids;
			foreach ($grids as $grid => $settings) {
				$new_grid_id = self::the_grid_import_settings($grid);
				foreach ($settings as $meta => $value) {
					add_post_meta($new_grid_id, $meta, $value);
				}
			}
		}
		$view = require_once('views/'.self::VIEW_GRID_LIST.'.php');
		echo $view;
		die;
	}
	
	/**
	* Import functionnality
	* @since 1.0.0
	*/
	public function the_grid_import_settings($grid) {
		$new_grid = array(
			'menu_order'     => '',
			'comment_status' => '',
			'ping_status'    => 'default_ping_status',
			'post_author'    => '',
			'post_content'   => '',
			'post_excerpt'   => '',
			'post_parent'    => 0,
			'post_password'  => '',
			'post_status'    => 'publish',
			'post_title'     => $grid,
			'post_name'      => $grid,
			'post_type'      => $this->plugin_slug,
		);
		$post_exists = post_exists($grid);
				
		if (!$post_exists && post_type_exists($this->plugin_slug)) {
			$new_grid_id = wp_insert_post($new_grid);	
			return $new_grid_id;
		}
	}
	
	/**
	* Save Global Settings
	* @since 1.0.0
	*/
	public function save_settings_callback() {
		
		// check the nonce
		$this->the_grid_check_nonce();
		
		// retrieve data from jquery
		$setting_data = $_POST['setting_data'];
		// add metadata
		foreach ($setting_data as $setting => $value) {
			update_option($setting, $value );
		}
		$settings = require_once('views/'.self::VIEW_GRID_SETTINGS.'.php');
		echo $settings;
		die();
	}
	
	/**
	* Enqueue admin styles
	* @since 1.0.0
	*/
	public function enqueue_admin_styles() {
		$screen = get_current_screen();
		$page_name = esc_html(get_admin_page_title());
		if ($screen->id == $this->plugin_slug) {		
			wp_enqueue_style($this->plugin_slug .'-admin-post-styles', TG_PLUGIN_URL . 'backend/assets/css/admin-post.css', array(), TG_VERSION);
		}
		if ($page_name == 'The Grid' || $page_name == 'Import/Export' || $page_name == 'Global Settings' || $screen->id == $this->plugin_slug) {
			wp_enqueue_style($this->plugin_slug .'-admin-page-styles', TG_PLUGIN_URL . 'backend/assets/css/admin-page.css', array(), TG_VERSION);
		}
	}
	
	/**
	* Enqueue admin scripts
	* @since 1.0.0
	*/
	public function enqueue_admin_scripts() {
		$screen = get_current_screen();
		$page_name = esc_html(get_admin_page_title());
		if ($screen->id == $this->plugin_slug) {
			wp_enqueue_script($this->plugin_slug . '-admin-post-js', TG_PLUGIN_URL . 'backend/assets/js/admin-post.js', array('jquery'), TG_VERSION);
			wp_localize_script($this->plugin_slug . '-admin-post-js', 'tg_admin_global_var', array('url' => admin_url( 'admin-ajax.php' ),'nonce' => wp_create_nonce( 'tg_admin_nonce' )));
			//remove auto save functionnality only on grid post type
			wp_dequeue_script( 'autosave' );
		}
		if ($page_name == 'The Grid' || $page_name == 'Import/Export' || $page_name == 'Global Settings' || $screen->id == $this->plugin_slug) { 
			wp_enqueue_script($this->plugin_slug . '-admin-page-js', TG_PLUGIN_URL . 'backend/assets/js/admin-page.js', array('jquery'), TG_VERSION, true );
			wp_localize_script($this->plugin_slug . '-admin-page-js', 'tg_admin_global_var', array('url' => admin_url( 'admin-ajax.php' ),'nonce' => wp_create_nonce( 'tg_admin_nonce' )));
		}
	}
	
}

new The_Grid_Admin;

// Extended class (of core grid) for grid preview backend
class The_Grid_preview extends The_Grid{
	
	protected $grid_data;
	
	/**
	* Initialization child
	* @since 1.0.0
	*/
	function __construct() {
		
        parent::__construct();
		
		// The Grid Preview
		add_action('wp_ajax_grid_preview', array($this, 'grid_preview_callback'));
		add_action('wp_ajax_nopriv_grid_preview', array($this, 'grid_preview_callback'));
    }

	/**
	* Grid preview ajax
	* @since 1.0.0
	*/
	public function grid_preview_callback() {
		
		// check nonce
		$nonce  = $_POST['nonce']; 
		$action = 'tg_admin_nonce';
		if (!wp_verify_nonce($nonce, $action)) {
			die();
		}

		$meta_data = $_POST['meta_data'];
		
		$base = new The_Grid_Base();
		
		$grid_name = $meta_data['the_grid_name'];
		$grid_info = $meta_data['the_grid_info'];

		if (!isset($grid_info)) {
			return false;
		}

		foreach ($meta_data as $data => $val) {
			$grid_data[$grid_name][$data] = wp_unslash($val);
		}
		$grid_data[$grid_name]['the_grid_ID']   = 'grid-'.$grid_info;
		$grid_data[$grid_name]['the_grid_name'] = $meta_data['the_grid_name'];
		$this->grid_data = $grid_data[$grid_name];
		
		global $tg_grid_data;
		$tg_grid_data = $grid_data[$grid_name];
		
		if ($this->grid_data != false) {
			$content = parent::the_grid_show();
			echo $content;
			die();
		}
		
	}
}

new The_Grid_preview();
<?php
/**
 * @package   The_Grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 */

// Exit if accessed directly
if (!defined( 'ABSPATH')) { 
	exit;
}

class the_grid_shortcode{
	
 	protected $shortcode_tag = TG_SLUG;
	
	/**
	* Initialization
	* @since 1.0.0
	*/
	function __construct(){
		// remove empty <p></p> tags
		add_filter('the_content', array($this, 'the_content_filter'));
		// register shortcode 
		add_shortcode('the_grid', array($this, 'the_grid_shortcode'));
		// add tinymce button and popup
		if (is_admin()){
			add_action('admin_head', array($this, 'admin_head'));
			add_action('admin_enqueue_scripts', array($this , 'admin_enqueue_scripts'));
		}
	}
	
	/**
	* Filter the content to remove empty p tags from the_grid shortcode
	* BitFade Method To clean shortcode (http://themeforest.net/forums/thread/how-to-add-shortcodes-in-wp-themes-without-being-rejected/98804?page=4#996848)
	* @since 1.0.0
	*/
	public function the_content_filter($content) {
		
		$block = array('the_grid');

		if (count($block) === 0) {
			return $content;
		}
	
		$block = join("|",$block);
		// opening tag
		$rep = preg_replace("/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/","[$2$3]",$content);
		// closing tag
		$rep = preg_replace("/(<p>)?\[\/($block)](<\/p>|<br \/>)?/","[/$2]",$rep);
		
		return $rep;
	}
	
	/**
	* Register Shortcode
	* @since 1.0.0
	*/
	public function the_grid_shortcode($atts, $content = null){
		extract(shortcode_atts(array(
			'name' => '',
	    ), $atts));
		if (!empty($name)) {
			$grid = new The_Grid();
			ob_start();
			$grid->the_grid_output($name);
			$content = ob_get_contents();
			ob_clean();
			ob_end_clean();
			if (empty($content)) {
				return '<div class="tg-no-post">'.__('No grid was found for:', 'tg-text-domain' ).' '.$name.'</div>';
			} else {
				return ($content);
			}
		} else {
			return '<div class="tg-no-post">'.__('The shortcode doesn\'t contain any grid name', 'tg-text-domain' ).'</div>';
		}
	}

	/**
	* Register Shortcode button
	* @since 1.0.0
	*/
	function admin_head() {
		// check user permissions
		if (!current_user_can('edit_posts') && !current_user_can('edit_pages')) {
			return;
		}
		// check if WYSIWYG is enabled
		if (get_user_option('rich_editing') == 'true') {
			
			// Localize Script / Pass php var in head for shortcode popup
			$WPML = new The_Grid_WPML();
			$WPML_meta_query   = $WPML->WPML_meta_query();
			$post_args = array(
				'post_type'      => 'the_grid',
				'post_status'    => 'any',
				'posts_per_page' => -1,
				'meta_query' => array(
					'relation' => 'AND',
					$WPML_meta_query
				)
			);
			$grids = get_posts($post_args);  
			$plugin_url = plugins_url( '/', __FILE__ );
			echo '<script type="text/javascript">';
				echo 'var tg_sc_title     = "'.__( 'The Grid - Shortcode Generator', 'tg-text-domain').'";';
				echo 'var tg_sc_tooltip   = "'.__( 'The Grid - Shortcode Generator', 'tg-text-domain').'";';
				echo 'var tg_list_label   = "'.__( 'Select a predefined Grid', 'tg-text-domain').'";';
				echo 'var tg_list_tooltip = "'.__( 'Select the grid you want', 'tg-text-domain').'";';
				echo 'var tg_but_label    = "'.__( 'Currently, you don\'t have any grid!', 'tg-text-domain').'";';
				echo 'var tg_but_tooltip  = "'.__( 'Select the grid you want', 'tg-text-domain').'";';
				echo 'var tg_but_text     = "'.__( 'Create a Grid', 'tg-text-domain').'";';
				echo 'var tg_but_url      = "'.admin_url( 'post-new.php?post_type=the_grid').'";';
				echo 'var tg_names  = [';
				if(!empty($grids)){
					foreach($grids as $grid){
						$favorited  = get_post_meta($grid->ID, 'the_grid_favorite', true);
						$icon = 'dashicons tg-dashicons-star-empty '.$favorited;
						$WPML_flag_data = $WPML->WPML_flag_data($grid->ID);
						$WPML_flag_data = ($WPML_flag_data) ? '['.$WPML_flag_data['alt'].'] ' : '';
						echo '{text:"'.$WPML_flag_data.$grid->post_title.'   ('.$grid->post_modified.')",value:"'.$grid->post_title.'",icon:"'.$icon.'"},';
					}
				}
				echo ']';
			echo '</script>';
			
			add_action( 'admin_enqueue_scripts', array( $this, 'localize_script' ) );
			add_filter( 'mce_external_plugins', array( $this ,'the_grid_mce_external_plugins' ) );
			add_filter( 'mce_buttons', array($this, 'the_grid_mce_buttons' ) );
		}
	}

	/**
	* mce_external_plugins / Tinymce plugin
	* @since 1.0.0
	*/
	function the_grid_mce_external_plugins( $plugin_array ) {
		$plugin_array[$this->shortcode_tag] = plugins_url( 'assets/js/admin-shortcode.js' , __FILE__ );
		return $plugin_array;
	}

	/**
	* mce_external_plugins / Tinymce button
	* @since 1.0.0
	*/
	function the_grid_mce_buttons( $buttons ) {
		array_push( $buttons, $this->shortcode_tag);
		return $buttons;
	}

	/**
	* Admin_enqueue_scripts
	* @since 1.0.0
	*/
	function admin_enqueue_scripts(){
		global $post;
		wp_enqueue_style('tg_shortcode', TG_PLUGIN_URL .'backend/assets/css/admin-shortcode.css' ); 
		if ( class_exists( 'woocommerce' ) && isset($post) && $post->post_type != 'product') {         
			wp_dequeue_style( 'select2' );
			wp_deregister_style( 'select2' );
			wp_dequeue_script( 'select2');
			wp_deregister_script('select2');
		}  	
		wp_register_script('select2', TOMB_URL.'assets/js/select2.min.js', 'jquery', '1.0', TRUE);
		wp_enqueue_script('select2');
		wp_register_style('select2', TOMB_URL. 'assets/css/select2.min.css');
		wp_enqueue_style('select2');
	}
	
}

new the_grid_shortcode();

// If Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
	
	// create new dropdown list with Select2 (for star/flag icons)
	vc_add_shortcode_param( 'grid_list', 'grid_list_settings_field');
	function grid_list_settings_field($settings, $value) {
		
		$WPML = new The_Grid_WPML();
		$WPML_meta_query   = $WPML->WPML_meta_query();
		
		$post_args = array(
			'post_type'      => 'the_grid',
			'post_status'    => 'any',
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'AND',
				$WPML_meta_query
			)
		);
		
		$grids = get_posts($post_args); 
		if(!empty($grids)){
			$output = '<label class="tg-grid-list-label">'.__("Select a predefined Grid", 'tg-text-domain').'</label>';
			$current_value = $value;
			$output .= '<select style="width: 350px;" name="'. $settings['param_name']. '" class="the-grid-list-vc wpb_vc_param_value wpb-input wpb-select '. $settings['param_name']. ' ' . $settings['type']. '">' . "\n";
			foreach($grids as $grid){
				$value = $grid->post_title;
				if ($current_value !== '' && (string) $value === (string) $current_value ) {
					$selected = ' selected="selected"';
				} else {
					$selected = null;
				}
				$WPML_flag_data = $WPML->WPML_flag_data($grid->ID);
				$WPML_flag_data = (!empty($WPML_flag_data)) ? 'data-flag="'.$WPML_flag_data['url'].'"' : '';
				$favorited  = get_post_meta($grid->ID, 'the_grid_favorite', true);
				$icon       = '<i class="dashicons tg-dashicons-star-empty '.$favorited.'"></i>';
				$title      = $grid->post_title;
				$date       = $grid->post_modified;
				$output    .= '<option value="' . $title . '" '.$WPML_flag_data.' data-icon="tg-dashicons-star-empty '.$favorited.'" class="tg-vc-sc " ' . $selected . '>'. $title .' ('.$date.')</option>' . "\n";
		}
		$output .= '</select>' . "\n";
		echo $output;
		echo '<script>
			jQuery(document).ready(function($){
				var $select_grid_list = jQuery(document).find(".the-grid-list-vc");
				$select_grid_list.select2({
					theme: "classic",
					containerCssClass : "grid-list-select2",
					dropdownCssClass: "grid-list-select2",
					templateResult: favoriteIcon
				});
				$select_grid_list.addClass("hide-select");
				$(".the-grid-list-vc").next(".select2").css({position: "relative",display: "block",margin: "0 auto 50px auto"});
					
				function favoriteIcon (icon) {
					var flag = $(icon.element).data("flag");
					var flag = (flag) ? \'<img class="tg-grid-list-flag" src="\'+flag+\'"/>\' : "";
					var icon = $(\'<span class="tg-grid-list-icon"><i class="\'+$(icon.element).data("icon")+\'"></i>\'+flag+\'  \'+icon.text+\'</span>\') ;
					return icon;
				};
			});
			</script>';
		} else {
			echo '<label class="tg-grid-list-label">'.__( 'Currently, you don\'t have any grid!', 'tg-text-domain').'</label>';
			echo '<div id="tg-vc-button-holder"><a id="tg-vc-button" href="'.admin_url( 'post-new.php?post_type=the_grid').'"><i class="dashicons dashicons-plus"></i>'.__( 'Create a Grid', 'tg-text-domain').'</a></div>';
		}
	}
	
	// add Visual Composer element to VC Popup List Elements
	add_action('vc_before_init', 'the_grid_VC');
	function the_grid_VC() {
		vc_map( array(
			'name' => __('The Grid', 'tg-text-domain'),
			'description' => __( 'Add a predefined Grid', 'tg-text-domain'),
			'base' => 'the_grid',
			'icon' => 'the-grid-vc-icon',
			'category' => __('The Grid', 'tg-text-domain'),
			'show_settings_on_create' => true,
			'js_view' => 'VcTheGrid',
			'admin_enqueue_js' => TG_PLUGIN_URL.'/backend/assets/js/admin-vc.js',
			'front_enqueue_js' => TG_PLUGIN_URL.'/backend/assets/js/admin-vc.js',
			'params' => array(
				array(
					'type' => 'grid_list',
					'holder' => '',
					'heading' => 'name',
					'param_name' => 'name',
					'admin_label' => true,
					'value' => '',
					'save_always' => true,
				)
			)
		));	
	}
	
}
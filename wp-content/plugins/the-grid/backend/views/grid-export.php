<?php
/**
 * @package   The_Grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 */

// Exit if accessed directly
if (!defined( 'ABSPATH')) { 
	exit;
}

$type       = 'the_grid';
$grids_name = null;
$grids_list = null;

$args = array( 
	'post_type'      => $type,
	'post_status'    => 'any',
	'posts_per_page' => -1,
	'suppress_filters' => true 
);
$loop = new WP_Query($args);

while($loop->have_posts()): $loop->the_post();
	$name = get_the_title();
	$ID = get_the_ID();
	$grids_name .= '<option value="'. $ID .'">'. $name .'</option>';
endwhile;

wp_reset_query();

if (!empty($grids_name)) {
	$grids_list  = '<select id="tg-list-export" multiple="multiple" data-placeholder="'. __( 'Select a grid(s)', 'tg-text-domain' ) .'">';
	$grids_list .= $grids_name;
	$grids_list .= '</select>';
}

$form_export  = '<div class="metabox-holder tg-export">';
	$form_export .= '<div class="postbox">';
		$form_export .= '<div class="tg-box-side">';
			$form_export .= '<h3>'. __( 'Exporter', 'tg-text-domain' ) .'</h3>';
			$form_export .= '<i class="tg-info-box-icon dashicons dashicons-upload"></i>';
		$form_export .= '</div>';
		$form_export .= '<div class="inside tg-box-inside">';
			$form_export .= '<h3>'. __( 'Export Grid(s)', 'tg-text-domain' ) .'</h3>';
			if (!empty($grids_name)) {
				$form_export .= '<p>'. __( 'Select the desired grid(s) to export.', 'tg-text-domain'  ) .'<br>'. __( 'The generated file will be a .json file compatibe with the grid importer.', 'tg-text-domain'  ) .'</p>';
			
				$form_export .= $grids_list;
				$form_export .='<span class="tg-button tg-list-export-all">'. __( 'Select All', 'tg-text-domain' ) .'</span>';
				$form_export .='<span class="tg-button tg-list-export-clear">'. __( 'Clear', 'tg-text-domain' ) .'</span>';
				$form_export .='<br><br><div class="tg-button" id="tg_post_export"><i class="tg-info-box-icon dashicons dashicons-upload"></i>'. __( 'Export Grid(s)', 'tg-text-domain' ) .'</div>';
				$form_export .='<a download="data.json" id="download-json-grid"></a>';
			} else {
				$form_export .= '<p>'. __( 'Currently, you don\'t have any grid.', 'tg-text-domain'  );
				$form_export .= '<br>'. __( 'You need to add a grid in order to export it.', 'tg-text-domain'  );
				$form_export .= '<br>'. __( 'You can create a new grid', 'tg-text-domain'  );
				$form_export .= ' <a href="'.admin_url( 'post-new.php?post_type=the_grid').'">'. __( 'here.', 'tg-text-domain'  ) .'</a></p>';
			}
		$form_export .= '</div>';
	$form_export .= '</div>';
$form_export .= '</div>';

echo $form_export;



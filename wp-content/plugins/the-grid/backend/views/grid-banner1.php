<?php
/**
 * @package   The_Grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 */

// Exit if accessed directly
if (!defined( 'ABSPATH')) { 
	exit;
}

$banner  = '<div id="tg-banner-holder">';
	$banner .= '<h2><span>The Grid</span>'.__( 'Overview', 'tg-text-domain').'</h2>';
$banner .= '</div>';

echo $banner;
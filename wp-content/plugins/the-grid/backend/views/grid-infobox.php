<?php
/**
 * @package   The_Grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 */

// Exit if accessed directly
if (!defined( 'ABSPATH')) { 
	exit;
}

$grid_place      = __( 'The grid name field is accessible in General tab.', 'tg-text-domain' );
$grid_name       = '<strong>'. __( 'This grid name already exist!', 'tg-text-domain' ).'</strong><br>';
$grid_name      .= __( 'Please, change your grid name in order to save your current settings.', 'tg-text-domain' ).'<br>'.$grid_place;
$grid_empty      = '<strong>'. __( 'The grid name field is empty!', 'tg-text-domain' ).'</strong><br>'.__( 'Please, you must fill the grid name field.', 'tg-text-domain' ).'<br>'.$grid_place;
$close_button    = '<br><div class="tg-button tg-close-infox-box"><i class="dashicons dashicons-no-alt"></i>'.__( 'Close', 'tg-text-domain' ).'</div>';
$grid_metaquery  = '<strong>'.__( 'Generated Meta Query', 'tg-text-domain' ).'</strong><br>';
$grid_metaquery .= '<span class="tg-meta-desc">'.__( 'This meta query will be used in order to filter the grid.', 'tg-text-domain' ).'<br>';
$grid_metaquery .= __( 'It will affect the number of item displayed in the grid.', 'tg-text-domain' ).'</span>';
$grid_meta_desc1 = '<span class="tg-meta-desc1">'. __( '// WP_Meta_Query arguments', 'tg-text-domain' ).'</span>';
$grid_meta_desc2 = '<span class="tg-meta-desc2">'. __( '// The Meta Query', 'tg-text-domain' ).'</span>';
$grid_meta_desc3 = '<span class="tg-meta-desc3">'. __( 'No Meta_Query available.', 'tg-text-domain' ).'<br>'. __( 'You need at least to set one Meta Key name and value.', 'tg-text-domain' ).'</span>';

$info_box = '<div id="tg-info-box">';
	$info_box .= '<div class="tg-info-overlay"></div>';
	$info_box .= '<div class="tg-info-holder">';
		$info_box .= '<div class="tg-info-inner">';
			$info_box .= '<div class="tg-info-box-msg" id="tg-info-box-grid-name">'.$grid_name.$close_button.'</div>';
			$info_box .= '<div class="tg-info-box-msg" id="tg-info-box-grid-empty">'.$grid_empty.$close_button.'</div>';
			$info_box .= '<div class="tg-info-box-msg" id="tg-info-box-metakey-array">'.$grid_metaquery.$grid_meta_desc3.$close_button.$grid_meta_desc1.$grid_meta_desc2.'</div>';
			$info_box .= '<div class="tg-info-box-msg" id="tg-info-box-deleting"><i class="tg-info-box-icon dashicons dashicons-admin-generic"></i>'.__( 'Deleting Grid', 'tg-text-domain').'</div>';
			$info_box .= '<div class="tg-info-box-msg" id="tg-info-box-saving"><i class="tg-info-box-icon dashicons dashicons-admin-generic"></i>' .__( 'Saving Settings', 'tg-text-domain').'</div>';
			$info_box .= '<div class="tg-info-box-msg" id="tg-info-box-deleted"><i class="tg-info-box-icon dashicons dashicons-yes"></i>'.__( 'Grid Deleted', 'tg-text-domain').'</div>';
			$info_box .= '<div class="tg-info-box-msg" id="tg-info-box-saved"><i class="tg-info-box-icon dashicons dashicons-yes"></i>'.__( 'Settings Saved', 'tg-text-domain').'</div>';
			$info_box .= '<div class="tg-info-box-msg" id="tg-info-box-error"><i class="tg-info-box-icon dashicons dashicons-no-alt"></i>'.__( 'Sorry, an error occurred during, please try again', 'tg-text-domain').'</div>';
		$info_box .= '</div>';
	$info_box .= '</div>';
$info_box .= '</div>';

$preview  = '<div id="tg-grid-preview">';
	$preview .= '<div id="tg-grid-preview-overlay"></div>';
	
	$preview .= '<div id="tg-grid-preview-header">';
		$preview .= '<div id="tg-grid-preview-viewport">';
			$preview .= '<div class="tg-grid-preview-mobile"><div class="tg-grid-preview-img"></div><div class="tg-grid-preview-tooltip">'.__( 'Mobile', 'tg-text-domain').'</div></div>';
			$preview .= '<div class="tg-grid-preview-tablet-small"><div class="tg-grid-preview-img"></div><div class="tg-grid-preview-tooltip">'.__( 'Tablet Small', 'tg-text-domain').'</div></div>';
			$preview .= '<div class="tg-grid-preview-tablet"><div class="tg-grid-preview-img"></div><div class="tg-grid-preview-tooltip">'.__( 'Tablet', 'tg-text-domain').'</div></div>';
			$preview .= '<div class="tg-grid-preview-desktop-small"><div class="tg-grid-preview-img"></div><div class="tg-grid-preview-tooltip">'.__( 'Desktop Small', 'tg-text-domain').'</div></div>';
			$preview .= '<div class="tg-grid-preview-desktop-medium"><div class="tg-grid-preview-img"></div><div class="tg-grid-preview-tooltip">'.__( 'Desktop Medium', 'tg-text-domain').'</div></div>';
			$preview .= '<div class="tg-grid-preview-desktop-large tg-viewport-active"><div class="tg-grid-preview-img"></div><div class="tg-grid-preview-tooltip">'.__( 'Desktop Large', 'tg-text-domain').'</div></div>';	
		$preview .= '</div>';
		$preview .= '<div id="tg-grid-preview-refresh"><i class="dashicons dashicons-update"></i></div>';
		$preview .= '<div id="tg-grid-preview-close"><i class="dashicons dashicons-no-alt"></i></div>';
	$preview .= '</div>';
	
	$preview .= '<div id="tg-grid-preview-wrapper">';
		$preview .= '<div id="tg-grid-preview-loading">'.__('Fetching grid data, please wait...', 'tg-text-domain').'</div>';
		$preview .= '<div id="tg-grid-preview-inner" ></div>';
		$preview .= '<div id="tg-grid-preview-settings">';
			$preview .= '<div id="tg-grid-preview-settings-footer">';
				$preview .= '<span class="tg-grid-preview-settings-wait">'.__('Please Wait', 'tg-text-domain').'</span><div class="spinner"></div>';
				$preview .= '<div class="tg-button" id="tg-grid-preview-settings-save">';
					$preview .= '<i class="dashicons dashicons-yes"></i>';
					$preview .= __( 'Save Changes', 'tg-text-domain' );
				$preview .= '</div>';
			$preview .= '</div>';
		$preview .= '</div>';
	$preview .= '</div>';
	
$preview .= '</div>';

echo $info_box;
echo $preview;
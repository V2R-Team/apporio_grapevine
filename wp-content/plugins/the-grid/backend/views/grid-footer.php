<?php
/**
 * @package   The_Grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 */

// Exit if accessed directly
if (!defined( 'ABSPATH')) { 
	exit;
}

// string for translation
$saved       = __( 'Settings Saved', 'tg-text-domain' );
$saving      = __( 'Saving Settings', 'tg-text-domain' );
$resetted    = __( 'Settings resetted', 'tg-text-domain' );
$resetting   = __( 'Resetting Settings', 'tg-text-domain' );
$exported    = __( 'Grid(s) exported', 'tg-text-domain' );
$exporting   = __( 'Exporting grid(s)', 'tg-text-domain' );
$imported    = __( 'Grid(s) imported', 'tg-text-domain' );
$importing   = __( 'Importing grid(s)', 'tg-text-domain' );
$d_imported  = __( 'Demo imported', 'tg-text-domain' );
$d_importing = __( 'Importing Demo', 'tg-text-domain' );
$set_error   = __( 'Sorry, an error occurred during saving settings', 'tg-text-domain' );
$res_error   = __( 'Sorry, an error occurred during resetting', 'tg-text-domain' );
$error_imp   = __( 'Sorry, an error occurred during importing', 'tg-text-domain' );
$error_exp   = __( 'Sorry, an error occurred during exporting', 'tg-text-domain' );

// infox box for ajax request
$box = '<div id="tg-info-box-general">';
	$box .= '<div class="tg-info-overlay"></div>';
	$box .= '<div class="tg-info-holder">';
		$box .= '<div class="tg-info-inner">';
			$box .= '<div class="tg-info-box-msg" id="tg-info-box-settings-saving"><i class="tg-info-box-icon dashicons dashicons-admin-generic"></i>'.$saving.'</div>';
			$box .= '<div class="tg-info-box-msg" id="tg-info-box-settings-saved"><i class="tg-info-box-icon dashicons dashicons-yes"></i>'.$saved.'</div>';
			$box .= '<div class="tg-info-box-msg" id="tg-info-box-settings-error"><i class="tg-info-box-icon dashicons dashicons-no-alt"></i>'.$set_error.'</div>';
			$box .= '<div class="tg-info-box-msg" id="tg-info-box-resetting-saved"><i class="tg-info-box-icon dashicons dashicons-yes"></i>'.$resetted.'</div>';
			$box .= '<div class="tg-info-box-msg" id="tg-info-box-resetting-saving"><i class="tg-info-box-icon dashicons dashicons-admin-generic"></i>'.$resetting.'</div>';
			$box .= '<div class="tg-info-box-msg" id="tg-info-box-resetting-error"><i class="tg-info-box-icon dashicons dashicons-no-alt"></i>'.$res_error.'</div>';		
			$box .= '<div class="tg-info-box-msg" id="tg-info-box-exported"><i class="tg-info-box-icon dashicons dashicons-yes"></i>'.$exported.'</div>';
			$box .= '<div class="tg-info-box-msg" id="tg-info-box-exporting"><i class="tg-info-box-icon dashicons dashicons-admin-generic"></i>'.$exporting.'</div>';
			$box .= '<div class="tg-info-box-msg" id="tg-info-box-imported"><i class="tg-info-box-icon dashicons dashicons-yes"></i>'.$imported.'</div>';
			$box .= '<div class="tg-info-box-msg" id="tg-info-box-importing"><i class="tg-info-box-icon dashicons dashicons-admin-generic"></i>'.$importing.'</div>';
			$box .= '<div class="tg-info-box-msg" id="tg-info-box-imported-demo"><i class="tg-info-box-icon dashicons dashicons-yes"></i>'.$d_imported.'</div>';
			$box .= '<div class="tg-info-box-msg" id="tg-info-box-importing-demo"><i class="tg-info-box-icon dashicons dashicons-admin-generic"></i>'.$d_importing.'</div>';
			$box .= '<div class="tg-info-box-msg" id="tg-info-box-import-error"><i class="tg-info-box-icon dashicons dashicons-no-alt"></i>'.$error_imp.'</div>';
			$box .= '<div class="tg-info-box-msg" id="tg-info-box-export-error"><i class="tg-info-box-icon dashicons dashicons-no-alt"></i>'.$error_exp.'</div>';
		$box .= '</div>';
	$box .= '</div>';
$box .= '</div>';

echo $box;

echo '</div>';
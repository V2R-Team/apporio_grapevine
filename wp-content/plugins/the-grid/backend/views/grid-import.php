<?php
/**
 * @package   The_Grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 */

// Exit if accessed directly
if (!defined( 'ABSPATH')) { 
	exit;
}

$form_import  = '<div class="metabox-holder tg-import">';
	$form_import .= '<div class="postbox">';
		$form_import .= '<div class="tg-box-side">';
			$form_import .= '<h3>'. __( 'Importer', 'tg-text-domain' ) .'</h3>';
			$form_import .= '<i class="tg-info-box-icon dashicons dashicons-download"></i>';
		$form_import .= '</div>';
		$form_import .= '<div class="inside tg-box-inside">';
			$form_import .= '<h3>'. __( 'Import Grid(s)', 'tg-text-domain' ) .'</h3>';
			$form_import .= '<p>'. __( 'Please select a .json file created with the current exporter feature.', 'tg-text-domain'  ) .'<br>'. __( 'Import a grid(s) will create new grid in overview list overview.', 'tg-text-domain'  ) .'</p>';
			$form_import .= '<input type="file" id="tg-import-file" name="import_file"/>';
			$form_import .= '<br><br><div class="tg-button" id="tg_post_import"><i class="tg-info-box-icon dashicons dashicons-download"></i>'. __( 'Import Grid(s)', 'tg-text-domain' ) .'</div>';
			$form_import .= ' <span><strong>'. __( 'or', 'tg-text-domain' ) .'</strong></span>';
			$form_import .= '<a class="tg-button" id="tg-import-demo" data-demo-content="'. TG_PLUGIN_PATH . '/backend/data/demo-content.json' .'"><i class="dashicons dashicons-download"></i>'.__('Import Demo', 'tg-text-domain').'</a>';
		$form_import .= '</div>';
	$form_import .= '</div>';
$form_import .= '</div>';


echo $form_import;



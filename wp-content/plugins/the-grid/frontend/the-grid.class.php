<?php
/**
 * @package   The_Grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 */

// Exit if accessed directly
if (!defined( 'ABSPATH')) { 
	exit;
}

/***********************************************
* THE GRID Main Class
***********************************************/

class The_Grid {

	protected $plugin_slug = TG_SLUG;
	protected $grid_prefix = TG_PREFIX;
	protected $grid_data;
	
	/**
	* _construct
	* @since 1.0.0
	*/
	public function __construct() {}
	
	/**
	* Output The Grid Markup (Main core function)
	* @since 1.0.0
	*/
	public function the_grid_output($grid_name) {
		
		$cache   = get_option('the_grid_caching', false);
		$page    = (get_query_var('paged')) ? max(1, get_query_var('paged')) : max(1, get_query_var('page'));
		$content = $this->the_grid_get_cache($grid_name, $page, $cache);
		
		if (!empty($content)) {
			if (strpos($content,'tg-grid-wrapper') !== false) {
				echo $content;
			} else {
				$content = null;
			}
		}
		
		if (empty($content)) {
			global $tg_item_count;
			$tg_item_count = 1;
			$this->grid_data = $this->the_grid_get_data($grid_name);
			if ($this->grid_data != false) {
				$content = $this->the_grid_show();
				echo $content;
				$grid_ID = str_replace('grid-', '', $this->getVar($this->grid_data,'ID'));
				$this->the_grid_save_cache($grid_ID, $page, $cache, $content);
			}	
		}
		
	}
	
	/**
	* Retrieve grid cache content if exist
	* @since 1.0.0
	*/
	public function the_grid_get_cache($grid_name, $page, $cache) {
		
		global $tg_is_ajax;
		
		$time_start = (!$tg_is_ajax) ? microtime(true) : null;
		$cache_msg  = null;
		$content    = null;
		
		if ($cache == true && !wp_is_mobile()) {
			
			$base = new The_Grid_Base();
			
			$grid_info = get_page_by_title(html_entity_decode($grid_name), 'OBJECT', 'the_grid');
			$grid_ID   = $grid_info->ID;
			$orderby   = get_post_meta($grid_ID, 'the_grid_orderby', true);
			$orderby   = (!empty($orderby)) ? $orderby : array();
			$grid_transient = 'tg_grid_transient_'.$grid_ID.'_page_'.$page;
			
			// disable grid cache while using W3 Total cache plugin
			$base->disable_W3_Total_Cache($grid_transient);
			
			$data_timeout = get_option('_transient_timeout_' . $grid_transient);
			
			if ($data_timeout >= time() && !in_array('rand',$orderby)) {
				$data = get_transient($grid_transient);
				if (isset($data['content']) && !empty($data['content'])) {
					$content = base64_decode($data['content']);
					if (!$tg_is_ajax) {
						$time_end   = microtime(true);
						$cache_date = (isset($data['cache_date'])) ? $data['cache_date'] : null;
						$cache_msg  = '<!-- The Grid Cache Enabled - Execution Time: '.round($time_end - $time_start,5).'s - Date: '.$cache_date.' -->';
					}
					$content = $cache_msg.$this->preserve_post_like($content);
				} else {
					$content = null;
				}
			}
			
			// enable grid cache while using W3 Total cache plugin
			$base->enable_W3_Total_Cache($grid_transient);

		}
		
		return $content;
	
	}
	
	/**
	* Save grid content in cache
	* @since 1.0.0
	*/
	public function the_grid_save_cache($grid_ID, $page, $cache, $content) {
		
		$orderby   = get_post_meta($grid_ID, 'the_grid_orderby', true);
		$orderby   = (!empty($orderby)) ? $orderby : array();
		
		if ($cache == true && !in_array('rand',$orderby) && !wp_is_mobile()) {
			$grid_transient = 'tg_grid_transient_'.$grid_ID.'_page_'.$page; // set a transitien per page even if ajax (simulate page)
			$data = array(
				'cache_date' => date('m/d/y, h:i:s A'),
				'content' => base64_encode($content)
			);
			set_transient($grid_transient, $data, apply_filters('tg_transient_expiration', 60*60*24*7));
		}
		
	}
	
	/**
	* Preserve post like (number & state) if cache enable
	* @since 1.0.0
	*/
	public function preserve_post_like($content){
		$dom = new DOMDocument();
		libxml_use_internal_errors(true);
		$dom->encoding = 'utf-8';
		$dom->loadHTML(utf8_decode($content));
		$finder = new DOMXPath($dom);
		$nodes  = $finder->query('//span[@data-post-id] | //a[@data-comment-id]');
		foreach ($nodes as $node) {
			$ID  = $node->getAttribute('data-post-id');
			if ($ID) {
				$like = TO_get_post_like($ID);
				$dom1 = new DOMDocument();
				$dom1->loadHTML($like);
				$finder1 = new DOMXPath($dom1);
				$classname1 = 'to-post-like';
				$classname2 = 'to-like-count';
				$nodes1  = $finder1->query("//*[contains(@class, '$classname1')]");
				$nodes2  = $finder1->query("//*[contains(@class, '$classname2')]");
				$classes = $nodes1->item(0)->getAttribute('class');
				$like_nb = $nodes2->item(0)->textContent;
				$node->setAttribute('class', $classes);
				$node->childNodes->item(1)->nodeValue = $like_nb;
			}
			$ID = $node->getAttribute('data-comment-id');
			if ($ID) {
				$text = $node->getAttribute('data-comment');
				$text = json_decode($text);
				$coms = get_comments_number($ID);
				$coms = ($coms < 1  && isset($text->no)) ? $text->no : $coms;
				$coms = ($coms == 1 && isset($text->one)) ? $coms.' '.$text->one : $coms;
				$coms = ($coms > 1  && isset($text->plus)) ? $coms.' '.$text->plus : $coms;
				if ($node->childNodes->item(1)) {
					$node->childNodes->item(1)->nodeValue = $coms;
				} else if ($node->childNodes->item(0)) {
					$node->childNodes->item(0)->nodeValue = $coms;
				}
			}
		}
		$content = preg_replace('/^<!DOCTYPE.+?>/', '', str_replace( array('<html>', '</html>', '<body>', '</body>', '</source>'), array('', '', '', '', ''), $dom->saveHTML()));
		return $content;
	}
	
	/**
	* getVar for MetaData
	* @since 1.0.0
	*/
	public function getVar($arr,$key,$default = ''){
		$prefix = $this->grid_prefix;
		$val = (isset($arr[$prefix.$key]) && !empty($arr[$prefix.$key])) ? $arr[$prefix.$key] : $default;
		return($val);
	}
	
	/**
	* Retrieve current grid data
	* @since 1.0.0
	*/
	public function the_grid_get_data($grid_name) {
		
		$grid_info = get_page_by_title(html_entity_decode($grid_name), 'OBJECT', 'the_grid');
		
		if (!isset($grid_info->ID)) {
			return false;
		}
		
		$grid_id = $grid_info->ID;
		$grid_data[$grid_name]['the_grid_ID'] = 'grid-'.$grid_id;
		
		$meta_keys = get_post_custom_keys($grid_id);
		if (empty($meta_keys)) {
			return;
		}

		foreach ($meta_keys as $meta_key) {
			$meta_values = get_post_custom_values($meta_key, $grid_id);
			if (strrpos($meta_key, $this->plugin_slug) !== false) {
				foreach ($meta_values as $meta_value) {
					$meta_value = maybe_unserialize($meta_value);
					$grid_data[$grid_name][$meta_key] = $meta_value;
				}
			}
		}
		
		global $tg_grid_data;
		$tg_grid_data = $grid_data[$grid_name];

		return $grid_data[$grid_name];
	}
	
	/**
	* Output main wrapper of The Grid
	* @since 1.0.0
	*/
	public function the_grid_show() {

		//echo '<h5>the_grid_show() running</h5>';
		
		global $tg_query_args;

		// set the custom query
		$tg_query_args = $this->the_grid_get_query();
		// run the custom query
		$this->the_grid_run_query();
		// get the custom query
		global $tg_grid_query;
		
		if ($tg_grid_query->have_posts()) {
			
			// get grid ID
			$grid_ID = $this->getVar($this->grid_data,'ID');
	
			// get grid data attr for js plugin
			$grid_attr = $this->the_grid_layout_data();
			
			// get grid layout for slider wrapper (after getting attr for corrected value)
			$grid_layout = $this->getVar($this->grid_data,'layout','vertical');
			
			// for masonry/grid class
			$grid_style = $this->getVar($this->grid_data,'style','grid');
			
			// preloader
			$fullHeight = $this->getVar($this->grid_data,'full_height',false);
			$fullHeight = ($grid_style == 'grid' && $fullHeight) ? ' full-height' : null;
			$preloader  = $this->getVar($this->grid_data,'preloader',false);
			$preloader  = ($preloader) ? $this->the_grid_preloader($grid_ID) : null;
			$load_class = ($preloader) ? ' tg-grid-loading' : null;
			$css_class  = $this->getVar($this->grid_data,'css_class',false);
			$nav_class  = $this->getVar($this->grid_data,'navigation_style','tg-std-nav');
			
			// start grid wrapper
			$html  = '<!-- The Grid Plugin Version '.TG_VERSION.' -->';
			$html .= '<!-- The Grid Wrapper Start -->';
			$html .= '<div class="tg-grid-wrapper'.esc_attr($load_class).' '.esc_attr($nav_class).' '.esc_attr($css_class.$fullHeight).'" id="'.esc_attr($grid_ID).'">';
			$html .= $this->the_grid_style($grid_ID);
			$html .= '<!-- The Grid Item Sizer -->';
			$html .= '<div class="tg-grid-sizer"></div>';
			// Top Area 1
			$html .= $this->the_grid_area('top1');
			$html .= $this->the_grid_area('top2');

			// Open slider wrapper
			if ($grid_layout == 'horizontal') {
				$html .= '<!-- The Grid Slider Wrapper Start -->';
				$html .= '<div class="tg-grid-slider">';
			}
			// grid item holder + item loop
			$html .= '<!-- The Grid Items Holder -->';
			$html .= '<div class="tg-grid-holder tg-layout-'.esc_attr($grid_style).'" '.$grid_attr.'>';
			$html .= $this->the_grid_post_loop();
			$html .= ($grid_layout == 'vertical') ? $this->the_grid_get_ajax_scroll() : null;
			$html .= '</div>';
			
			// close slider wrapper	
			if ($grid_layout == 'horizontal') {	
				// Left/Right Areas
				$html .= $this->the_grid_area('left');
				$html .= $this->the_grid_area('right');
				$html .= '</div>';
				$html .= '<!-- The Grid Slider Wrapper End -->';
			}	
			
			// Bottom Area 1
			$html .= $this->the_grid_area('bottom1');
			$html .= $this->the_grid_area('bottom2');
			
			// grid custom script
			$html .= $this->the_grid_jquery();
			
			// grid preloader
			$html .= $preloader;
			
			// close grid wrapper
			$html .= '</div>';
			$html .= '<!-- The Grid Wrapper End -->';
			
			// reset the custom query
			$this->the_grid_reset_query();

			//return the grid;
			return $html;
			
		}
			
	}
	
	/**
	* Preload Markup
	* @since 1.0.0
	*/
	public function the_grid_preloader($grid_ID) {
		
		// retrieve the preloader skin
		$preloader_base   = new The_Grid_Preloader_Skin();
		$preloader_name   = esc_attr($this->getVar($this->grid_data,'preloader_style','square-grid-pulse'));
		$preloader_color  = esc_attr($this->getVar($this->grid_data,'preloader_color','#34495e'));
		$preloader_size   = esc_attr($this->getVar($this->grid_data,'preloader_size','1'));
		
		if (!empty($preloader_name)) {
		
			$preloader_border = ($preloader_name == 'pacman' || $preloader_name == 'ball-clip-rotate') ? '#'.$grid_ID.' .tg-grid-preloader-inner>div{border-color:'.$preloader_color.'}' : null;
			$preloader_color  = '#'.$grid_ID.' .tg-grid-preloader-inner>div{background:'.$preloader_color.'}';
			$preloader_color  = $preloader_color.$preloader_border;
			$preloader_size   = '#'.$grid_ID.' .tg-grid-preloader-scale{transform:scale('.$preloader_size.')}';
			$preloader_skin   = $preloader_base->$preloader_name();
			// build preloader
			$preloader  = '<div class="tg-grid-preloader">';
				$preloader .= '<style class="preloader-styles" type="text/css" scoped>'.$preloader_skin['css'].$preloader_color.$preloader_size.'</style>';
				$preloader .= '<div class="tg-grid-preloader-holder">';	
					$preloader .= '<div class="tg-grid-preloader-scale">';
						$preloader .= '<div class="tg-grid-preloader-inner '.esc_attr($preloader_name).'">';	
							$preloader .= $preloader_skin['html'];
						$preloader .= '</div>';
					$preloader .= '</div>';
				$preloader .= '</div>';
			$preloader .= '</div>';	
			return $preloader;
		
		}
	}
	
	/**
	* Custom Loop through items in The Grid
	* @since 1.0.0
	*/
	public function the_grid_post_loop() {

		global $tg_item_count,
			   $tg_grid_query;
		
		$data = array();
		$grid_style = $this->getVar($this->grid_data,'style','grid');
		$item_size  = $this->getVar($this->grid_data,'item_force_size');
		if ($item_size == true) {
			$data_row  = ' data-row="'.$this->getVar($this->grid_data,'items_row').'"';
			$data_col  = ' data-col="'.$this->getVar($this->grid_data,'items_col').'"';
			$item_size = ($grid_style == 'grid') ? $data_row.$data_col : $data_col;
		}
		$preloader  = $this->getVar($this->grid_data,'preloader',false);
		$settings   = $this->getVar($this->grid_data,'settings',false);
		$sort_by    = $this->getVar($this->grid_data,'sort_by',array());
		$item_skins = json_decode($this->getVar($this->grid_data,'skins',array()), TRUE);
		
		// set items data (only once per grid to prevent multi call)
		$data['sort_by']    = $sort_by;
		$data['settings']   = $settings;
		$data['item_size']  = $item_size;
		$data['item_skins'] = $item_skins;
		$data['preloader']  = $preloader;
		$data['grid_style'] = $grid_style;

		// start loop with custom query
		$output = null;
		while ($tg_grid_query->have_posts()) : $tg_grid_query->the_post();
			$child   = new The_Grid_Item();
			$output .= '<!-- The Grid item #'.$tg_item_count.' -->';
			$output .= $child->item_output(get_the_ID(), $data);			
			$tg_item_count++;		
		endwhile;
		
		return $output;
	
	}
	
	/**
	* Generate Grid CSS
	* @since 1.0.0
	*/
	public function the_grid_style($grid_ID) {

		$styles = null;
		
		// retrieve main styles from child style class
		$child_style = new The_Grid_Style();
		$styles = $child_style->process_styles('#'.$grid_ID);
		
		// custom styles
		$custom_css  = $this->getVar($this->grid_data,'custom_css');
		$styles .= (!empty($custom_css)) ? $custom_css : null;
		
		// compress css
		$base = new The_Grid_Base();
		$styles = $base->compress_css($styles);

		if ($styles) {
			return '<!-- The Grid Styles -->'."\n".'<style type="text/css" class="the_grid_styles" scoped>'.$styles.'</style>';
		}
	
	}
	
	/**
	* Generate Grid JS
	* @since 1.0.0
	*/
	public function the_grid_jquery() {
		
		$custom_js = $this->getVar($this->grid_data,'custom_js');
		return (!empty($custom_js)) ? '<script type="text/javascript">(function($) {"use strict";'.$custom_js.'})(jQuery)</script>' : null;
		
	}
	
	/**
	* Build Grid Area (from dragged areas in admin)
	* @since 1.0.0
	*/
	public function the_grid_area($position) {
		
		$data = $this->getVar($this->grid_data,'area_'.$position);
		$data = json_decode($data, TRUE);

		if (isset($data['functions']) && !empty($data['functions'])) {
			
			$area_before = null;
			$area_after  = null;
			
			if (strpos($position,'left') !== false || strpos($position,'right') !== false) {
				$area_before .= '<div class="tg-grid-area-inner">';
					$area_before .= '<div class="tg-grid-area-wrapper">';
					$area_after  .= '</div>';
				$area_after  .= '</div>';
			}
			
			$area  = '<!-- The Grid Area '.$position.' -->';
			$area .= '<div class="tg-grid-area-'.esc_attr($position).'">';
				$area .= $area_before;
				foreach($data['functions'] as $function) {
					$func  = $function;
					$area .= (strrpos($function, 'the_grid_get_filters_')=== false) ? $this->$func() : $this->the_grid_get_filters(substr($func, -1));			
				}
				$area .= $area_after;
			$area .= '</div>';
			
			return $area;
		}
		
	}
	
	/**
	* Build the Main Query array
	* @since 1.0.0
	*/
	public function the_grid_get_query( $userId = 0 ) {
		
		$base = new The_Grid_Base();
		
		// item number on load
		$posts_per_page = $this->getVar($this->grid_data,'item_number',0);
	
		// post type and associated categories
		$post_type = $this->getVar($this->grid_data,'post_type',array('post'));
		// Attachment images ID for gallery
		$gallery_img = explode(',',$this->getVar($this->grid_data,'gallery',0));
		// post type status
		$post_status = (array) $this->getVar($this->grid_data,'post_status', 'publish');
		// associated categories
		$post_cats = $this->getVar($this->grid_data,'categories');
		$post_cats_child = $this->getVar($this->grid_data,'categories_child');
		
		$taxonomies = array();
		
		// Build taxonomy query from selected cats/tags for post types
		$i = 0;
		if ($post_cats) {
			foreach($post_cats as $taxonomy) {
				$key   = explode(':', $taxonomy);
				$tax   = $key[0];
				$terms = $key[1];
				$taxonomies[$tax]['include_children'] = $post_cats_child;
				$taxonomies[$tax]['taxonomy'] = $tax;
				//$taxonomies[$tax]['field']    = 'term_id';
				$taxonomies[$tax]['terms'][]  = $terms;
				$taxonomies[$tax]['operator'] = 'IN';
				$i++;
			}
		}
			
		// Add tax query and relation or for everything
		$tax_query['relation'] = 'OR';
		foreach($taxonomies as $query) {
			$tax_query[] = $query;
		}
		
		// Get post order and orderby key
		$post_order   = $this->getVar($this->grid_data,'order');
		$post_orderby = $this->getVar($this->grid_data,'orderby', array());
		
		$post_orderby_val = null;
		if ($post_orderby ) {
			$gap = '';
			foreach($post_orderby as $orderby) {
				$post_orderby_val .= $orderby.$gap;
				$gap = ' ';
			}
		}
		// Grab custom post ID to preserve post orderby
		$post_orderby_id = $this->getVar($this->grid_data,'orderby_id');
		$post_orderby_id = !empty($post_orderby_id) ? explode(',', $post_orderby_id) : array();
	
		//get all page ids
		$all_page = array();
		if (in_array('page', $post_type)) {
			$all_pages = $base->get_all_page_id();
			$all_page  = array();
			foreach ($all_pages as $ID => $pages) {
				$all_page[] = $ID;
			}
		}
		$pages_id = $this->getVar($this->grid_data,'pages_id');
		$pages_id = !empty($pages_id) ? $pages_id : $all_page;
		
		// excluded items
		$excluded_items = $this->getVar($this->grid_data,'post_not_in');
		$excluded_items = (!empty($excluded_items)) ? explode(', ', $excluded_items) : array();

		// revert process : declare page not__in, to get page and post at same time
		$post_not_in = array_diff($all_page, $pages_id);
		$post_not_in = array_merge ($post_not_in, $excluded_items);
		
		// preserve post ID order : merge existing page IDs with selected post IDs
		if (!empty($post_orderby_id) && in_array('post__in', $post_orderby)) {
			$post_in = array_merge($pages_id, $post_orderby_id);
		} else {
			$post_in = array();
		}
		
		// If Attachment force post order and image order from drag & drop image gallery field
		if (in_array('attachment',$post_type)) {
			
			// add post statut inherit to post_statut options to retrieve attachments
			array_push($post_status, 'inherit');
			
			if (sizeof($post_type) > 1) {
				$ids = get_posts(array(
					'post_type'      => 'attachment', 
					'post_mime_type' => 'image', 
					'post_status'    => 'inherit', 
					'posts_per_page' => -1,
					'fields'         => 'ids',
				));
				$img_ids = array();
				foreach ($ids as $id) {
					$img_ids[] = $id;
				}
				$img_ids = array_diff($img_ids,$gallery_img);
				$post_not_in = array_merge($post_not_in,$img_ids);
			} else {
				$post_orderby_val = 'post__in';
				$post_in = $gallery_img;
			}
			
		}
		
		// remove filter category and force post_in ids of all cat to keep selected page
		if ($post_cats && in_array('page', $post_type)) {
			$post_ids_cat = $base->get_post_ids_by_cat($post_type,$tax_query,$post_cats_child,$terms,$tax);
			$post_in      = array_merge($pages_id, $post_orderby_id, $post_ids_cat);
			$tax_query    = null;
		}
		
		//retrieve meta_key to order by meta_value
		$meta_key = null;
		if ($base->strpos_array($post_orderby_val,array('meta_value','meta_value_num')) !== false) {
			$meta_key = $this->getVar($this->grid_data,'meta_key');
		}
	
		// get authors filter
		$author  = null;
		$authors = (array) $this->getVar($this->grid_data,'author');
		$author  = (count($authors) > 1) ? implode(',', array_map(function($item) { return $item; }, $authors)) : $authors[0];
		
		// get meta query
		$meta_query = $this->the_grid_meta_query();
		
		// retrieve current page
		if (isset($grid_page)) {
			$paged = $grid_page;	
		} else {
			$paged = (get_query_var('paged')) ? max(1, get_query_var('paged')) : max(1, get_query_var('page'));
		}
		
		// get query args
		global $tg_query_args;
		// setup the query args
		// WordPress already takes care of the necessary sanitization in querying the database
		$tg_query_args = array(  
			'post_type'      => $post_type, 
			'posts_per_page' => $posts_per_page, 
			'post_status'    => $post_status,
			'author'         => $author,
			'paged'          => $paged,
			'post__in'       => $post_in,
			'post__not_in'   => $post_not_in,
			'order'          => $post_order,
			'orderby'        => $post_orderby_val,
			'tax_query'      => $tax_query,
			'meta_key'       => $meta_key,
			'meta_query'     => $meta_query
		);
/*
		echo '$tg_query_args<pre>';
	        print_r($tg_query_args);
	        echo '</pre>';
*/
		if( $userId === 0 ){

			if( isset($_SESSION['user_data']['id']) && !is_page_template( 'public-news-feed.php' ) ){

				$userId = $_SESSION['user_data']['id'];

			}

		}

		if( $userId != 0 && !is_page_template( 'public-news-feed.php' ) ){

			unset($tg_query_args['post__in']);
			unset($tg_query_args['post__not_in']);
			unset($tg_query_args['tax_query']);
/*
			$tg_query_args = array(
	            'posts_per_page' => $this->getVar($this->grid_data,'item_number',0),
	            //'offset'         => $offset,
	            'cat'            => '3',
	            'post_status'    => $post_status,
	            //'tag'            => $user_id == 'public' ? '' : $interests['interests'],
	            'order'          => $post_order,
				'orderby'        => $post_orderby_val,
	            'paged'          => $paged,
	            'post_type'      => $post_type 
	        );
*/
	        

			global $wpdb;
			//$wpdb->show_errors();
			$interest_data = $wpdb->get_row("SELECT interests FROM users WHERE id = " . $userId);
/*
			echo '$interest_data<pre>';
	        print_r($interest_data);
	        echo '</pre>';
*/
			$interests = json_decode($interest_data->interests);


	            $tags = $interests->interests;
/*
	            echo '$tags<pre>';
	        print_r($tags);
	        echo '</pre>';
*/
	            if (count($tags) > 0) {
	                foreach ($tags as $key => $value) {


	                    if ($value == "" || $value == null) {
	                        unset($tags[$key]);
	                    }
	                }
	            }
	            else{
	                $tags = array();
	            }

	        $tg_query_args['tag'] = implode(",",$tags);
	        $tg_query_args['cat'] = '3';

		}
/*
		echo 'the_grid_get_query() -> $tg_query_args<pre>';
	        print_r($tg_query_args);
	        echo '</pre>';
*/
/*
	    $the_query = new WP_Query( $tg_query_args );

	      echo '$the_query<pre>';
	        print_r(count($the_query->posts));
	        echo '</pre>';
		
*/

/*
		$tg_query_args = array(
            'posts_per_page' => $post_per_page,
            //'offset'         => 0,
            'cat'            => '3',
            'post_status'    => 'publish',
            //'tag'            => $user_id == 'public' ? '' : $interests['interests'],
            'orderby'        => 'date',
            'order'          => 'DESC',
            'post_type'      => 'post'
        );
*/
		return $tg_query_args;
	}
	
	/**
	* Run the main Query for all instance (to get item in loop & pagination)
	* @since 1.0.0
	*/
	public function the_grid_run_query() {
		
		// retieve custom query arg and set new query
		global $tg_query_args, $tg_grid_query, $tg_is_ajax;
/*
		echo 'the_grid_run_query() -> $tg_query_args:<pre>';
		print_r($tg_query_args);
		echo '</pre>';
*/		
		$tg_grid_query = new WP_Query($tg_query_args);
       
        //$tg_grid_query = new WP_Query($tg_query_args);

		if ($tg_grid_query->post_count == 0 && !$tg_is_ajax) {
			echo '<div class="tg-no-post">';
			_e( 'No post was found with your current grid settings.', 'tg-text-domain' );
			echo '<br><br>';
			_e( 'You should verify if you have posts inside the current selected post type(s) and if the meta key filter is not too much restrictive.', 'tg-text-domain' );
			echo '</div>';
			exit;
		}
		
		$ajax_method = array_filter($this->grid_data, function($s){
			$ajax_button = (is_string($s)) ? strpos($s,'the_grid_get_pagination') : false;
			$ajax_pages  = (is_string($s)) ? strpos($s,'the_grid_get_ajax_button') : false;
			$ajax_method = ($ajax_button || $ajax_pages) ? true : false;
			return $ajax_method;
		});
		
		// get all items skin if exist
		global $tg_item_skins;

		if ($ajax_method) {
			$no_page_query = $tg_query_args;
			$no_page_query['posts_per_page'] = '-1';
			$posts = get_posts($no_page_query);
		} else {
			$posts = $tg_grid_query->posts;
		}
	
		$tg_item_skins = wp_list_pluck($posts, 'the_grid_item_skin');
			
	}
	
	/**
	* Reset the main query
	* @since 1.0.0
	*/
	public function the_grid_reset_query() {
		// reset custom query grid
		wp_reset_query();
		// clean up after the query and pagination
		wp_reset_postdata();
	}
	
	/**
	* Build the meta query
	* @since 1.0.0
	*/
	public function the_grid_meta_query() {
		
		// get meta query info
		$meta_query = $this->getVar($this->grid_data,'meta_query');
		$meta_query = json_decode($meta_query, TRUE);
		
		// loop options and rebuild query array logic
		if ($meta_query && count($meta_query) > 1) {
			$i = 0;
			$y = 0;
			$meta = array();
			$relation = false;
			foreach ($meta_query as $meta_keys=>$meta_key) {
				if (isset($meta_key['relation']) && $i == 0) {
					$meta['relation'] = $meta_key['relation'];
				} else if (isset($meta_key['relation'])) {
					$meta[$i] = array();
					$meta[$i]['relation'] = $meta_key['relation'];
					$relation = true;
					$y = 0;
					$i++;
				} else {
					if ($relation == true) {
						$meta[$i-1][$y]['key']      = $meta_key['key'];
						$meta[$i-1][$y]['value']    = $meta_key['value'];
						$meta[$i-1][$y]['compare']  = $meta_key['compare'];
						if (isset($meta_key['type'])) {
							$meta[$i-1][$y]['type'] = $meta_key['type'];
						}
						$y++;
					} else {
						$meta[$i]['key']      = $meta_key['key'];
						$meta[$i]['value']    = $meta_key['value'];
						$meta[$i]['compare']  = $meta_key['compare'];
						if (isset($meta_key['type'])) {
							$meta[$i]['type'] = $meta_key['type'];
						}
						$i++;
					}				
				}			
			}
		} else {
			$meta = null;
		}

		return $meta;
		
	}
	
	/**
	* Get & set main options for the grid data (For JS script)
	* @since 1.0.0
	*/
	public function the_grid_layout_data() {
		
		global $pagenow;

		// grid name
		$grid_name = $this->getVar($this->grid_data,'name');
		
		// grid type (masonry/grid)
		$grid_style = $this->getVar($this->grid_data,'style','grid');
		
		// grid layout (horizontal/vertical)
		$grid_layout = $this->getVar($this->grid_data,'layout','vertical');
		
		// grid layout RTL
		$grid_rtl = $this->getVar($this->grid_data,'rtl',false);
		
		// grid filter combination
		$filter_comb = $this->getVar($this->grid_data,'filter_combination',false);
		
		// grid filter logic
		$filter_logic = $this->getVar($this->grid_data,'filter_logic','AND');
		
		// grid filter on load
		$filters        = $this->getVar($this->grid_data,'filter_onload', array());
		$filters_sep    = ($filter_logic === 'OR') ? ',' : '';
		$active_filters = array();
		$filter_onload  = null;
		foreach ($filters as $filter=>$value) {
			$filter = explode(':', $value);
			$filter_onload .= '.f'.$filter[1].$filters_sep;
			array_push($active_filters, $filter[1]);
		}
		$filter_onload = rtrim($filter_onload, ',');
		
		// grid sortby on load
		$sortby_onload = $this->getVar($this->grid_data,'sort_by_onload');
		
		// grid sort order on load
		$order_onload = $this->getVar($this->grid_data,'sort_order_onload',false);
		
		// grid full width mode
		$grid_full_w = $this->getVar($this->grid_data,'full_width');
		
		// grid full height mode (horizontal)
		$grid_full_h = $this->getVar($this->grid_data,'full_height');
		$grid_full_h = ($grid_layout == 'horizontal' && $grid_style != 'masonry') ? $grid_full_h : 'null';
		
		// slider row number (only horizontal mode)
		$grid_row_nb = $this->getVar($this->grid_data,'row_nb',1);
		$grid_row_nb = ($grid_style == 'masonry') ? 1 : $grid_row_nb;

		// grid items gutter (masonry/grid)
		$item_gutter = $this->getVar($this->grid_data,'gutter',0);
		
		// grid items ratio
		$item_x_ratio = $this->getVar($this->grid_data,'item_x_ratio',4);
		$item_y_ratio = $this->getVar($this->grid_data,'item_y_ratio',3);
		$item_ratio   = number_format((float)$item_x_ratio/$item_y_ratio, 2, '.', '');
		
		// slider attribute
		$swingSpeed = $this->getVar($this->grid_data,'slider_swingSpeed',0.1);
		$itemNav    = $this->getVar($this->grid_data,'slider_itemNav', 'null');
		$itemNav    = (!empty($itemNav) && $grid_row_nb == 1) ? $itemNav : 'null';
		$autoplay   = $this->getVar($this->grid_data,'slider_autoplay');
		$cycle      = $this->getVar($this->grid_data,'slider_cycleInterval', 5000);
		$cycleBy    = (!empty($autoplay)) ? 'pages'  : 'null';		
		$startAt    = $this->getVar($this->grid_data,'slider_startAt', 1);
		$startAt    = ($itemNav != 'null') ? $startAt : 1;

		// columns number
		$col_desktop_large  = $this->getVar($this->grid_data,'desktop_large',6);
		$col_desktop_medium = $this->getVar($this->grid_data,'desktop_medium',5);
		$col_desktop_small  = $this->getVar($this->grid_data,'desktop_small',4);
		$col_tablet         = $this->getVar($this->grid_data,'tablet',3);
		$col_tablet_small   = $this->getVar($this->grid_data,'tablet_small',2);
		$col_mobile         = $this->getVar($this->grid_data,'mobile',1);
		
		// columns window widths
		$ww_desktop_medium = $this->getVar($this->grid_data,'desktop_medium_width',1200);
		$ww_desktop_small  = $this->getVar($this->grid_data,'desktop_small_width',980);
		$ww_tablet         = $this->getVar($this->grid_data,'tablet_width',768);
		$ww_tablet_small   = $this->getVar($this->grid_data,'tablet_small_width',480);
		$ww_mobile         = $this->getVar($this->grid_data,'mobile_width',320);

		// build columns/width array
		$grid_cols = array(
			array((int)$ww_mobile, (int)$col_mobile),
			array((int)$ww_tablet_small, (int)$col_tablet_small),
			array((int)$ww_tablet, (int)$col_tablet),
			array((int)$ww_desktop_small, (int)$col_desktop_small),
			array((int)$ww_desktop_medium, (int)$col_desktop_medium),
			array(9999, (int)$col_desktop_large),
		);
		// encode array in json for html data attribute
		$grid_cols = htmlspecialchars(json_encode($grid_cols, TRUE), ENT_QUOTES, 'UTF-8');
		
		// Get item animation style
		$anim_name = new The_Grid_Item_Animation();
		$anim_arr  = $anim_name->get_animation_name();
		$animation = $this->getVar($this->grid_data,'animation','fade_in');
		$animation = (isset($anim_arr[$animation])) ? $animation : 'none';
		// Get animation duration (item transition)
		$transition = $this->getVar($this->grid_data,'transition',0);
		$transition = ($transition == 0 || $animation == 'none') ? 0 : $transition.'ms';
		$animation = json_encode($anim_arr[$animation]);
		
		// ajax functionnality
		$posts_per_page = $this->getVar($this->grid_data,'item_number',12);
		$ajax_method = $this->getVar($this->grid_data,'ajax_method');
		$ajax_method = ($posts_per_page != '-1' && $grid_layout != 'horizontal') ? $ajax_method : '';
		$ajax_delay  = $this->getVar($this->grid_data,'ajax_item_delay',0);
		
		// preloader functionnality
		$preloader  = $this->getVar($this->grid_data,'preloader');
		$item_delay = $this->getVar($this->grid_data,'item_delay');
		
		// check if we are in admin grid page setup for retrieving metadata in ajax
		$settings = (isset($pagenow) && $pagenow == 'admin-ajax.php') ? true : false;
		
		// reset checked values for later
		$prefix = $this->grid_prefix;
		$this->grid_data[$prefix.'style']  = $grid_style;
		$this->grid_data[$prefix.'layout'] = $grid_layout;
		$this->grid_data[$prefix.'active_filters'] = $active_filters;
		$this->grid_data[$prefix.'gutter'] = $item_gutter;
		$this->grid_data[$prefix.'row_nb'] = $grid_row_nb;
		$this->grid_data[$prefix.'slider_itemNav'] = $itemNav;
		$this->grid_data[$prefix.'animation'] = $animation;
		$this->grid_data[$prefix.'transition'] = $transition;
		$this->grid_data[$prefix.'settings']  = $settings;

		// check layout values
		$data_attr  = ' data-name="'.esc_attr($grid_name).'" ';
		$data_attr .= ' data-style="'.esc_attr($grid_style).'"';
		$data_attr .= ' data-row="'.esc_attr($grid_row_nb).'"';
		$data_attr .= ' data-layout="'.esc_attr($grid_layout).'"';
		$data_attr .= ' data-rtl="'.esc_attr($grid_rtl).'"';
		$data_attr .= ' data-filtercomb="'.esc_attr($filter_comb).'"';
		$data_attr .= ' data-filterlogic="'.esc_attr($filter_logic).'"';
		$data_attr .= ' data-filterload ="'.esc_attr($filter_onload).'"';
		$data_attr .= ' data-sortbyload ="'.esc_attr($sortby_onload).'"';
		$data_attr .= ' data-orderload ="'.esc_attr($order_onload).'"';
		$data_attr .= ' data-fullwidth="'.esc_attr($grid_full_w).'"';
		$data_attr .= ' data-fullheight="'.esc_attr($grid_full_h).'"';
		$data_attr .= ' data-gutter="'.esc_attr($item_gutter).'"';
		$data_attr .= ' data-slider=\'{"itemNav":"'.esc_attr($itemNav).'","swingSpeed":'.esc_attr($swingSpeed).',"cycleBy":"'.esc_attr($cycleBy).'","cycle":'.esc_attr($cycle).',"startAt":'.esc_attr($startAt).'}\'';
		$data_attr .= ' data-ratio="'.esc_attr($item_ratio).'"';
		$data_attr .= ' data-cols="'.esc_attr($grid_cols).'"';
		$data_attr .= ' data-animation="'.esc_attr($animation).'"';
		$data_attr .= ' data-transition="'.esc_attr($transition).'"';
		$data_attr .= ' data-ajaxmethod="'.esc_attr($ajax_method).'"';
		$data_attr .= ' data-ajaxdelay="'.esc_attr($ajax_delay).'"';
		$data_attr .= ' data-preloader="'.esc_attr($preloader).'"';
		$data_attr .= ' data-itemdelay="'.esc_attr($item_delay).'"';
		
		// add data attribute for js plugin
		return $data_attr;
		
	}
	
	/**
	* Build Sorter
	* @since 1.0.0
	*/
	public function the_grid_get_sorters() {
		
		$sortBy   = $this->getVar($this->grid_data,'sort_by', array());
		$sort_txt = $this->getVar($this->grid_data,'sort_by_text',__( 'Sort By', 'tg-text-domain' ));
		$sort_onload = $this->getVar($this->grid_data,'sort_by_onload');
		
		$base    = new The_Grid_Base();
		$sorting = $base->grid_sorting();

		$sortBy = (class_exists('WooCommerce')) ? $sortBy : array_filter($sortBy, array($this, 'the_grid_sorter_filter'));
		$order_onload = $this->getVar($this->grid_data,'sort_order_onload',false);
		
		if (isset($sortBy) && !empty($sortBy)) {
			
			// mobile detection (preserve natural dropdown list on mobile devices)
			$mobile = (wp_is_mobile()) ? ' is-mobile' : null;
			
			// retrieve dropdown colors
			$dd_color         = $this->getVar($this->grid_data,'dropdown_color', '#777777');
			$dd_bg            = $this->getVar($this->grid_data,'dropdown_bg', '#ffffff');
			$data_color       = 'data-color="'.esc_attr($dd_color).'"';
			$data_bg          = ' data-bg="'.esc_attr($dd_bg).'"';
			$data_hover_color = ' data-hover-color="'.esc_attr($this->getVar($this->grid_data,'dropdown_hover_color', '#444444')).'"';
			$data_hover_bg    = ' data-hover-bg="'.esc_attr($this->getVar($this->grid_data,'dropdown_hover_bg', '#f5f6fa')).'"';
			$data_colors      = $data_color.$data_bg .$data_hover_color.$data_hover_bg;
			$li_color         = 'style="background:'.esc_attr($dd_bg).';color:'.esc_attr($dd_color).'"';
			
			$sorter  = '<!-- The Grid sorters holder -->';
			$sorter .= '<div class="tg-sorters-holder">';
				$sorter .= '<div class="tg-dropdown-holder tg-nav-border tg-nav-font'.esc_attr($mobile).'">';
					$sorter .= '<span class="tg-dropdown-title tg-nav-color tg-nav-font">'.esc_attr($sort_txt).'</span>';
					$sorter .= '&nbsp;<span class="tg-dropdown-value tg-nav-color tg-nav-font">'.ucfirst(esc_attr($sort_onload)).'</span>';
					if ($mobile != null) {
						$sorter .= '<select class="tg-dropdown-list tg-sorter'.esc_attr($mobile).'">';
							foreach ($sortBy as $sort) {
								if (isset($sorting[$sort])) {
									$sorter .= '<option class="tg-dropdown-item" data-value="'.esc_attr(str_replace('woo_','', $sort)).'">'.ucfirst(esc_attr($sorting[$sort])).'</option>';
								}
							}
						$sorter .= '</select>';
					} else {
						$sorter .= '<ul class="tg-dropdown-list tg-sorter">';
							foreach ($sortBy as $sort) {
								if (isset($sorting[$sort])) {
									$sorter .= '<li class="tg-dropdown-item"  data-value="'.esc_attr(str_replace('woo_','', $sort)).'" '.$li_color.' '.$data_colors.'>'.ucfirst(esc_attr($sorting[$sort])).'</li>';
								}
							}
						$sorter .= '</ul>';
					}
				$sorter .= '</div>';
				$sorter .= '<div class="tg-sorter-order tg-nav-color tg-nav-border" data-asc="'.esc_attr($order_onload).'">';
					$sorter .= '<i class="tg-icon-sorter-down tg-nav-color tg-nav-font"></i>';
					$sorter .= '<i class="tg-icon-sorter-up tg-nav-color tg-nav-font"></i>';
				$sorter .= '</div>';
			$sorter .= '</div>';

			return $sorter;
		}
	}
	
	/**
	* Sorter filter for woocommerce (callback)
	* @since 1.0.0
	*/
	public function the_grid_sorter_filter($string) {
		return strpos($string, 'woo_') === false;
	}
	
	/**
	* Build search field
	* @since 1.0.0
	*/
	public function the_grid_get_search_field() {
		
		$search_txt = $this->getVar($this->grid_data,'search_text');
		
		$search  = '<!-- The Grid Search holder -->';
		$search .= '<div class="tg-search-holder">';
			$search .= '<div class="tg-search-inner tg-nav-border">';
				$search .= '<span class="tg-search-icon tg-nav-color tg-nav-font"></span>';
				$search .= '<input type="text" class="tg-search tg-nav-color tg-nav-font" autocomplete="off" placeholder="'. esc_attr($search_txt) .'" />';
				$search .= '<span class="tg-search-clear tg-nav-color tg-nav-border tg-nav-font"></span>';
			$search .= '</div>';
		$search .= '</div>';
		
		return $search;
	}
	
	/**
	* Build Filters
	* @since 1.0.0
	*/
	public function the_grid_get_filters($index) {
		
		// retrieve filter options
		$terms      = json_decode($this->getVar($this->grid_data,'filters_'.$index));
		$order      = $this->getVar($this->grid_data,'filters_order_'.$index);
		$all_txt    = $this->getVar($this->grid_data,'filter_all_text_'.$index);
		$count      = $this->getVar($this->grid_data,'filter_count_'.$index);
		$counter    = ($count == 'inline') ? ' (<span class="tg-filter-count"></span>)' : null;
		$data_count = ($count == 'tooltip') ? ' data-count="1"' : null;
		$type       = $this->getVar($this->grid_data,'filter_type_'.$index, 'button');
			
		// get available terms name
		$i = 0;
		$taxonomies = array();
		if (isset($terms) && !empty($terms)) {
			foreach ($terms as $term) {
				$name = get_term( $term->id, $term->taxonomy );
				if (isset($name->name)) {
					$taxonomies[$i]['id']    = $term->id;
					$taxonomies[$i]['name']  = strtolower($name->name);
					$taxonomies[$i]['count'] = $name->count;
					$i++;
				}
			}
		}
		
		// sort array with current available terms
		switch ($order) {
			case 'number_asc':
				usort($taxonomies, function ($a,$b){ return $a['count'] - $b['count']; });
				break;
			case 'number_desc':
				usort($taxonomies, function ($a,$b){ return $b['count'] - $a['count']; });
				break;
			case 'alphabetical_asc':
				usort($taxonomies, function ($a,$b){ return strcmp($a['name'], $b['name']); });
				break;
			case 'alphabetical_desc':
				usort($taxonomies, function ($a,$b){ return strcmp($b['name'], $a['name']); });
				break;
		}
		
		// Build Filter list
		if (isset($taxonomies) && !empty($taxonomies)) {
			
			$active_filters = $this->getVar($this->grid_data,'active_filters', array());
			
			$filters  = '<!-- The Grid filters holder -->';
			switch ($type) {
				case 'button':
					$filters .= '<div class="tg-filters-holder">';
					if (!empty($all_txt)) {
						$active_all = (empty($active_filters)) ? 'tg-filter-active' : null;
						$filters .= '<div class="tg-filter '.esc_attr($active_all).' tg-nav-color tg-nav-border tg-nav-font" data-filter="*">';
						$filters .= '<span class="tg-filter-name tg-nav-color tg-nav-font"'.$data_count.'>'.esc_attr($all_txt).$counter.'</span>';
						$filters .= '</div>';
					}
					foreach ($taxonomies as $taxonomy) {
						$class    = (in_array($taxonomy['id'], $active_filters)) ? ' tg-filter-active' : null;
						$filters .= '<div class="tg-filter tg-nav-color tg-nav-border tg-nav-font'.esc_attr($class).'" data-filter=".f'. esc_attr($taxonomy['id']) .'">';
						$filters .= '<span class="tg-filter-name tg-nav-color tg-nav-font"'.$data_count.'>'.ucfirst(esc_attr($taxonomy['name'])).$counter.'</span>';
						$filters .= '</div>';
					}
					$filters .= '</div>';
					break;
				case 'dropdown':
				
					// retrieve filter colors
					$dd_color         = $this->getVar($this->grid_data,'dropdown_color', '#777777');
					$dd_bg            = $this->getVar($this->grid_data,'dropdown_bg', '#ffffff');
					$data_color       = 'data-color="'.esc_attr($dd_color).'"';
					$data_bg          = ' data-bg="'.esc_attr($dd_bg).'"';
					$data_hover_color = ' data-hover-color="'.esc_attr($this->getVar($this->grid_data,'dropdown_hover_color', '#444444')).'"';
					$data_hover_bg    = ' data-hover-bg="'.esc_attr($this->getVar($this->grid_data,'dropdown_hover_bg', '#f5f6fa')).'"';
					$data_colors      = $data_color.$data_bg .$data_hover_color.$data_hover_bg;
					$li_color         = 'style="background:'.esc_attr($dd_bg).';color:'.esc_attr($dd_color).'"';
					
					// mobile detection (preserve natural dropdown list on mobile devices)
					$mobile = (wp_is_mobile()) ? ' is-mobile' : null;

					// retrieve dropdown title
					$dropdown_title = $this->getVar($this->grid_data,'filter_dropdown_title_'.$index, 'Filter Categories');
					
					$filters .= '<div class="tg-filters-holder">';
						$filters .= '<div class="tg-dropdown-holder tg-nav-border tg-nav-font">';
							$filters .= '<span class="tg-dropdown-title tg-nav-color tg-nav-font">'.esc_attr($dropdown_title).'</span>';
							$filters .= '<i class="tg-icon-dropdown-open tg-nav-color tg-nav-font"></i>';
							if ($mobile != null) {
								$multi_op = ($this->getVar($this->grid_data,'filter_combination',false)) ? ' multiple' : null;
								$filters .= '<select class="tg-dropdown-list'.esc_attr($mobile).'" '.$data_colors.$multi_op.'>';
									if (!empty($all_txt)) {
										$active_all = (empty($active_filters)) ? 'tg-filter-active' : null;
										$selected   = ($active_all) ? ' selected' : null;
										$filters .= '<option class="tg-dropdown-item tg-filter '.esc_attr($active_all).'" data-filter="*"'.esc_attr($selected).'>';
										$filters .= esc_attr($all_txt);
										$filters .= '</option>';
									}
									foreach ($taxonomies as $taxonomy) {
										$class    = (in_array($taxonomy['id'], $active_filters)) ? ' tg-filter-active' : null;
										$selected = ($class) ? ' selected' : null;
										$filters .= '<option class="tg-dropdown-item tg-filter'.esc_attr($class).'" data-filter=".f'. esc_attr($taxonomy['id']) .'"'.esc_attr($selected).'>';
										$filters .= ucfirst(esc_attr($taxonomy['name']));
										$filters .= '</option>';
									}
								$filters .= '</select>';
							} else {
								$filters .= '<ul class="tg-dropdown-list" '.$data_colors.'>';
									if (!empty($all_txt)) {
										$active_all = (empty($active_filters)) ? 'tg-filter-active' : null;
										$filters .= '<li class="tg-dropdown-item tg-filter '.esc_attr($active_all).'" data-filter="*" '.$li_color.' '.$data_colors.'>';
											$filters .= '<span class="tg-filter-name">'.esc_attr($all_txt).'</span>';
										if ($count != 'none') {
											$filters .= '&nbsp;<span>(</span><span class="tg-filter-count"></span><span>)</span>';
										}
										$filters .= '</li>';
									}
									foreach ($taxonomies as $taxonomy) {
										$class    = (in_array($taxonomy['id'], $active_filters)) ? ' tg-filter-active' : null;
										$filters .= '<li class="tg-dropdown-item tg-filter'.esc_attr($class).'" data-filter=".f'. esc_attr($taxonomy['id']) .'" '.$li_color.' '.$data_colors.'>';
											$filters .= '<span class="tg-filter-name">'.ucfirst(esc_attr($taxonomy['name'])).'</span>';
											if ($count != 'none') {
												$filters .= '&nbsp;<span>(</span><span class="tg-filter-count"></span><span>)</span>';
											}
										$filters .= '</li>';
									}
								$filters .= '</ul>';
							}
						$filters .= '</div>';
					$filters .= '</div>';
					break;
				}
			
			return $filters;
		}
		
	}
	
	/**
	* Build Left Nav Slider
	* @since 1.0.0
	*/
	public function the_grid_get_left_arrow() {

		$grid_layout = $this->getVar($this->grid_data,'layout');
		
		if ($grid_layout == 'horizontal') {
			$arrow = '<div class="tg-left-arrow tg-nav-color tg-nav-font">';
				$arrow .= '<i class="tg-icon-left-arrow tg-nav-color tg-nav-border tg-nav-font"></i>';
			$arrow .= '</div>';
			return $arrow;
		}
		
	}
	
	/**
	* Build Right Nav Slider
	* @since 1.0.0
	*/
	public function the_grid_get_right_arrow() {
		
		$grid_layout = $this->getVar($this->grid_data,'layout');
		
		if ($grid_layout == 'horizontal') {
			$arrow = '<div class="tg-right-arrow tg-nav-color tg-nav-font">';
				$arrow .= '<i class="tg-icon-right-arrow tg-nav-color tg-nav-border tg-nav-font"></i>';
			$arrow .= '</div>';
			return $arrow;
		}
		
	}
	
	/**
	* Build Bullets Slider
	* @since 1.0.0
	*/
	public function the_grid_get_slider_bullets() {
		
		$grid_layout = $this->getVar($this->grid_data,'layout');
		
		if ($grid_layout == 'horizontal') {
			$bullets  = '<!-- The Grid Slider Bullets -->';
			$bullets .= '<div class="tg-slider-bullets-holder">';
			$bullets .= '<div class="tg-slider-bullets"></div>';
			$bullets .= '</div>';
			return $bullets;
		}
		
	}
	
	/**
	* Build Pagination System
	* @since 1.0.0
	*/
	public function the_grid_get_pagination() {
		
		$ajax_method = $this->getVar($this->grid_data,'ajax_method');
		
		if ($ajax_method != 'on_scroll') {
			
			global $tg_grid_query;

			$big   = 999999999;
			
			$pagination   = null;
			$next_text    = $this->getVar($this->grid_data,'pagination_next', __('Next &#187;', 'tg-text-domain'));
			$prev_text    = $this->getVar($this->grid_data,'pagination_prev', __('&#171; Prev', 'tg-text-domain'));
			$current_page = (get_query_var('paged')) ? max(1, get_query_var('paged')) : max(1, get_query_var('page'));
			$total_pages  = $tg_grid_query->max_num_pages;
			
			$ajax = $this->getVar($this->grid_data,'ajax_pagination');
			$type = $this->getVar($this->grid_data,'pagination_type','number');
			$type = ($ajax) ? 'ajax' : $type;
			
			switch ($type) {
				case 'number':
				
					$show_all  = $this->getVar($this->grid_data,'pagination_show_all',false);
					$end_size  = $this->getVar($this->grid_data,'pagination_end_size',1);
					$mid_size  = $this->getVar($this->grid_data,'pagination_mid_size',2);
					$prev_next = $this->getVar($this->grid_data,'pagination_prev_next');
				
					$pages = paginate_links(array(
						'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
						'format'    => '?paged=%#%',
						'current'   => $current_page,
						'total'     => $total_pages,
						'show_all'  => $show_all,
						'end_size'  => $end_size,
						'mid_size'  => $mid_size,
						'type'      => 'array',
						'prev_next' => $prev_next,
						'prev_text' => $prev_text,
						'next_text' => $next_text,
					));
					if(is_array($pages)) {
						$pagination .= '<div class="tg-pagination-holder">';
							$pagination .= '<ul class="tg-pagination-number">';
							$first = 'first';
							foreach ($pages as $page) {
									preg_match("'<(.*?)>(.*?)</(.*?)>'si", $page, $matches);
									$page_nb = (isset($matches[2]) && (int)$matches[2]) ? (int) $matches[2] : 0;
									$page_nb = ' data-page="'.esc_attr($page_nb).'"';
									$current = ' data-current="'.esc_attr($current_page).'"';
									$pagination .= '<li class="tg-page '.esc_attr($first).'"'.$page_nb.$current.'>'.str_replace(array('page-numbers', 'current'), array('tg-page-number tg-nav-color tg-nav-border tg-nav-font','tg-page-current'), $page).'</li>';
									$first = null;
							}
							$pagination .= '</ul>';
						$pagination .= '</div>';
					}
					break;
					
				case 'button':
					if ($total_pages > 1) {
						if ($current_page == 1) {
							$prev_link = null;
							$next_link = '<a href="'. esc_url(get_pagenum_link($current_page+1)) .'" class="tg-nav-color">'. esc_attr($next_text) .'</a>';
						} else if ($current_page == $total_pages) {
							$prev_link = '<a href="'. esc_url(get_pagenum_link($current_page-1)) .'" class="tg-nav-color">'. esc_attr($prev_text) .'</a>';
							$next_link = null;
						} else {
							$prev_link = '<a href="'. esc_url(get_pagenum_link($current_page-1)) .'" class="tg-nav-color">'. esc_attr($prev_text) .'</a>';
							$next_link = '<a href="'. esc_url(get_pagenum_link($current_page+1)) .'" class="tg-nav-color">'. esc_attr($next_text) .'</a>';
						}
						$pagination .= '<div class="tg-pagination-holder">';
							$pagination .= (!empty($prev_link)) ? '<div class="tg-pagination-prev tg-nav-color tg-nav-border tg-nav-font">'. $prev_link .'</div>' : null;
							$pagination .= (!empty($next_link)) ? '<div class="tg-pagination-next tg-nav-color tg-nav-border tg-nav-font">'. $next_link .'</div>' : null;
						$pagination .= '</div>';
					}
					break;
				
				case 'ajax':
					$current = ' tg-page-current';
					$pagination .= '<div class="tg-pagination-holder">';
						$pagination .= '<ul class="tg-pagination-number">';
						for ($i = 1; $i <= $total_pages; $i++) {
							$pagination .= '<li class="tg-page">';
								$pagination .= '<span class="tg-page-number tg-page-ajax tg-nav-color tg-nav-border tg-nav-font'.esc_attr($current).'" data-page="'.esc_attr($i-1).'">'.esc_attr($i).'</span>';
							$pagination .= '</li>';
							$current = null;
						}
						$pagination .= '</ul>';
					$pagination .= '</div>';
					break;
			}
	
			return $pagination;
		
		}

	}
	
	/**
	* Build Ajax button
	* @since 1.0.0
	*/
	public function the_grid_get_ajax_button() {
		
		$ajax_method = $this->getVar($this->grid_data,'ajax_method');
			
		global $tg_grid_query;
			
		$ajax_button  = null;
		$button_count = null;
		$item_total   = $tg_grid_query->found_posts;
		$item_loaded  = (int)$this->getVar($this->grid_data,'item_number',12);
		$item_remain  = $this->getVar($this->grid_data,'ajax_items_remain');
			
		$item_to_load = $item_total - $item_loaded;
			
		if ($item_remain) {
			$button_count = ' ('.$item_to_load.')';
		}
			
		if ($item_to_load > 0 && $item_loaded != -1) {
				
			$button_text = $this->getVar($this->grid_data,'ajax_button_text',__( 'Load More', 'tg-text-domain' ));
				
			$text    = ' data-button="'.esc_attr($button_text).'"';
			$loading = ' data-loading="'.esc_attr($this->getVar($this->grid_data,'ajax_button_loading')).'"';
			$no_more = ' data-no-more="'.esc_attr($this->getVar($this->grid_data,'ajax_button_no_more', __( 'No more item', 'tg-text-domain' ))).'"';
			$remain  = ' data-remain="'.esc_attr($item_remain).'"';

			$ajax_button .= '<!-- The Grid Ajax Button -->';
			$ajax_button .= '<div class="tg-ajax-button-holder">';
			$ajax_button .= '<div class="tg-ajax-button tg-nav-color tg-nav-border tg-nav-font" data-item-tt="'.esc_attr($item_total).'"'.$text.$loading.$no_more.$remain.'><span class="tg-nav-color">'.esc_attr($button_text).esc_attr($button_count).'</span></div>';
			$ajax_button .= '</div>';
		}
			
		return $ajax_button;

	}

	/**
	* Add Ajax on scroll functionnality
	* @since 1.0.0
	*/
	public function the_grid_get_ajax_scroll() {
		
		$ajax_method = $this->getVar($this->grid_data,'ajax_method');
		
		if ($ajax_method == 'on_scroll') {

			$loading = esc_attr($this->getVar($this->grid_data,'ajax_button_loading'));
			
			if (!empty($loading)) {
				$no_more = ' data-no-more="'.esc_attr($this->getVar($this->grid_data,'ajax_button_no_more')).'"';
				$ajax_scroll  = '<!-- The Grid Ajax Scroll -->';
				$ajax_scroll .= '<div class="tg-ajax-scroll-holder">';
				$ajax_scroll .= '<div class="tg-ajax-scroll" '.$no_more.'>'.esc_attr($loading).'</div>';
				$ajax_scroll .= '</div>';
				return $ajax_scroll;
			}
			
		}
	}
	
}

/***********************************************
* THE GRID Extended Class for item loop & skin
***********************************************/


class The_Grid_Item extends The_Grid {
	
	public function __construct() {
		parent::__construct();
	}
	
	/**
	* Output item in the grid
	* @since 1.0.0
	*/
	public function item_output($post_ID, $data) {
		
		// if preloader add hidden class to reveal after
		$preloader  = $data['preloader'];
		$settings   = $data['settings'];
		$grid_style = $data['grid_style'];
		$item_size  = $data['item_size'];
		$item_skins = $data['item_skins'];
		$preloader  = ($preloader == 'true') ? ' tg-item-reveal' : '';
		
		// retieve item data
		$cats = $this->item_category($post_ID);
		$size = ($item_size == true) ? $item_size : $this->item_size($post_ID, $grid_style);
		$meta = $this->item_data($post_ID,$data);
		
		// global item skins
		$item_base = new The_Grid_Item_Skin();
		$get_skins = $item_base->get_skin_names();
		$post_type = get_post_type();
		
		// current item skin
		$item_skin = get_post_meta($post_ID, 'the_grid_item_skin', true);
		if (!empty($item_skin) && isset($get_skins[$item_skin]) && $get_skins[$item_skin]['type'] == $grid_style) {
			$item_skins[$post_type] = $item_skin;
		}

		// check for a default skin if skin not exist
		if (!isset($item_skins[$post_type]) || !array_key_exists($item_skins[$post_type],$get_skins)) {
			$base = new The_Grid_Base();
			$skin = $base->default_skin($grid_style);
			if (!$skin) {
				return false;
			}
		} else {
			$skin  = $item_skins[$post_type];
		}

		// get slug & content
		$skin_slug = $get_skins[$skin]['slug'];
		$skin_php  = $get_skins[$skin]['php'];
		
		$post_class  = ' tg-post-'.$post_ID;
		$post_sticky = (is_sticky($post_ID)) ? ' sticky' : null;
		
		// start output item
		$output  = '<article class="tg-item'.$post_class.$post_sticky.$cats.$preloader.' '.$skin_slug.'"'.$meta.$size.'>';
		$output .= '<div class="tg-item-inner">';

			ob_start();
			$skin = include($skin_php);	
			$output .= ob_get_contents();
			ob_end_clean();
			$output .= $skin;

		// output end
		$output .= '</div>';
		
		// add setting button if backend
		$output .= ($settings == true) ? $this->item_settings($post_ID) : null;
		
		$output .= '</article>';
		
		return $output;
	}
	
	/**
	* Item setting buttons for backend
	* @since 1.0.0
	*/
	public function item_settings($post_ID) {
		$WPML = new The_Grid_WPML();
		$output  = '<div class="tg-item-hidden-overlay"></div>';
		$output .= '<div class="tg-item-settings" data-id="'.esc_attr($post_ID).'" data-action="'.admin_url( 'post.php?post='.esc_attr($post_ID).'&action=edit'.$WPML->WPML_post_query_lang($post_ID)).'">';
			$output .= '<span>'.__( 'Loading', 'tg-text-domain' ).'</span>';
		$output .= '</div>';
		$output .= '<div class="tg-item-exclude" data-id="'.esc_attr($post_ID).'">';
			$output .= '<span class="tg-item-hide">'.__( 'Hide item', 'tg-text-domain' ).'</span>';
			$output .= '<span class="tg-item-show">'.__( 'Show item', 'tg-text-domain' ).'</span>';
		$output .= '</div>';
		return $output;
	}
	
	/**
	* Get item categories
	* @since 1.0.0
	*/
	public function item_category($post_ID) {
		
		$cats       = null;
		// retrieve all tax
		$taxonomies = get_object_taxonomies(get_post_type());
		
		// loop throught each tax
		foreach( $taxonomies as $taxonomy ) {
			$terms = get_the_terms($post_ID, $taxonomy);
			if ($terms && !is_wp_error($terms)) {
				foreach( $terms as $term ) {
					$cats .= ' f'.$term->term_id;
				}
			}
		}
		
		return $cats;
	}
	
	/**
	* Get item size
	* @since 1.0.0
	*/
	public function item_size($post_ID, $grid_style) {
		// get meta
		$item_col = get_post_meta($post_ID, 'the_grid_item_col', true);
		$item_row = get_post_meta($post_ID, 'the_grid_item_row', true);
		// set col/row number
		$item_col = (!empty($item_col) && isset($item_col)) ? $item_col : 1;
		$item_row = (!empty($item_row) && isset($item_row)) ? $item_row : 1;
		// assign data
		$data_col  = ' data-col="'.esc_attr($item_col).'"';
		$data_row  = ($grid_style == 'grid') ? ' data-row="'.esc_attr($item_row).'"' : null;
		$data_size =  $data_row.$data_col;
		
		return $data_size;
	}
	
	/**
	* Add data for sort/filter/search field
	* @since 1.0.0
	*/
	public function item_data($post_ID,$data) {
		
		$sortBy  = $data['sort_by'];
		$sortBy  = (class_exists('WooCommerce')) ? $sortBy : array_filter($sortBy, array('parent', 'the_grid_sorter_filter'));
		$product = (class_exists('WooCommerce')) ? get_product($post_ID) : null;

		if (isset($sortBy) && !empty($sortBy)) {
			foreach ($sortBy as $sort) {
				switch($sort){
					case 'id':
						$data_attr[$sort] = $post_ID;
						break;
					case 'title':
						$data_attr[$sort] = substr(get_the_title(), 0, 12);
						break;
					case 'author':
						$data_attr[$sort] = get_the_author();
						break;
					case 'date':
						$data_attr[$sort] = get_the_date('Ymd');
						break;
					case 'comment':
						$data_attr[$sort] = wp_count_comments(get_the_ID())->approved;
						break;
					case 'woo_total_sales':
						$data_attr['total-sales'] = get_post_meta($post_ID,'meta_num_total_sales',true);
						break;
					case 'woo_regular_price':
						$data_attr['regular-price'] = $product->get_price();
						break;
					case 'woo_sale_price':
						$data_attr['sale-price'] = $product->get_sale_price();
						break;
					case 'woo_featured':
						$data_attr[str_replace('woo_','', $sort)] = ($product->is_featured()) ? '1' : '0';
						break;
					case 'woo_SKU':
						$data_attr[str_replace('woo_','', $sort)] = $product->get_sku();
						break;
					case 'woo_stock':
						$data_attr[str_replace('woo_','', $sort)] = $product->get_stock_quantity();
						break;
				}
			}
		}
		
		$attr = null;
		if (isset($data_attr) && !empty($data_attr)) {
			foreach ($data_attr as $key => $val ) {
				$attr .= (!empty($val)) ? ' data-'.esc_attr($key).'="'.esc_attr($val).'"' : null;
			}
		}
		
		return $attr;
	}
}

/***********************************************
* THE GRID Extended Class for ajax request
***********************************************/


class The_Grid_Ajax extends The_Grid {
	
	/**
	* Construct for Ajax
	* @since 1.0.0
	*/
	public function __construct() {
		parent::__construct();
		// ajax load more item in grid
		add_action('wp_ajax_the_grid_load_more', array($this, 'the_grid_load_more_items'));
		add_action('wp_ajax_nopriv_the_grid_load_more', array($this, 'the_grid_load_more_items'));
	}
	
	/**
	* Load more item with ajax (button or scroll)
	* @since 1.0.0
	*/
	public function the_grid_load_more_items() {
		
		$nonce  = $_POST['grid_nonce']; 
		$action = $_POST['action'];
		
		// check ajax nounce
		if (!wp_verify_nonce($nonce, $action) && is_user_logged_in()) {
			$response['success'] = false;				
			$response['message'] = __('Loading Error', 'tg-text-domain');
			$response['content'] = null;
			wp_send_json($response);				
			die();
		} else {
			
			// set ajax mode to true
			global $tg_is_ajax;
			$tg_is_ajax = true;
				
			// retrive ajax info
			$grid_page = (isset($_POST['grid_page'])) ? $_POST['grid_page'] : die();
			$grid_name = (isset($_POST['grid_name']) && !empty($_POST['grid_name'])) ? $_POST['grid_name'] : die();
			$grid_data = (isset($_POST['grid_data']) && !empty($_POST['grid_data'])) ? $_POST['grid_data'] : null;
			
			// prevent caching if in backend grid preview
			$cache = false;

			// get grid options (back end if data and front end if not)
			if ($grid_data) {
				
				$grid_name = $grid_data['the_grid_name'];
				$grid_info = get_page_by_title($grid_name, 'OBJECT', 'the_grid');

				if (isset($grid_info->ID)) {
					$grid_id = $grid_info->ID;
				} else {
					$grid_id = $grid_info;
				}

				foreach ($grid_data as $data => $val) {
					$grid_data[$grid_name][$data] = wp_unslash($val);
				}
				
				$grid_data[$grid_name]['the_grid_ID']   = 'grid-'.$grid_id;
				$grid_data[$grid_name]['the_grid_name'] = $grid_data['the_grid_name'];
				// disable preload since grid already loaded
				$grid_data[$grid_name]['the_grid_preloader'] = null;
				$grid_data[$grid_name]['the_grid_settings'] = true;
				$this->grid_data = $grid_data[$grid_name];
				
				global $tg_grid_data;
				$tg_grid_data = $grid_data[$grid_name];
				
			} else {

				$cache   = get_option('the_grid_caching', false);
				$content = parent::the_grid_get_cache($grid_name, $grid_page+1, $cache);
				if (!empty($content)) {
					$response['success'] = true;				
					$response['message'] = __('Content correctly retrieved', 'tg-text-domain');
					$response['content'] = $content;
					wp_send_json($response);
					die();
				}

				$this->grid_data = parent::the_grid_get_data($grid_name);
				// disable preload since grid already loaded
				$this->grid_data['the_grid_preloader'] = null;
			}
			
			// redefined main globals
			global $tg_query_args, $tg_item_count;
			
			// redefined query
			$tg_query_args = parent::the_grid_get_query( $_POST['user_id'] );
			$ajax_page_nav = parent::getVar($this->grid_data,'ajax_pagination');
			$ajax_item_nb  = parent::getVar($this->grid_data,'ajax_item_number',4);
			$tg_query_args['paged'] = $grid_page;
			// check if ajax pagination to keep post per page instead of nb of post to load with ajax (load more button or on scroll)
			$pagination = array_filter($this->grid_data, function($s){
				return $pagination = (is_string($s)) ? strpos($s, 'the_grid_get_pagination') : false;
			});
			$tg_query_args['posts_per_page'] = ($ajax_page_nav && !empty($pagination)) ? $tg_query_args['posts_per_page'] : $ajax_item_nb;
			$tg_query_args['offset'] = parent::getVar($this->grid_data,'item_number',12)+$tg_query_args['posts_per_page']*($grid_page-1);
			
			//redefine item count html comment markup
			$tg_item_count = $tg_query_args['offset']+1;

			// run the custom query
			parent::the_grid_run_query();
			
			// loop to get new items
			$content = parent::the_grid_post_loop();

			// add grid ajax content to cache
			$grid_ID = str_replace('grid-', '', parent::getVar($this->grid_data,'ID'));
			$this->the_grid_save_cache($grid_ID, $grid_page+1, $cache, $content);
			
			// reset the custom query
			parent::the_grid_reset_query();
			
			// send json response
			$response['success'] = true;				
			$response['message'] = __('Content correctly retrieved', 'tg-text-domain');
			$response['content'] = $content;
			wp_send_json($response);

		}

		exit();
	}
}

new The_Grid_Ajax();

/***********************************************
* THE GRID Init (enqueue scripts & registrer shortcode)
***********************************************/

class The_Grid_Init {
	
	protected $grid_options;
	protected $plugin_slug = TG_SLUG;
	protected $debug_mode  = false;
	
	/**
	* Register main actions to load scripts
	* @since 1.0.0
	*/
	public function __construct() {
		$this->debug_mode = get_option('the_grid_debug', false);
		// Load admin style sheet and JavaScript.
		add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'),9);
		// for grid preview
		add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'),9);
		// dequeue Mediaelement & register new styles
		add_action('wp_enqueue_scripts', array($this, 'mediaelement_styles'),100);
		add_action('admin_enqueue_scripts', array($this, 'mediaelement_styles'),100);
	}
		
	/**
	* Set main var from global settings panel
	* @since 1.0.0
	*/
	public function global_settings() {
		$is_mobile = (wp_is_mobile()) ? true : null;
		$mediaelement = (!$is_mobile) ? get_option('the_grid_mediaelement', '') : null;
		$options = array(
			'url' => admin_url('admin-ajax.php'),
			'nonce' => wp_create_nonce('the_grid_load_more'),
			'is_mobile' => $is_mobile,
			'mediaelement' => $mediaelement,
			'mediaelement_ex' => get_option('the_grid_mediaelement_css', ''),
			'debounce' => get_option('the_grid_debounce', '')
		);
		$this->grid_options = $options;
	}
		
	/**
	* Enqueue scripts on front-end
	* @since 1.0.0
	*/
	public function enqueue_scripts($options) {
		$this->global_scripts();
		$this->global_styles();
	}
	
	/**
	* Enqueue scripts in Admin
	* @since 1.0.0
	*/
	public function admin_enqueue_scripts() {
		$screen = get_current_screen();
		if ($screen->id == $this->plugin_slug) {
			$this->global_scripts();
			$this->global_styles();
		}
	}
	
	/**
	* Register Globlad style and inline css from Global Settings
	* @since 1.0.0
	*/
	public function global_styles() {
		if ($this->debug_mode) {
			wp_enqueue_style('the-grid', TG_PLUGIN_URL . 'frontend/assets/css/the-grid.css', array(), TG_VERSION);
		} else {
			wp_enqueue_style('the-grid', TG_PLUGIN_URL . 'frontend/assets/css/the-grid.min.css', array(), TG_VERSION);
		}
		// then add inline styles (from global settings panel)
		$base      = new The_Grid_Base();
		$bg_color  = get_option('the_grid_ligthbox_background', 'rgba(0,0,0,0.8)');
		$txt_color = get_option('the_grid_ligthbox_color', '#ffffff');
		$custom_css = '
        	.tolb-holder {
            	background: '.$bg_color.';
			}
			.tolb-holder .tolb-close,
			.tolb-holder .tolb-title,
			.tolb-holder .tolb-counter,
			.tolb-holder .tolb-next i,
			.tolb-holder .tolb-prev i {
            	color: '.$txt_color.';
			}
			.tolb-holder .tolb-load {
			    border-color: '.$base->HEX2RGB($txt_color,0.2).';
    			border-left: 3px solid '.$txt_color.';
			}
        ';
		$custom_css = $base->compress_css($custom_css);
        wp_add_inline_style('the-grid', $custom_css);
	}
	
	/**
	* Enqueue main JS scripts
	* @since 1.0.0
	*/
	public function global_scripts() {
		// retrieve global js var (from global settings panel)
		$this->global_settings();
		
		// enqueue easing in case if missing in a theme
		wp_enqueue_script('jquery-effects-core');
		
		// enqueue mediaelement if enable (to be sure in case)
		if ($this->grid_options['mediaelement']) {
			wp_enqueue_script('mediaelement');
		}
		
		// to debug script with native Wordpress functionnality (SCRIPT_DEBUG)
		if ($this->debug_mode) {
			wp_enqueue_script('the-grid-layout', TG_PLUGIN_URL . 'frontend/assets/js/the-grid-layout.js', 'jquery', TG_VERSION, TRUE );
			wp_enqueue_script('the-grid-slider', TG_PLUGIN_URL . 'frontend/assets/js/the-grid-slider.js', 'jquery', TG_VERSION, TRUE );
			wp_enqueue_script('the-grid', TG_PLUGIN_URL . 'frontend/assets/js/the-grid.js', 'jquery', TG_VERSION, TRUE );
		} else {
			wp_enqueue_script('the-grid', TG_PLUGIN_URL . 'frontend/assets/js/the-grid.min.js', 'jquery', TG_VERSION, TRUE );		
		}
		// localize from main var
		wp_localize_script('the-grid', 'tg_global_var', $this->grid_options);
	}
	
	/**
	* Add medialement if set in Global settings
	* @since 1.0.0
	*/
	public function mediaelement_styles() {
		if ($this->grid_options['mediaelement'] && $this->grid_options['mediaelement_ex']) {
			wp_dequeue_style('wp-mediaelement');       // native Wordpress
			wp_deregister_style('wp-mediaelement');    // native Wordpress
			wp_dequeue_style('mediaelementplayer');    // alternative Theme
			wp_deregister_style('mediaelementplayer'); // alternative Theme		
			if ($this->debug_mode) {
				wp_enqueue_style('wp-mediaelement', TG_PLUGIN_URL . 'frontend/assets/css/wp-mediaelement.css', array(), TG_VERSION);
			} else {
				wp_enqueue_style('wp-mediaelement', TG_PLUGIN_URL . 'frontend/assets/css/wp-mediaelement.min.css', array(), TG_VERSION);
			}
		}
	}
	
}

new The_Grid_Init();
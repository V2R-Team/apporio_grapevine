<?php
/**
 * @package   The_Grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 */

// Exit if accessed directly
if (!defined( 'ABSPATH')) { 
	exit;
}
 
/***********************************************
* THE GRID STYLE Class
***********************************************/

class The_Grid_Style extends The_Grid {

	/**
	 * Plugin slug & prefix
	 * @since 1.0.0
	 */
	protected $plugin_slug = TG_SLUG;
	protected $grid_prefix = TG_PREFIX;
	protected $grid_data;
	
	/**
	* initialization
	* @since 1.0.0
	*/
	public function __construct() {}
	
	/**
	* Processing styles
	* @since 1.0.0
	*/
	public function process_styles($grid_ID) {
		
		global $tg_grid_data;
		
		$this->grid_data = $tg_grid_data;
		
		$styles  = $this->main_wrapper_style( $grid_ID);
		$styles .= $this->main_background_style( $grid_ID);
		$styles .= $this->navigation_style( $grid_ID);
		$styles .= $this->areas_style( $grid_ID);
		$styles .= $this->item_style( $grid_ID);
		$styles .= $this->item_skin( $grid_ID);
		$styles .= $this->item_color( $grid_ID);

		return $styles;
		
	}
	
	/**
	* Process main wrapper styles
	* @since 1.0.0
	*/
	public function main_wrapper_style($grid_ID) {
		
		$marg    = null;
		$styles  = null;
		$wrapper = 'wrap_marg_';
		$margins = array('top','bottom','left','right');
		
		foreach($margins as $margin) {
			
			$value = parent::getVar($this->grid_data,$wrapper.$margin,0);
			
			if ($value) {
				$marg .= 'margin-'.$margin.':'.esc_attr($value).'px;';
			}
		}
		
		if (!empty($marg)) {
			$styles = $grid_ID.'{'.$marg.'}';
		}
		
		return $styles;
		
	}
	
	/**
	* Process main background color
	* @since 1.0.0
	*/
	public function main_background_style($grid_ID) {
		
		$styles  = null;
		$grid_bg = parent::getVar($this->grid_data,'grid_background');
		$grid_layout = parent::getVar($this->grid_data,'layout','vertical');
		
		if (!empty($grid_bg)) {
			
			if ($grid_layout == 'horizontal') {
				$styles .= $grid_ID.' .tg-grid-slider{background:'.esc_attr($grid_bg).';}';
			} else {
				$styles .= $grid_ID.' .tg-grid-holder:before{content:"";background-color:'.esc_attr($grid_bg).';}';
			}
			
		}
		
		return $styles;
		
	}
	
	/**
	* Process navigation style
	* @since 1.0.0
	*/
	public function navigation_style($grid_ID) {
		
		$styles  = null;
		
		// navigation styles
		$base = new The_Grid_Base();
		$navigation_name  = parent::getVar($this->grid_data,'navigation_style','tg-std-nav');
		$nav_text_color   = parent::getVar($this->grid_data,'navigation_color', '#999999');
		$nav_accent_color = parent::getVar($this->grid_data,'navigation_accent_color', '#ff6863');
		$nav_background   = parent::getVar($this->grid_data,'navigation_bg', '#999999');
		$nav_accent_bg    = parent::getVar($this->grid_data,'navigation_accent_bg', '#ff6863');
		$brightness = $base->brightness($nav_text_color);
		$nav_border_color = ($brightness == 'bright') ? $base->HEX2RGB($nav_text_color,$alpha=0.6) : $base->HEXLighter($nav_text_color,$ratio=3);
		
		// navigation styles
		global $tg_nav_colors;
		$tg_nav_colors['css_ID']       = $grid_ID;
		$tg_nav_colors['text_color']   = esc_attr($nav_text_color);
		$tg_nav_colors['accent_color'] = esc_attr($nav_accent_color);
		$tg_nav_colors['border_color'] = esc_attr($nav_border_color);
		$tg_nav_colors['background']   = esc_attr($nav_background);
		$tg_nav_colors['accent_background'] = esc_attr($nav_accent_bg);
		
		$navigation_base = new The_Grid_Navigation_Skin();
		$navigation_skin = $navigation_base->$navigation_name();
		$styles .= $navigation_skin['css'];
		
		// navigation colors
		$styles .= $grid_ID.' .tg-nav-color,
				   '.$grid_ID.' .tg-search-icon:hover:before,
				   '.$grid_ID.' .tg-search-icon:hover input,
				   '.$grid_ID.' .tg-disabled:hover .tg-icon-left-arrow,
				   '.$grid_ID.' .tg-disabled:hover .tg-icon-right-arrow,
				   '.$grid_ID.' .tg-dropdown-title.tg-nav-color:hover {
					   color:'.esc_attr($nav_text_color).';
				   }
				   '.$grid_ID.' input.tg-search:hover {
					   color:'.esc_attr($nav_text_color).' !important;
				   }
				   '.$grid_ID.' input.tg-search::-webkit-input-placeholder {
					   color:'.esc_attr($nav_text_color).';
				   }
				   '.$grid_ID.' input.tg-search::-moz-placeholder {
					   color:'.esc_attr($nav_text_color).';
					   opacity: 1;
				   }
				   '.$grid_ID.' input.tg-search:-ms-input-placeholder {
					   color:'.esc_attr($nav_text_color).';
				   }';
		
		// slider colors
		$slider_arrows_color  = parent::getVar($this->grid_data,'navigation_arrows_color');
		$styles .= (!empty($slider_arrows_color)) ? 
			$grid_ID.' .tg-grid-area-left > div > div i,
			'.$grid_ID.' .tg-grid-area-right > div > div i,
			'.$grid_ID.' .tg-grid-area-right .tg-disabled:hover i,
			'.$grid_ID.' .tg-grid-area-left .tg-disabled:hover i {
				color:'.esc_attr($slider_arrows_color).';
			}' : null;
			
		$slider_arrows_bg = parent::getVar($this->grid_data,'navigation_arrows_bg');
		$styles .= (!empty($slider_arrows_bg)) ? 
			$grid_ID.' .tg-grid-area-left > div > div div,
			'.$grid_ID.' .tg-grid-area-right > div > div div,
			'.$grid_ID.' .tg-grid-area-left .tg-disabled:hover,
			'.$grid_ID.' .tg-grid-area-right .tg-disabled:hover {
				background:'.esc_attr($slider_arrows_bg).';
			}' : null;
		
		$slider_bullet_color  = parent::getVar($this->grid_data,'navigation_bullets_color', '#DDDDDD');
		$slider_bullet_colorA = parent::getVar($this->grid_data,'navigation_bullets_color_active', '#59585b');
		$styles .= $grid_ID.' .tg-slider-bullets li.tg-active-item span{background:'.esc_attr($slider_bullet_colorA).';}';
		$styles .= $grid_ID.' .tg-slider-bullets li span{background:'.esc_attr($slider_bullet_color).';}';
		
		return $styles;
		
	}
	
	/**
	* Process areas styles
	* @since 1.0.0
	*/
	public function areas_style($grid_ID) {
		
		$styles  = null;
		
		$areas = array('top1','top2','left','right','bottom1','bottom2');
		$area_div = ' .tg-grid-area-';
		
		foreach($areas as $area) {
			
			$data = parent::getVar($this->grid_data,'area_'.$area,'');
			$data = json_decode($data, TRUE);
			
			if (isset($data['functions']) && !empty($data['functions']) && isset($data['styles']) && !empty($data['styles'])) {
				$styles .= $grid_ID.$area_div.$area.'{';
				foreach($data['styles'] as $style => $value) {
					if (isset($value) && !empty($value)) {
						$unit = (is_numeric($value)) ? 'px' : '';
						$styles  .= esc_attr($style).':'.esc_attr($value.$unit).';';
					}
				}
				$styles .= '}';
			}
			
		}
		
		return $styles;
		
	}

	/**
	* Process item style
	* @since 1.0.0
	*/
	public function item_style($grid_ID) {
		
		$styles  = null;
		
		$grid_layout = parent::getVar($this->grid_data,'layout','vertical');
		$grid_style  = parent::getVar($this->grid_data,'style','grid');
		$grid_row_nb = parent::getVar($this->grid_data,'row_nb',1);
		$itemNav     = parent::getVar($this->grid_data,'slider_itemNav', 'null');
		$itemNav     = (!empty($itemNav) && $grid_row_nb == 1) ? $itemNav : 'null';
		$item_gutter = parent::getVar($this->grid_data,'gutter',0);

		if ($grid_layout == 'horizontal' && $item_gutter > 0) {
			
			if ($itemNav != 'null' || $grid_style == 'masonry') {
				$right   = floor($item_gutter/2);
				$left    = $item_gutter-$right;
				$styles .=  $grid_ID.' .tg-item{';
				$styles .= 'margin: 0 '. esc_attr($right) .'px 0 '. esc_attr($left) .'px;';
				$styles .=  '}';
			}
			
		} else if ($grid_style == 'masonry') {
			
			$styles .=  $grid_ID.' .tg-item{';
			
			if ($grid_layout == 'horizontal') {
				$right   = floor($item_gutter/2);
				$left    = $item_gutter-$right;
				$styles .= 'margin: 0 '. esc_attr($right) .'px '.esc_attr($item_gutter) .'px '. esc_attr($left) .'px;';
			} else {
				$styles .= 'margin-bottom: '. esc_attr($item_gutter) .'px;';
			}
			$styles .=  '}';
			
		}
		
		if ($grid_layout == 'horizontal' && $itemNav == 'null' && $item_gutter > 0 && $grid_style != 'masonry') {
			$styles .= $grid_ID.' .tg-grid-holder{padding: 0 '. esc_attr($item_gutter)/2 .'px;}';
		}
		
		// add perspective on item if necessary
		$animation = parent::getVar($this->grid_data,'animation','zoom_in');
		if ($animation === 'perspective_x') {
			$styles .= $grid_ID. '.tg-item{-webkit-transform-origin: 50% 0%;-moz-transform-origin: 50% 0%;-ms-transform-origin: 50% 0%;-o-transform-origin: 50% 0%;transform-origin: 50% 0%;}';
		} else if ($animation === 'perspective_y')  {
			$styles .= $grid_ID. '.tg-item{-webkit-transform-origin: 0% 50%;-moz-transform-origin: 0% 50%;-ms-transform-origin: 0% 50%;-o-transform-origin: 0% 50%;transform-origin: 0% 50%;}';
		}
		
		return $styles;
		
	}
	
	/**
	* Process item skin
	* @since 1.0.0
	*/
	public function item_skin($grid_ID) {
		
		global $tg_item_skins;
		
		$styles     = null;
		$base       = new The_Grid_Base();
		$item_base  = new The_Grid_Item_Skin();
		$get_skins  = $item_base->get_skin_names();
		$skins      = json_decode(parent::getVar($this->grid_data,'skins',''), TRUE);
		$grid_style = parent::getVar($this->grid_data,'style','grid');
		
		
		foreach ($tg_item_skins as $item_skin) {	
			$item_skin_slug = (array_key_exists($item_skin,$get_skins)) ? $item_skin : '';
			if (!empty($item_skin_slug) && $get_skins[$item_skin_slug]['type'] != $grid_style) {
				$item_skin_slug = '';
			}
			$item_skins[] = $item_skin_slug;
		}
		
		$item_skins = array_filter( $item_skins, 'strlen' );
		$skins = array_merge($skins,$item_skins);

		foreach ($skins as $skin) {	
			
			$skin_slug = (array_key_exists($skin,$get_skins)) ? $skin : $base->default_skin($grid_style);
			if (!$skin_slug) {
				return false;
			}
			
			$skin_css  = $get_skins[$skin_slug]['css'];
			$css_file  = file_get_contents($skin_css);	
			$styles   .= $css_file;
			
		}
		
		return $styles;
		
	}
	
	/**
	* Process item skin
	* @since 1.0.0
	*/
	public function item_color($grid_ID) {
		
		$styles = null;
		
		$schemes    = array('dark','light');
		$title_tags = array('h2','h2 a','h3','h3 a','a','a.tg-link-url','i','.tg-media-button');
		$para_tags  = array('p');
		$span_tags  = array('span','.no-liked .to-heart-icon path','.empty-heart .to-heart-icon path');
		
		$tags = array(
			'title' => $title_tags,
			'text'  => $para_tags,
			'span'  => $span_tags
		);
		
		$default = array(
			'dark_title'  => '#444444',
			'dark_text'   => '#777777',
			'dark_span'   => '#999999',
			'light_title' => '#ffffff',
			'light_text'  => '#f5f5f5',
			'light_span'  => '#f6f6f6',
		);
		
		$colors = null;
		foreach ($schemes as $scheme) {
			foreach ($tags as $tag => $classes) {
				$classes   = implode(',.tg-item .'.$scheme.' ', $classes);
				$def_color = $default[$scheme.'_'.$tag];
				$color     = get_option('the_grid_'.$scheme.'_'.$tag, $def_color);
				$colors   .= '.tg-item .'.$scheme.' '.$classes.'{color:'.$color.';fill:'.$color.';stroke:'.$color.'}';
			}
		}

		$styles = $colors;
		
		$content_bg_skin = parent::getVar($this->grid_data,'skin_content_background','');
		$overlay_bg_skin = parent::getVar($this->grid_data,'skin_overlay_background','');
		$styles .= $grid_ID.' .tg-item-content-holder {
					   background-color:'.esc_attr($content_bg_skin).';
				   }
				   '.$grid_ID.' .tg-item-overlay {
					   background-color:'.esc_attr($overlay_bg_skin).';
				   }';
				   
		return $styles;
		
	}
		
}
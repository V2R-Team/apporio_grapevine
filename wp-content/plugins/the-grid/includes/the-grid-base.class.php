<?php
/**
 * @package   The_Grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 */

// Exit if accessed directly
if (!defined( 'ABSPATH')) { 
	exit;
}

class The_Grid_Base {
	
	/**
	* Get all grid names
	* @since 1.0.0
	*/
	public static function get_all_grid_names() {
		$post_args = array(
			'post_type'      => 'the_grid',
			'post_status'    => 'any',
			'posts_per_page' => -1,
		);
		$grids = get_posts($post_args); 
		if(!empty($grids)){
			foreach($grids as $grid){
				$grid_name[$grid->post_title] = $grid->ID;
			}
			return($grid_name);
		}
	}
	
	/**
	* Get grid list names
	* @since 1.0.0
	*/
	public static function get_grid_list_names() {
		$grid_list  = null;
		$grid_names = self::get_all_grid_names();
		if (isset($grid_names) && !empty($grid_names)) {
			foreach ($grid_names as $grid_name => $grid_ID) {
				$grid_list .= $grid_name.':'.$grid_ID.',';
			}
		}
		$grid_list = rtrim($grid_list, ',');
		return $grid_list;
	}
	
	/**
	* Get users/authors
	* @since 1.0.0
	*/
	public static function get_all_users() {
		$args  = array(
			'orderby' => 'display_name'
		);
		$wp_user_query = new WP_User_Query($args);
		$authors = $wp_user_query->get_results();
		$authors_ID = array();
		if (!empty($authors)) {
			foreach ($authors as $author) {
				$author_info = get_userdata($author->ID);
				$authors_ID[$author->ID] = $author_info->user_nicename;
			}
		}
		return $authors_ID;
	}

	/**
	* Get post types & categories.
	* @since 1.0.0
	*/
	public static function get_post_types_and_categories(){	
		$post_types = self::get_all_categories(true);
		$counter = 0; // prevent to override already set disable option / name category
		$post_types_taxonomies = array();
		foreach($post_types as $post_type => $categories) {
			$taxonomies = array();
			foreach($categories as $taxonomy) {	
				$counter++;
				$category_count = count($taxonomy['cats']);
				$taxonomy_name  = $taxonomy['name'];
				$taxonomy_title = $taxonomy['title'];
				$taxonomies['post_type:'.$post_type.', taxonomy:'.$taxonomy_name.', option: option_disabled'.$counter] = $taxonomy_title.' ('.$category_count.')';
				foreach($taxonomy['cats'] as $category_ID => $category_title) {
					$taxonomies['post_type:'.$post_type.', taxonomy:'.$taxonomy_name.', id:'.$category_ID] = $category_title;
				}	
			}
			$post_types_taxonomies[$post_type] = $taxonomies;
		}
		return($post_types_taxonomies);
	}
	
	/**
	* Get post types with associated categories
	* @since 1.0.0
	*/
	public static function get_all_categories() {
		$post_types = self::get_all_taxonomies();
		$post_types_categories = array();
		foreach($post_types as $name => $taxonomy_array){
			$taxonomies = array();
			foreach($taxonomy_array as $taxonomy_name => $taxonomy_title){
				$categories = self::get_associated_categories($taxonomy_name);
				if(!empty($categories)) {
					$taxonomies[] = array(
						"name"  => $taxonomy_name,
						"title" => $taxonomy_title,
						"cats"  => $categories
					);
				}
			}
			$post_types_categories[$name] = $taxonomies;
		}
		return($post_types_categories);
	}
	
	/**
	* Get post types array with taxomonies
	* @since 1.0.0
	*/
	public static function get_all_taxonomies() {
		$post_types = self::get_all_post_types();
		foreach($post_types as $post_type => $title){
			$taxomonies = self::get_taxomonies_post_type($post_type);
			$post_types[$post_type] = $taxomonies;
		}
		return($post_types);
	}
	
	/**
	* Get post type with taxomonies names
	* @since 1.0.0
	*/
	public static function get_taxomonies_post_type($post_types) {
		$taxonomies = get_object_taxonomies(
			array('post_type' => $post_types),
			'objects'
		);	
		$names = array();
		foreach($taxonomies as $key => $values) {
			//print_r($post_types.'   :   '.$values->labels->name. '   -   ');
			$names[$values->name] = $values->labels->name;
		}
		return($names);
	}
	
	/**
	* Get All Post Types (builtin and custom)
	* @since 1.0.0
	*/
	public static function get_all_post_types() {
		$builtin_post_types = array(
			'post' => 'post',
			'page' => 'page',
			'attachment' => 'Media Library'
		);
		$custom_post_types = get_post_types(
			array('_builtin' => false)
		);
		unset($custom_post_types['the_grid']);
		if ( class_exists( 'WooCommerce' ) ) {
			unset($custom_post_types['shop_order']);	
		}
		$post_types = array_merge($builtin_post_types, $custom_post_types);
		foreach($post_types as $key => $type){
			$post_type_object = get_post_type_object($type);
			if(empty($post_type_object)){
				$post_types[$key] = $type;
				continue;
			}
			$post_types[$key] = $post_type_object->labels->name;
		}
		return($post_types);
	}
	
	/**
	* Get post categories list with associated id & title
	* @since 1.0.0
	*/
	public static function get_associated_categories($taxonomy = 'category') {
		if(strpos($taxonomy,',') !== false){
			$taxonomies = explode(',', $taxonomy);
			$categories = array();
			foreach($taxonomies as $taxonomy) {
				$associated_categories = self::get_associated_categories($taxonomy);
				$categories = array_merge($categories,$associated_categories);
			}
			return($categories);
		}
		$args = array('taxonomy' => $taxonomy);
		$cats = get_categories($args);
		$subcat = array();
		$categories = array();
		foreach($cats as $cat){
			$numItems = $cat->count;
			$id       = $cat->cat_ID;
			$subcat   = array_merge(get_term_children( $cat->cat_ID, $taxonomy ),$subcat);
			if (in_array($id, $subcat)) {
				$title = '&#8212; '.$cat->name . ' ('.$numItems.')';
			} else {
				$title = $cat->name . ' ('.$numItems.')';
			}
			$categories[$id] = $title;
		}
		return($categories);
	}
	
	/**
	* Format cat name and ID array
	* @since 1.0.0
	*/
	public static function get_formated_categories() {
		$post_types_and_categories = self::get_post_types_and_categories();
		$cat = array();
		if(!empty($post_types_and_categories)){
			foreach($post_types_and_categories as $post_type => $ID) {
				$post_type_info   = get_post_type_object($post_type);
				$post_type_name   = $post_type_info->labels->name;
				$post_type_single = $post_type_info->name;
				$post_types[$post_type_single] = $post_type_name;
				foreach($ID as $id => $name) {
					$cat[$id] = $name;
				}
			}
		}
		return $cat;
	}
	
	/**
	* Retrieve all metadata
	* @since 1.0.0
	*/
	public static function get_all_meta_field() {
		$post_types = self::get_all_post_types();
		foreach( $post_types as $post_type => $value ) {
			if(post_type_exists($post_type)) { 
				$query_args = array(
					'post_type'   => $post_type,
					'numberposts' => 1,
					'post_status' => 'any'
				);
				$items = get_posts($query_args);
				if ($items) {
					foreach($items as $item) {
						$custom_field_keys = get_post_custom_keys($item->ID);
						if ($custom_field_keys) {
							foreach ($custom_field_keys as $key => $value) {
								$post_key[$post_type.':'.$value] = $value;
							}
						}
					}
				}
			}
		}
	}
	
	/**
	* Get all page title and id
	* @since 1.0.0
	*/
	public static function get_all_page_id() {
		$pages = get_pages();
		$pages_data = array();
		if (isset($pages) && !empty($pages)) {
			foreach ($pages as $page) {
				$pages_data[$page->ID] = $page->post_title;	
			} 
		}
		return $pages_data;
	}
	
	/**
	* Get post type in category
	* @since 1.0.0
	*/
	public static function get_post_ids_by_cat($post_type,$tax_query,$post_cats_child, $cat, $taxonomy='category') {
		return get_posts(array(
			'post_type'     => $post_type, 
			'numberposts'   => -1,
			'tax_query'     => $tax_query,
			'fields'        => 'ids',
		));
	}
	
	/**
	* List all available image sizes
	* @since 1.0.0
	*/
	public static function get_image_size() {
		$new_sizes = array();
		$added_sizes = get_intermediate_image_sizes();
		foreach($added_sizes as $key => $value) {
			$new_sizes[$value] = ucfirst(str_replace('_', ' ', $value));
		}
		$std_sizes = array(
			'full'      => __('Original Size', 'tg-text-domain'),
			'thumbnail' => __('Thumbnail', 'tg-text-domain'),
			'medium'    => __('Medium', 'tg-text-domain'),
			'large'     => __('Large', 'tg-text-domain')
		);
		$new_sizes = array_merge($std_sizes,$new_sizes);
		return $new_sizes;
	}
	
	/**
	* Sorting array data grid
	* @since 1.0.0
	*/
	public static function grid_sorting() {
		$sorting = array();
		$sorting['std-disabled'] = __( 'Standard', 'tg-text-domain'  );
		$sorting['none']        = __( 'None', 'tg-text-domain'  );
		$sorting['id']      = __( 'ID', 'tg-text-domain'  );
		$sorting['date']    = __( 'Date', 'tg-text-domain'  );
		$sorting['title']   = __( 'Title', 'tg-text-domain'  );
		$sorting['excerpt'] = __( 'Excerpt', 'tg-text-domain'  );
		$sorting['author']  = __( 'Author', 'tg-text-domain'  );
		$sorting['comment'] = __( 'Number of comment', 'tg-text-domain'  );
		if ( class_exists( 'WooCommerce' ) ) {
			$sorting['woo_disabled']      = 'Woocommerce';
			$sorting['woo_SKU']           = __( 'SKU', 'tg-text-domain'  );
			$sorting['woo_regular_price'] = __( 'Price', 'tg-text-domain'  );
			$sorting['woo_sale_price']    = __( 'Sale Price', 'tg-text-domain'  );
			$sorting['woo_total_sales']   = __( 'Number of sales', 'tg-text-domain'  );
			$sorting['woo_featured']      = __( 'Featured Products', 'tg-text-domain'  );
			$sorting['woo_stock']         = __( 'Stock Quantity', 'tg-text-domain'  );
		}
		return $sorting;
	}
	
	/**
	* Compress css function
	* @since 1.0.0
	*/
	public static function compress_css($styles) {
		$styles = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $styles);
    	$styles = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $styles);
		$styles = str_replace(' {', '{', $styles);
		$styles = str_replace('{ ', '{', $styles);
    	$styles = str_replace(' }', '}', $styles);
		$styles = str_replace( '} ', '}', $styles);
		$styles = str_replace( ';}', '}', $styles);
		$styles = str_replace( ', ', ',', $styles);
		$styles = str_replace('; ', ';', $styles);
		$styles = str_replace(': ', ':', $styles);
		return $styles;
	}
	
	/**
	* Lighter color function
	* @since 1.0.0
	*/
	public static function HEXLighter($col,$ratio) {
		$col = Array(hexdec(substr($col, 1, 2)), hexdec(substr($col, 3, 2)), hexdec(substr($col, 5, 2)));
		$lighter = Array(
			255-(255-$col[0])/$ratio,
			255-(255-$col[1])/$ratio,
			255-(255-$col[2])/$ratio
		);
		return "#".sprintf("%02X%02X%02X", $lighter[0], $lighter[1], $lighter[2]);
	}
	
	/**
	* Darker color function
	* @since 1.0.0
	*/
	public static function HEXDarker($col,$ratio) {
		$col = Array(hexdec(substr($col, 1, 2)), hexdec(substr($col, 3, 2)), hexdec(substr($col, 5, 2)));;
		$darker = Array(
			$col[0]/$ratio,
			$col[1]/$ratio,
			$col[2]/$ratio
		);
		return '#'.sprintf('%02X%02X%02X', $darker[0], $darker[1], $darker[2]);
	}
	
	/**
	* HEX to RGB function
	* @since 1.0.0
	*/
	public static function HEX2RGB($hex,$alpha=1) {
		$hex = str_replace("#", "", $hex);
		if(strlen($hex) == 3) {
			$r = hexdec(substr($hex,0,1).substr($hex,0,1));
			$g = hexdec(substr($hex,1,1).substr($hex,1,1));
			$b = hexdec(substr($hex,2,1).substr($hex,2,1));
		} else {
			$r = hexdec(substr($hex,0,2));
			$g = hexdec(substr($hex,2,2));
			$b = hexdec(substr($hex,4,2));
		}
		$rgb['red']   = $r;
		$rgb['green'] = $g;
		$rgb['blue']  = $b;
		
		if ($alpha < 1) {
			$rgb = 'rgba('.$r.','.$g.','.$b.','.$alpha.')';
		}
		
		return $rgb;
	}
	
	/**
	* RGB to HEX function
	* @since 1.0.0
	*/
	public static function RGB2HEX($rgb) {
	   $hex  = str_pad(dechex($rgb[0]), 2, "0", STR_PAD_LEFT);
	   $hex .= str_pad(dechex($rgb[1]), 2, "0", STR_PAD_LEFT);
	   $hex .= str_pad(dechex($rgb[2]), 2, "0", STR_PAD_LEFT);
	   return $hex;
	}
	
	/**
	* Brightness Color function
	* @since 1.0.0
	*/
	public static function brightness($hex) {

		$rgb = self::HEX2RGB($hex);
		
		$r = ((float)$rgb['red']) / 255.0;
		$g = ((float)$rgb['green']) / 255.0;
		$b = ((float)$rgb['blue']) / 255.0;

		$maxC = max($r, $g, $b);
		$minC = min($r, $g, $b);

		$l = ($maxC + $minC) / 2.0;
		$l = (int)round(255.0 * $l);
		
		if($l > 200) {
			$brightness = 'bright';
		} else {
			$brightness = 'dark';
		}
		
		return $brightness;

	}
	
	/**
	* Search in array strpos function
	* @since 1.0.0
	*/
	public static function strpos_array($haystack, $needles, $offset = 0) {
		if (is_array($needles)) {
			foreach ($needles as $needle) {
				$pos = self::strpos_array($haystack, $needle);
				if ($pos !== false) {
					return true;
				}
			}
			return false;
		} else {
			return strpos($haystack, $needles, $offset);
		}
	}
	
	/**
	* Get Default Grid Skins
	* @since 1.0.0
	*/
	public static function default_skin($style) {
		$default_skin = ($style == 'grid') ? 'brasilia' : 'kampala';
		$item_base = new The_Grid_Item_Skin();
		$get_skins = $item_base->get_skin_names();
		if (!array_key_exists($default_skin,$get_skins)){
			$default_skin = null;
			foreach($get_skins as $skin => $data) {
				if ($data['type'] == $style) {
				 	$default_skin = $data['slug'];
					break;
				}
			}
		}
		return $default_skin;
	}
	
	/**
	* Detect IE browsers
	* @since 1.0.0
	*/
	public static function is_ie() {
		
		if(isset($_SERVER) && !empty($_SERVER) && isset($_SERVER['HTTP_USER_AGENT'])) {
			if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false) || preg_match('~MSIE|Internet Explorer~i', $_SERVER['HTTP_USER_AGENT'])) {
				return 'is-ie';
			}
		}
	
	}
	
	/**
	* Disable W3 Total cache for the grid transient
	* @since 1.0.0
	*/
	public function disable_W3_Total_Cache($grid_transient) {
		
		add_filter( 'pre_set_transient_'.$grid_transient, array($this,'disable_linked_in_cached') );
		add_filter( 'pre_transient_'.$grid_transient, array($this,'disable_linked_in_cached') );
		add_action( 'delete_transient_'.$grid_transient, array($this,'disable_linked_in_cached') );	
		
	}
	
	/**
	* Disable W3 Total cache for the grid transient
	* @since 1.0.0
	*/
	public function disable_linked_in_cached($value=null){
		
		global $_wp_using_ext_object_cache, $w3_total_cache;
		$w3_total_cache = $_wp_using_ext_object_cache;
		$_wp_using_ext_object_cache = false;
		return $value;
		
	}
	
	/**
	* Re-enable W3 Total cache plugin
	* @since 1.0.0
	*/
	public function enable_W3_Total_Cache($grid_transient) {
		
		add_action( 'set_transient_'.$grid_transient, array($this,'disable_linked_in_cached') );
		add_filter( 'transient_'.$grid_transient, array($this,'enable_linked_in_cached') );
		add_action( 'deleted_transient_'.$grid_transient, array($this,'disable_linked_in_cached') );
		
	}
	
	/**
	* Re-enable W3 Total cache plugin
	* @since 1.0.0
	*/
	public function enable_linked_in_cached($value=null){
		
		global $_wp_using_ext_object_cache, $w3_total_cache;
		$_wp_using_ext_object_cache = $w3_total_cache;
		return $value;
		
	}

}
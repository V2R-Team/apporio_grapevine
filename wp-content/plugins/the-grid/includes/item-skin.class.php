<?php
/**
 * @package   The_Grid
 * @author    Themeone <themeone.master@gmail.com>
 * @copyright 2015 Themeone
 */

// Exit if accessed directly
if (!defined( 'ABSPATH')) { 
	exit;
}

class The_Grid_Register_Item_Skin {

    private $skins = array();

    function __construct() {
        $this->skins = apply_filters( 'tg_register_item_skin', $this->skins );
    }

	// get skin array
	function get_registered_skins() {
		return $this->skins;
    }

}

add_filter('tg_register_item_skin', function($skins){
	
	$skins = array(
		// grid skins
		'alofi',
		'apia',
		'brasilia',
		'camberra',
		'caracas',
		'honiara',
		'lome',
		'malabo',
		'male',
		'oslo',
		'pracia',
		'sofia',
		'suva',
		// masonry skins
		'doha',
		'kampala',
		'lima',
		'lusaka',
		'maren',
		'panama',
		'praia',
		'quito',
		'riga',
		'sanaa',
	);
	
	return $skins;
});
 
class The_Grid_Item_Skin {

    private $skins = array();

    function __construct() {
        $this->skins = apply_filters( 'tg_add_item_skin', $this->skins );
    }

	// get skin array
	function get_skin_names() {
		return $this->skins;
    }

}



add_filter('tg_add_item_skin', function($skins){
	
	$dirname1 = TG_PLUGIN_PATH.'/includes/item-skins';
	$dirname2 = get_stylesheet_directory().'/the-grid';
	$findphp = '*.php';
	$findcss = '*.css';
	$types   = array('grid','masonry');
	
	$register_skins_base = new The_Grid_Register_Item_Skin();
	$register_skins = $register_skins_base->get_registered_skins();
			
	$sub_dirs1 = glob($dirname1.'/*', GLOB_ONLYDIR|GLOB_NOSORT);
	$sub_dirs2 = glob($dirname2.'/*', GLOB_ONLYDIR|GLOB_NOSORT);
	$sub_dirs  = array_merge($sub_dirs1,$sub_dirs2);
			
	if(count($sub_dirs)) {
		foreach($sub_dirs as $sub_dir) {
			$sub_dir_name = basename($sub_dir);	
			$path = (str_replace('/the-grid/grid','',$sub_dir));
			$path = (str_replace('/the-grid/masonry','',$path));
			if ($path == get_stylesheet_directory()) {
				$filter = wp_get_theme();
			} else {
				$filter = __( 'The Grid', 'tg-text-domain' );
			}
			if (in_array($sub_dir_name,$types)) {
				$sub_sub_dirs = glob($sub_dir.'/*', GLOB_ONLYDIR|GLOB_NOSORT);
				if(count($sub_sub_dirs)) {
					foreach($sub_sub_dirs as $sub_sub_dir) {
						$php  = glob($sub_sub_dir.'/'.$findphp);
						$css  = glob($sub_sub_dir.'/'.$findcss);
						$name = basename($php[0], '.php');
						$sub_sub_dir_name =  basename($sub_sub_dir);
						if (in_array($name,$register_skins)) {
							$skins[$sub_sub_dir_name] = array(
								'type'   => $sub_dir_name,
								'filter' => $filter,
								'slug'   => $sub_sub_dir_name,
								'name'   => $name,
								'php'    => $php[0],
								'css'    => $css[0]
							);
						}
					}
				}
			}
		}
	}

    return $skins;
	
});

// add a skin in a plugin/theme
/*add_filter('tg_add_item_skin', function($skins){
	
	$URI = get_template_directory();
	
	// register a skin and add it to the main skins array
	$skins['new_skin1'] = array(
		'type'   => 'masonry',
		'filter' => 'your-filter',
		'slug'   => 'your-slug',
		'name'   => 'new skin 1',
		'php'    => $URI . '/your_path/new_skin1.php',
		'css'    => $URI . '/your_path/new_skin1.css'
	);
	
	$skins['new_skin2'] = array(
		'type'   => 'grid',
		'filter' => 'your-filter',
		'slug'   => 'your-slug',
		'name'   => 'new skin 2',
		'php'    => $URI . '/your_path/new_skin2.php',
		'css'    => $URI . '/your_path/new_skin2.css'
	);	
	
	// return the skins array + the new one you added (in this example 2 new skins was added)
	return $skins;
	
});*/
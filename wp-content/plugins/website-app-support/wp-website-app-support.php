<?php
/*
Plugin Name: Website App Support

Description: Ability to show a banner and redirect users to an app or the app store, on multiple devices
Version:     1.0
Author:      B60 Thomas Prince

License:     GPL2

Website App Support is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
Website App Support is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with Website App Support. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
*/

// Include and instantiate the class.

if( is_admin() ){

	/** Step 2 (from text above). */
	add_action( 'admin_menu', 'plugin_menu' );

	add_action( 'wp_enqueue_scripts', 'prefix_add_my_stylesheet' );

	/** Step 1. */
	function plugin_menu() {
		add_options_page( 'WAS Plugin Options', 'Website App Support', 'manage_options', 'website-app-support', 'plugin_options' );
	}

	/** Step 3. */
	function plugin_options() {
		if ( !current_user_can( 'manage_options' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}
		?>
			<div class="wrap">
				<div class="WAS_row">
					<div class="WAS_inputSet">
						<label><strong>App Name</strong></label>
						<input id="WAS_appName" type="text"></input>
					</div>
					<div class="WAS_inputSet">
						<label><strong>Tagline</strong> - max 20 characters</label>
						<input id="WAS_appName" max="20" type="text"></input>
					</div>
				</div>
				<div class="WAS_row">
					<div class="WAS_inputSet">
						<label><strong>iOS App Id</strong></label>
						<input id="WAS_iosAppId" type="text"></input>
					</div>
					<div class="WAS_inputSet">
						<label><strong>iOS App Argument</strong></label>
						<input id="WAS_iosAppArgument" type="text"></input>
					</div>
				</div>
				<div class="WAS_row">
					<div class="WAS_inputSet">
						<label><strong>Android Package Name</strong></label>
						<input id="WAS_androidPackageName" type="text"></input>
					</div>
					<div class="WAS_inputSet">
						<label><strong>Android App Argument</strong></label>
						<input id="WAS_androidAppArgument" type="text"></input>
					</div>
				</div>
				<div class="WAS_row">
					<div class="WAS_inputSet">
						<label><strong>Desktop Google Play store</strong></label>
						<input id="WAS_desktopPlayStore" type="text"></input>
					</div>
					<div class="WAS_inputSet">
						<label><strong>Desktop Itunes Store</strong></label>
						<input id="WAS_desktopAppStore" type="text"></input>
					</div>
				</div>
			</div>
		<?php
	}

} else {

	include 'Mobile_Detect.php';
	include 'config.php';

	//In the themes header.php 'wp_head' must be called for the plugin to work
	add_action('wp_head','init');

	add_action( 'wp_enqueue_scripts', 'prefix_add_my_stylesheet' );

	/**
	 * Enqueue plugin style-file
	 */
	function prefix_add_my_stylesheet() {
	    // Respects SSL, Style.css is relative to the current file
	    wp_register_style( 'wp-website-app-support-style', plugins_url('style.css', __FILE__) );
	    wp_enqueue_style( 'wp-website-app-support-style' );
	    
	}
	function init() {
		loadConfig();
		$appSupport = new pluginFunctions;
	}

	class detectPlatform{

		private $detect;
		public $platform;

		public function __construct(){

			$this->detect = new Mobile_Detect;

			//Any mobile device
			if ( $this->detect->isMobile() === true && $this->detect->isTablet() === false) {

				if( $this->detect->isiOS() === true ){

					$this->platform = 'iPhone';

				} elseif ( $this->detect->isAndroidOS() === true ){

					$this->platform = 'anPhone';

				} else {

					$this->platform = 'desktop';
				}

			} elseif( $this->detect->isTablet() === true ){

				if( $this->detect->isiOS() === true ){

					$this->platform = 'iPad';

				} elseif ( $this->detect->isAndroidOS() === true ){

					$this->platform = 'anTablet';

				} else {

					$this->platform = 'desktop';
				}

			} else {

				$this->platform = 'desktop';
			}

		}

	}

	class pluginFunctions{

		public $device;
		public $currentPage;

	 	public $redirect = 0;

	 	public $storeUrl;
	 	public $appUrl;

	 	public $redirectSettings;
	 	public $redirectPages; 

	 	public function __construct(){

	 		$bannerSettings = unserialize(WAS_bannerSettings);

	 		$this->redirectSettings = unserialize(WAS_redirectSettings);
	 		$this->redirectPages = split(',', WAS_redirectPages);

	 		$this->device = new detectPlatform;
	 		$this->currentPage = basename(get_permalink());

	 		$this->setDeviceVariables();
	 		$this->checkBanner($bannerSettings);
	 		$this->checkRedirect();
	 		
	 	}

	 	public function setDeviceVariables (){
		
	 		foreach($this->redirectSettings as $setting => $value) {
				
				if($this->device->platform == $setting){

		 			if( substr($setting,0,1) == 'i' ){

						$this->storeUrl = 'https://itunes.apple.com/us/app/id'.WAS_iosAppId;
					
						$this->appUrl = WAS_iosAppArgument;

					} elseif ( substr($setting,0,1) == 'a' ){

						$this->storeUrl = WAS_androidPackageName;
						$this->appUrl = WAS_androidAppArgument;

					} else {

						$this->storeUrl = 'https://itunes.apple.com/us/app/id'.WAS_iosAppId;
						//https://itunes.apple.com/us/app/id585027354
						$this->appUrl = WAS_desktopPlayStore;
					}
				}
			}

	 	}

		private function checkBanner($bannerSettings) {

			foreach($bannerSettings as $setting => $value) {

				if($value == 1 && $this->device->platform == $setting){

					$this->showBanner();
				}
			}

		}

		private function checkRedirect() {

			foreach($this->redirectSettings as $setting => $value) {

				if($value == 1 && $this->device->platform == $setting){

					foreach ($this->redirectPages as $page) {

						if($this->currentPage == $page){
							$this->redirect();
						} elseif($this->currentPage == basename(home_url()) && $page == 'home'){
							$this->redirect();
						}
					}
				}
				
			}

		}

		private function showBanner(){
			$appIcon = plugins_url('appIcon.png', __FILE__);

			$banner_html = 
			'<div id="WAS_banner">'.'<div id="WAS_bannerContainer">'.'<div id="WAS_appIcon">'.'</div>'.'<div id="WAS_title">'.'<p id="WAS_appName">'. WAS_appName .'</p>'.'<p id="WAS_tagline">'. WAS_bannerText .'</p>'.'</div>'.'<div id="WAS_btn">'.'<p>Download</p>'.'</div>'.'</div>'.'</div>'
			;

		?>

			<script language="JavaScript" type="text/javascript">
				$(document).ready(function(){
					$('body').prepend('<?php echo $banner_html ?>');
					$('#WAS_btn').on('click', function(){
						document.getElementById("l").src = '<?php echo $this->appUrl ?>';
					    	setTimeout(function () { window.location = '<?php echo $this->storeUrl ?>'; }, 50);
					});
				});
			    
			</script>

		<?php
		}

		public function redirect(){



			if( !$this->storeUrl == NULL && !$this->appUrl == NULL ){
				//inject javascript to redirect user
				//suitable code only for iOS 7 - 8
		?>
				<script language="JavaScript" type="text/javascript">
					window.onload = function() {
						// loading the app in the iframe suppresses the error popup
	                			document.getElementById("l").src = '<?php echo $this->appUrl ?>';
					    	setTimeout(function () { window.location = '<?php echo $this->storeUrl ?>'; }, 50);
					};
				</script>

		<?php
			}

		}


	}
	/*
	// Alternative method is() for checking specific properties.
	// WARNING: this method is in BETA, some keyword properties will change in the future.
	$detect->is('Chrome');
	$detect->is('iOS');
	$detect->is('UC Browser');
	// [...]
	 
	// Batch mode using setUserAgent():
	$userAgents = array(
	'Mozilla/5.0 (Linux; Android 4.0.4; Desire HD Build/IMM76D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19',
	'BlackBerry7100i/4.1.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 VendorID/103',
	// [...]
	);
	foreach($userAgents as $userAgent){
	 
	  $detect->setUserAgent($userAgent);
	  $isMobile = $detect->isMobile();
	  $isTablet = $detect->isTablet();
	  // Use the force however you want.
	 
	}
	 
	// Get the version() of components.
	// WARNING: this method is in BETA, some keyword properties will change in the future.
	$detect->version('iPad'); // 4.3 (float)
	$detect->version('iPhone'); // 3.1 (float)
	$detect->version('Android'); // 2.1 (float)
	$detect->version('Opera Mini'); // 5.0 (float)
	*/
}

?>
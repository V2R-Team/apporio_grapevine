<?php 
/* generic values for the app */
function loadConfig(){

	//The display name of the app
	define('WAS_appName', 'Grapevine'); 

	//The tagline of the app to display on the banner
	define('WAS_bannerText', 'The Wine Lovers App!'); //No more than 20 characters please! (iphoine 5s and similar screen widths)

	//The pages that you want the redirect to automatiaclly fire
	define('WAS_redirectPages','home,feeds'); //seperate pages by commas. leave blank if all

	/* ios specific settings */
	// The itunes number to identify the app
	// The script will automatically apply the prefix 'id' to the url.
	define('WAS_iosAppId', '585027354');

	// The App root and deeplink (optional) that you want to take the user to
	define('WAS_iosAppArgument', 'grapevine://'); // e.g. my-app://

	/* android specific settings */
	define('WAS_androidPackageName', 'com.google.android.apps.maps');

	define('WAS_androidAppArgument', 'grapevine://');

	/* desktop settings */
	define('WAS_desktopPlayStore', 'Grapevine');

	define('WAS_desktopAppStore', 'Grapevine');

	/* Banner Display Settings */
	$BANNER_SETTINGS = array(
		/* where true is 1 */
		'iPhone'    => 1, //display on iphone
		'anPhone'   => 1, //display on android phone
		'iPad'      => 1, //display on ipad
		'anTablet'  => 1, //display on android tablet
		'desktop'   => 0, //display on desktop
	);
	$banner = serialize($BANNER_SETTINGS);

	define('WAS_bannerSettings', $banner);

	/* Redirect settings */
	$REDIRECT_SETTINGS = array(
		/* where true is 1 */
		'iPhone'   => 1, //redirect on iphone
		'anPhone'  => 1, //redirect on android phone
		'iPad'     => 0, //redirect on ipad
		'anTablet' => 0, //redirect on android tablet
		'desktop'  => 0, //redirect on desktop
	);

	$redirect = serialize($REDIRECT_SETTINGS);

	define('WAS_redirectSettings', $redirect);
}


?>
Place this folder in the plugins directory of the wordpress site.

Android apps must have an intent filter set in the AndroidManifest.xml

<activity android:name=".MyUriActivity">
    <intent-filter>
        <action android:name="android.intent.action.VIEW" />
        <category android:name="android.intent.category.DEFAULT" />
        <category android:name="android.intent.category.BROWSABLE" />
        <data android:scheme="myapp" android:host="path" />
    </intent-filter>
</activity>

IOS apps must have universal links setup see: http://stackoverflow.com/questions/31891777/ios-9-safari-iframe-src-with-custom-url-scheme-not-working/32708258#32708258

ONLY INCLUDE THIS IN A PROVISINING PROFILE WITH THE RIGHT ENTITLEMENTS;

1. Configure your app to register approved domains

i. Registered your app at developer.apple.com if you haven't

ii. Enable ‘Associated Domains’ on your app identifier on developer.apple.com

iii. Enable ‘Associated Domain’ on in your Xcode project 
(target - capabilities -associated domains)

iv. Add the proper domain entitlement, 'applinks:yourdomain.com', in your app

v. Create structured ‘apple-app-site-association’ JSON file and save into server root

{
   "applinks": {
       "apps": [ ],
       "details": {
           "TEAM-IDENTIFIER.YOUR.BUNDLE.IDENTIFIER": {
               "paths": [
                   "*"
               ]
           }
       }
   }
}

Edit the configuration options in the settings tab of Wordpress
<?php
if(!session_id()) {
    session_start();
}
require_once __DIR__.'/wp-content/themes/grapevine/Facebook/autoload.php';

$fb = new Facebook\Facebook([
  'app_id' => '670197269749466',
  'app_secret' => '6c79a1c7709207547c245e9d26e2d8e',
  'default_graph_version' => 'v2.5',
]);



$helper = $fb->getRedirectLoginHelper();

try {
 echo $accessToken = $helper->getAccessToken();
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

if (! isset($accessToken)) {
  if ($helper->getError()) {
    header('HTTP/1.0 401 Unauthorized');
    echo "Error: " . $helper->getError() . "\n";
    echo "Error Code: " . $helper->getErrorCode() . "\n";
    echo "Error Reason: " . $helper->getErrorReason() . "\n";
    echo "Error Description: " . $helper->getErrorDescription() . "\n";
  } else {
    header('HTTP/1.0 400 Bad Request');
    echo 'Bad request';
  }
  exit;
}
